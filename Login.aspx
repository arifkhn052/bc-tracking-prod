﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="BCTrackingWeb.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <title>BC Track</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="Scripts/jquery.md5.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/Login.js"></script>

</head>


<body>

    <div class="container">
        <div id="loginbox" style="margin-top: 50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="alert alert-danger" ng-hide="myAlert">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Attention!</strong>&nbsp;&nbsp;Invalid User. Please login with correct credentials
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">Login</div>
                </div>
                <div style="padding-top: 30px" class="panel-body">
                    <div style="display: none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                    <form name="loginForm" ng-submit="login(loginForm.$valid)" novalidate class="form-horizontal">
                                    <div class="form-group left-icon" ng-class="{ 'has-error' : loginForm.userId.$invalid && !loginForm.userId.$pristine }">
                                		<label for="exampleInputEmail1" class="col-sm-3 control-label">UserId</label>
                                		<div class="col-sm-9">
                                              <span class="glyphicon glyphicon-envelope form-left-icon"></span>
                                             <input type="text" class="form-control" placeholder="Enter UserId" ng-model="user.userId" name="userId" required />
                                              <p ng-show="loginForm.userId.$invalid && !loginForm.userId.$pristine" class="help-block">Enter a valid userId
                                          
                                			
                                		</div>
                                	</div>
                                    <div class="form-group left-icon" ng-class="{ 'has-error' : loginForm.password.$invalid && !loginForm.password.$pristine }">
                                		<label for="exampleInputPassword1" class="col-sm-3 control-label">Password</label>
                                		<div class="col-sm-9">
                                            <span class="glyphicon glyphicon-tags form-left-icon"></span>
                                			 <input type="password" class="form-control" placeholder="Password" ng-model="user.password" name="password" ng-required="true" />
                            <p ng-show="loginForm.password.$error.minlength" class="help-block">Password is too short.</p>
                                		</div>
                                	</div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <div class="checkbox op-check">
                                                <label>
                                                    <input type="checkbox" name="remember" class="flat-blue-style" checked> <span class="ml-10">Remember me</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-20">
                                        <div class="">
                                        <%--  <button type="submit" class="btn btn-success btn-labeled pull-right" value="Sign In" ng-disabled="loginForm.$invalid || loaderClass"><i class="fa fa-circle-o-notch fa-spin" ng-show="loaderClass"></i>Sign In</button>--%>
                                            <button type="submit" class="btn btn-success btn-labeled pull-right" ng-disabled="loginForm.$invalid || loaderClass">Sign in<span class="btn-label btn-label-right"><i class="fa fa-check"></i></span></button>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </form>
                    <%--<form class="sign-box" name="loginForm" ng-submit="login(loginForm.$valid)" novalidate>


                        <div class="form-group" ng-class="{ 'has-error' : loginForm.userId.$invalid && !loginForm.userId.$pristine }">
                            <input type="text" class="form-control" placeholder="E-Mail" ng-model="user.userId" name="userId" required />
                            <p ng-show="loginForm.userId.$invalid && !loginForm.userId.$pristine" class="help-block">Enter a valid userId.</p>
                        </div>
                        <div class="form-group" ng-class="{ 'has-error' : loginForm.password.$invalid && !loginForm.password.$pristine }">
                            <input type="password" class="form-control" placeholder="Password" ng-model="user.password" name="password" ng-required="true" />
                            <p ng-show="loginForm.password.$error.minlength" class="help-block">Password is too short.</p>
                        </div>

                        <p ng-show="showError" class="help-block has-danger">{{error}}</p>
                        
                        <button type="submit" class="btn btn-success" value="Sign In" ng-disabled="loginForm.$invalid || loaderClass"><i class="fa fa-circle-o-notch fa-spin" ng-show="loaderClass"></i>Sign In</button>
                     <a href="/Pages/Login/ForgetPassword.aspx" class="text-dark"><i class="fa fa-lock m-r-5"></i>Forgot your password?</a>

                        <!--<button type="button" class="close">
                        <span aria-hidden="true">&times;</span>
                    </button>-->
                    </form>--%>


                </div>
            </div>


        </div>
    </div>


</body>


</html>
