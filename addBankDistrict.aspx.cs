﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using BCTrackingBL;
using BCEntities;

namespace BCTrackingWeb
{
    public partial class addBankDistrict : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string getProduct()
        {
            // Paging parameters:
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);

            // Sorting parameters
            var iSortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var iSortDir = HttpContext.Current.Request.Params["sSortDir_0"];
            // Search parameters
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToLower();
            int totalRecords = 0;
            if (System.Web.HttpContext.Current.Session["UserInfo"] != null) ;
            var BankId = HttpContext.Current.Request.Params["adminBankId"].ToLower();
            var leadList = Common.getBankDistrict(iDisplayStart, iDisplayLength, iSortCol, iSortDir, sSearch, out totalRecords, BankId);

            Func<UserEntity, object> order = p =>
            {
                if (iSortCol == 0)
                {
                    return p.productId;
                }

                else if (iSortCol == 1)
                {
                    return p.productName;
                }
                else if (iSortCol == 2)
                {
                    return p.UserRoles;
                }



                return p.productId;
            };

            //    // Define the order direction based on the iSortDir parameter
            if ("desc" == iSortDir)
            {
                leadList = leadList.OrderByDescending(order).ToList<UserEntity>();
            }
            else
            {
                leadList = leadList.OrderBy(order).ToList<UserEntity>();
            }
            var finalResult = leadList
                    .Select(p => new[] { p.productId.ToString(), p.productName, p.UserName });

            int count = finalResult.Count();

            // prepare an anonymous object for JSON serialization
            var result = new
            {
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = finalResult
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            return json;
        }
    }
}