﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="AddBCIndex.aspx.cs" Inherits="BCTrackingWeb.AddBCIndex" %>

     
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/metro.min.css">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.min.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">

    <style>
        a:visited {
            color: white;
        }

        .nav {
            font-size: small;
        }
    </style>

    
    <h1><a href="Home.aspx" class="nav-button transform"><span></span></a></h1>

    <div class="container page-content">


        <div>
            <ul class="breadcrumbs mini">
              <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                <li><a href="AddBCHome.aspx"><span>Add Business Correspondents</span></a></li>
            </ul>
        </div>

        <br>

        <div class="tile-area no-padding">
            <div class="tile-container">
                <a href="bulkupload.aspx">
                    <div class="tile bg-orange fg-white tile-wide">
                        <div class="tile-content iconic">
                            <span class="icon mif-cloud-upload"></span>
                            <span class="tile-label align-center">Bulk Upload</span>
                        </div>
                    </div>
                </a>
                <a href="SingleEntry.aspx">
                    <div class="tile bg-blue fg-white tile-wide">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-list-alt"></span>
                            <span class="tile-label align-center">Single Entry</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <br>
    </div>
    <script src="js/jquery-1.12.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/metro.min.js"></script>
    <script src="js/main.js"></script>


    
