﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="reportList.aspx.cs" Inherits="BCTrackingWeb.reportList" %>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/metro.min.css">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.min.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">


       <br /><br /><br /><br /><br />
    <div class="container page-content">
        
        <div class="loader"></div>
        <div>
            <ul class="breadcrumbs mini">
                <li><a href="Home.aspx"><span class="icon mif-home"></span></a></li>
                <li><a href="Reports.aspx"><span>Reports</span></a></li>
                <li><a href="reportList.aspx"><span>List Reports</span></a></li>
            </ul>
        </div>

        <br>
        
        <div class="tile-area no-padding">

            <div class="tile-container">
                <a href="listason.aspx">
                    <div class="tile bg-orange fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-filter"></span>
                            <span class="tile-label align-center">List as on date</span>
                        </div>
                    </div>
                </a>

                <a href="CorporateBC.aspx">
                    <div class="tile bg-green fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-filter"></span>
                            <span class="tile-label align-center">Corporate BC's</span>
                        </div>
                    </div>
                </a>
                <a href="BranchesLastUpdated.aspx">
                    <div class="tile bg-blue fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-filter"></span>
                            <span class="tile-label align-center">Branches Last Updated</span>
                        </div>
                    </div>
                </a>
                <a href="AllocationBasedList.aspx">
                    <div class="tile bg-gray fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-filter"></span>
                            <span class="tile-label align-center">Allocation Based List</span>
                        </div>
                    </div>
                </a>
                <a href="areaWiseAllocation.aspx">
                    <div class="tile bg-red fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-eye-open"></span>
                            <span class="tile-label align-center">Area wise BC allocation</span>
                        </div>
                    </div>
                </a>
                <a href="BcPerProduct.aspx">
                    <div class="tile bg-brown fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-filter"></span>
                            <span class="tile-label align-center">BC per products</span>
                        </div>
                    </div>
                </a>
                <a href="BlacklistedBCs.aspx">
                    <div class="tile bg-red fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon mif-not"></span>
                            <span class="tile-label align-center">BlackListed BC</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <br>

    </div>
    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>    
    <script src="js/getbc.js" type="text/javascript"></script>

