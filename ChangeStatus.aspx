﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="ChangeStatus.aspx.cs" Inherits="BCTrackingWeb.ChangeStatus" %>

|
    <div ng-init="getBcDetails()">
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-6">
                    <h2 class="title">Terminate BC</h2>

                </div>


                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                        <li><a ui-sref="all">Business Correspondent List</a></li>
                        <li><a>Terminate BC</a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">


                    <!-- /.col-md-6 -->

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    {{lblName}}
                                </div>
                            </div>
                            <div class="panel-body p-20">

                                <div class="panel-body">

                                    <div class="row">
                                        <%--<div class="col-sm-2">
                                            <label for="fileImage" class=" control-label">New Status</label>
                                        </div>--%>
                                        <%--<div class="col-sm-4 form-group">
                                         <select class="form-control formcontrolheight" id="selNewStatus" required>
                                                <option value="Active">Active</option>
                                                <option value="Inactive">Inactive</option>
                                                <option value="Left">Left</option>
                                                <option value="Left">Provisional</option>
                                            </select>
                                        </div>--%>
                                        <div class="col-sm-2">
                                            <label for="fileImage" class="control-label">
                                                Date</label>
                                        </div>
                                        <div class="col-sm-10 form-group">
                                         <input type="date" class="form-control formcontrolheight" id="txtDate" required placeholder="Date">
                                        </div>
                                    </div>


                               

                                          <div class="row">

                                     <div class="col-sm-2">
                                        <label message class="col-sm-2 control-label">Reason</label>
                                         </div>
                                        <div class="col-sm-10 form-group">
                                            <input type="text" class="form-control formcontrolheight" id="txtReason" placeholder="Reason">
                                        </div>
                                    </div>
                                               </div>
                                    <br />
                                    <div class="center-block">
                                        <button type="button" id="btnSubmitChangeStatus" class="btn btn-success center-block">Submit</button>
                                    </div>
                                </div>


                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->




                    <!-- /.col-md-8 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>
    <%--<div class="container page-content">

        <br />
        <br />
        <br />
        <br />
        <br />
        <br />

        <div>
            <ul class="breadcrumbs mini">
                <li><a href="Home.aspx"><span class="icon mif-home"></span></a></li>
                <li><a href="BCListHome.aspx"><span>Business Correspondent List</span></a></li>
                <li><a href="ChangeStatus.html"><span>Change Status</span></a></li>

            </ul>
        </div>
       

        <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
            <h4>Change Status</h4>
        </div>
        <hr>
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="formmargin">
                    <label message class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <label id="lblName" style="font-weight:bold;"></label>
                    </div>
                </div>


                <div class="formmargin">
                    <label message class="col-sm-2 control-label">New Status</label>
                    <div class="col-sm-10">
                        <select class="form-control formcontrolheight" id="selNewStatus" required>
                            <option value="Active">Active</option>
                            <option value="Inactive">Inactive</option>
                            <option value="Left">Left</option>
                            <option value="Left">Provisional</option>
                        </select>
                    </div>
                </div>



                <div class="formmargin">
                    <label message class="col-sm-2 control-label">Date</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control formcontrolheight" id="txtDate" required placeholder="Date">

                    </div>
                </div>

                <div class="formmargin">
                    <label message class="col-sm-2 control-label">Reason</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control formcontrolheight" id="txtReason" placeholder="Reason">
                    </div>
                </div>
                <br />
                <div class="center-block">
                    <button type="submit" id="btnSubmitChangeStatus" class="btn btn-default center-block">Submit</button>
                </div>
            </div>

        </div>

    </div>--%>



    <%--  <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/bc.js"></script>--%>

