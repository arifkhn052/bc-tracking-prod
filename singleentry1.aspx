﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mainMaster.Master" AutoEventWireup="true" CodeBehind="singleentry1.aspx.cs" Inherits="BCTrackingWeb.singleentry1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/metro.min.css">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.min.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
	<script src="js/jquery-1.12.2.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <br /><br /><br /><br /><br />
    <div class="container page-content">
        
        <div class="loader"></div>
        <div>
            <ul class="breadcrumbs mini">
                <li><a href="index.html"><span class="icon mif-home"></span></a></li>
                <li><a href="addbc.html"><span>Add Business Correspondents</span></a></li>
                <li><a href="singleentry.html"><span>Single Entry</span></a></li>
            </ul>
        </div>
        <br>
        
        <div class="example">
		    <form class="form-horizontal">
		        <br>
		        <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
		            <h4>Personal Details</h4> </div>
		        <hr>
		        <?php include("../includes/emptyForm/personal.php") ?>
		        <br>
		        <!-- <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
		            <h4>Certification Details</h4> </div>
		        <hr> -->
		        <?php include("../includes/emptyForm/certification.php") ?>
		        
				

		        <br>
		        <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
		            <h4>Corporate Association</h4> </div>
		        <hr>
		        <?php include("../includes/emptyForm/association.php") ?>
		        
		        <br>

		        <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
		            <h4>Business Correspondent Allocation</h4> </div>
		        <hr>
		        <div class="form-group">
		            <label for="inputEmail6" class="col-sm-2 control-label">Allocated</label>
		            <div class="col-sm-10">
		                <label class="radio-inline">
						  <input type="radio" name="allocated" id="inlineRadio1" value="yes"> Yes
						</label>
						<label class="radio-inline">
						  <input type="radio" name="allocated" id="inlineRadio2" value="no"> No
						</label>
		            </div>
		        </div>
		        <div id="bcallocate" class="hidden">
			        <?php include("../includes/emptyForm/allocation.php") ?>
			        <br>
		        	<?php include("../includes/emptyForm/device.php") ?>
		        	<br>
		        	<?php include("../includes/emptyForm/services.php") ?>
		        </div>
		        
		        <div id="bcsponsor" class="hidden">
		        	<br>
		        	<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
			            <h4>Business Correspondent Sponsored By</h4> </div>
			        <hr>
		        	<div class="form-group">
			            <label for="bankName" class="col-sm-2 control-label">Sponsored By</label>
			            <div class="col-sm-10">
		                	<input type="text" class="form-control" id="inputPassword3" placeholder="Sponsored By">
			            </div>
			        </div>
			        <br>
		        </div>
		       <hr>
		        <div class="form-group center-block">
		            <div class="center-block">
		                <button type="submit" class="btn btn-default center-block">Submit</button>
		            </div>
		        </div>
		    </form>
		</div>
        <br>
    </div>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/getbc.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {


            $('input[type=radio][name=allocated]').change(function () {
                if (this.value == 'yes') {
                    $("#bcallocate").removeClass('hidden');
                    $("#productOffered").removeClass('hidden');

                    $("#bcsponsor").addClass('hidden');
                }
                else if (this.value == 'no') {
                    $("#bcsponsor").removeClass('hidden');
                    $("#bcallocate").addClass('hidden');
                    $("#productOffered").addClass('hidden');
                }
            });
        });
    </script>
</asp:Content>
