﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="stateslist.aspx.cs" Inherits="BCTrackingWeb.stateslist" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <title>BC Tracking System</title>
<link href="css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
   <div >
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-6">
                    <h2 class="title">List of State</h2>

                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 right-side">
                    <a href="AddStateInfo.aspx" class="btn bg-black" role="button"><i class="fa fa-plus"></i> Add State</a>
                </div>
                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="home.aspx"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="StateList.aspx"> State List</a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">


                    <!-- /.col-md-6 -->

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">

                                </div>
                            </div>
                            <div class="panel-body p-20">

                                <table class="display" id="tblLeadList">
                            <thead>
                                <tr>
                                 <th class="text-left">StateId</th>
                                     <th class="text-left">State Name</th>                                 
                                    <th class="text-left">Edit</th>
                                    <th class="text-left">View</th>
                                </tr>
                            </thead>
                        </table>

                                
                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-8 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>
    </form>
       <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script type="text/javascript" src="js/parsley.min.js" defer></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js"></script>
<%--    <script src="js/states.js"></script>--%>
      <script>

          $(document).ready(function () {



              var leadTable = $('#tblLeadList').dataTable({
                  "oLanguage": {
                      "sZeroRecords": "No records to display",
                      "sSearch": "Search "
                  },
                  "sDom": 'T<"clear">lfrtip',
                  "tableTools": {

                      "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
                  },
                  "iDisplayLength": 15,
                  "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                  "bSortClasses": false,
                  "bStateSave": false,
                  "bPaginate": true,
                  "bAutoWidth": false,
                  "bProcessing": true,
                  "bServerSide": true,
                  "bDestroy": true,
                  "sAjaxSource": "StateItemList.aspx/getState",
                  "columnDefs": [

                      {
                          "orderable": false,
                          "targets": -2,
                          "render": function (data, type, full, meta) {
                              return '<a  class="btn btn-success btn-sm btn-labeled btn-rounded" href="editState.aspx?stateId=' + full[0] + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                          }
                      }, { orderable: true, "targets": -1 }, {
                          "orderable": true,
                          "targets": -1,
                          "render": function (data, type, full, meta) {
                              return '<a href="#" class="btn btn-danger btn-sm btn-labeled btn-rounded"  onclick="return deleteUser(' + full[0] + ');"><i class="fa fa-trash-o" aria-hidden="true"></a>';


                          }
                      }
                  ],
                  "bDeferRender": true,
                  "fnServerData": function (sSource, aoData, fnCallback) {
                      $.ajax({
                          "dataType": 'json',
                          "contentType": "application/json; charset=utf-8",
                          "type": "GET",
                          "url": sSource,
                          "data": aoData,
                          "success":
                                      function (msg) {
                                          var json = jQuery.parseJSON(msg.d);
                                          fnCallback(json);
                                          $("#tblLeadList").show();
                                      }
                      });
                  }
              });
              //  leadTable.fnSetFilteringDelay(300);
          });

          function deleteUser(stateid) {
              
              if (confirm("Do you want to delete?")) {

                  $.ajax({
                      type: 'POST',
                      dataType: 'json',
                      contentType: 'application/json',
                      url: "DeleteBcHandler.ashx?mode=state&stateid=" + stateid,
                      async: false,
                      success: function (user) {
                          window.location.reload();

                      }
                  });
              }
              return false;
          }
    </script>
</body>
</html>
