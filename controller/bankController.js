﻿bctrackApp.controller("bankController", function ($scope, $cookies) {
     
    var userId = $cookies.get("userId");
    var leadTable = $('#tblBanks').dataTable({
        "oLanguage": {
            "sZeroRecords": "No records to display",
            "sSearch": "Search "
        },
        "sDom": 'T<"clear">lfrtip',
        "tableTools": {

            "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
        },
        "iDisplayLength": 15,
        "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
        "bSortClasses": false,
        "bStateSave": false,
        "bPaginate": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "BankList.aspx/getBank",
        "columnDefs": [

            {
                "orderable": false,
                "targets": -1,
                "render": function (data, type, full, meta) {
                    return '<a  class="btn btn-success btn-sm btn-labeled btn-rounded" href="#/editbank/' + full[0] + '" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                }
            }
            //}, { orderable: true, "targets": -1 }, {
            //    "orderable": true,
            //    "targets": -1,
            //    "render": function (data, type, full, meta) {
            //        return '<a href="#" class="btn btn-danger btn-sm btn-labeled btn-rounded"  onclick="return deleteUser(' + full[0] + ');"><i class="fa fa-trash-o" aria-hidden="true"></a>';


            //    }
            //}
        ],
        "bDeferRender": true,
        "fnServerData": function (sSource, aoData, fnCallback) {
            
           // aoData["userId"] = $cookies.get("userId"),
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success":
                            function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblBanks").show();
                            }
            });
        }
    });


    ///////////////////////////////////////////////////////////// Add Bank\\\\\\\\\\\\\\\\\\\\\\\\\\\\

$scope.btnedit = 'Edit';
$scope.divVIEW = false;
$scope.divEdit = true;

$scope.editView = function () {

    if ($scope.btnedit == "Edit") {
        $scope.btnedit = 'View';
        $scope.divVIEW = true;
        $scope.divEdit = false;
    }
    else {
        $scope.btnedit = 'Edit';
        $scope.divVIEW = false;
        $scope.divEdit = true;
    }

}
var errorMessage = "";

$scope.brancheslist = [];
$scope.circles = [];
$scope.zoneList = [];
$scope.regionList = [];
var zones = [];
var regions = [];
var regions = [];
var branches = [];
 
var b = getParameterByName('bankId')
var bankId = '';
if (b != "")
    bankId = parseInt(b);
else
    bankId = 0;

if (bankId != 0) {
    $('.loader').show();
    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: "GetBCHandler.ashx?mode=Bank&bankId=" + bankId,
        async: false,
        success: function (bank) {
            $('#txtName').val(bank.BankName);
            $('#txtAddress').val(bank.Address);
            $('#txtContact').val(bank.ContactNumber);
            $('#txtEmail').val(bank.Email);
            $('#lblName').val(bank.BankName);
            $scope.lblName = bank.BankName
            $scope.lblAddress = bank.Address;
            $scope.lblContact = bank.ContactNumber;
            $scope.lblEmail = bank.Email;


            if (bank.BankCircles != null) {
                for (var c = 0; c < bank.BankCircles.length; c++) {
                    var circle = bank.BankCircles[c];
                    
                    //   circles.push(circle);
                    $scope.circles.push(circle);

                    if (circle.CircleZones != null) {
                        for (var z = 0; z < circle.CircleZones.length; z++) {
                            var zone = circle.CircleZones[z];
                            zones.push(zone);
                            $scope.zoneList.push(zone);
                            if (zone.ZoneRegions != null) {
                                for (var r = 0; r < zone.ZoneRegions.length; r++) {
                                    var region = zone.ZoneRegions[r];
                                    regions.push(region);
                                    $scope.regionList.push(region);

                                    if (region.RegionBranches != null) {
                                        for (var b = 0; b < region.RegionBranches.length; b++) {
                                            var branch = region.RegionBranches[b];
                                            branches.push(branch);
                                            //    AddBranch(branch, region.RegionName);
                                            $scope.brancheslist.push(branch);
                                        }

                                    }
                                }

                            } // if (zone.ZoneRegions
                        }
                    }
                }

            }

            PopulateCircles();
            Populatezone();
            PopulateRegions();


        }
    });
}


function getParameterByName(name) //courtesy Artem
{
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else {
        if ((results[1].indexOf('?')) > 0)
            return decodeURIComponent(results[1].substring(0, results[1].indexOf('?')).replace(/\+/g, " "));
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
$scope.addCircle = function () {

    var circle = new Object();
    circle.CircleName = $('#txtCircleName').val();
    circle.CircleZones = [];

    $scope.circles.push(circle);
    $('#txtCircleName').val('');
    PopulateCircles();
}


$scope.addZone = function () {

    var zone = new Object();
    zone.ZoneName = $('#txtZoneName').val();
    zone.CircleId = $('#selZoneCircle').val();
    zone.ZoneRegions = [];
    var circleName = $("#selZoneCircle option:selected").text();
    $scope.zoneList.push({
        "ZoneName": zone.ZoneName,
        "CircleName": circleName
    });

    zones.push(zone);
    //Add to relevant Cirlce
    for (var c = 0; c < $scope.circles.length; c++) {
        var circ = $scope.circles[c];
        if (circ.CircleName == circleName) {
            circ.CircleZones.push(zone);

        }

    }

    $('#txtZoneName').val('');
    Populatezone();
}
$scope.addRegion = function () {



    var region = new Object();
    region.RegionName = $('#txtRegionName').val();
    region.ZoneId = $("#selRegionZone").val();
    region.RegionBranches = [];
    var zoneName = $("#selRegionZone option:selected").text();

    $scope.regionList.push({
        "RegionName": region.RegionName,
        "zone": zoneName
    })
    regions.push(region);
    PopulateRegions();

    for (var z = 0; z < zones.length; z++) {
        var zon = zones[z];
        if (zon.ZoneName == zoneName) {
            zon.ZoneRegions.push(region);

        }

    }
    $('#txtRegionName').val('');

}
function PopulateRegions() {
    $('#selBranchRegion').empty();
    for (var c = 0; c < regions.length; c++) {
        $('#selBranchRegion').append($('<option>', {
            value: 0,
            text: regions[c].RegionName
        }));


    }
}
$scope.removeCircle = function (index) {

    $scope.circleList.splice(index, 1);
}
$scope.removeZone = function (index, cName) {

    $scope.zoneList.splice(index, 1);

    for (var c = 0; c < $scope.circles.length; c++) {
        var circ = $scope.circles[c];
        if (circ.CircleName == cName) {
            circ.CircleZones.splice(index, 1);

        }

    }

}
$scope.removeRegion = function (index, zoneName) {
    $scope.regionList.splice(index, 1);
    for (var z = 0; z < zones.length; z++) {
        var zon = zones[z];
        if (zon.ZoneName == zoneName) {
            zon.ZoneRegions.splice(index, 1);

        }

    }
}

function PopulateCircles() {

    $('#selZoneCircle').empty();
    for (var c = 0; c < $scope.circles.length; c++) {
        $('#selZoneCircle').append($('<option>', {
            value: 0,
            text: $scope.circles[c].CircleName
        }));
    }
}
function Populatezone() {
    
    $('#selRegionZone').empty();
    for (var c = 0; c < $scope.zoneList.length; c++) {
        $('#selRegionZone').append($('<option>', {
            value: 0,
            text: $scope.zoneList[c].ZoneName
        }));
    }
}



function validate() {

    errorMsg = '';

    var email = $('#txtEmail').val();
    var empId = $('#txtAddress').val();
    var name = $('#txtName').val();
    var phone = $('#txtContact').val();

    var result = true;



    if (!IsEmail(email)) {
        $('#txtEmail').addClass("form-group has-error");
        errorMsg = errorMsg + "<br/>Enter valid email Id.";
        result = false;
    }
    else {
        $("#divEmail").removeClass("form-group has-error");
    }

    if (name == "") {
        $('#divName').addClass("form-group has-error");
        errorMsg = errorMsg + "<br/>Enter Name.";
        result = false;
    }
    else {
        if (name.match(/\d+/g) != null) {
            $('#divName').addClass("form-group has-error");
            result = false;
        }
        else {
            $("#divName").removeClass("form-group has-error");
        }

    }

    if (phone == "" || phone.length > 10 || phone.length < 10 || isNaN(phone)) {
        $('#divPhone').addClass("form-group has-error");
        errorMsg = errorMsg + "<br/>Enter Valid Mobile Number.";
        result = false;
    }
    else {
        $("#divPhone").removeClass("form-group has-error");
    }

    return result;
}

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

$scope.addBank = function () {

    if (validate() == true) {
        var bank = new Object();
        bank.BankId = 0;
        bank.BankName = $('#txtName').val();
        bank.Address = $('#txtAddress').val();
        bank.ContactNumber = $('#txtContact').val();
        bank.Email = $('#txtEmail').val();
        bank.BankCircles = $scope.circles;

        $.ajax({
            url: "InsertUpdateBankHandler.ashx",
            method: "POST",
            datatype: "json",
            data: { "bank": JSON.stringify(bank) },
            success: function () {
                alert('Bank Saved!');
            },
            error: function (xmlHttpRequest, errorString, errorMessage) {
                alert(errorMessage);
            }
        });

    }

    else {
        ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    }
}

$scope.addBranch = function () {

    var branch = new Object();
    branch.IFSCCode = $('#txtBranchIFSCCode').val();
    branch.Address = $('#txtBranchAddress').val();
    branch.Address = $('#txtBranchAddress').val();
    branch.RegionId = $('#selBranchRegion').val();
    branch.StateId = $('#selBranchState').val();
    branch.DistrictId = $('#selBranchDistrict').val();
    branch.TalukaId = $('#selBranchTaluka').val();
    branch.CityId = $('#selBranchCity').val();
    branch.VillageId = $('#selBranchVillage').val();
    branch.PinCode = $('#txtBranchPinCode').val();
    branch.ContactNumber = $('#txtBranchContact').val();
    branch.Email = $('#txtBranchEmail').val();
    var RegionName = $("#selBranchRegion option:selected").text();
    branches.push(branch);

    for (var r = 0; r < regions.length; r++) {
        var reg = regions[r];
        if (reg.RegionName == RegionName) {
            reg.RegionBranches.push(branch);

        }

    }
    var s = $scope.circles;
    $scope.brancheslist.push(branch);


};
$scope.removeBranch = function (index, RegionName) {

    for (var r = 0; r < regions.length; r++) {
        var reg = regions[r];
        if (reg.RegionName == RegionName) {
            reg.RegionBranches.push(branch);

        }

    }
    $scope.brancheslist.splice(index, 1);
}


function ShowDialog(message, height, width, title, padding, icon, showOkButton, link) {

    $("#helloModalMessage").html(message);

    $('#helloModal').modal('show');

    if (showOkButton == false) {
        $("#closemodalbutton").show();
        $("#redirectbutton").hide();
    }
    else {
        if (link != '')
            $("#redirectbutton").attr("href", link);
        $("#closemodalbutton").hide();
        $("#redirectbutton").show();
    }

    //$.Dialog({
    //    overlay: true,
    //    shadow: true,
    //    flat: true,
    //    title: title,
    //    icon: icon,
    //    content: '',
    //    width: width,
    //    padding: padding,
    //    height: height,//600,400
    //    onShow: function (_dialog) {
    //        var content = _dialog.children('.content');
    //        content.html(message);
    //        $.Metro.initInputs();
    //    }
    //});
}


$scope.GetStates = function () {

    var allStates = new Array();
    var requestUrl = 'GetBCHandler.ashx?mode=state';
    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: requestUrl,
        async: false,
        success: function (stateList) {
            $('#selBranchState').empty();
            $('#selBranchState').append('<option value="' + '' + '">' + '--Select--' + '</option>');
            $scope.allState = stateList;
            for (var i = 0; i < $scope.allState.length; i++) {
                var k = $scope.allState[i].StateId;
                var v = $scope.allState[i].StateName;
                $('#selBranchState').append('<option value="' + k + '">' + v + '</option>');
            }

        },
        error: function (xhr, errorString, errorMessage) {
            alert('For some reason, the bank list could not be populated!\n' + errorMessage);
        }
    });
    return allStates;
}

$scope.sDistricts = [];
$scope.sCities = [];
$("#selBranchState").change(function () {
    
    $('#selBranchDistrict').empty();
    var StateId = $("#selBranchState").val();
    var ar = $scope.allState.filter(function (x) {
        return x.StateId == StateId;
    });
    $scope.sDistricts = ar[0].Districts;
    $('#selBranchDistrict').append('<option value="' + '' + '">' + '--Select--' + '</option>');
    for (var i = 0; i < ar[0].Districts.length; i++) {
        var k = ar[0].Districts[i].DistrictId;
        var v = ar[0].Districts[i].DistrictName;
        $('#selBranchDistrict').append('<option value="' + k + '">' + v + '</option>');
    }

});

$("#selBranchDistrict").change(function () {
    
    $('#selBranchTaluka').empty();
    $('#selBranchCity').empty();
    $('#selBranchVillage').empty();
    var DistrictId = $("#selBranchDistrict").val();
    DistrictId = parseInt(DistrictId);
    var ar = $scope.sDistricts.filter(function (x) {
        return x.DistrictId == DistrictId;
    });

    $('#selBranchTaluka').append('<option value="' + '' + '">' + '--Select--' + '</option>');
    for (var i = 0; i < ar[0].Talukas.length; i++) {
        var k = ar[0].Talukas[i].TalukaId;
        var v = ar[0].Talukas[i].TalukaName;
        $('#selBranchTaluka').append('<option value="' + k + '">' + v + '</option>');
    }
    $('#selBranchCity').append('<option value="' + '' + '">' + '--Select--' + '</option>');
    for (var i = 0; i < ar[0].Cities.length; i++) {
        var k = ar[0].Cities[i].CityId;
        var v = ar[0].Cities[i].CityName;
        $('#selBranchCity').append('<option value="' + k + '">' + v + '</option>');
    }
    $('#selBranchVillage').append('<option value="' + '' + '">' + '--Select--' + '</option>');
    for (var i = 0; i < ar[0].Villages.length; i++) {
        var k = ar[0].Villages[i].VillageId;
        var v = ar[0].Villages[i].VillageName;
        $('#selBranchVillage').append('<option value="' + k + '">' + v + '</option>');
    }

});

});