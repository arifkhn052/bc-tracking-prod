﻿

bctrackApp.controller("groupProductController", function ($scope, $state, $cookies) {
   

    $scope.products = [];


    $scope.getProduct = function () {
    

    var leadTable = $('#tblProduct').dataTable({
        "oLanguage": {
            "sZeroRecords": "No records to display",
            "sSearch": "Search "
        },
        "sDom": 'T<"clear">lfrtip',
        "tableTools": {

            "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
        },
        "iDisplayLength": 15,
        "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
        "bSortClasses": false,
        "bStateSave": false,
        "bPaginate": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "bcGroupProduct.aspx/getgroupProduct",
        "columnDefs": [

            {
                "orderable": false,
                "targets": -1,
                "render": function (data, type, full, meta) {
                    return '<a  class="btn btn-success btn-sm btn-labeled btn-rounded"  href="#/editgroupProduct/' + full[0] + '" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                }
            }
        ],
        "bDeferRender": true,
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success":
                            function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblProduct").show();
                            }
            });
        }
    });
    leadTable.fnSetFilteringDelay(300);
    }

    $scope.removeProduct = function (index) {
     
        $scope.products.splice(index, 1);
    }
    $scope.addMoreProduct =function()
    {
        //
        //var addprdct = new Object();
        //addprdct.ProductId = $('#productIds').val();
        //addprdct.ProductName = $('#productIds :selected').text();
        $scope.products.push({
            "ProductId": $('#productIds').val(),
            "ProductName":$('#productIds :selected').text()
        });
    }
      
        
   
    var errorMessage = "";
    $scope.deletegroupProduct = function () {

        if (confirm("Do you want to delete?")) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: "api/DeleteBcHandler.ashx?mode=groupProduct&grpproductId=" + groupId,
                async: false,
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                success: function () {
         

                },
                error: function (xmlHttpRequest, errorString, errorMessage) {

                    window.location.href = "#/groupproduct";
                    $scope.getProduct();
                    alert(' Group Product details deleted successfully.');
                }
            });

        }
        return false;
    }

   
    var b = $state.params.groupId;
    var groupId = 0;
    if (b != undefined)
        groupId = b;
    var defaultOption = '--Select--';
    var defaultOptionValue = -1;   

    if (groupId != 0) {
        
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: "api/GetBCHandler.ashx?mode=userGroupProduct&groupId=" + groupId,
            async: false,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            success: function (groupList) {
                $scope.products = groupList[0].ProductGrouplist;
            
                $('#txtgroupProductName').val(groupList[0].GroupName);
              

            }
        });
    }

  

    function validate() {

        errorMsg = '';       
        var name = $('#txtgroupProductName').val();
        var result = true;


        if (name == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter group Product Name.";
            result = false;
        }
       
        return result;
    }



    $('#btnAddgroupProduct').on('click', function () {
        
        if (validate() == true) {
            var group = new Object();
            group.id = groupId;
            group.GroupName = $('#txtgroupProductName').val();
            var prod = angular.toJson($scope.products);
           
            $.ajax({
                url: "api/InsertUpdateGroupProductHandler.ashx",
                method: "POST",
                datatype: "json",
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                data: {
                    "ProductGroupName": JSON.stringify(group),
                    "ProductGroup": prod,
                },
                success: function () {
                    
                    $('#productIds').val('');
                    $('#txtgroupProductName').val('');
                    window.location.href = "#/groupProduct";
                    $scope.getProduct();
                    alert('Product details saved successfully.');
                   
                },
                error: function (xmlHttpRequest, errorString, errorMessage) {
                    
                    $('#productIds').val('');
                    $('#txtgroupProductName').val('');
                    window.location.href = "#/groupProduct";
                    $scope.getProduct();
                    alert('Product details saved successfully.');
                }
            });
        }
        else
        {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
    });

   

    $scope.getProductforDropdownBind = function () {

        let corporates = new Array();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: 'api/GetBCHandler.ashx?mode=userProductS',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            async: false,
            success: function (product) {

                if (product.length != 0) {
                    for (let i = 0; i < product.length; i++) {
                        $('#productIds').append($('<option>', {
                            value: product[i].productId,
                            text: product[i].productName
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('Failed. ' + errorMessage);
            }
        });
        return corporates;
    }
    
});
