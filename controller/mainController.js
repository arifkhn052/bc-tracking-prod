﻿bctrackApp.controller("mainController", function ($scope, $state, $cookies, $rootScope, $timeout, $window) {
    console.log("main controller loadded..")
    $rootScope.$on('hideNavigation', function (event, args) {
        $scope.showNavigation = false;
    });
    $rootScope.$on('hideHeader', function (event, args) {
        $scope.showHeader = false;
    })

    $rootScope.$on('showNavigation', function (event, args) {
        $scope.showNavigation = true;
      
    });
    $rootScope.$on('showHeader', function (event, args) {
        $scope.showHeader = true;
       
    })
    $rootScope.$on('showLoader', function (event, args) {
        $scope.loaderClass = true;

    })
    $rootScope.$on('hideLoader', function (event, args) {
        $scope.loaderClass = false;

    })
    $rootScope.$on('loadMenu', function (event, args) {
        
        var logedUser;
        logUser = $cookies.get("userId")
        if ($scope.menuList.length > 0)
            logedUser = $scope.menuList[0].UserName;
        else
            logedUser = "-2";
        if (logUser != logedUser) {
            $scope.getMenuByUser();
        }

        if ($scope.menuList == undefined || $scope.menuList.length == 0) {
            $scope.getMenuByUser();
        }

    })

    $scope.menuList = [];
    $scope.getMenuByUser = function () {
     
        var allStates = new Array();
        var requestUrl = 'api/GetBCHandler.ashx?mode=userMenu';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            headers: {
                //"userId": sessionStorage.getItem("userId"),
                //"TokenId": sessionStorage.getItem("TokenId")
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            success: function (menuList) {
               
                $scope.username = menuList[0].UserName;

                $scope.userRole = menuList[0].Role;
                if ($scope.userRole == "1") {
                    $scope.hideBIResources = true;
                }
                else {
                    $scope.hideBIResources = false;
                }


                $scope.menuList = menuList;
                $cookies.put('Role', menuList[0].Role);
                $cookies.put('BankId', menuList[0].BankId);
              
                $timeout(function () {
                    menuList.forEach(function (menu) {
                 
                        $scope.navMenuOption = $scope.navMenuOption.map(function (option) {
                            if (option.state == menu.state) {
                                option.hidden = false;
                            }

                            return option;
                        })
                        $scope.navMenuOption1 = $scope.navMenuOption1.map(function (option) {
                            if (option.state == menu.state) {
                                option.hidden = false;
                            }

                            return option;
                        })
                        $scope.navMenuOption2 = $scope.navMenuOption2.map(function (option) {
                            if (option.state == menu.state) {
                                option.hidden = false;
                            }

                            return option;
                        })
                        //$scope.navMenuAdditionalReport = $scope.navMenuAdditionalReport.map(function (option) {
                        //    if (option.state == menu.state) {
                        //        option.hidden = false;
                        //    }

                        //    return option;
                        //})
                        $scope.navMenuOption3 = $scope.navMenuOption3.map(function (option) {
                            if (option.state == menu.state) {
                                option.hidden = false;
                            }

                            return option;
                        })
                        for (var i = 0; i < $scope.navMenuOption.length; i++) {
                     
                            var submenu = $scope.navMenuOption[i].subMenu;
                            if (submenu != undefined) {
                                submenu = submenu.map(function (option) {
                                    if (option.state == menu.state) {
                                        option.hidden = false;
                                    }

                                    return option;
                                })
                            }

                        }
                        for (var i = 0; i < $scope.navMenuOption1.length; i++) {

                            var submenu = $scope.navMenuOption1[i].subMenu;
                            if (submenu != undefined) {
                                submenu = submenu.map(function (option) {
                                    if (option.state == menu.state) {
                                        option.hidden = false;
                                    }

                                    return option;
                                })
                            }

                        }
                        for (var i = 0; i < $scope.navMenuOption2.length; i++) {

                            var submenu = $scope.navMenuOption2[i].subMenu;
                            if (submenu != undefined) {
                                submenu = submenu.map(function (option) {
                                    if (option.state == menu.state) {
                                        option.hidden = false;
                                    }

                                    return option;
                                })
                            }

                        }
                        //for (var i = 0; i < $scope.navMenuAdditionalReport.length; i++) {

                        //    var submenu = $scope.navMenuAdditionalReport[i].subMenu;
                        //    if (submenu != undefined) {
                        //        submenu = submenu.map(function (option) {
                        //            if (option.state == menu.state) {
                        //                option.hidden = false;
                        //            }

                        //            return option;
                        //        })
                        //    }

                        //}
                        for (var i = 0; i < $scope.navMenuOption3.length; i++) {

                            var submenu = $scope.navMenuOption3[i].subMenu;
                            if (submenu != undefined) {
                                submenu = submenu.map(function (option) {
                                    if (option.state == menu.state) {
                                        option.hidden = false;
                                    }

                                    return option;
                                })
                            }

                        }


                    })
                }, 500);
            },
            error: function (xhr, errorString, errorMessage) {
              
            }
        });
        return allStates;
    }

    $scope.getMenuByUsers = function () {
    
        $scope.menuItems.forEach(function (menu) {
          
            $scope.navMenuOption = $scope.navMenuOption.map(function (option) {
                if (option.state == menu.state) {
                    option.hidden = false;
                }

                return option;
            })
          
            for (var i = 0; i < $scope.navMenuOption.length; i++) {
             
                var submenu = $scope.navMenuOption[i].subMenu;
                if (submenu != undefined)
                {
                    submenu = submenu.map(function (option) {
                        if (option.state == menu.state) {
                            option.hidden = false;
                        }

                        return option;
                    })
                }
              
            }
                

         
        })
    }
   

    var navMenuOption = [
       
        {
              name: "Dashboard",
              state: "deshboard",
              icon: "fa fa-tachometer",
              color: "orange-red",
              menustaus: true,
              menuurl: "undefined",
              restricted: 1,
              hidden: true,
            

        },
          //{
          //    name: "DashBoard",
          //    state: "bankcode",
          //    icon: "fa fa-leaf",
          //    restricted: 1,
          //    hidden: true

          //},
          {
              name: "Add BC",
              state: "bc1",
              icon: "fa fa-university",
              color: "orange-red",
              restricted: 1,
              hidden: true,
              menuurl: "undefined",
              subMenu: [
                  {
                      name: "Bulk Upload",
                      state: "bulkUpload",
                      icon: "fa fa-upload",
                      restricted: 1,
                      hidden: true

                  },
                  {
                      name: "Add BC",
                      state: "singleEntry",
                      icon: "fa fa-list",
                      restricted: 1,
                      hidden: true

                  },
                    {
                        name: "Add Prospective BC",
                        state: "addProspectiveBCRegistry",
                        icon: "fa fa-list",
                        restricted: 1,
                        hidden: true

                    }
                 
              ]
          },
           {
               name: "View/Modify BC",
               state: "bc2",
               icon: "fa fa-list",
               color: "orange-red",
               restricted: 1,
               hidden: true,
               menuurl: "undefined",
               subMenu: [
                     {
                         name: "Find BC",
                         state: "findbc",
                         icon: "fa fa-search",
                         restricted: 1,
                         hidden: true

                     },
                   {
                       name: "All",
                       state: "all",
                       icon: "fa fa-bank",
                       restricted: 1,
                       hidden: true

                   },
                    {
                        name: "Prospective BC List",
                        state: "ProspectiveBCRegistryList",
                        icon: "fa fa-bank",
                        restricted: 1,
                        hidden: true

                    }
                 
                  
               ]
           }
   
    ]

    var navMenuOption3 = [
  {
      name: "Settings",
      state: "setting",
      icon: "fa fa-bars",
      color: "orange-red",
      restricted: 1,
      hidden: true,
      menuurl: "undefined",
      subMenu: [
         
          {
              name: "Change Password",
              state: "chnagepassword",
              icon: "fa fa-plus",
              restricted: 1,
              hidden: true

          }]
  },
    {
        name: "Utilities",
        state: "utilities",
    icon: "fa fa-bars",
    color: "orange-red",
    restricted: 1,
    hidden: true,
    menuurl: "undefined",
    subMenu: [
        {
            name: "Notification",
            state: "notifications",
            icon: "fa fa-bell",
            color: "orange-red",
            restricted: 1,
            hidden: true

        },
          {
              name: "Search Village Code",
              state: "searchvillage",
              icon: "fa fa-home",
              color: "orange-red",
              restricted: 1,
              hidden: true

          }]
       }

    ]


    var navMenuOption2 = [
        
          {
              name: "Main Reports",
              state: "reportList",
              icon: "fa fa-map-signs",
              color: "orange-red",
              restricted: 1,
              hidden: true,
              menuurl: "undefined",
              subMenu: [
                    {
                        name: "Bank wise BC's",
                        state: "byBranches",
                        icon: "fa fa-bank",
                        restricted: 1,
                        hidden: true

                    },
                    
                     {
                         name: "Location wise BC's",
                         state: "bclistLocationWise",
                         icon: "fa fa-map-marker",
                         restricted: 1,
                         hidden: true

                     },
                      //{
                      //    name: "Former ",
                      //    state: "bclistUnallocated",
                      //    icon: "fa fa-times",
                      //    restricted: 1,
                      //    hidden: true

                      //},
                      // {
                      //     name: "By Status",
                      //     state: "bclistStatus",
                      //     icon: "fa fa-info",
                      //     restricted: 1,
                      //     hidden: true

                      // },
                  //{
                  //    name: "List as on date",
                  //    state: "listason",
                  //    icon: "fa fa-calendar",
                  //    restricted: 1,
                  //    hidden: true

                  //},
                  {
                      name: "Corporate BC wise",
                      state: "CorporateBC",
                      icon: "fa fa-plane",
                      restricted: 1,
                      hidden: true

                  },
                    {
                      name: "Location wise POS List",
                      state: "listason",
                      icon: "fa fa-map-marker",
                      restricted: 1,
                      hidden: true

                  },
                  //{
                  //    name: "Branches Last Updated",
                  //    state: "BranchesLastUpdated",
                  //    icon: "fa fa-calendar-plus-o",
                  //    restricted: 1,
                  //    hidden: true

                  //}
                  //,
                  //{
                  //    name: "Allocation Based List",
                  //    state: "AllocationBasedList",
                  //    icon: "fa fa-road",
                  //    restricted: 1,
                  //    hidden: true

                  //}
                  // ,
                  //{
                  //    name: "Area wise BC allocation",
                  //    state: "areaWiseAllocation",
                  //    icon: "fa fa-map-marker",
                  //    restricted: 1,
                  //    hidden: true

                  //}
                  // ,
                  //{
                  //    name: "BC per products",
                  //    state: "BcPerProduct",
                  //    icon: "fa fa-product-hunt",
                  //    restricted: 1,
                  //    hidden: true

                  //}
                  //  ,
                  //{
                  //    name: "BC per multi-product",
                  //    state: "BcMultiroduct",
                  //    icon: "fa fa-product-hunt",
                  //    restricted: 1,
                  //    hidden: true

                  //}
                  // ,
                  {
                      name: "Black Listed BC",
                      state: "BlacklistedBCs",
                      icon: "fa fa-ban",
                      restricted: 1,
                      hidden: true

                  },
                   {
                       name: " Remuneration BC's",
                       state: "BlacklistedBCs",
                       icon: "fa fa-inr",
                       restricted: 1,
                       hidden: true

                   },
                    {
                        name: "Location wise Fixed vs Mobile BC's",
                        state: "fixedVSMobile",
                        icon: "fa fa-map-marker",
                        restricted: 1,
                        hidden: true

                    }

              ]
          },
            {
                name: "Additonal Reports",
                state: "summaryReport",
                icon: "fa fa-map-signs",
                color: "orange-red",
                restricted: 1,
                hidden: true,
                menuurl: "undefined",
                subMenu: [
                    {
                        name: "Allotment between dates",
                        state: "reportbetween",
                        icon: "fa fa-line-chart",
                        restricted: 1,
                        hidden: true

                    },
                    {
                        name: "Former BC",
                        state: "summaryUnallocated",
                        icon: "fa fa-line-chart",
                        restricted: 1,
                        hidden: true

                    },-
                    {
                        name: "Black listed BC",
                        state: "blacklistedBcGraph",
                        icon: "fa fa-line-chart",
                        restricted: 1,
                        hidden: true

                    }
                    ,
                    {
                        name: "Active/Inactive BC",
                        state: "activeBcGraph",
                        icon: "fa fa-line-chart",
                        restricted: 1,
                        hidden: true

                    }
                     ,
                    {
                        name: "Area wise BC Between Dates",
                        state: "areaWiseAllocationBetweenDate",
                        icon: "fa fa-line-chart",
                        restricted: 1,
                        hidden: true

                    }

                ]
            }

       
    ]
    //var navMenuAdditionalReport = [

    //     {
    //         name: "List",
    //         state: "reportList",
    //         icon: "fa fa-map-signs",
    //         color: "orange-red",
    //         restricted: 1,
    //         hidden: true,
    //         menuurl: "undefined",
    //         subMenu: [
                 
    //                 {
    //                     name: "former ",
    //                     state: "bclistunallocated",
    //                     icon: "fa fa-times",
    //                     restricted: 1,
    //                     hidden: true

    //                 },
    //                  {
    //                      name: "by status",
    //                      state: "bcliststatus",
    //                      icon: "fa fa-info",
    //                      restricted: 1,
    //                      hidden: true

    //                  },
    //             {
    //                 name: "list as on date",
    //                 state: "listason",
    //                 icon: "fa fa-calendar",
    //                 restricted: 1,
    //                 hidden: true

    //             },
    //             {
    //                 name: "Branches Last Updated",
    //                 state: "BranchesLastUpdated",
    //                 icon: "fa fa-calendar-plus-o",
    //                 restricted: 1,
    //                 hidden: true

    //             }
    //             ,
    //             {
    //                 name: "Allocation Based List",
    //                 state: "AllocationBasedList",
    //                 icon: "fa fa-road",
    //                 restricted: 1,
    //                 hidden: true

    //             }
    //              ,
    //             {
    //                 name: "Area wise BC allocation",
    //                 state: "areaWiseAllocation",
    //                 icon: "fa fa-map-marker",
    //                 restricted: 1,
    //                 hidden: true

    //             }
    //              ,
    //             {
    //                 name: "BC per products",
    //                 state: "BcPerProduct",
    //                 icon: "fa fa-product-hunt",
    //                 restricted: 1,
    //                 hidden: true

    //             }
    //               ,
    //             {
    //                 name: "BC per multi-product",
    //                 state: "BcMultiroduct",
    //                 icon: "fa fa-product-hunt",
    //                 restricted: 1,
    //                 hidden: true

    //             }
               
    //           ]
    //       }


    //]

    var navMenuOption1 = [
        {
            name: "Bank",
            state: "aBank",
            icon: "fa fa-university",
            color: "orange-red",
            restricted: 1,
            hidden: true,
            menuurl: "undefined",
            subMenu: [
                {
                    name: "Bank List",
                    state: "bankList",
                    icon: "fa fa-list-alt",
                    restricted: 1,
                    hidden: true

                },
                 {
                     name: "Add Branch",
                     state: "addHierarchy",
                     icon: "fa fa-list-alt",
                     restricted: 1,
                     hidden: true
                 },

                //{
                //    name: "Bulk Upload Bank",
                //    state: "uploadBank",
                //    icon: "fa fa-list-alt",
                //    restricted: 1,
                //    hidden: true

                //},
                {
                    name: "Branch List",
                    state: "branchlist",
                    icon: "fa fa-list-alt",
                    restricted: 1,
                    hidden: true

                },
                {
                    name: "Add Bank",
                    state: "addBank",
                    icon: "fa fa-plus",
                    restricted: 1,
                    hidden: true

                }]
        },
         {
             name: "Corporate",
             state: "Corporate",
             icon: "fa fa-building-o",
             color: "orange-red",
             restricted: 1,
             hidden: true,
             menuurl: "undefined",
             subMenu: [
                 {
                     name: "List of Corporate BC",
                     state: "corporateList",
                     icon: "fa fa-list-alt",
                     restricted: 1,
                     hidden: true

                 },
                 {
                     name: "Add Corporate BC",
                     state: "addCorporate",
                     icon: "fa fa-plus",
                     restricted: 1,
                     hidden: true

                 }]
         },
           {
               name: "Users",
               state: "aUsers",
               icon: "fa fa-user",
               color: "orange-red",
               restricted: 1,
               hidden: true,
               menuurl: "undefined",
               subMenu: [
                   {
                       name: "Users List",
                       state: "userList",
                       icon: "fa fa-list-alt",
                       restricted: 1,
                       hidden: true

                   },
                   {
                       name: "Add Users",
                       state: "addUser",
                       icon: "fa fa-plus",
                       restricted: 1,
                       hidden: true

                   }]
           },
            
            {
                name: "State",
                state: "aState",
                icon: "fa fa-bars",
                color: "orange-red",
                restricted: 1,
                hidden: true,
                menuurl: "undefined",
                subMenu: [
                    {
                        name: "State List",
                        state: "statelist",
                        icon: "fa fa-list-alt",
                        restricted: 1,
                        hidden: true

                    },
                    {
                        name: "Add State",
                        state: "addstate",
                        icon: "fa fa-plus",
                        restricted: 1,
                        hidden: true

                    }]
            },
                 {
                     name: "Activities",
                     state: "products",
                     icon: "fa fa-plus",
                     restricted: 1,
                     hidden: true,
                     menuurl: "undefined",
                     subMenu: [
                         {
                             name: "Add Activities",
                             state: "addproduct",
                             icon: "fa fa-bell",
                             color: "orange-red",
                             restricted: 1,
                             hidden: true

                         },
                           {
                               name: "Add Group Activities",
                               state: "groupproduct",
                               icon: "fa fa-home",
                               color: "orange-red",
                               restricted: 1,
                               hidden: true

                           },

                     ]
                 }

           

           
            
    ]

    $scope.navMenuOption = angular.copy(navMenuOption);
    $scope.navMenuOption1 = angular.copy(navMenuOption1);
    $scope.navMenuOption2 = angular.copy(navMenuOption2);
    $scope.navMenuOption3 = angular.copy(navMenuOption3);
   // $scope.navMenuAdditionalReport = angular.copy(navMenuAdditionalReport);

    $scope.navigateToRootMenu = function (menu, position) {
        
        if (position == "lower")
        {
            //if (menu.subMenu === undefined) {
            
            //    $state.go(menu.state, {
            //        reload: true
            //    });
            //}
        }
        if (position == "top") {
            if (menu.menuurl != "undefined") {

                $state.go(menu.state, {
                    reload: true
                });
            }
        }

      
    };
   
    $scope.redirectArrowaiBi = function () {
        $window.open('https://bcregistrybi.herokuapp.com/auth/loginauto/' + $scope.username + '@bcregistry.in', '_blank');
    };
   

});