﻿bctrackApp.controller("mobilevsfixedBcController", function ($scope, $cookies, $timeout, $window) {
    
   
   
    //$scope.ListOfFixedvsMobile = function () {
    //    debugger;
    //    var state = $('#selAddressState').val();
    //    var district = $('#selAddressDistrict').val();
    //    var subdistrict = $('#selAddresssubDistrict').val();
    //    var village = $('#selAddressVillage').val();
    //    var leadTable = $('#tblFixedVsMobileList').dataTable({
    //        "oLanguage": {
    //            "sZeroRecords": "No records to display",
    //            "sSearch": "Search "
    //        },
    //        "sDom": 'T<"clear">lfrtip',
    //        "tableTools": {

    //            "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
    //        },
    //        "iDisplayLength": 15,
    //        "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
    //        "bSortClasses": false,
    //        "bStateSave": false,
    //        "bPaginate": true,
    //        "bAutoWidth": false,
    //        "bProcessing": true,
    //        "bServerSide": true,
    //        "bDestroy": true,
    //        "sAjaxSource": "bclistLocationWise.aspx/getBCLocationWise",
    //        "columnDefs": [

    //          {
    //              "orderable": false,
    //              "targets": -1,
    //              "render": function (data, type, full, meta) {
    //                  return '';
    //              }
    //          }
    //        ],
    //        "bDeferRender": true,
    //        "fnServerData": function (sSource, aoData, fnCallback) {
    //            aoData["state"] = state,
    //            aoData["district"] = district,
    //            aoData["subdistrict"] = subdistrict,
    //            aoData["village"] = village,
    //            aoData["adminuserid"] = $cookies.get("userId"),
    //            $.ajax({
    //                "dataType": 'json',
    //                "contentType": "application/json; charset=utf-8",
    //                "type": "POST",
    //                "url": sSource,
    //                "data": aoData,
    //                "success":
    //                            function (msg) {
    //                                var json = jQuery.parseJSON(msg.d);
    //                                fnCallback(json);
    //                                $("#tblFixedVsMobileList").show();
    //                            }
    //            });
    //        }
    //    });
    //    leadTable.fnSetFilteringDelay(300);

    //}
    $scope.ListOfFixedvsMobile = function () {
        debugger;
        var state = $('#selAddressState').val();
        var type = 0;
            var district = $('#selAddressDistrict').val();
            var subdistrict = $('#selAddresssubDistrict').val();
            var village = $('#selAddressVillage').val();
            if (state == null || state == '') {
                state = "0";
            }
            if (district == null || district == '') {
                district = "0";
            }
            if (subdistrict == null || subdistrict == '') {
                subdistrict = "0";
            }
          
            if (village == null || village == '') {
                village = "0";
            }

            if (state != "0")
            {
                type = 1;
            }
          
             if (state != "0" && district == "0") {
                type = 2;
            }
             if ( state != "0" && district != "0" && subdistrict == "0") {
                type = 3
            }
          
           

        var leadTable = $('#tblbclistLocationWise').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "mobileVSfixedBC.aspx/getFixedVsMbileBC",
            "columnDefs": [

               
            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
        
                aoData["adminuserid"] = $cookies.get("userId");
                aoData["State"] = state;
                aoData["District"] = district;
                aoData["SubDistrict"] = subdistrict;
                aoData["Village"] = village;
                aoData["type"] = type;

                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblMobilevsFixedtype0").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);


    }
    //bind state
    $scope.GetStates = function () {
        debugger;
        //  $('#selAddressState').empty();
        $('#selAddressState').empty();
        $('#selAddressDistrict').empty();

        $('#selAddresssubDistrict').empty();

        $('#selAddressVillage').empty();

        $('#selAddressState').append($('<option>', { value: 0, text: '--Select State--' }));
        $('#selAddressDistrict').append($('<option>', { value: 0, text: '--Select District--' }));
        $('#selAddresssubDistrict').append($('<option>', { value: 0, text: '--Select Sub District--' }));
        $('#selAddressVillage').append($('<option>', { value: 0, text: '--Select Village--' }));


        var allStates = new Array();
        var requestUrl = 'api/state.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (stateList) {
                if (stateList.length != 0) {
                    for (let i = 0; i < stateList.length; i++) {
                        let state = new Object();
                        state.StateId = stateList[i].StateId;
                        state.StateName = stateList[i].StateName;
                        state.StateCode = stateList[i].StateCode;
                        state.StateCircles = stateList[i].StateCircles;
                        state.Branches = stateList[i].Branches;
                        allStates.push(state);
                    }
                }
                states = stateList;
                if (allStates.length != 0) {
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selAddressState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }
                if (allStates.length != 0) {
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selAddressState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }

            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
     //   return allStates;
    }

    //bind district
    $('#selAddressState').on('change', function () {

        $('#selAddressDistrict').empty();
        $('#selAddresssubDistrict').empty();
        $('#selAddressVillage').empty();

        $('#selAddressDistrict').append($('<option>', { value: 0, text: '--Select District--' }));
        $('#selAddresssubDistrict').append($('<option>', { value: 0, text: '--Select Sub District--' }));
        $('#selAddressVillage').append($('<option>', { value: 0, text: '--Select Village--' }));
        var allDistrict = new Array();
        var requestUrl = 'api/Districts.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"stateId": "' + $('#selAddressState').val() + '"}'
           ,
            async: false,
            success: function (districtList) {
                if (districtList.length != 0) {
                    for (let i = 0; i < districtList.length; i++) {
                        let district = new Object();
                        district.DistrictCode = districtList[i].DistrictCode;
                        district.DistrictName = districtList[i].DistrictName;
                        district.DistrictId = districtList[i].DistrictId;

                        allDistrict.push(district);
                    }
                }
                // states = districtList;
                if (allDistrict.length != 0) {


                    $('#selAddressDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allDistrict.length; i++) {
                        $('#selAddressDistrict').append($('<option>', {
                            value: allDistrict[i].DistrictId,
                            text: allDistrict[i].DistrictName + '(' + allDistrict[i].DistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
       // return allDistrict;

    });
    // bind subdistrict
    $('#selAddressDistrict').on('change', function () {
       $('#selAddresssubDistrict').empty();
        $('#selAddressVillage').empty();
       $('#selAddresssubDistrict').append($('<option>', { value: 0, text: '--Select Sub District--' }));
        $('#selAddressVillage').append($('<option>', { value: 0, text: '--Select Village--' }));

        var allSubDistrict = new Array();
        var requestUrl = 'api/GetSubDistrict.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"distirctId": "' + $('#selAddressDistrict').val() + '"}'
           ,
            async: false,
            success: function (subdistrictList) {
                if (subdistrictList.length != 0) {
                    for (let i = 0; i < subdistrictList.length; i++) {
                        let subdistrict = new Object();
                        subdistrict.SubDistrictCode = subdistrictList[i].SubDistrictCode;
                        subdistrict.SubDistrictName = subdistrictList[i].SubDistrictName;
                        subdistrict.SubDistrictId = subdistrictList[i].SubDistrictId;

                        allSubDistrict.push(subdistrict);
                    }
                }
                // states = districtList;
                if (allSubDistrict.length != 0) {
                      for (let i = 0; i < allSubDistrict.length; i++) {
                        $('#selAddresssubDistrict').append($('<option>', {
                            value: allSubDistrict[i].SubDistrictId,
                            text: allSubDistrict[i].SubDistrictName + '(' + allSubDistrict[i].SubDistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
       // return allDistrict;
      
    });
    //bind village
    $('#selAddresssubDistrict').on('change', function () {
        $('#selAddressVillage').empty();
        $('#selAddressVillage').append($('<option>', { value: 0, text: '--Select Village--' }));

        var allVillage = new Array();
        var requestUrl = 'api/GetVillage.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"subDistrictId": "' + $('#selAddresssubDistrict').val() + '"}'
           ,
            async: false,
            success: function (villageList) {
                if (villageList.length != 0) {
                    for (let i = 0; i < villageList.length; i++) {
                        let village = new Object();
                        village.VillageCode = villageList[i].VillageCode;
                        village.VillageName = villageList[i].VillageName;
                        village.VillageId = villageList[i].VillageId;

                        allVillage.push(village);
                    }
                }
                $scope.allVillageList = allVillage;
              
                if (allVillage.length != 0) {
                    for (let i = 0; i < allVillage.length; i++) {
                        $('#selAddressVillage').append($('<option>', {
                            value: allVillage[i].VillageId,
                            text: allVillage[i].VillageName + '(' + allVillage[i].VillageCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the village list could not be populated!\n' + errorMessage);
            }
        });
       // return allVillage;
   
    });
    $scope.redirectToDashBoard = function () {
        $window.open('http://bcregistrybi.herokuapp.com/public/question/77b859e4-c700-4040-b9f0-d8d3f59b334a/');
    };
    $scope.redirectToTopList = function () {
        $window.open('https://bcregistrybi.herokuapp.com/question/11');
    };
});