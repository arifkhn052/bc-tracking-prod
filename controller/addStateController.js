﻿bctrackApp.controller("addStateController", function ($scope, $state, $cookies) {
     
    var b = $state.params.Bankid;

    var cid = '';
    if (b != undefined)
        cid = parseInt(b);
    else
        bankId = 0;
    //if (cid != 0) {

    //    $('.loader').show();
    //    $.ajax({
    //        type: 'POST',
    //        dataType: 'json',
    //        headers: {
    //            "userId": sessionStorage.getItem("userId"),
    //            "TokenId": sessionStorage.getItem("TokenId")
    //        },
    //        contentType: 'application/json',
    //        url: "api/GetBCHandler.ashx?mode=stateid&sid=" + cid,
    //        async: false,
    //        success: function (c) {

    //            $('#txtName').val(c.stateName);
    //            $('#txtEmail').val(c.Email);
    //            $('#txtType').val(c.stateType);
    //            $('#txtAddress').val(c.Address);
    //            $('#txtContact').val(c.ContactNumber);

    //        }
    //    });
    //}
    $('#btnAddstate').on('click', function () {

        var state = new Object();

         
        state.bankid = cid;
        state.stateName = $('#txtStateName').val();
     
        $.ajax({
            url: "api/GetStatesHandler.ashx",
            method: 'POST',
            datatype: 'json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            data: { 'info': JSON.stringify(state) },
            success: function () {
                alert('state details saved successfully.');
                window.location.href = "#/addBankState/" + $state.params.Bankid + "";
                $scope.GetBankState();
                $('#txtStateName').val('');
            },
            error: function (xhr, errorString, errorMessage) {
                alert('state details saved successfully.');
                window.location.href = "#/addBankState/" + $state.params.Bankid + "";
                $scope.GetBankState();
                $('#txtStateName').val('');
            }
        });
    });

    $scope.GetBankState = function () {


        var leadTable = $('#tblProduct').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "addstates.aspx/getProduct",
            //"columnDefs": [

            //    //{
            //    //    "orderable": false,
            //    //    "targets": -1,
            //    //    "render": function (data, type, full, meta) {
            //    //        return '<a  class="btn btn-success btn-sm btn-labeled btn-rounded"  href="#/editProduct/' + full[0] + '" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            //    //    }
            //    //}
            //],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                
                aoData["adminBankId"] = $state.params.Bankid;;
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblProduct").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);
    }
})