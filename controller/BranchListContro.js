﻿



bctrackApp.controller("BranchListContro", function ($scope, $state, $timeout, $cookies) {

   
    $scope.divbtnBank = true;
    $scope.divbtnState = true;
    $scope.divbtnCity = true;
    $scope.divbtnDistrict = true;

    $scope.GetBanks = function () {
         
        if ($cookies.get("BankId") == -1) {

            $timeout(function () { $scope.divbtnBank = false; }, 100);
        }
        else {

        }
        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=Bank";
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {


                //console('Got it!');
                for (var cnt = 0; cnt < Banks.length; cnt++) {
                    let bank = new Object();
                    bank.BankId = Banks[cnt].BankId;
                    bank.BankName = Banks[cnt].BankName;
                    bank.Branches = Banks[cnt].Branches;
                    bank.BankCircles = Banks[cnt].BankCircles;
                    banks.push(bank);
                }
                if ($cookies.get("Role") == "1") {
                    allBanks = banks;
                }
                else {
                    var bankid = parseInt($cookies.get("BankId"));
                    var updateBankList = banks.filter(function (x) {
                        return x.BankId === bankid
                    });
                    allBanks = updateBankList;
                    $('#allBanks').val($cookies.get("BankId"));


                }
                $('#selBank').append($('<option>', { value: 0, text: '--Select Bank--' }));
                console.log(allBanks);
                for (let i = 0; i < allBanks.length; i++) {
                    $('#selBank').append($('<option>', {
                        value: allBanks[i].BankId,
                        text: allBanks[i].BankName
                    }));
                }


                $('.loader').hide();
            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }
        });
        return banks;
    }

    $('#selBank').on('change', function () {


         
        $('#selState').empty();
        Bankid = $('#selBank').val();
        $('#selState').append($('<option>', { value: 0, text: '--Select State--' }));


        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=State&BankId=" + Bankid;
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {
                 
                if (Banks.length > 0) {

                }
                else {
                    $timeout(function () { $scope.divbtnState = true; }, 100);
                    $timeout(function () { $scope.divbtnCity = true; }, 100);
                    $timeout(function () { $scope.divbtnDistrict = true; }, 100);
                    $('#selCity').empty();                  
                    $('#selDistrict').empty();
                 



                }

                for (let i = 0; i < Banks.length; i++) {
                    $('#selState').append($('<option>', {
                        value: Banks[i].CircleId,
                        text: Banks[i].CircleName
                    }));
                }
            },
            error: function (err) {

            }
        });
        return banks;
    });
    $('#selState').on('change', function () {


        $('#selCity').empty();
        StateId = $('#selState').val();
        $('#selCity').append($('<option>', { value: 0, text: '--Select City--' }));
        if (StateId == 0) {
            $timeout(function () { $scope.divbtnState = true; }, 100);
        }
        else {
            $timeout(function () { $scope.divbtnState = false; }, 100);
        }
        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=City&StateId=" + StateId;
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {
                 
                for (let i = 0; i < Banks.length; i++) {
                    $('#selCity').append($('<option>', {
                        value: Banks[i].ZoneId,
                        text: Banks[i].ZoneName
                    }));
                }
            },
            error: function (err) {

            }
        });
        return banks;
    });
    $('#selCity').on('change', function () {
         


        $('#selDistrict').empty();
        var districtId = $('#selCity').val();
        $('#selDistrict').append($('<option>', { value: 0, text: '--Select City--' }));
        if (districtId == 0) {
            $timeout(function () { $scope.divbtnCity = true; }, 100);
        }
        else {
            $timeout(function () { $scope.divbtnCity = false; }, 100);
        }
        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=District&districtId=" + districtId;
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {
                 
                for (let i = 0; i < Banks.length; i++) {
                    $('#selDistrict').append($('<option>', {
                        value: Banks[i].RegionId,
                        text: Banks[i].RegionName
                    }));
                }
            },
            error: function (err) {

            }
        });
        return banks;
    });
    $('#selDistrict').on('change', function () {

        var disid = $('#selDistrict').val();
        if (disid == 0) {
            $timeout(function () { $scope.divbtnDistrict = true; }, 100);
        }
        else {
            $timeout(function () { $scope.divbtnDistrict = false; }, 100);
        }
    });
    $scope.getBrnahList = function () {
        var leadTable = $('#tblBranchList').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "BranchList.aspx/GetBranch",
            "columnDefs": [

            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["adminuserId"] = $cookies.get("userId"),
              //  aoData["BankId"] = sessionStorage.getItem("userId"),
             //   aoData["StateId"] = sessionStorage.getItem("userId"),
             //   aoData["DistrictId"] = sessionStorage.getItem("userId"),
             //   aoData["iType"] = sessionStorage.getItem("userId"),
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblBranchList").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);

    }
    $scope.getBrnahListFilter = function () {
         
        let bankId = $('#selBank').val();
        let district = $('#selDistrict').val();
        var itype;

        if (bankId == "0") {
            itype = 1;  
        }

        if (bankId != "0" && district == null ) {
            itype = 2;
         
        }
        if (bankId != "0" && district != null) {
            itype = 3;

        }
        

        var leadTable = $('#tblBranchList').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "BranchList.aspx/GetBranchFilter",
            "columnDefs": [

            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["adminuserId"] = $cookies.get("userId"),
                  aoData["adminBankId"] = bankId
                   aoData["DistrictId"] = district
                   aoData["iType"] = itype
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblBranchList").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);

    }


})