﻿bctrackApp.controller("uploadController", function ($scope, $cookies) {
    var days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    var origYearFrom = 2017;
    var origMonthFrom = 0;
    var range = [];
    var secondrange = [];
    var monthRange = 1;
    var apptMode = 'AppointmentMode';
    var createMode = 'CreateMode';
    blackListMode = 'BlackList';
    var yearTo = 2017;

    $('#genDataAppointDates').on('click', function () {
        range = [];
        var filter = "&AppointmentDateFrom=" + $('#txtStartDate').val();
        filter += "&AppointmentDateTo=" + $('#txtEndDate').val();

        mode = 'Allocated';
        var bcList = GetBC(mode, filter);

        var dtTo = new Date(bcList[bcList.length - 1].AppointmentDate);
        var dtFrom = new Date(bcList[0].AppointmentDate);

        CreateChartData(bcList, dtFrom, dtTo, apptMode);
        PlotChart('BC allocations ' + origYearFrom + ' - ' + yearTo);

    });

    $('#btnGenDataUnallocated').on('click', function () {
        range = [];
        var filter = "&CreateDateFrom=" + $('#txtStartDateUnAlloc').val();
        filter += "&CreateDateTo=" + $('#txtEndDateUnAlloc').val();

        mode = 'All';
        var bcList = GetBC(mode, filter);

        var dtTo = new Date(bcList[bcList.length - 1].CreatedOn);
        var dtFrom = new Date(bcList[0].CreatedOn);

        CreateChartData(bcList, dtFrom, dtTo, createMode);
        PlotChart('FORMER BC ' + origYearFrom + ' - ' + yearTo);

    });

    $('#btnGenDataBlackList').on('click', function () {
        range = [];
        var filter = "&BlackListDateFrom=" + $('#txtStartDate').val();
        filter += "&BlackListDateTo=" + $('#txtEndDate').val();

        mode = 'BlackList';
        var bcList = GetBC(mode, filter);

        var dtTo = new Date(bcList[bcList.length - 1].BlacklistDate);
        var dtFrom = new Date(bcList[0].BlacklistDate);

        CreateChartData(bcList, dtFrom, dtTo, blackListMode);
        PlotChart('Blacklisted BC ' + origYearFrom + ' - ' + yearTo);

    });

    $('#btnGenDateStatus').on('click', function () {
        range = [];
        var filter = "&CreateDateFrom=" + $('#txtStartDate').val();
        filter += "&CreateDateTo=" + $('#txtEndDate').val();
        // filter += "&IsBlackListed=true";

        mode = 'All';
        var bcList = GetBC(mode, filter);

        var dtTo = new Date(bcList[bcList.length - 1].CreatedOn);
        var dtFrom = new Date(bcList[0].CreatedOn);

        CreateActiveChartData(bcList, dtFrom, dtTo, createMode);
        PlotChart('Active BC', 'Inactive BC');

    });

    function getCommonFilter(filter) {
        let bankId = $('#allBanks :selected').val();
        let bankCircle = $('#bankCircle :selected').val();
        let bankZone = $('#bankZone :selected').val();
        let bankRegion = $('#bankRegion :selected').val();
        let bankBranch = $('#bankBranch :selected').val();
        let category = $('#bankCategory :selected').text();
        let ssa = $('#bankSsa :selected').text();
        let state = $('#bankState :selected').val();

        if (typeof bankId != 'undefined' && bankId != 0)
            filter += "&bankId=" + bankId;
        if (typeof bankCircle != 'undefined' && bankCircle != 0)
            filter += "&bankCircleId=" + bankCircle;
        if (typeof bankZone != 'undefined' && bankZone != 0)
            filter += "&bankZoneId=" + bankZone;
        if (typeof bankRegion != 'undefined' && bankRegion != 0)
            filter += "&bankRegionId=" + bankRegion;
        if (typeof category != 'undefined' && category != 'Select Category')
            filter += "&branchCategory=" + category;
        if (typeof ssa != 'undefined' && ssa != 'Select SSA')
            filter += "&bankSsa=" + ssa;
        if (typeof state != 'undefined' && state != 0)
            filter += "&stateId=" + state;

        return filter;
    }

    function CreateChartData(bcList, dtFrom, dtTo, dateMode) {

        var monthFrom = dtFrom.getMonth();
        var monthTo = dtTo.getMonth();

        var yearFrom = dtFrom.getFullYear();
        var yearTo = dtTo.getFullYear();
        origYearFrom = yearFrom;
        origMonthFrom = monthFrom;

        var diffMonths = monthTo - monthFrom;
        diffMonths += (yearTo - yearFrom) * 12;

        monthRange = parseInt(diffMonths / diffMonths);
        range.push(GetBCWithinDateRange(bcList, yearFrom, monthFrom, dateMode));


        for (var i = 0; i < diffMonths ; i++) {

            monthFrom += monthRange;
            if (monthFrom > 12) {
                yearFrom++;
                monthFrom -= 12;
            }
            range.push(GetBCWithinDateRange(bcList, yearFrom, monthFrom, dateMode));

        }

        yearTo = yearFrom;


    }




    function CreateActiveChartData(bcList, dtFrom, dtTo, dateMode) {

        var monthFrom = dtFrom.getMonth();
        var monthTo = dtTo.getMonth();

        var yearFrom = dtFrom.getFullYear();
        var yearTo = dtTo.getFullYear();
        origYearFrom = yearFrom;
        origMonthFrom = monthFrom;

        var diffMonths = monthTo - monthFrom;
        diffMonths += (yearTo - yearFrom) * 12;

        monthRange = parseInt(diffMonths / diffMonths);
        range.push(GetBCWithinDateRange(bcList, yearFrom, monthFrom, dateMode, true));
        secondrange.push(GetBCWithinDateRange(bcList, yearFrom, monthFrom, dateMode, false));


        for (var i = 0; i < diffMonths ; i++) {

            monthFrom += monthRange;
            if (monthFrom > 12) {
                yearFrom++;
                monthFrom -= 12;
            }
            range.push(GetBCWithinDateRange(bcList, yearFrom, monthFrom, dateMode, true));
            secondrange.push(GetBCWithinDateRange(bcList, yearFrom, monthFrom, dateMode, false));

        }

        yearTo = yearFrom;



    }




    function GetBCWithinDateRange(bcList, yearFrom, monthFrom, dateMode, getActive) {
        var bcCnt = 0;
        for (var b = 0; b < bcList.length; b++) {

            var bc = bcList[b];
            var apptDate = new Date(bc.AppointmentDate);
            var createDate = new Date(bc.CreatedOn);
            var blackListDate = new Date(bc.BlacklistDate);

            if (getActive != null && getActive != undefined) {
                if (getActive)
                    if (bc.Status != 'Active' || bc.IsBlackListed)
                        continue;
                    else if (!getActive) {
                        if (bc.Status == 'Active')
                            continue;
                    }
            }


            if (dateMode == apptMode) {
                if (apptDate <= Date.UTC(yearFrom, monthFrom, days[monthFrom]) && apptDate >= Date.UTC(yearFrom, monthFrom, 1))
                    bcCnt++;
            }
            else if (dateMode == createMode) {
                if (createDate <= Date.UTC(yearFrom, monthFrom, days[monthFrom]) && (!bc.isAllocated || apptDate > Date.UTC(yearFrom, monthFrom, days[monthFrom])))
                    bcCnt++;
            }
            else if (dateMode == blackListMode) {
                if (blackListDate <= Date.UTC(yearFrom, monthFrom, days[monthFrom]))
                    bcCnt++;
            }



        }
        return bcCnt;
    }

    function PlotChart(seriesName, secondSeriesName) {

        var seriesOption = [{ name: seriesName, data: range }];
        if (secondSeriesName != null)
            seriesOption.push({ name: secondSeriesName, data: secondrange });

        $('#container').highcharts({
            chart: {
                type: 'line'
            },
            title: {
                text: 'BC Allotment in the Bank'
            },
            xAxis: {
                type: 'datetime',
                //    dateTimeLabelFormats: { // don't display the dummy year
                //        month: '%e. %b',
                //        year: '%b'
                //    },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                title: {
                    text: 'BC Count'
                },
                min: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.0f} BC'
            },

            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                },
                series: {
                    pointStart: Date.UTC(origYearFrom, origMonthFrom, 1),
                    pointIntervalUnit: 'month'
                }
            },

            series: seriesOption
        });

    }



    function GetBC(mode, filter) {
        var requrl = "GetBCHandler.ashx?mode=" + mode;
        if (filter != '')
            requrl += filter;

        var bcl = [];

        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            async: false,
            success: function (BankCorrs) {
                bcl = BankCorrs;
            }
        });

        return bcl;
    }

    function GetBanks() {
        let allBanks = new Array();
        var requestUrl = 'GetBCHandler.ashx?mode=Bank';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            async: false,
            success: function (bankList) {
                if (bankList.length != 0) {
                    for (let i = 0; i < bankList.length; i++) {
                        let bank = new Object();
                        bank.BankId = bankList[i].BankId;
                        bank.BankName = bankList[i].BankName;
                        bank.BankCircles = bankList[i].BankCircles;
                        bank.Branches = bankList[i].Branches;
                        allBanks.push(bank);
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allBanks;
    }

    //function GetStates() {
    //    var allStates = new Array();
    //    var requestUrl = 'GetBCHandler.ashx?mode=state';
    //    $.ajax({
    //        type: 'POST',
    //        dataType: 'json',
    //        contentType: 'application/json',
    //        url: requestUrl,
    //        async: false,
    //        success: function (stateList) {
    //            if (stateList.length != 0) {
    //                for (let i = 0; i < stateList.length; i++) {
    //                    let state = new Object();
    //                    state.StateId = stateList[i].StateId;
    //                    state.StateName = stateList[i].StateName;
    //                    state.StateCircles = stateList[i].StateCircles;
    //                    state.Branches = stateList[i].Branches;
    //                    allStates.push(state);
    //                }
    //            }
    //        },
    //        error: function (xhr, errorString, errorMessage) {
    //            alert('For some reason, the bank list could not be populated!\n' + errorMessage);
    //        }
    //    });
    //    return allStates;
    //}

    function GetProducts() {
        let products = new Array();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: 'GetBCHandler.ashx?mode=product',
            async: false,
            succes: function (productList) {
                if (productList != null) {
                    for (let i = 0; i < productList.length; i++) {
                        products.push(productList[i]);
                    }
                }
            },
            error: function (xhr, errorString, errorMsg) {
                alert(errorMsg);
            }
        });
        return products;
    }

    function GetCorporates() {
        let corporates = new Array();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: 'GetBCHandler.ashx?mode=Corporate',
            async: false,
            success: function (corpList) {
                for (let i = 0; i < corpList.length; i++) {
                    corporates.push(corpList[i]);
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('Failed. ' + errorMessage);
            }
        });
        return corporates;
    }

    var allBanks = GetBanks();
    if (allBanks.length != 0) {
        for (let i = 0; i < allBanks.length; i++) {
            $('#allBanks').append($('<option>', {
                value: allBanks[i].BankId,
                text: allBanks[i].BankName
            }));
        }
    }

    var allStates = GetStates();
    if (allStates.length != 0) {
        for (let i = 0; i < allStates.length; i++) {
            $('#bankState').append($('<option>', {
                value: allStates[i].StateId,
                text: allStates[i].StateName
            }));
        }
    }

    var allProducts = GetProducts();
    if (allProducts != null) {
        for (let i = 0; i < allProducts.length; i++) {
            $('#productIds').append($('<option>', { value: allProducts[i].ProductId, text: allProducts[i].ProductName }));
        }
    }

    var allCorporates = GetCorporates();
    if (allCorporates != null) {
        for (let i = 0; i < allCorporates.length; i++) {
            $('#selCorporate').append($('<option>', { value: allCorporates[i].CorporateId, text: allCorporates[i].CorporateName }));
        }
    }

    function getBank(bankList, bankId) {
        let bank = new Object();
        for (let i = 0; i < bankList.length; i++) {
            if (bankList[i].BankId == bankId) {
                bank = bankList[i];
                break;
            }
        }
        return bank;
    }

    function getZones(circleList, circleId) {
        let zones = new Array();
        for (let i = 0; i < circleList.length; i++) {
            if (circleList[i].CircleId == circleId) {
                if (circleList[i].CircleZones != null) {
                    zones = circleList[i].CircleZones;
                    break;
                }
            }
        }
        return zones;
    }

    function getRegions(zoneList, zoneId) {
        let regions = new Array();
        for (let i = 0; i < zoneList.length; i++) {
            if (zoneList[i].ZoneId == zoneId) {
                if (zoneList[i].ZoneRegions != null) {
                    regions = zoneList[i].ZoneRegions;
                    break;
                }
            }
        }
        return regions;
    }

    function getBranches(regionList, regionId) {
        let branches = new Array();
        for (let i = 0; i < regionList.length; i++) {
            if (regionId == regionList[i].RegionId) {
                if (regionList[i].RegionBranches != null) {
                    branches = regionList[i].RegionBranches;
                    break;
                }
            }
        }
        return branches;
    }

    //Given a bank, list all of it's circles
    $('#allBanks').on('change', function () {
        $('#bankCircle').empty();
        $('#bankBranch').empty();
        $('#bankCircle').append($('<option>', { value: 0, text: 'Select Circle' }));
        $('#bankBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        var selectedBankId = $('#allBanks :selected').val();
        if (selectedBankId != 0) {
            let bank = getBank(allBanks, selectedBankId);
            let bankCircles = null, bankBranches = null;
            if (bank.BankCircles != null)
                bankCircles = bank.BankCircles;
            if (bank.Branches != null)
                bankBranches = bank.Branches;
            if (bankCircles != null) {
                for (let i = 0; i < bankCircles.length; i++) {
                    $('#bankCircle').append($('<option>', { value: bankCircles[i].CircleId, text: bankCircles[i].CircleName }));
                }
            }
        }
    });

    //Given a bank's circle, list all of it's zones
    $('#bankCircle').on('change', function () {
        let selectedBankId = $('#allBanks :selected').val();
        let selectedBank = getBank(allBanks, selectedBankId);
        let selectedCircleId = $('#bankCircle :selected').val();
        $('#bankZone').empty();
        $('#bankZone').append($('<option>', { value: 0, text: 'Select Zone' }));
        if (selectedBank != null) {
            if (selectedBank.BankCircles != null) {
                let zones = getZones(selectedBank.BankCircles, selectedCircleId);
                if (zones != null) {
                    for (let i = 0; i < zones.length; i++) {
                        $('#bankZone').append($('<option>', { value: zones[i].ZoneId, text: zones[i].ZoneName }));
                    }
                }
            }
        }
    });

    //Given a bank's zone, list all of it's regions
    $('#bankZone').on('change', function () {
        let selectedZoneId = $('#bankZone :selected').val();
        let selectedBankId = $('#allBanks :selected').val();
        let selectedCircleId = $('#bankCircle :selected').val();
        let bank = getBank(allBanks, selectedBankId);
        $('#bankRegion').empty();
        $('#bankRegion').append($('<option>', { value: 0, text: 'Select Region' }));
        if (bank != null) {
            if (bank.BankCircles != null) {
                let zones = getZones(bank.BankCircles, selectedCircleId);
                if (zones != null) {
                    let regions = getRegions(zones, selectedZoneId);
                    if (regions != null) {
                        for (let i = 0; i < regions.length; i++) {
                            $('#bankRegion').append($('<option>', { value: regions[i].RegionId, text: regions[i].RegionName }));
                        }
                    }
                }
            }
        }
    });

    //Given a bank's region, list all of it's branches under that region
    $('#bankRegion').on('change', function () {
        $('#bankBranch').empty();
        $('#bankBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        let selectedBankId = $('#allBanks :selected').val();
        let selectedCircleId = $('#bankCircle :selected').val();
        let selectedZoneId = $('#bankZone :selected').val();
        let selectedRegionId = $('#bankRegion :selected').val();
        let bank = getBank(allBanks, selectedBankId);
        if (bank != null) {
            if (bank.BankCircles != null) {
                let zones = getZones(bank.BankCircles, selectedCircleId);
                if (zones != null) {
                    let regions = getRegions(zones, selectedZoneId);
                    if (regions != null) {
                        let branches = getBranches(regions, selectedRegionId);
                        if (branches != null) {
                            for (let i = 0; i < branches.length; i++) {
                                $('#bankBranch').append($('<option>', { value: branches[i].BranchId, text: branches[i].BranchName }));
                            }
                        }
                    }
                }
            }
        }
    });

    var tblBranchesLastUpdated = $('#tblBranchesLastUpdated').DataTable({
        'paging': true,
        'ordering': true,
        'info': false,
        'searching': false
    });

    var tblBlacklistedBCs = $('#tblBlacklistedBcs').DataTable({
        'paging': true,
        'ordering': true,
        'info': false,
        'searching': false
    });

    var tblProducts = $('#tblProducts').DataTable({
        'paging': true,
        'ordering': true,
        'info': false,
        'searching': false
    });

    $('#btnAllocationBasedList').on('click', function () {
        let allocationDate = $('#txtAllocationDate').val();
        let filter = "&allocationDate=" + allocationDate;
        filter = getCommonFilter(filter);
        //alert('Filtering with ' + filter);
    });

    $('#btnAreaWiseAllocation').on('click', function () {
        let allocationDate = $('#txtAllocationDate').val();
        let filter = "&allocationDate=" + allocationDate;
        filter = getCommonFilter(filter);
        //alert('Filter using ' + filter);
    });
    /*
    $('#btnListAsOnDate').on('click', function () {
        let date = $('#createFromDate').val();
        let filter = "&createFromDate=" + date;
        filter = getCommonFilter(filter);
        let mode = 'Allocated';
        let bcList = GetBC(mode, filter);
        if (bcList != null) {
            let t = $('#tblBCList').DataTable({
                "paging": true,
                "ordering": true,
                "info": false,
                "searching": true
            });
            for (let i = 0; i < bcList.length; i++) {
                t.row.add([bcList[i].BCId,bcList[i].BCName,bcList[i].Phone,bcList[i].AadharCard, bcList[i].Certificates,bcList[i].Villages]);
            }
        }
        else {
            alert('No data!');
        }
    });
    */
    /*
    $('#btnListCorporateBCs').on('click', function () {
        let date = $('#txtCreateDate').val();
        let corporateId = $('#selCorporate :selected').val();
        let filter = "&createFromDate=" + date + "&corporateId=" + corporateId;
        filter = getCommonFilter(filter);
    });
    */
    $('#btnBranchesLastUpdated').on('click', function () {
        let date = $('#txtUpdateDate').val();
        let filter = "&UpdatedOn=" + date;
        let mode = 'Allocated';
        filter = getCommonFilter(filter);
    });

    $('#btnGetBcsPerProduct').on('click', function () {
        let productId = $('#productIds').val();
        let filter = "&productId=" + productId;
        filter = getCommonFilter(filter);
    });

    $('#btnBlacklistedBCs').on('click', function () {
        let blacklistDate = $('#txtBlacklistDate').val();
        let filter = "&blacklistDate=" + blacklistDate;
        filter = getCommonFilter(filter);
        alert('Filtering using ' + filter);
    });

    $('#btnFilterBC').on('click', function () {
        let filter = '';
        filter = getCommonFilter(filter);
    });

    $('#btnFilterBCLocationWise').on('click', function () {
        let pinCode = $('#txtPincode').val();
        let filter = "&pinCode=" + pinCode;
        filter = getCommonFilter(filter);
    });
});