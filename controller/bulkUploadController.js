﻿bctrackApp.controller("bulkUploadController", function ($scope, $timeout, $cookies) {
    $scope.fileStatusText = "File Uploading";
    $scope.fileIcon = "fa fa-upload";
    $scope.zipStatusText = "Zip Uploading";
    $scope.zipIcon = "fa fa-file-archive-o";

    $scope.DivCheckFileStatus = true;
    $scope.DivCheckZipStatus = true;
    $scope.DivCheckDownloadError = true;
    $scope.divUploadingArea = false;
    $scope.divUploadingAreaZip = false;
    $scope.divtextStatus = true;
    $scope.myfILEAlert = true;
    $scope.myZipAlert = true;
    $scope.mytable = true;
    
    $scope.getFileStatus1 = function () {

        var requestUrl = 'api/getBankHandler.ashx?mode=FileStatus';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            url: requestUrl,
            async: false,
            success: function (Status) {
                
                if (Status.length == 0) {

                    $timeout(function () { $scope.DivCheckFileStatus = true; }, 100);
                    $timeout(function () { $scope.divUploadingArea = false; }, 100);
                }
                else {
                    if (Status[0].fileStatus == '0') {
                        $scope.fileStatusText = "File uploading in progress. Please wait or check the status by clicking the Button below.";
                        $scope.fileIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        //   alert('File uploading in progress. Get Status from the Check file status button.')
                        $timeout(function () { $scope.DivCheckFileStatus = false; }, 100);
                        $timeout(function () { $scope.divUploadingArea = true; }, 100);
                        $timeout(function () { $scope.myfILEAlert = true; }, 100);

                    }
                    if (Status[0].fileStatus == '1') {
                        $scope.fileStatusText = "File uploading in progress. Please wait or check the status by clicking the Button below.";
                        $scope.fileIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        //   alert('File uploading in progress. Get Status from the Check file status button.')
                        $timeout(function () { $scope.DivCheckFileStatus = false; }, 100);
                        $timeout(function () { $scope.divUploadingArea = true; }, 100);
                        $timeout(function () { $scope.myfILEAlert = true; }, 100);
                    }
                    if (Status[0].fileStatus == '2') {
                        $scope.fileStatusText = "File Uploading";
                        $scope.fileIcon = "fa fa-upload";
                        $scope.fileName = Status[0].fileName;
                        $timeout(function () { $scope.DivCheckFileStatus = true; }, 100);
                        $timeout(function () { $scope.divUploadingArea = false; }, 100);
                        $timeout(function () { $scope.myfILEAlert = true; }, 100);

                    }
                    if (Status[0].fileStatus == '3') {
                        $scope.fileStatusText = "Last file upload failed. Please resolve that and upload again.";
                        $scope.fileIcon = "fa fa-exclamation-triangle";
                        //   alert('File uploading in progress. Get Status from the Check file status button.')
                        $timeout(function () { $scope.DivCheckDownloadError = false; }, 100);
                        $timeout(function () { $scope.DivCheckFileStatus = true; }, 100);
                        $timeout(function () { $scope.divUploadingArea = false; }, 100);
                        $timeout(function () { $scope.myfILEAlert = true; }, 100);

                    }


                }


            },
            error: function (xhr, errorString, errorMessage) {

            }
        });

    }

    $scope.getZipStatus1 = function () {
        
        var requestUrl = 'api/getBankHandler.ashx?mode=ZipStatus';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            url: requestUrl,
            async: false,
            success: function (Status) {
                
                if (Status.length == 0) {
                    $scope.zipStatusText = "Zip Uploading";
                    $scope.zipIcon = "fa fa-file-archive-o";
                    $timeout(function () { $scope.DivCheckZipStatus = true; }, 100);
                    $timeout(function () { $scope.divUploadingAreaZip = false; }, 100);
                    $timeout(function () { $scope.myZipAlert = true; }, 100);

                }
                else {
                    if (Status[0].fileStatus == '0') {
                        $scope.zipStatusText = "Zip uploading in progress. Please wait or check the status by clicking the Button below..";
                        $scope.zipIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        //   alert('File uploading in progress. Get Status from the Check file status button.')
                        $timeout(function () { $scope.DivCheckZipStatus = false; }, 100);
                        $timeout(function () { $scope.divUploadingAreaZip = true; }, 100);
                        $timeout(function () { $scope.myZipAlert = true; }, 100);


                    }
                    if (Status[0].fileStatus == '1') {
                        $scope.zipStatusText = "Zip uploading in progress. Please wait or check the status by clicking the Button below..";
                        $scope.zipIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        $timeout(function () { $scope.DivCheckZipStatus = false; }, 100);
                        $timeout(function () { $scope.divUploadingAreaZip = true; }, 100);
                        $timeout(function () { $scope.myZipAlert = true; }, 100);
                    }
                    if (Status[0].fileStatus == '2') {
                        $scope.zipStatusText = "Zip Uploading";
                        $scope.zipIcon = "fa fa-file-archive-o";

                        $timeout(function () { $scope.DivCheckZipStatus = true; }, 100);
                        $timeout(function () { $scope.divUploadingAreaZip = false; }, 100);
                        $timeout(function () { $scope.myZipAlert = true; }, 100);


                    }



                }


            },
            error: function (xhr, errorString, errorMessage) {

            }
        });

    }
    
  
    $scope.getFileStatus = function () {
     
        var requestUrl = 'api/getBankHandler.ashx?mode=FileStatus';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            url: requestUrl,
            async: false,
            success: function (Status) {
                
                if (Status.length==0)
                {
                   
                    $timeout(function () { $scope.DivCheckFileStatus = true; }, 100);
                    $timeout(function () { $scope.divUploadingArea = false; }, 100);
                }
                else
                {
                    if (Status[0].fileStatus == '0') {
                        $scope.fileStatusText = "File uploading in progress. Please wait or check the status by clicking the Button below.";
                        $scope.fileIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                     //   alert('File uploading in progress. Get Status from the Check file status button.')
                        $timeout(function () { $scope.DivCheckFileStatus = false; }, 100);
                        $timeout(function () { $scope.divUploadingArea = true; }, 100);
                        $timeout(function () { $scope.myfILEAlert = true; }, 100);
                        
                    }
                    if (Status[0].fileStatus == '1') {
                        $scope.fileStatusText = "File uploading in progress. Please wait or check the status by clicking the Button below.";
                        $scope.fileIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        //   alert('File uploading in progress. Get Status from the Check file status button.')
                        $timeout(function () { $scope.DivCheckFileStatus = false; }, 100);
                        $timeout(function () { $scope.divUploadingArea = true; }, 100);
                        $timeout(function () { $scope.myfILEAlert = true; }, 100);
                    }
                    if (Status[0].fileStatus == '2') {
                        $scope.fileStatusText = "File Uploading";
                        $scope.fileIcon = "fa fa-upload";
                        $scope.fileName = Status[0].fileName;
                        $timeout(function () { $scope.DivCheckFileStatus = true; }, 100);
                        $timeout(function () { $scope.divUploadingArea = false; }, 100);
                        $timeout(function () { $scope.myfILEAlert = false; }, 100);

                    }
                    if (Status[0].fileStatus == '3') {
                        $scope.fileStatusText = "Last file upload failed. Please resolve that and upload again.";
                        $scope.fileIcon = "fa fa-exclamation-triangle";
                        //   alert('File uploading in progress. Get Status from the Check file status button.')
                        $timeout(function () { $scope.DivCheckDownloadError = false; }, 100);
                        $timeout(function () { $scope.DivCheckFileStatus = true; }, 100);
                        $timeout(function () { $scope.divUploadingArea = false; }, 100);
                        $timeout(function () { $scope.myfILEAlert = true; }, 100);
                   
                    }
                  
                  
                }

              
            },
            error: function (xhr, errorString, errorMessage) {
             
            }
        });
       
    }

    $scope.getZipStatus = function () {
        
        var requestUrl = 'api/getBankHandler.ashx?mode=ZipStatus';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            url: requestUrl,
            async: false,
            success: function (Status) {
                
                if (Status.length == 0) {                  
                    $scope.zipStatusText = "Zip Uploading";
                    $scope.zipIcon = "fa fa-file-archive-o";
                    $timeout(function () { $scope.DivCheckZipStatus = true; }, 100);
                    $timeout(function () { $scope.divUploadingAreaZip = false; }, 100);
                    $timeout(function () { $scope.myZipAlert = true; }, 100);
                    
                }
                else {
                    if (Status[0].fileStatus == '0') {
                        $scope.zipStatusText = "Zip uploading in progress. Please wait or check the status by clicking the Button below..";
                        $scope.zipIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        //   alert('File uploading in progress. Get Status from the Check file status button.')
                        $timeout(function () { $scope.DivCheckZipStatus = false; }, 100);
                        $timeout(function () { $scope.divUploadingAreaZip = true; }, 100);
                        $timeout(function () { $scope.myZipAlert = true; }, 100);


                    }
                    if (Status[0].fileStatus == '1') {
                        $scope.zipStatusText = "Zip uploading in progress. Please wait or check the status by clicking the Button below..";
                        $scope.zipIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        $timeout(function () { $scope.DivCheckZipStatus = false; }, 100);
                        $timeout(function () { $scope.divUploadingAreaZip = true; }, 100);
                        $timeout(function () { $scope.myZipAlert = true; }, 100);
                    }
                    if (Status[0].fileStatus == '2') {
                        $scope.zipStatusText = "Zip Uploading";
                        $scope.zipIcon = "fa fa-file-archive-o";
                        $scope.zipName = Status[0].fileName;
                        $timeout(function () { $scope.DivCheckZipStatus = true; }, 100);
                        $timeout(function () { $scope.divUploadingAreaZip = false; }, 100);
                        $timeout(function () { $scope.myZipAlert = false; }, 100);


                    }
                   


                }


            },
            error: function (xhr, errorString, errorMessage) {

            }
        });

    }


    $("#btnsubmitfile").click(function (evt) {
        
      
        var requestUrl = 'api/getBankHandler.ashx?mode=FileStatus';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            url: requestUrl,
            async: false,
            success: function (Status) {
                
               
                if (Status.length == 0) {                   

                    $scope.uploadFile(evt)
                }
                else
                {
                    if (Status[0].fileStatus == '0') {
                      
                       
                        $timeout(function () { $scope.myfILEAlert = true; }, 100);
                        $scope.fileStatusText = "File uploading in progress. Please wait or check the status by clicking the Button below.";
                        $scope.fileIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        $timeout(function () { $scope.DivCheckFileStatus = false; }, 100);
                        $timeout(function () { $scope.divUploadingArea = true; }, 100);
                    }
                    if (Status[0].fileStatus == '2') {
                        //$scope.uploadFile(evt)
                        //$scope.fileStatusText = "File uploading in progress, please or manual check the status or by clicking.";
                        //$scope.fileIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        //$timeout(function () { $scope.DivCheckFileStatus = true; }, 100);
                        //$timeout(function () { $scope.divUploadingArea = false; }, 100);
                        //$timeout(function () { $scope.DivCheckDownloadError = true; }, 100);
                        //$timeout(function () { $scope.mytable = true; }, 100);

                        $timeout(function () { $scope.myfILEAlert = true; }, 100);
                        $scope.uploadFile(evt)
                        $scope.fileStatusText = "File uploading in progress. Please wait or check the status by clicking the Button below.";
                        $scope.fileIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        $timeout(function () { $scope.DivCheckDownloadError = true; }, 100);
                        $timeout(function () { $scope.mytable = true; }, 100);

                    }
                    if (Status[0].fileStatus == '3') {
                        $timeout(function () { $scope.myfILEAlert = true; }, 100);
                        $scope.uploadFile(evt)
                        $scope.fileStatusText = "Last file upload failed. Please resolve that and upload again.";
                        $scope.fileIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        $timeout(function () { $scope.DivCheckDownloadError = true; }, 100);
                        $timeout(function () { $scope.mytable = true; }, 100);
                       
                    }
                    else
                    {

                            //alert('File uploading in progress, please or manual check the status or by clicking.')
                            //$scope.DivCheckFileStatus = false;
                        
                    }

                }


            },
            error: function (xhr, errorString, errorMessage) {

            }
        });
       
    });

    $scope.uploadFile = function (evt) {
        $scope.fileStatusText = "File uploading in progress. Please wait or check the status by clicking the Button below.";
        $scope.fileIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
        $timeout(function () { $scope.DivCheckFileStatus = false; }, 100);
        $timeout(function () { $scope.divUploadingArea = true; }, 100);
        $timeout(function () { $scope.DivCheckDownloadError = true; }, 100);
        $timeout(function () { $scope.mytable = true; }, 100);
        $timeout(function () { $scope.myfILEAlert = true; }, 100);
        var fileUpload = $("#uploadcsv").get(0);
        var files = fileUpload.files;

        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }

        $.ajax({
            url: "api/FileUploader.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            success: function (result) {
           
                //    window.location.href = "#/bulkUpload";
              //  alert(result);
            },
            error: function (err) {
               
            }
        });

        evt.preventDefault();
    }

    $scope.zipupload = function (evt) {

        $scope.zipStatusText = "Zip uploading in progress. Please wait or check the status by clicking the Button below..";
        $scope.zipIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
        //   alert('File uploading in progress. Get Status from the Check file status button.')
        $timeout(function () { $scope.DivCheckZipStatus = false; }, 100);
        $timeout(function () { $scope.divUploadingAreaZip = true; }, 100);
        $timeout(function () { $scope.myZipAlert = true; }, 100);
        var fileUpload = $("#uploadzip").get(0);
        var files = fileUpload.files;

        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }

        $.ajax({
            url: "api/ZipUploader.ashx",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            success: function (result) {

                //    window.location.href = "#/bulkUpload";
               // alert(result);
                // alert("zip file uploaded sucessfully!!");
            },
            error: function (err) {
                alert(err.statusText)
            }
        });

        evt.preventDefault();
    }

    $("#btnsubmitzip").click(function (evt) {
        

        var requestUrl = 'api/getBankHandler.ashx?mode=ZipStatus';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            url: requestUrl,
            async: false,
            success: function (Status) {
                

                if (Status.length == 0) {

                    $timeout(function () { $scope.DivCheckZipStatus = true; }, 100);
                    $timeout(function () { $scope.divUploadingAreaZip = false; }, 100);
                    $timeout(function () { $scope.myZipAlert = true; }, 100);
                    $scope.zipupload(evt);

                }
                else {
                    if (Status[0].fileStatus == '0') {
                        $scope.zipStatusText = "Zip uploading in progress. Please wait or check the status by clicking the Button below..";
                        $scope.zipIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        //   alert('File uploading in progress. Get Status from the Check file status button.')
                        $timeout(function () { $scope.DivCheckZipStatus = false; }, 100);
                        $timeout(function () { $scope.divUploadingAreaZip = true; }, 100);
                        $timeout(function () { $scope.myZipAlert = true; }, 100);


                    }
                    if (Status[0].fileStatus == '1') {
                        $scope.zipStatusText = "Zip uploading in progress. Please wait or check the status by clicking the Button below..";
                        $scope.zipIcon = "fa fa-spinner fa-pulse fa-1x fa-fw";
                        $timeout(function () { $scope.DivCheckZipStatus = false; }, 100);
                        $timeout(function () { $scope.divUploadingAreaZip = true; }, 100);
                        $timeout(function () { $scope.myZipAlert = true; }, 100);

                    }
                    if (Status[0].fileStatus == '2') {
                        $scope.zipStatusText = "Zip Uploading";
                        $scope.zipIcon = "fa fa-file-archive-o";

                        $timeout(function () { $scope.DivCheckZipStatus = true; }, 100);
                        $timeout(function () { $scope.divUploadingAreaZip = false; }, 100);
                        $timeout(function () { $scope.myZipAlert = false; }, 100);
                        $scope.zipupload(evt);

                    }

                }


            },
            error: function (xhr, errorString, errorMessage) {

            }
        });
    });


    $scope.getFileError = function () {
        
        $scope.mytable = false;
        var requestUrl = 'api/getBankHandler.ashx?mode=FileError';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            url: requestUrl,
            async: false,
            success: function (errorList) {
                $scope.errorList = errorList;

            },
            error: function (xhr, errorString, errorMessage) {

            }
        });

    }
    
    $scope.downloadfile = function () {
        $("#mytable").table2excel({
            name: "Table2Excel",
            filename: "myFileName",
            fileext: ".xls"
        });
    }
   
    setInterval(function () {
        $scope.getFileStatus();
        $scope.getZipStatus();
    }, 6000);
 
    //$('#btnsubmit').on('click', function () {
    //     
    //    $.ajax({
    //        url: "FileUploader.ashx",
    //        method: "POST",
    //        contentType: 'text/plain',
    //        async: false,
    //        success: function (data) {

    //            var fileName = $(this).val().replace(/^.*[\\\/]/, '');
    //            var currFile = $(this)[0].files[0];
    //            var fieldId = $(this).attr('id');
    //            var xhr = new XMLHttpRequest();
    //            var uploadData = new FormData();
    //            var url = 'FileUploader.ashx';
    //            uploadData.append('file', currFile);
    //            xhr.open("POST", url, true);
    //            xhr.send(uploadData);
    //            alert(data);
    //        },
    //        error: function () {

    //            var fileName = $(this).val().replace(/^.*[\\\/]/, '');
    //            var currFile = $(this)[0].files[0];
    //            var fieldId = $(this).attr('id');
    //            var xhr = new XMLHttpRequest();
    //            var uploadData = new FormData();
    //            var url = 'FileUploader.ashx';
    //            uploadData.append('file', currFile);
    //            xhr.open("POST", url, true);
    //            xhr.send(uploadData);

    //        }
    //    });
    //});

    //$(document).on('change', '#uploadcsv', function () {
    //     
    //    var fileName = $(this).val().replace(/^.*[\\\/]/, '');
    //    var currFile = $(this)[0].files[0];
    //    var fieldId = $(this).attr('id');
    //    var xhr = new XMLHttpRequest();
    //    var uploadData = new FormData();
    //    var url = 'FileUploader.ashx';
    //    uploadData.append('file', currFile);
    //    xhr.open("POST", url, true);
    //    xhr.send(uploadData);
    //    alert('File uploaded successfully');
    //});


});