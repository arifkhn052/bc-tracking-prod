﻿bctrackApp.controller("AllocationBasedListController", function ($scope, $cookies) {
    $scope.datetimeBind = function () {
        //tabing();
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        var firstDay = '01/' + mm + '/' + yyyy;
        $("#dtStartDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: firstDay
        });
        $("#dtStartDate").val(firstDay);

        $("#dtEndDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#dtEndDate").val(today);
    }


    $scope.getblackListedDatabydate=function()
    {
        
      
        //startDate = $('#dtStartDate').val();
      //  endDate = $('#dtEndDate').val();
        let bankId = $('#allBanks :selected').val();
        if (bankId == null || bankId=='undefined')
        {
            bankId=0;
        }
       
      
    
        var leadTable = $('#tblBlacklistedBcs').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "itype": 1,
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "BlacklistedBCs.aspx/getBlackListedBcfilter",
            "columnDefs": [

          
            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["adminbankId"] = bankId;            
                aoData["adminuserid"] = $cookies.get("userId"),
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblBlacklistedBcs").show();
                                }
                });
            }
        });
        //  leadTable.fnSetFilteringDelay(300);
    }
//------------------------------------------------------------- --BLACK LIST END--  --------------------------------------------------------------------\\








    var days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    var origYearFrom = 2017;
    var origMonthFrom = 0;
    var range = [];
    var secondrange = [];
    var monthRange = 1;
    var apptMode = 'AppointmentMode';
    var createMode = 'CreateMode';
    blackListMode = 'BlackList';
    var yearTo = 2017;
    let allBanks = new Array();

    var fullurl = $(location).attr('href');

    var c = fullurl.match(/cId=([^&]+)/);
    var cId = '';
    if (c != null)
        cId = c[1];

    var t2 = '';
    if ($('#tblBCList').length > 0) {


        var mode = '';
        var url = $(location).attr('href');
        if (url.indexOf('BCListAll') > 0 || url.indexOf('bclistStatus') > 0)
            mode = 'All';
        else if (url.indexOf('bclistBankWise') > 0 || url.indexOf('bclistLocationWise') > 0)
            mode = 'Allocated';
        else if (url.indexOf('bclistUnallocated') > 0)
            mode = 'Unallocated';

        var bcList = [];
        var allCorporates;
        bcList = GetBC(mode, '');
        FillTable(t2);
        GetCorporates();


    }
    var tblBranchesLastUpdated = $('#tblBranchesLastUpdated').DataTable({
        'paging': true,
        'ordering': true,
        'info': false,
        'searching': false
    });

    allCorporates = GetCorporates();
    if (allCorporates != null) {
        for (let i = 0; i < allCorporates.length; i++) {
            $('#selCorporate').append($('<option>', { value: allCorporates[i].CorporateId, text: allCorporates[i].CorporateName }));
        }
    }
    function GetBC(mode, filter) {

        var requrl = "api/GetBCHandler.ashx?mode=" + mode;
        if (filter != '')
            requrl += filter;

        var bcl = [];

        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            contentType: 'application/json',
            url: requrl,
            async: false,
            success: function (BankCorrs) {
                bcl = BankCorrs;
            }
        });

        return bcl;
    }
    function FillTable(table) {

        table.clear().draw();

        for (var cnt = 0; cnt < bcList.length; cnt++) {
            var bankc = bcList[cnt];


            var certs = '';
            if (bankc.Certifications != null) {
                for (var j = 0; j < bankc.Certifications.length; j++)
                    certs += bankc.Certifications[j].CourseName;
            }

            var villages = '';
            if (bankc.OperationalAreas != null) {
                for (var k = 0; k < bankc.OperationalAreas.length; k++)
                    villages += bankc.OperationalAreas[k].VillageCode;
            }
            var black = "BlackList";
            var glp = 'ban-circle';
            if (bankc.IsBlackListed) {
                black = "WhiteList";
                glp = 'ok-circle';
            }

            var actionRow = '<a href="SingleEntry.aspx?bc=' + bankc.BankCorrespondId + '&mode=Edit" title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
            actionRow += '&nbsp;<a href="SingleEntry.aspx?bc=' + bankc.BankCorrespondId + '&mode=View"  title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
            actionRow += '&nbsp;<a href="BlackListBC.aspx?bc=' + bankc.BankCorrespondId + '&mode=' + black + '" title="' + black + '"><span class="glyphicon glyphicon-' + glp + '" aria-hidden="true"></span></a>';
            actionRow += '&nbsp;<a href="ChangeStatus.aspx?bc=' + bankc.BankCorrespondId + '" title="ChangeStatus"><span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span></a>';

            table.row.add([bankc.BankCorrespondId, bankc.Name, bankc.PhoneNumber1, bankc.AadharCard, certs, villages, actionRow
            ]).draw(false);

        }

    }

    $('#btnListAsOnDate').on('click', function () {
        let date = $('#createFromDate').val();
        let filter = "&createFromDate=" + date;
        filter = getCommonFilter(filter);
        let mode = 'Allocated';
        let bcList = GetBC(mode, filter);
        if (bcList != null) {
            console.log(JSON.stringify(bcList));
            for (let i = 0; i < bcList.length; i++) {
                t2.row.add([bcList[i].BankCorrespondId, bcList[i].Name, bcList[i].PhoneNumber1, bcList[i].UniqueIdentificationNumber, bcList[i].Certificates, bcList[i].Villages]).draw(false);
            }
        }
        else {
            alert('No data!');
        }
    });
    function GetCorporates() {
        let corporates = new Array();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: 'api/GetBCHandler.ashx?mode=Corporate',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (corpList) {
                for (let i = 0; i < corpList.length; i++) {
                    corporates.push(corpList[i]);
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('Failed. ' + errorMessage);
            }
        });
        return corporates;
    }
    function getCommonFilter(filter) {
        let bankId = $('#allBanks :selected').val();
        let bankCircle = $('#bankCircle :selected').val();
        let bankZone = $('#bankZone :selected').val();
        let bankRegion = $('#bankRegion :selected').val();
        let bankBranch = $('#bankBranch :selected').val();
        let category = $('#bankCategory :selected').val();
        let ssa = $('#bankSsa :selected').val();
        let state = $('#bankState :selected').val();

        if (bankId != 0 && bankId !== 'Select Bank')
            filter += "&bankId=" + bankId;
        if (bankCircle != 0)
            filter += "&bankCircleId=" + bankCircle;
        if (bankZone != 0)
            filter += "&bankZoneId=" + bankZone;
        if (bankRegion != 0)
            filter += "&bankRegionId=" + bankRegion;
        if (category != 0 && category !== 'Select Category')
            filter += "&bankCategory=" + category;
        if (ssa !== 'Select SSA' && ssa != 0)
            filter += "&bankSsa=" + ssa;
        if (state != 0)
            filter += "&stateId=" + state;

        return filter;
    }
    //$scope.GetBanks = function () {

    //    let banks = new Array();
    //    var requrl = "api/getBankHandler.ashx?mode=Bank";
    //    $('.loader').show();
    //    $.ajax({
    //        type: 'POST',
    //        dataType: 'json',
    //        contentType: 'application/json',
    //        url: requrl,
    //        headers: {
    //            "userId": $cookies.get("userId"),
    //            "TokenId": $cookies.get("TokenId")
    //        },
    //        async: false,
    //        success: function (Banks) {
                 
    //            //console('Got it!');
    //            for (var cnt = 0; cnt < Banks.length; cnt++) {
    //                let bank = new Object();
    //                bank.BankId = Banks[cnt].BankId;
    //                bank.BankName = Banks[cnt].BankName;
    //                bank.Branches = Banks[cnt].Branches;
    //                bank.BankCircles = Banks[cnt].BankCircles;
    //                banks.push(bank);
    //            }
    //            allBanks = banks;
               

    //            console.log(allBanks);
    //            for (let i = 0; i < allBanks.length; i++) {
    //                $('#selBank').append($('<option>', {
    //                    value: allBanks[i].BankId,
    //                    text: allBanks[i].BankName
    //                }));
    //            }
    //            for (let i = 0; i < allBanks.length; i++) {
    //                $('#allBanks').append($('<option>', {
    //                    value: allBanks[i].BankId,
    //                    text: allBanks[i].BankName
    //                }));
    //            }

    //            $('.loader').hide();
    //        },
    //        error: function (err) {
    //            alert(err);
    //            $('.loader').hide();
    //        }
    //    });
    //    return banks;
    //}
    $scope.GetBanks = function () {

        if ($cookies.get("BankId") == -1) {

           // $timeout(function () { $scope.divbtnBank = false; }, 100);
        }
        else {

        }
        let banks = new Array();
        var requrl = 'api/getBankHandler.ashx?mode=All';

        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {
                debugger;

                //console('Got it!');
                for (var cnt = 0; cnt < Banks.length; cnt++) {

                    let bank = new Object();
                    bank.BankId = Banks[cnt].BankId;
                    bank.BankName = Banks[cnt].BankName;
                    bank.Branches = Banks[cnt].Branches;
                    bank.BankCircles = Banks[cnt].BankCircles;
                    banks.push(bank);
                }
                if ($cookies.get("Role") == "1") {
                    allBanks = banks;
                }
                else {

                    var bankid = parseInt($cookies.get("BankId"));
                    var updateBankList = banks.filter(function (x) {
                        return x.BankId === bankid
                    });
                    allBanks = updateBankList;
                    //$('#allBanks').val($cookies.get("BankId"));


                }
               
                console.log(allBanks);
                for (let i = 0; i < allBanks.length; i++) {
                    $('#selBank').append($('<option>', {
                        value: allBanks[i].BankId,
                        text: allBanks[i].BankName
                    }));
                }


                $('.loader').hide();
            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }
        });
     //   return banks;
    }
    //Given a bank, list all of it's circles
    $('#allBanks').on('change', function () {
        $('#bankCircle').empty();
        $('#bankBranch').empty();
        $('#bankCircle').append($('<option>', { value: 0, text: 'Select Circle' }));
        $('#bankBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        var selectedBankId = $('#allBanks :selected').val();
        if (selectedBankId != 0) {
            let bank = getBank(allBanks, selectedBankId);
            let bankCircles = null, bankBranches = null;
            if (bank.BankCircles != null)
                bankCircles = bank.BankCircles;
            if (bank.Branches != null)
                bankBranches = bank.Branches;
            if (bankCircles != null) {
                for (let i = 0; i < bankCircles.length; i++) {
                    $('#bankCircle').append($('<option>', { value: bankCircles[i].CircleId, text: bankCircles[i].CircleName }));
                }
            }
        }
    });
    function getBank(bankList, bankId) {
        
        let bank = new Object();
        for (let i = 0; i < bankList.length; i++) {
            if (bankList[i].BankId == bankId) {
                bank = bankList[i];
                break;
            }
        }
        return bank;
    }
    //Given a bank's circle, list all of it's zones
    $('#bankCircle').on('change', function () {
        let selectedBankId = $('#allBanks :selected').val();
        let selectedBank = getBank(allBanks, selectedBankId);
        let selectedCircleId = $('#bankCircle :selected').val();
        $('#bankZone').empty();
        $('#bankZone').append($('<option>', { value: 0, text: 'Select Zone' }));
        if (selectedBank != null) {
            if (selectedBank.BankCircles != null) {
                let zones = getZones(selectedBank.BankCircles, selectedCircleId);
                if (zones != null) {
                    for (let i = 0; i < zones.length; i++) {
                        $('#bankZone').append($('<option>', { value: zones[i].ZoneId, text: zones[i].ZoneName }));
                    }
                }
            }
        }
    });
    function getZones(circleList, circleId) {
        let zones = new Array();
        for (let i = 0; i < circleList.length; i++) {
            if (circleList[i].CircleId == circleId) {
                if (circleList[i].CircleZones != null) {
                    zones = circleList[i].CircleZones;
                    break;
                }
            }
        }
        return zones;
    }
    //Given a bank's region, list all of it's branches under that region
    $('#bankRegion').on('change', function () {
        $('#bankBranch').empty();
        $('#bankBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        let selectedBankId = $('#allBanks :selected').val();
        let selectedCircleId = $('#bankCircle :selected').val();
        let selectedZoneId = $('#bankZone :selected').val();
        let selectedRegionId = $('#bankRegion :selected').val();
        let bank = getBank(allBanks, selectedBankId);
        if (bank != null) {
            if (bank.BankCircles != null) {
                let zones = getZones(bank.BankCircles, selectedCircleId);
                if (zones != null) {
                    let regions = getRegions(zones, selectedZoneId);
                    if (regions != null) {
                        let branches = getBranches(regions, selectedRegionId);
                        if (branches != null) {
                            for (let i = 0; i < branches.length; i++) {
                                $('#bankBranch').append($('<option>', { value: branches[i].BranchId, text: branches[i].BranchName }));
                            }
                        }
                    }
                }
            }
        }
    });
    function getRegions(zoneList, zoneId) {
        let regions = new Array();
        for (let i = 0; i < zoneList.length; i++) {
            if (zoneList[i].ZoneId == zoneId) {
                if (zoneList[i].ZoneRegions != null) {
                    regions = zoneList[i].ZoneRegions;
                    break;
                }
            }
        }
        return regions;
    }
    //Given a bank's zone, list all of it's regions
    $('#bankZone').on('change', function () {
        let selectedZoneId = $('#bankZone :selected').val();
        let selectedBankId = $('#allBanks :selected').val();
        let selectedCircleId = $('#bankCircle :selected').val();
        let bank = getBank(allBanks, selectedBankId);
        $('#bankRegion').empty();
        $('#bankRegion').append($('<option>', { value: 0, text: 'Select Region' }));
        if (bank != null) {
            if (bank.BankCircles != null) {
                let zones = getZones(bank.BankCircles, selectedCircleId);
                if (zones != null) {
                    let regions = getRegions(zones, selectedZoneId);
                    if (regions != null) {
                        for (let i = 0; i < regions.length; i++) {
                            $('#bankRegion').append($('<option>', { value: regions[i].RegionId, text: regions[i].RegionName }));
                        }
                    }
                }
            }
        }
    });
    function getBranches(regionList, regionId) {
        let branches = new Array();
        for (let i = 0; i < regionList.length; i++) {
            if (regionId == regionList[i].RegionId) {
                if (regionList[i].RegionBranches != null) {
                    branches = regionList[i].RegionBranches;
                    break;
                }
            }
        }
        return branches;
    }
    $('#btnListCorporateBCs').on('click', function () {
        let date = $('#txtCreateDate').val();
        let corporateId = $('#selCorporate :selected').val();
        let filter = "&createFromDate=" + date + "&corporateId=" + corporateId;
        filter = getCommonFilter(filter);
    });
});