module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            web_angular: {
                options: {
                    stripBanners: true,
                    banner: "'use strict';\n",
                    process: function(src, filepath) {
                        return '// Source: ' + filepath + '\n' +
                            src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
                    }
                },
                src: [
                     'Angularjs/addBankController.js',
                     'Angularjs/homeController.js',
                     'controller/*.js',
                     'js/stateController.js',
                     'Service/authenticationService.js'
                     ],
                dest: 'build/build_production.js'
            },
            web_lib: {
                src: [
                    'js/jquery-1.12.2.min.js',
                     'js/bootstrap.min.js',
                    'js/metro.min.js',
                    'js/parsley.min.js',
                    'Scripts/*.js',
                    'Scripts/jquery/*.js'
                    
                ],
                dest: 'build/build_lib.js'
            },
            web_css: {
                src: [
                    'ot/css/*.css',
                    'ot/css/animate-css/*.css',
                    'ot/css/prism/prism.css',
                    'css/lib/bootstrap-switch/bootstrap-switch.min.css',
                    'css/green.css',
                    'node_modules/ng-tags-input/build/ng-tags-input.min.css',
                    'node_modules/ng-tags-input/build/ng-tags-input.bootstrap.min.css',
                    'node_modules/bootstrap-horizon/bootstrap-horizon.css',
                    'css/lib/summernote/summernote.css',
                    'css/custom_web.css'
                    // '/node_modules/angularjs-slider/dist/rzslider.css'
                ],
                dest: 'build/web_build.css'
            }
        },
        cssmin: {
            web: {
                files: {
                    'css/web_build.min.css': ['css/web_build.css'],
                }
            }
        },
        uglify: {
            web: {
                options: {
                    sourceMap: true,
                    // sourceMapName: 'path/to/sourcemap.map'
                },
                files: {
                    'js/angular_app/build/build_lib.min.js': ['js/angular_app/build/build_lib.js']
                }
            }
        },
        // Advanced config. Run specific tasks when specific files are added, changed or deleted.
        watch: {
            options: {
                dateFormat: function(time) {
                    grunt.log.writeln('The watch finished in ' + time + 'ms at' + (new Date()).toString());
                    grunt.log.writeln('Waiting for more changes...');
                },
            },
            web_angular_production: {
                files: ['js/angular_app/**/*.js', '!js/angular_app/build/*.js'],
                tasks: ['concat:web_angular']
            },
            webcss: {
                files: ["css/*.css", "!css/web_build*.css"],
                tasks: ['concat:web_css', 'concat:web_css_external_listening', 'cssmin:web']
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    // grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s
    grunt.registerTask('web', ['concat:web_angular', 'concat:web_lib', 'concat:web_css', 'uglify:web', 'cssmin:web']);

};
