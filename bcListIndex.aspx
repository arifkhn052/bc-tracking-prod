﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="bcListIndex.aspx.cs" Inherits="BCTrackingWeb.bcListIndex" %>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/metro.min.css">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.min.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">

      
       
        <br /><br /><br /><br /><br />

    <div class="container page-content">
        <div>
            <ul class="breadcrumbs mini">
                <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                <li><a href="bcListIndex.aspx"><span>Business Correspondent List</span></a></li>
            </ul>
        </div>

    

        <div class="tile-area no-padding">
            <div class="tile-container">
                <a href="BCListAll.aspx" id="BCListAll">
                    <div class="tile bg-orange fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon mif-cloud-upload"></span>
                            <span class="tile-label align-center">All</span>
                        </div>
                    </div>
                </a>
                <a href="bclistBankWise.aspx" id="bclistBankWise">
                    <div class="tile bg-blue fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-list-alt"></span>
                            <span class="tile-label align-center">By Branches</span>
                        </div>
                    </div>
                </a>
                <a href="bclistLocationWise.aspx" id="bclistLocationWise">
                    <div class="tile bg-red fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-list-alt"></span>
                            <span class="tile-label align-center">By BC Address</span>
                        </div>
                    </div>
                </a>
                <a href="bclistUnallocated.aspx">
                    <div class="tile bg-green fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-list-alt"></span>
                            <span class="tile-label align-center">Unallocated</span>
                        </div>
                    </div>
                </a>
                <a href="bclistStatus.aspx" id="bclistStatus">
                    <div class="tile bg-orange fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon mif-cloud-upload"></span>
                            <span class="tile-label align-center">By Status</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <br>

    </div>
    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>

