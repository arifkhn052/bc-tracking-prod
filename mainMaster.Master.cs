﻿using SBI.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCTrackingWeb
{
    public partial class mainMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Util.IsUserLoggedIn())
            //    Response.Redirect("Login.aspx?ReturnUrl=" + Page.AppRelativeVirtualPath.Replace("~/", ""));

        }
        protected void btnLogout_Click(object sender, EventArgs e)
        {
           
            Session["UserInfo"] = null;
            Response.Redirect("../Login.aspx");
        }
    }
}