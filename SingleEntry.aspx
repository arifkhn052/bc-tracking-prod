﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true"
    CodeBehind="SingleEntry.aspx.cs" Inherits="BCTrackingWeb.SingleEntry" EnableEventValidation="false" %>



    <div>
        <ul class="breadcrumbs mini">
            <li><a href="Home.aspx"><span class="icon mif-home"></span></a></li>
            <li><a href="AddBCIndex.aspx"><span>Add Business Correspondents</span></a></li>
            <li><a href="SingleEntry.aspx"><span>Single Entry</span></a></li>
        </ul>
    </div>


    <div class="bs-callout bs-callout-warning hidden">
        <h4>Oh snap!</h4>
        <p>This form seems to be invalid :(</p>
    </div>

    <div class="bs-callout bs-callout-info hidden">
        <h4>Yay!</h4>
        <p>Everything seems to be ok :)</p>
    </div>



    <div class="example">
        <form id="frmMain" action="#" class="form-horizontal"  data-parsley-validate novalidate>
            <br>
            
            <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
                <h4>Personal Details</h4>
            </div>
            <hr>
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="formmargin">
                        <label for="txtName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtName" required placeholder="Name" parsley-trigger="change">
                        </div>
                       <%-- <div class="alert alert-danger col-sm-3 alertheight">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Name is compulsory
                        </div>--%>
                    </div>

                    <div class="formmargin">
                        <label for="fileImage" class="col-sm-2 control-label">Upload Image</label>
                        <div class="col-sm-10">
                            <input  type="file" id="fileImage" runat="server" class="uploaderCss" accept= '.png,.jpg,.gif'>
                           
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtGender" class="col-sm-2 control-label">Gender</label>
                        <div class="col-sm-10">
                            <select runat="server" class="form-control formcontrolheight" required id="selGender">
                                <option value="">Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Transgender">Transgender</option>
                            </select>
                        </div>

                    </div>

                    <div class="formmargin">
                        <label for="txtDOB" class="col-sm-2 control-label">Date Of Birth</label>
                        <div class="col-sm-10">
                            <input runat="server" type="date" class="form-control formcontrolheight" id="txtDOB" required placeholder="Date of Birth">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtFather" class="col-sm-2 control-label">Father Name</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtFather" placeholder="Name">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtSpouse" class="col-sm-2 control-label">Spouse Name</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtSpouse" placeholder="Name">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="selCategory" class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-10">
                            <select runat="server" class="form-control formcontrolheight" id="selCategory">
                                <option value="General">General</option>
                                <option value="OBC">OBC</option>
                                <option value="SC">SC</option>
                                <option value="ST">ST</option>
                            </select>
                        </div>

                    </div>

                    <div class="formmargin">
                        <label for="txtGender" class="col-sm-2 control-label">Physically Handicapped</label>
                        <div class="col-sm-10">
                            <input type="radio" id="radHandicapNo" name="Handicap" value="No" checked />&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;&nbsp;
                          
                    <input type="radio" id="radHandicapYes" name="Handicap" value="Yes" />&nbsp;&nbsp;Yes
		           
                        </div>

                    </div>
                    <br />

                    <div class="formmargin">
                        <label for="txtPhone1" class="col-sm-2 control-label">Phone Number</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtPhone1" placeholder="Phone Number">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtPhone1" class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtPhone2" placeholder="Phone Number 2">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtPhone1" class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtPhone3" placeholder="Phone Number 3">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtEmail" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input runat="server" type="email" class="form-control formcontrolheight" id="txtEmail" placeholder="Email">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtAadharCard" class="col-sm-2 control-label">Aadhar Card</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtAadharCard" placeholder="Aadhar Card Number">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtPanCard" class="col-sm-2 control-label">Pan Card</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtPanCard" placeholder="Pan Card Number">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtVoterId" class="col-sm-2 control-label">Voter Id Card</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtVoterId" placeholder="Voter Id Card">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtDriverLic" class="col-sm-2 control-label">Drivers License</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtDriverLic" placeholder="Drivers License">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtNregaCard" class="col-sm-2 control-label">NREGA Card</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtNregaCard" placeholder="NREGA Card">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtRationCard" class="col-sm-2 control-label">Ration Card</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtRationCard" placeholder="Ration Card">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="selAddressState" class="col-sm-2 control-label">Address State</label>
                        <div class="col-sm-10">
                            <select runat="server" class="form-control formcontrolheight" id="selAddressState">
                            </select>
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="selAddressDistrict" class="col-sm-2 control-label">Address District</label>
                        <div class="col-sm-10">
                            <select class="form-control formcontrolheight" id="selAddressDistrict">
                            </select>
                        </div>
                    </div>


                    <div class="formmargin">
                        <label for="txtAddressSubDistrict" class="col-sm-2 control-label">Address Sub District</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtAddressSubDistrict" placeholder="Address Sub District">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="selAddressCity" class="col-sm-2 control-label">Address City</label>
                        <div class="col-sm-10">
                            <select class="form-control formcontrolheight" id="selAddressCity">
                            </select>
                        </div>
                    </div>


                    <br />
                    <div class="formmargin">
                        <label for="txtAddressArea" class="col-sm-2 control-label">Address Area</label>
                        <div class="col-sm-10">
                            <textarea runat="server" class="form-control" id="txtAddressArea" placeholder="Address Area" />
                        </div>
                    </div>
                    <br />
                    <div class="formmargin">
                        <label for="txtPinCode" class="col-sm-2 control-label">Pin Code</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtPinCode" placeholder="Pin Code">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="SelAlternateOccupation" class="col-sm-2 control-label">Alternate Occupation Type</label>
                        <div class="col-sm-10">
                            <select class="form-control formcontrolheight" id="SelAlternateOccupation" runat="server">
                                <option value="Government">Government</option>
                                <option value="Public Sector">Public Sector</option>
                                <option value="Private">Private</option>
                                <option value="Self Employed">Self Employed</option>
                                <option value="Any Other">Any Other</option>


                            </select>
                        </div>

                    </div>
                    <br />
                    <div class="formmargin">
                        <label for="txtAlternateOccupationDtl" class="col-sm-2 control-label">Alternate Occupation Detail</label>
                        <div class="col-sm-10">
                            <textarea runat="server" class="form-control" id="txtAlternateOccupationDtl" placeholder="Alternate Occupation Detail" />
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtUniqueId" class="col-sm-2 control-label">Unique Id Number</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtUniqueId" placeholder="Unique Identification Number">
                        </div>
                    </div>
                    <br />
                    <div class="formmargin">
                        <label for="txtBankReferenceNumber" class="col-sm-2 control-label">Bank Reference Number</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtBankReferenceNumber" placeholder="Bank Reference Number">
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
                <h4>BC Certification</h4>
            </div>
            <hr>

            <div class="panel panel-default">
                <div class="panel-body">
                   <%-- <form action="#" data-parsley-validate novalidate>--%>
                    <div class="formmargin">
                        <label for="txtPassingDate" class="col-sm-2 control-label">Date of Passing</label>
                        <div class="col-sm-10">
                            <input runat="server" type="date" class="form-control formcontrolheight" id="txtPassingDate" placeholder="Date of Passing">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="SelInstitute" class="col-sm-2 control-label">Institute Name</label>
                        <div class="col-sm-10">
                            <select runat="server" class="form-control formcontrolheight" id="SelInstitute" datatextfield="InstituteName" datavaluefield="InstituteId">
                            </select>
                        </div>

                    </div>

                    <div class="formmargin">
                        <label for="SelCourse" class="col-sm-2 control-label">Course</label>
                        <div class="col-sm-10">
                            <select runat="server" class="form-control formcontrolheight" id="SelCourse" datatextfield="CourseName" datavaluefield="CourseId">
                            </select>
                        </div>

                    </div>

                    <div class="formmargin">
                        <label for="txtGrades" class="col-sm-2 control-label">Grades</label>
                        <div class="col-sm-10">
                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtGrades" placeholder="Grades">
                        </div>
                    </div>
                    
                    <div class="formmargin">
                        <div class="col-sm-3 col-sm-offset-9">
                            <input type="button" class="pull-right btn btn-danger" id="txtAddMoreCerts" value="Add More" data-toggle="modal" data-target=".bs-example-modal-sm-txtAddMoreCerts">
                             <div class="modal fade bs-example-modal-sm-txtAddMoreCerts" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title" id="mySmallModalLabel">Message</h4>
                                                </div>
                                                <div class="modal-body">
                                                  Certification details added!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <%--</form>--%>
                </div>
            </div>

            <div class="formmargin">
                <table id="tblCertificates" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Certification Date</th>
                            <th>Institute</th>
                            <th>Course</th>
                            <th>Grades</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>



            <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
                <h4>Educational Qualifications</h4>
            </div>
            <hr>

            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="formmargin">
                        <label for="selQualification" class="col-sm-2 control-label">Qualification</label>
                        <div class="col-sm-10">
                            <select runat="server" class="form-control formcontrolheight" id="selQualification">
                                <option value="SSC">SSC</option>
                                <option value="HSC">HSC</option>
                                <option value="Graduate">Graduate</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                    </div>
                    <br />
                    <div class="formmargin">
                        <label for="txtOtherQualification" class="col-sm-2 control-label">If Other Qualification</label>
                        <div class="col-sm-10">
                            <textarea runat="server" class="form-control" id="txtOtherQualification" placeholder="Other Qualification" />
                        </div>
                    </div>

                </div>
            </div>



            <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
                <h4>Previous Experience as BC (If any)</h4>
            </div>
            <hr>

            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="formmargin">
                        <label for="selPreviousExpBank" class="col-sm-2 control-label">Bank Name</label>
                        <div class="col-sm-10">
                            <select runat="server" class="form-control formcontrolheight" id="selPreviousExpBank" datatextfield="BankName" datavaluefield="BankId">
                            </select>
                        </div>

                    </div>


                    <div class="formmargin">
                        <label for="selPreviousExpBranch" class="col-sm-2 control-label">Branch</label>
                        <div class="col-sm-10">
                            <select runat="server" class="form-control formcontrolheight" id="selPreviousExpBranch" datatextfield="BranchName" datavaluefield="BranchId" >
                            </select>
                        </div>

                    </div>

                    <div class="formmargin">
                        <label for="txtFromDateExp" class="col-sm-2 control-label">From Date</label>
                        <div class="col-sm-10">
                            <input runat="server" type="date" class="form-control formcontrolheight" id="txtFromDateExp" placeholder="Experience From Date">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtToDateExp" class="col-sm-2 control-label">To Date</label>
                        <div class="col-sm-10">
                            <input runat="server" type="date" class="form-control formcontrolheight" id="txtToDateExp" placeholder="Experience To Date">
                        </div>
                    </div>

                    <div class="formmargin">
                        <label for="txtReasons" class="col-sm-2 control-label">Reasons</label>
                        <div class="col-sm-10">
                            <textarea runat="server" class="form-control" id="txtReasons" placeholder="Reasons" />
                        </div>
                    </div>



                    <div class="formmargin">
                        <div class="col-sm-3 col-sm-offset-9">
                            <input type="button" class="pull-right btn btn-danger" id="btnAddMoreExperience" value="Add More" data-toggle="modal" data-target=".bs-example-modal-sm-btnAddMoreExperience">
                            
                             <div class="modal fade bs-example-modal-sm-btnAddMoreExperience" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title" id="mySmallModalLabel">Message</h4>
                                                </div>
                                                <div class="modal-body">
                                                  Previous experience details added!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="formmargin">
                <table id="tblPreviousExperience" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Bank</th>
                            <th>Branch</th>
                            <th>From Date</th>
                            <th>To Date</th>
                            <th>Reasons</th>
                        </tr>
                    </thead>
                </table>
            </div>




            <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
                <h4>Corporate Association</h4>
            </div>
            <hr>

            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="formmargin">
                        <label for="selCorporate" class="col-sm-2 control-label">Corporate List</label>
                        <div class="col-sm-10">                           
                              <select runat="server" class="form-control formcontrolheight" id="selCorporate" datatextfield="CorporateName" datavaluefield="CorporateId">
                            </select>
                        </div>

                    </div>
                </div>
            </div>



            <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
                <h4>Business Correspondent Allocation</h4>
            </div>
            <hr>
            <div class="formmargin">
                <label for="txtGender" class="col-sm-2 control-label">Allocated</label>
                <div class="col-sm-10">
                    <input type="radio" id="radAllocatedNo" name="Allocated" value="No"  checked />&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;&nbsp;
                           
                    <input type="radio" id="radAllocatedYes" name="Allocated" value="Yes" />&nbsp;&nbsp;Yes
		           
                </div>

            </div>


            <div id="divAllocationDetails">

                <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
                    <h4>Associated Bank Detail</h4>
                </div>
                <hr>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="formmargin">
                            <label for="txtAppointmentDate" class="col-sm-2 control-label">Appointment Date</label>
                            <div class="col-sm-10">
                                <input runat="server" type="date" class="form-control formcontrolheight" id="txtAppointmentDate" placeholder="Appointment Date">
                            </div>
                        </div>


                        <div class="formmargin">
                            <label for="inputIFSCCode" class="col-sm-2 control-label">IFSC Code</label>
                            <div class="col-sm-10">
                                <input runat="server" type="text" class="form-control formcontrolheight" id="ifscCode" placeholder="IFSC Code">
                            </div>
                        </div>

                        <div class="formmargin">
                            <label class="col-sm-2 col-sm-offset5 control-label">-- or --</label>
                        </div>

                        <div class="formmargin">
                            <label for="selAllocationBank" class="col-sm-2 control-label">Bank Name</label>
                            <div class="col-sm-10">
                                <select runat="server" class="form-control formcontrolheight" id="selAllocationBank" datatextfield="BankName" datavaluefield="BankId">
                                </select>
                            </div>
                        </div>

                        <div class="formmargin">
                            <label for="selAllocationBranch" class="col-sm-2 control-label">Branch</label>
                            <div class="col-sm-10">
                                <select runat="server" class="form-control formcontrolheight" id="selAllocationBranch" datatextfield="BranchName" datavaluefield="BranchId">
                                </select>
                            </div>
                        </div>

                    </div>
                </div>



                <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
                    <h4>BC Type and Workings</h4>
                </div>
                <hr>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="formmargin">
                            <label for="selBCType" class="col-sm-2 control-label">Type of BC</label>
                            <div class="col-sm-10">
                                <select runat="server" class="form-control formcontrolheight" id="selBCType">
                                    <option value="Individual">Individual</option>
                                    <option value="Corporate">Corporate</option>
                                    <option value="Fixed">Fixed</option>
                                    <option value="Mobile">Mobile</option>
                                    <option value="Both">Both</option>

                                </select>
                            </div>
                        </div>


                        <div class="formmargin">
                            <label for="txtWorkingDays" class="col-sm-2 control-label">Working Days</label>
                            <div class="col-sm-10">
                                <input runat="server" type="text" class="form-control formcontrolheight" id="txtWorkingDays" placeholder="Working Days">
                            </div>
                        </div>

                        <div class="formmargin">
                            <label for="txtWorkingHours" class="col-sm-2 control-label">Working Hours</label>
                            <div class="col-sm-10">
                                <input runat="server" type="text" class="form-control formcontrolheight" id="txtWorkingHours" placeholder="Working Hours">
                            </div>
                       </div>

                    </div>
                </div>


                <div runat="server" class="bs-callout bs-callout-info" id="divPrimaryLocation">
                    <h4>Primary Location (For Fixed BC)</h4>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="formmargin">
                            <label for="postalAddress" class="col-sm-2 control-label">Postal Address</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control formcontrolheight" id="txtPostalAddress" runat="server" placeholder="Postal Address">
                            </div>
                        </div>
                        <div class="formmargin">
                            <label for="villageCode1" class="col-sm-2 control-label">Village Code</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control formcontrolheight" id="txtVillageCode1" runat="server" placeholder="Enter Village Code">
                            </div>
                        </div>
                        <div class="formmargin">
                            <label for="villageDetail1" class="col-sm-2 control-label">Village Detail</label>
                            <div class="col-sm-10">
                                <textarea name="villageDetail" id="txtVillageDetail1" runat="server" class="form-control" rows="3" placeholder="Village Detail"></textarea>
                            </div>
                        </div>
                        <div class="formmargin">
                            <label for="selPLState" class="col-sm-2 control-label">State</label>
                            <div class="col-sm-10">
                                <select class="form-control formcontrolheight" id="selPLState" runat="server">
                                </select>
                            </div>
                        </div>




                        <div class="formmargin">
                            <label for="selPLTaluk" class="col-sm-2 control-label">Taluk</label>
                            <div class="col-sm-10">
                                <select class="form-control formcontrolheight" id="selPLTaluk" runat="server">
                                </select>
                            </div>
                        </div>



                        <div class="formmargin">
                            <label for="txtPLPinCode" class="col-sm-2 control-label">Pin Code</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control formcontrolheight" runat="server" id="txtPLPinCode" placeholder="Pin Code">
                            </div>
                        </div>
                    </div>
                </div>




                <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
                    <h4>Areas of Operation</h4>
                </div>
                <hr>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="formmargin">
                            <label for="txtOperationVillageCode" class="col-sm-2 control-label">Village Code</label>
                            <div class="col-sm-10">
                                <input runat="server" type="text" class="form-control formcontrolheight" id="txtOperationVillageCode" placeholder="Village Code">
                            </div>
                        </div>
                        <div class="formmargin">
                            <label for="txtOperationVillageDetail" class="col-sm-2 control-label">Village Detail</label>
                            <div class="col-sm-10">
                                <textarea runat="server" class="form-control" id="txtOperationVillageDetail" placeholder="Village Detail" />
                            </div>
                        </div>

                        <div class="formmargin">
                            <div class="col-sm-3 col-sm-offset-9">
                             <input type="button" class="pull-right btn btn-danger" id="btnAddMoreOpAreas" value="Add More" data-toggle="modal" data-target=".bs-example-modal-sm-btnAddMoreOpAreas">
                              
                             <div class="modal fade bs-example-modal-sm-btnAddMoreOpAreas" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title" id="mySmallModalLabel">Message</h4>
                                                </div>
                                                <div class="modal-body">
                                                  Operation areas details added!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="formmargin">
                    <table id="tblOperationAreas" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Village Code</th>
                                <th>Village Detail</th>

                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>



                <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
                    <h4>Device Details</h4>
                </div>
                <hr>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="formmargin">
                            <label for="txtOperationVillageCode" class="col-sm-2 control-label">Given On</label>
                            <div class="col-sm-10">
                                <input runat="server" type="date" class="form-control formcontrolheight" id="txtGivenOn" placeholder="dd-mm-yyyy">
                            </div>
                        </div>
                        <div class="formmargin">
                            <label for="txtOperationVillageCode" class="col-sm-2 control-label">Device Name</label>
                            <div class="col-sm-10">
                                <select runat="server" class="form-control formcontrolheight" id="selDeviceName">
                                    <option value="POS">POS</option>
                                    <option value="Micro ATM">Micro ATM</option>
                                    <option value="USSD Phones">USSD Phones</option>
                                    <option value="KIOSK">KIOSK</option>
                                    <option value="VSAT">VSAT</option>
                                </select>
                            </div>
                        </div>


                        <div class="formmargin">
                            <div class="col-sm-3 col-sm-offset-9">
                                <input type="button" class="pull-right btn btn-danger" id="btnAddMoreDevices" value="Add More" data-toggle="modal" data-target=".bs-example-modal-sm-btnAddMoreDevices">
                               
                              
                             <div class="modal fade bs-example-modal-sm-btnAddMoreDevices" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title" id="mySmallModalLabel">Message</h4>
                                                </div>
                                                <div class="modal-body">
                                                  Device details added!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="formmargin">
                    <table id="tblDevices" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Device Date</th>
                                <th>Device Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>





                <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
                    <h4>Connectivity Details</h4>
                </div>
                <hr>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="formmargin">
                            <label for="txtCommType" class="col-sm-2 control-label">Connectivity Type</label>
                            <div class="col-sm-10">
                                <select runat="server" class="form-control formcontrolheight" id="selConnType">
                                    <option value="">Select</option>
                                    <option value="LandLine">LandLine</option>
                                    <option value="Mobile">Mobile</option>
                                    <option value="USAT">USAT</option>

                                </select>
                            </div>
                        </div>
                        <div class="formmargin">
                            <label for="txtProvider" class="col-sm-2 control-label">Provider</label>
                            <div class="col-sm-10">
                                <input runat="server" type="text" class="form-control formcontrolheight" id="txtProvider" placeholder="Provider">
                            </div>
                        </div>
                        <div class="formmargin">
                            <label for="txtNumber" class="col-sm-2 control-label">Number</label>
                            <div class="col-sm-10">
                                <input runat="server" type="text" class="form-control formcontrolheight" id="txtNumber" placeholder="Number">
                            </div>
                        </div>

                        <div class="formmargin">
                            <div class="col-sm-3 col-sm-offset-9">
                                <input type="button" class="pull-right btn btn-danger" id="btnAddMoreConnectivity" value="Add More" data-toggle="modal" data-target=".bs-example-modal-sm-btnAddMoreConnectivity">
                                
                               
                              
                             <div class="modal fade bs-example-modal-sm-btnAddMoreConnectivity" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title" id="mySmallModalLabel">Message</h4>
                                                </div>
                                                <div class="modal-body">
                                                  Connectivity details added!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="formmargin">
                    <table id="tblConnectivity" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Provider</th>
                                <th>Number</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>





                <div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
                    <h4>Products/Services Offered</h4>
                </div>
                <hr>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="formmargin">
                            <label for="selProductsOffered" class="col-sm-3 control-label">Product Offered</label>
                            <div class="col-sm-9">
                                <select runat="server" class="form-control formcontrolheight" id="selProductsOffered">
                                </select>
                            </div>

                        </div>

                        <div class="formmargin">
                            <label for="txtMinCash" class="col-sm-3 control-label">Minimum Cash Handling Limit</label>
                            <div class="col-sm-9">
                                <input runat="server" type="text" class="form-control formcontrolheight" id="txtMinCash" placeholder="Minimum Cash Handling Limit">
                            </div>
                        </div>
                        <div class="formmargin">
                            <label for="txtMonthlyFixed" class="col-sm-3 control-label">Monthly Fixed Renumeration</label>
                            <div class="col-sm-9">
                                <input runat="server" type="text" class="form-control formcontrolheight" id="txtMonthlyFixed" placeholder="Monthly Fixed Renumeration">
                            </div>
                        </div>
                        <div class="formmargin">
                            <label for="txtMonthlyVariable" class="col-sm-3 control-label">Monthly Variable Renumeration</label>
                            <div class="col-sm-9">
                                <input runat="server" type="text" class="form-control formcontrolheight" id="txtMonthlyVariable" placeholder="Monthly Variable Renumeration">
                            </div>
                        </div>

                        <%-- <div class="formmargin">
                            <div class="col-sm-3 col-sm-offset-9">
                                <input runat="server" type="button" class="pull-right btn btn-danger" id="Button2" value="Add More">
                            </div>
                        </div>--%>
                    </div>
                </div>

                <%--  <div class="formmargin">
                    <table id="tblProducts" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Minimum Cash Limit</th>
                                <th>Monthly Fixed Remuneration</th>
                                <th>Monthly Variable Remuneration</th>
                            </tr>
                        </thead>
                    </table>
                </div>--%>
            </div>


            <div class="formmargin center-block">
                <div class="center-block">

                    <button type="submit" id="btnSubmitSingleEntry"  class="btn btn-default center-block">Submit</button>
                    <%--<div id="result"></div>--%>
                        
                    <!-- Modal -->
                            <div id="getCodeModal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title"><b>Message</b> </h4>
                                            
                                        </div>
                                        <div class="modal-body"  style="overflow-x: hidden;">
                                                Record inserted!
                                            </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                   
                </div>
            </div>
        </form>
    </div>



    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function () {
            var t = $('#example').DataTable({
                "paging": false,
                "ordering": false,
                "info": false,
                "searching": false
            });







            $("#villageCode").on('blur', function () {
                $("#villageDetail").text('village 1, district 1, state');
            }).on('focus', function () {
                $("#villageDetail").text('');
            });


            $('#addMoreButton').on('click', function () {
                t.row.add([
                    $("#villageCode").val(),
                    $("#villageDetail").text(),
                    'delete'
                ]).draw(false);
                $("#villageCode").val("");
                $("#villageDetail").text("");
            });

            $("#ifscCode").on('blur', function () {
                $("#bankName").val("1");
                $("#bankCircle").val("1");
                $("#bankState").val("2");
                $("#bankZone").val("3");
                $("#bankRegion").val("4");
                $("#bankCategory").val("5");
                $("#bankBranch").val("5");
            }).on('focus', function () {
                $("#bankName").val("0");
                $("#bankCircle").val("0");
                $("#bankState").val("0");
                $("#bankZone").val("0");
                $("#bankRegion").val("0");
                $("#bankCategory").val("0");
                $("#bankBranch").val("0");
            });
        });
    </script>




    <!--<script src="js/report.js" type="text/javascript"></script>-->
    <script src="js/bc.js" type="text/javascript"></script>
    <script src="js/main.js" type="text/javascript"></script>



