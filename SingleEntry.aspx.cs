﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCEntities;
using BCTrackingServices;
using BCTrackingBL;
using SBI.Web.Helpers;

namespace BCTrackingWeb
{
    public partial class SingleEntry : System.Web.UI.Page
    {


        public static ListItem GetDefaultItem()
        {
            ListItem item = new ListItem(Constants.DEFAULTCHOICE, "-1");
            return item;
        }

        protected void fileImage_Change(object sender, EventArgs e)
        {
            int s = 1;

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Util.IsUserLoggedIn())
                Response.Redirect("Login.aspx?ReturnUrl=" + Page.AppRelativeVirtualPath.Replace("~/", ""));

            BankCorrespondentBL bl = new BankCorrespondentBL();
            BankBL bankBL = new BankBL();
            //List<Bank> allBanks = bankBL.GetBanks(null);

            //if (allBanks != null)
            //{
            //    List<Branch> allBranches = allBanks.SelectMany(b => b.Branches).ToList();

            //    selPreviousExpBank.DataSource = allBanks;
            //    selPreviousExpBank.DataBind();
            //    selPreviousExpBank.Items.Insert(0, GetDefaultItem());

            //    selAllocationBank.DataSource = allBanks;
            //    selAllocationBank.DataBind();
            //    selAllocationBank.Items.Insert(0, GetDefaultItem());


            //    selPreviousExpBranch.DataSource = allBranches;
            //    selPreviousExpBranch.DataBind();
            //    selPreviousExpBranch.Items.Insert(0, GetDefaultItem());

            //    selAllocationBranch.DataSource = allBranches;
            //    selAllocationBranch.DataBind();
            //    selAllocationBranch.Items.Insert(0, GetDefaultItem());
            //}

            List<Institutes> allInstitutes = bl.GetInstitutes(null);
            if (allInstitutes != null)
            {
                List<Courses> allCourses = allInstitutes.SelectMany(i => i.Courses).ToList();

                SelInstitute.DataSource = allInstitutes;
                SelInstitute.DataBind();
                SelInstitute.Items.Insert(0, GetDefaultItem());

                SelCourse.DataSource = allCourses;
                SelCourse.DataBind();
                SelCourse.Items.Insert(0, GetDefaultItem());
            }

            List<Corporates> allCorporates = bl.GetCorporates(null);

            selCorporate.DataSource = allCorporates;
            selCorporate.DataBind();
            selCorporate.Items.Insert(0, GetDefaultItem());



        }

        private void btnSumbitSingleEntry_Click(object sender, EventArgs e)
        {

            BankCorrespondence bcorrespondene = null;
            if (Session["CurrentBC"] != null)
            {
                bcorrespondene = Session["CurrentBC"] as BankCorrespondence;


            }
            else
            {
                bcorrespondene = new BankCorrespondence();
                bcorrespondene.BankCorrespondId = 0;


            }

        }

    }
}
