﻿using BCEntities;
using BCTrackingBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCTrackingWeb
{
    public partial class searchVillage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string getVillages()
        {
            // Paging parameters:
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            //   var userid = int.Parse(HttpContext.Current.Request.Params["userId"]);
            // Sorting parameters
            var iSortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var iSortDir = HttpContext.Current.Request.Params["sSortDir_0"];
            // Search parameters
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToLower();
            int totalRecords = 0;

            var StateId = HttpContext.Current.Request.Params["StateId"].ToLower();
            var DistrictId = HttpContext.Current.Request.Params["DistrictId"].ToLower();
            var SubDistrictId = HttpContext.Current.Request.Params["SubDistrictId"].ToLower();
         
         //   if (System.Web.HttpContext.Current.Session["UserInfo"] != null) ;


            var leadList = Common.searchState(iDisplayStart, iDisplayLength, iSortCol, iSortDir, sSearch, out totalRecords, StateId, DistrictId, SubDistrictId);

            Func<BankCorrespondence, object> order = p =>
            {
                if (iSortCol == 0)
                {
                    return p.VillageCode;
                }

                else if (iSortCol == 1)
                {
                    return p.VillageName;
                }
                else if (iSortCol == 2)
                {
                    return p.Subdistrict;
                }
                else if (iSortCol == 3)
                {
                    return p.District;
                }
                else if (iSortCol == 4)
                {
                    return p.State;
                }

                return p.VillageCode;
            };

            //    // Define the order direction based on the iSortDir parameter
            if ("desc" == iSortDir)
            {
                leadList = leadList.OrderByDescending(order).ToList<BankCorrespondence>();
            }
            else
            {
                leadList = leadList.OrderBy(order).ToList<BankCorrespondence>();
            }
            var finalResult = leadList
                    .Select(p => new[] { p.VillageCode.ToString(), p.VillageName,p.Subdistrict,p.District,p.State});

            int count = finalResult.Count();

            // prepare an anonymous object for JSON serialization
            var result = new
            {
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = finalResult
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            return json;
        }
    }
}