﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="editState.aspx.cs" Inherits="BCTrackingWeb.editState" %>

      <div ng-init="getStateDetails()">
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-12">
                    <h2 class="title">Edit State </h2>

                </div>


                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                           <li><a href="#"  ui-sref="home"><i class="fa fa-home"></i> Home</a></li>
                        <li><a ui-sref="statelist"> State List</a></li>
                          <li><a href="#">Edit State Info</a></li>
                      

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    State Details
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                                <div class="col-md-12">
                                                    <div class="panel">                                                       
                                                                                                                    
                                                        <div class="panel-body p-20">
                                                            <div class="panel-body" ng-hide="divVIEW">
                                                         
                                                                <div class="row">
                                                                    <div class="col-sm-2">
                                                                        <label for="fileImage" class=" control-label">State Name</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                            <label for="fileImage"   id="lblName" placeholder="Name" class=" control-label">{{lblStateName}}</label>
                                                                    
                                                                    </div>
                                                                  
                                                                </div>    

                                                            </div>
                                                            <div class="panel-body" ng-hide="divEdit">
                                                         
                                                                <div class="row">
                                                                    <div class="col-sm-2">
                                                                        <label for="fileImage" class=" control-label">State Name</label>
                                                                    </div>
                                                                    <div class="col-sm-10 form-group">
                                                                      
                                                               <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtStateName"
                                                                                   ng-model="txtStateName"
                                                                                   placeholder="State Name"
                                                                                   parsley-trigger="change"  required />
                                                                    </div>
                                                               
                                                                </div>



                                                                

                                                          

                                                            </div>
                                    <div  class="row pull-right">                                     
                                    <div class="col-sm-6">
                                        <a class="btn btn-danger btn-sm" ng-click="deleteState()">
                                            <span class="fa fa-trash-o" aria-hidden="true"></span>&nbsp;Delete</a>
                                    </div>


                                    <div class="col-sm-2">
                                        <a ng-click="editView()" class="btn btn-primary btn-sm" id="tabedit">
                                            <span class="fa fa-pencil-square-o" aria-hidden="true"></span>&nbsp;{{btnedit}} </a>
                                    </div>
                                </div>
                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                      
                                                        
                                            
                                                    </div>
                                                   
                                                    <!-- /.panel -->
                                                </div>
                                            </div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class=""><a  ng-click="displayTab('home')" aria-controls="home"
                                                                              role="tab" data-toggle="tab"><label
                                            for="exampleInputEmail1">Districts Details </label> </a></li>
                                    <li role="presentation"><a ng-click="displayTab('circle')" aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">City
                                        Details
                                    </label> </a></li>
                                    <li role="presentation"><a ng-click="displayTab('profile')" aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Taluka
                                        Details
                                    </label> </a></li>
                                    <li role="presentation"><a ng-click="displayTab('setting')" aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Village
                                        Details
                                    </label> </a></li>


                                </ul>

                                <!-- Tab panes -->
                               
                                    <div role="tabpanel" class="tab-pane" ng-hide="edithome">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">
                                                                
                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-title">
                                                                            Districts
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body p-20">

                                                                        <div class="panel-body">
                                                                            <div class="row">


                                                                                <div class="col-sm-12">
                                                                                    <strong>District Namee</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtDistrictName"
                                                                                           ng-model="txtDistrictName"
                                                                                           placeholder="District Name"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>


                                                                            </div>
                                                                            <div class="row pull-right" id="Div2">
                                                                                <div class="col-sm-12">
                                                                                    <input type="button"
                                                                                           class="btn btn-primary  btn-sm form-control"
                                                                                           ng-click="addDistrict()"
                                                                                           ng-disabled="!txtDistrictName"
                                                                                           value="Add More  "
                                                                                           id="Button2">
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            <br/>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table id="" class="table ">
                                                                                        <tr
                                                                                        ">
                                                                                        <td><strong>District
                                                                                            Name</strong>
                                                                                        </td>
                                                                                        <td><strong>Remove</strong></td>
                                                                                        </tr>
                                                                                        <tr ng-repeat="item in districtsList ">
                                                                                            <td>{{item.DistrictName}}</td>

                                                                                            <td>
                                                                                                <div ng-click="removeDistrict($index)"
                                                                                                     class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <!-- /.col-md-12 -->
                                                                    </div>
                                                                </div>

                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" ng-hide="divcircle">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">

                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-title">
                                                                            Cities
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body p-20">

                                                                        <div class="panel-body">
                                                                            <div class="row">


                                                                                <div class="col-sm-6">
                                                                                    <strong>City Name</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtCityName"
                                                                                           ng-model="txtCityName"
                                                                                           placeholder="City Name"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <strong>District</strong>
                                                                                    <select type="text"
                                                                                            class="form-control formcontrolheight"
                                                                                            id="selectDistrictNameForCity"></select>
                                                                                </div>


                                                                            </div>
                                                                            <div class="row pull-right" id="Div2">
                                                                                <div class="col-sm-12">
                                                                                    <input type="button"
                                                                                           class="btn btn-primary  btn-sm form-control"
                                                                                           ng-click="addCities()"
                                                                                            ng-disabled="!txtCityName"
                                                                                           value="Add More  "
                                                                                           id="Button2">
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            <br/>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table id="" class="table ">
                                                                                        <tr>
                                                                                            <td><strong>City
                                                                                                Name</strong>
                                                                                            <td><strong>District
                                                                                                Name</strong>
                                                                                            </td>
                                                                                            <td><strong>Remove</strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr ng-repeat="item in citiesList ">
                                                                                            <td>{{item.CityName}}</td>
                                                                                              <td>{{item.DistrictName}}</td>
                                                                                           
                                                                                            <td></td>
                                                                                            <td>
                                                                                                <div ng-click="removeCities($index)"
                                                                                                     class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <!-- /.col-md-12 -->
                                                                    </div>
                                                                </div>

                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" ng-hide="divprofile">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">

                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-title">
                                                                            Talukas
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body p-20">

                                                                        <div class="panel-body">
                                                                            <div class="row">


                                                                                <div class="col-sm-6">
                                                                                    <strong>Taluka Name</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtTalukaName"
                                                                                           ng-model="txtTalukaName"
                                                                                           placeholder="Taluka Name"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <strong>District</strong>
                                                                                    <select type="text"
                                                                                            class="form-control formcontrolheight"
                                                                                            id="selectDistrictNameForTaluka"></select>
                                                                                </div>


                                                                            </div>
                                                                            <div class="row pull-right" id="Div2">
                                                                                <div class="col-sm-12">
                                                                                    <input type="button"
                                                                                           class="btn btn-primary  btn-sm form-control"
                                                                                           ng-click="addTalukas()"
                                                                                           ng-disabled="!txtTalukaName "
                                                                                           value="Add More  "
                                                                                           id="Button2">
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            <br/>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table id="" class="table ">
                                                                                        <tr>
                                                                                            <td><strong>Taluka
                                                                                                Name</strong>
                                                                                            <td><strong>District
                                                                                                Name</strong>
                                                                                            </td>
                                                                                            <td><strong>Remove</strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr ng-repeat="item in talukaslist">
                                                                                            <td>{{item.TalukaName}}</td>
                                                                                            <td>{{item.DistrictName}}</td>
                                                                                            <td>
                                                                                                <div ng-click="removeTalukas($index)"
                                                                                                     class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <!-- /.col-md-12 -->
                                                                    </div>
                                                                </div>

                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" ng-hide="divsetting">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">

                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-title">
                                                                            Villages
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body p-20">

                                                                        <div class="panel-body">
                                                                            <div class="row">


                                                                                <div class="col-sm-4">
                                                                                    <strong>Villages Name</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtVillageName"
                                                                                           ng-model="txtVillageName"
                                                                                           placeholder="Village Name"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <strong>District</strong>
                                                                                    <select type="text"
                                                                                            class="form-control formcontrolheight"
                                                                                            id="selectDistrictNameForVillage"></select>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <strong>Taluka</strong>
                                                                                    <select type="text"
                                                                                            class="form-control formcontrolheight"
                                                                                            id="selectTalukaNameForVillage"></select>
                                                                                </div>


                                                                            </div>
                                                                            <div class="row pull-right" id="Div2">
                                                                                <div class="col-sm-12">
                                                                                    <input type="button"
                                                                                           class="btn btn-primary  btn-sm form-control"
                                                                                           ng-click="addVilages()"
                                                                                           ng-disabled="!txtVillageName "
                                                                                           value="Add More  "
                                                                                           id="Button2">
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            <br/>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table id="" class="table ">
                                                                                        <tr>
                                                                                           <th>Village Name</th>
                                                                                            <th>District Name</th>
                                                                                            <th>Taluka Name</th>

                                                                                            <td><strong>Remove</strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr ng-repeat="item in villagesList ">
                                                                                            <td>{{item.VillageName}}</td>
                                                                                         <td>{{item.DistrictName}}</td>
                                                                                         <td>{{item.TalukaName}}</td>
                                                                                            <td>
                                                                                                <div ng-click="removeVillage($index)"
                                                                                                     class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <!-- /.col-md-12 -->
                                                                    </div>
                                                                </div>

                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>

                                           <div class="formmargin center-block">
                    <div class="center-block">
                        <button type="button" ng-click="addState()" class="btn btn-success center-block">Update</button>
                    </div>
                </div>
                                


                                <!-- /.src-code -->

                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>


    
    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/parsley.min.js" defer></script>
    <script src="js/main.js"></script>
    <script src="js/states.js"></script>

