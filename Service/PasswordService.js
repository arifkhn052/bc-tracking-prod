﻿'use strict';

bctrackApp.service('PasswordService', [
    '$http',
    '$q',
    '$cookies',
    function ($http, $q, $cookies) {
        return {
            changePassword: function (Password) {
                
                var deferred = $q.defer();
                $http.post("api/UpdatePassword.ashx", { oldPassword: Password.oldPassword, newpassword: Password.newpassword, userId: Password.userId })
               .then(function (response) {
                   console.log(response);
                   var dataReceived = response.data;
                   deferred.resolve(dataReceived);
                 });
                return deferred.promise;
            },
        }
    }
]);
