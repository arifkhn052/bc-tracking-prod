﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="changePassword.aspx.cs" Inherits="BCTrackingWeb.changePassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Change Password </h2>
                                   
                                </div>
                           
                             
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							 <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                           
                                        
            							
            						</ul>
                                </div>
                              
                                <!-- /.col-md-6 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                                <div class="row">

                                  
                                    <!-- /.col-md-6 -->

                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                  
                                                </div>
                                            </div>
                                            <div class="panel-body p-20">

                                                  <div class="panel-body">

                <div class="formmargin">
                    <label message class="col-sm-2 control-label">Old Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control formcontrolheight" id="txtOldPassword" ng-model="password.oldPassword" placeholder="Old Password" autocomplete="off"
                               parsley-trigger="change" required>
                    </div>
                </div>

                <div class="formmargin">
                    <label message class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control formcontrolheight" id="txtPassword" ng-model="password.newpassword" placeholder="Password" autocomplete="off"
                               parsley-trigger="change" required>
                    </div>
                </div>
                                                        <div class="formmargin">
                    <label message class="col-sm-2 control-label">Retype Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control formcontrolheight" id="txtrePassword" ng-model="password.txtrePassword" placeholder=" Retype Password" autocomplete="off"
                               parsley-trigger="change" required>
                    </div>
                </div>
                                <div class="col-sm-9">
                                            <div class="input-group">
                                               <label id="errormsg" style="color:red">Password Mismatch!!</label>
                                            </div>
                                         
                                        </div>
            

              

              
              

                <div class="formmargin">
                    <div class="center-block">
                        <button type="button" id="btnChangePassword" ng-click="changePassword()" class="btn btn-success">Change Password</button>
                        
                    </div>
                </div>




            </div>

                                        
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                               

                                    
                                    <!-- /.col-md-8 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </section>
    </div>
    </form>
</body>
</html>
