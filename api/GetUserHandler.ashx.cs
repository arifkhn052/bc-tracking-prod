﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for GetUserHandler
    /// </summary>
    public class GetUserHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string mode = Utils.GetStringValue(context.Request[Constants.MODE]);
            string userid = Utils.GetStringValue(context.Request[Constants.USERSESSIONID]);

            if (mode == "Current")
            {
                UserEntity user = context.Session[Constants.USERSESSIONID] as UserEntity;

                if (user != null)
                {
                    context.Response.ContentType = "application/json";
                    context.Response.ContentEncoding = System.Text.Encoding.UTF8;
                    context.Response.Write(JsonConvert.SerializeObject(user));

                }
            }
            else if (mode == "All")
            {
                UserBL bl = new UserBL();
                List<UserEntity> allUsers = bl.GetUsers(null);
                if (allUsers != null)
                {
                    context.Response.ContentType = "application/json";
                    context.Response.ContentEncoding = System.Text.Encoding.UTF8;
                    context.Response.Write(JsonConvert.SerializeObject(allUsers));

                }
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}