﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCEntities;
using Newtonsoft.Json;
using BCTrackingBL;
using BCTrackingServices;
using System.Web.SessionState;
using System.Text;

namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for InsertUpdateHandler
    /// </summary>
    public class InsertUpdateHandler : IHttpHandler, IRequiresSessionState
    {



        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string userId = context.Request.Headers.Get("userId");
                string TokenId = context.Request.Headers.Get("TokenId");
                UserBL businessLogic = new UserBL();
                int access = businessLogic.getToken(userId, TokenId);
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = Encoding.UTF8;
                if (access == 1)
                {
                    UserEntity user = businessLogic.AuthenticateUserData(userId);
                    string mode = Utils.GetStringValue(context.Request[Constants.MODE]);

                    if (user != null)
                    {

                        string bcData = context.Request["bcorr"];

                        bcData = HttpUtility.UrlDecode(bcData);
                        BankCorrespondence bc = JsonConvert.DeserializeObject<BankCorrespondence>(bcData);



                        if (mode != Constants.BLACKLISTMODE && mode != Constants.WHITELISTMODE && mode != Constants.CHANGESTATUSMODE)
                        {
                            string rtnData = context.Request["allCerts"];
                            if (!String.IsNullOrEmpty(rtnData))
                            {
                                rtnData = HttpUtility.UrlDecode(rtnData);
                                List<BCCertifications> certs = JsonConvert.DeserializeObject<List<BCCertifications>>(rtnData);
                                bc.Certifications = certs;
                            }

                            string rtnexps = context.Request["allExps"];
                            if (!String.IsNullOrEmpty(rtnexps))
                            {
                                rtnexps = HttpUtility.UrlDecode(rtnexps);//new line
                                List<PreviousExperiene> exps = JsonConvert.DeserializeObject<List<PreviousExperiene>>(rtnexps);
                                bc.PreviousExperience = exps;
                            }
                            string rtnareas = context.Request["allAreas"];
                            if (!String.IsNullOrEmpty(rtnareas))
                            {
                                rtnareas = HttpUtility.UrlDecode(rtnareas);//new line
                                List<OperationalAreas> areas = JsonConvert.DeserializeObject<List<OperationalAreas>>(rtnareas);
                                bc.OperationalAreas = areas;
                            }
                            string rtndevices = context.Request["allDevices"];
                            if (!String.IsNullOrEmpty(rtndevices))
                            {
                                rtndevices = HttpUtility.UrlDecode(rtndevices);//new line
                                List<BCDevices> devices = JsonConvert.DeserializeObject<List<BCDevices>>(rtndevices);
                                bc.Devices = devices;
                            }
                            string rtnconns = context.Request["allConns"];
                            if (!String.IsNullOrEmpty(rtnconns))
                            {
                                rtnconns = HttpUtility.UrlDecode(rtnconns);//new line
                                List<ConnectivityDetails> conns = JsonConvert.DeserializeObject<List<ConnectivityDetails>>(rtnconns);
                                bc.ConnectivityDetails = conns;
                            }
                            string rtnssas = context.Request["allSSAs"];
                            if (!String.IsNullOrEmpty(rtnssas))
                            {
                                rtnssas = HttpUtility.UrlDecode(rtnssas);//new line
                                List<SsaDetails> ssas = JsonConvert.DeserializeObject<List<SsaDetails>>(rtnssas);
                                bc.SsaDetails = ssas;
                            }
                            string rtnallCommsion = context.Request["allCommsion"];
                            if (!String.IsNullOrEmpty(rtnallCommsion))
                            {
                                rtnallCommsion = HttpUtility.UrlDecode(rtnallCommsion);//new line
                                List<CommssionList> rtnallCommsions = JsonConvert.DeserializeObject<List<CommssionList>>(rtnallCommsion);
                                bc.CommssionList = rtnallCommsions;
                            }

                            if (bc.BankCorrespondId == 0)
                                mode = Constants.ADDMODE;
                            else if (bc.BankCorrespondId != 0)
                                mode = Constants.UPDATEMODE;

                        }
                        BankCorrespondentBL bl = new BankCorrespondentBL();
                        bl.InsertUpdateBC(bc, user.UserId, mode);

                    }


                    //lb.Insert(bc);

                }
                else
                {
                    context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
                }
            }
            catch (Exception ex)
            {
                context.Response.Write(JsonConvert.SerializeObject(""+ex));
            }

            }
        
      

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}