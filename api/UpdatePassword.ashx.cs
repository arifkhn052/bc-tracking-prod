﻿using BCEntities;
using BCTrackingBL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for UpdatePassword
    /// </summary>
    public class UpdatePassword : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var jsonString = String.Empty;
            
            context.Request.InputStream.Position = 0;
            using (var inputStream = new StreamReader(context.Request.InputStream))
            {
                jsonString = inputStream.ReadToEnd();
            }
 
            JObject jsonObject = JObject.Parse(jsonString);
            var oldPassword = jsonObject["oldPassword"].ToString();
            var newpassword = jsonObject["newpassword"].ToString();
            var userId = jsonObject["userId"].ToString();

            UserBL obj_pass = new UserBL();
            int authenticatedEntity = obj_pass.UpdatePassword(oldPassword, newpassword, userId);
            

            if (authenticatedEntity ==1)
            {
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = System.Text.Encoding.UTF8;
                context.Response.Write(JsonConvert.SerializeObject("Password Updated Successfully"));
            }
            else if (authenticatedEntity == 0)
            {

                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = System.Text.Encoding.UTF8;
                context.Response.Write(JsonConvert.SerializeObject("You have entered a wrong Password"));
            }
            else
            {
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = System.Text.Encoding.UTF8;
                context.Response.Write(JsonConvert.SerializeObject("Something Went Wrong"));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}