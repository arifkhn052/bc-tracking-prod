﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBI.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for AllocateBCHandler
    /// </summary>
    public class AllocateBCHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;

            if (access == 1)
            {
                UserEntity user = businessLogic.AuthenticateUserData(userId);
                var jsonString = String.Empty;
                context.Request.InputStream.Position = 0;
                using (var inputStream = new StreamReader(context.Request.InputStream))
                {
                    jsonString = inputStream.ReadToEnd();
                }
                JObject jsonObject = JObject.Parse(jsonString);
                var bankid = jsonObject["bankId"].ToString();
                var bcId = jsonObject["bcId"].ToString();
                
                if(bankid=="0")
                {
                    bankid = user.BankId.ToString();
                }
                else
                {
                    bankid = bankid;
                }
                UserBL logic = new UserBL();
                string authenticatedEntity = businessLogic.allocateBC(bankid, bcId);
                if (authenticatedEntity != null)
                {
                    context.Response.Write(JsonConvert.SerializeObject(authenticatedEntity));
                }
                else
                {
                    context.Response.ContentType = "application/json";
                    context.Response.ContentEncoding = System.Text.Encoding.UTF8;
                    context.Response.Write(JsonConvert.SerializeObject("Userid or password is invalid"));
                }
            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}