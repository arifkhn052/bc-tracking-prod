﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;
namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for InserUpdateProductHandler
    /// </summary>
    public class InserUpdateProductHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string userId = context.Request.Headers.Get("userId");
                string TokenId = context.Request.Headers.Get("TokenId");
                UserBL businessLogic = new UserBL();
                int access = businessLogic.getToken(userId, TokenId);
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = Encoding.UTF8;

                if (access == 1)
                {
                    UserEntity userEntity = businessLogic.AuthenticateUserData(userId);
                    if (userEntity != null)
                    {
                        string postedData = context.Request["Products"];
                        UserEntity user = JsonConvert.DeserializeObject<UserEntity>(postedData);
                        UserBL logic = new UserBL();

                        string mode = Constants.ADDMODE;
                        if (user.productId != 0)
                            mode = Constants.UPDATEMODE;
                        if (logic.insertUpdateProduct(user, mode, userEntity.UserId) != 0)
                            throw new Exception(String.Format("Adding bank {0} failed", user.UserName));
                    }
                }
                else
                {
                    context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}