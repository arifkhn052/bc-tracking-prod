﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for getBcByState
    /// </summary>
    public class getBcByState : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            DateTime From_Date;
            //if (access == 1)
            //{

                string mode = Utils.GetStringValue(context.Request[Constants.MODE]);
                BankCorrespondentBL bl = new BankCorrespondentBL();
                var jsonString = String.Empty;
                context.Request.InputStream.Position = 0;
                using (var inputStream = new StreamReader(context.Request.InputStream))
                {
                    jsonString = inputStream.ReadToEnd();
                }
                JObject jsonObject = JObject.Parse(jsonString);

                string stateId = validation.required(jsonObject["stateId"].ToString());
                string district = validation.required(jsonObject["district"].ToString());
                string subdistrict = validation.required(jsonObject["subdistrict"].ToString());
                string village = validation.required(jsonObject["village"].ToString());



                if (mode == "state")
                {
                    List<BankCorrespondence> bcs = bl.GetBankCorrespondentsbystate(stateId, district, subdistrict, village);
                 
                        context.Response.Write(JsonConvert.SerializeObject(bcs));
                }
              
            //}
            //else
            //{
            //    context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            //}
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}