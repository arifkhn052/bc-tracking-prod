﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for GetBcDetails
    /// </summary>
    public class GetBcDetails : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            if (access == 1)
            {
                UserEntity userdata = businessLogic.AuthenticateUserData(userId);
                string mode = Utils.GetStringValue(context.Request[Constants.MODE]);
                string AdhaarNo = Utils.GetStringValue(context.Request["AdhaarNo"]);
                BankCorrespondentBL bl = new BankCorrespondentBL();
                if (mode == Constants.CORPORATEMODE)
                {
                    List<Corporates> allCorporates = bl.GetCorporates(null);
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if (mode == "product")
                {
                    List<Products> allCorporates = bl.GetProduct();
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if(mode == "One")
                {
                    //List<BankCorrespondence> bcs = bl.GetBankCorrespondents(mode, filters);
                    //if (mode == "One")
                    //    context.Response.Write(JsonConvert.SerializeObject(bcs[0]));
                    //else
                    //    context.Response.Write(JsonConvert.SerializeObject(bcs));
                }
            
            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }




        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}