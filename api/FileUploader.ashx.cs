﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using System.Configuration;
using BCTrackingBL;
using System.Web.SessionState;
using BCEntities;
using BCTrackingServices;
using System.Text;
using Newtonsoft.Json;
using Microsoft.Azure; //Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob;
using System.Threading.Tasks; // Namespace for Blob storage types

namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for FileUploader
    /// </summary>
    public class FileUploader : IHttpHandler, IRequiresSessionState
    {
   

        public void ProcessRequest(HttpContext context)
        {
            
            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            string adhaar = context.Request.Headers.Get("adhaar");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            
            if (access == 1)
            {

                UserEntity user = businessLogic.AuthenticateUserData(userId);               
            //    UserEntity user = context.Session[Constants.USERSESSIONID] as UserEntity;
                string mode = Utils.GetStringValue(context.Request[Constants.MODE]);
                string extension = string.Empty;
                context.Response.ContentType = "text/plain";
                context.Response.Expires = -1;

                foreach (string keyName in context.Request.Files)
                {
                    extension = Path.GetExtension(context.Request.Files[keyName].FileName);
                }              
                //extension == ".xls"               
                    try
                    {
                        if (extension == ".jpg" || extension == ".png" || extension == ".gif" || extension == ".jpeg")
                        {

                            HttpPostedFile pFile = context.Request.Files[0];
                            ////string currentRecordId = context.Request["CurrentRecordId"];
                            ////string currentFieldId = context.Request["CurrentFieldId"];

                            //int len = pFile.ContentLength;

                            //byte[] input = new byte[len];

                            //System.IO.Stream fStream = pFile.InputStream;
                            //fStream.Read(input, 0, len);

                            //string dirPath = System.IO.Path.GetFullPath(".");
                            //string path = System.AppDomain.CurrentDomain.BaseDirectory;
                            //path = Path.Combine(path, "FileUpload", pFile.FileName);

                            //HttpPostedFile postedFile = context.Request.Files[0];
                            //var contentType = postedFile.ContentType;
                            //var streamContents = postedFile.InputStream;
                            //var StoreBlob = adhaar + "_" + pFile.FileName;

                            //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
                            //CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                            //CloudBlobContainer container = blobClient.GetContainerReference("dev-photos");
                            //CloudBlockBlob blockBlob = container.GetBlockBlobReference(StoreBlob);
                            //blockBlob.Properties.ContentType = contentType;
                            //blockBlob.UploadFromStream(streamContents);

                            HttpPostedFile postedFile = context.Request.Files[0];
                            var contentType = postedFile.ContentType;
                            var streamContents = postedFile.InputStream;
                            var blobName = postedFile.FileName;
                            var StoreBlob = adhaar + "_" + blobName;

                            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
                            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                            CloudBlobContainer container = blobClient.GetContainerReference("dev-photos");
                            CloudBlockBlob blockBlob = container.GetBlockBlobReference(StoreBlob);
                            blockBlob.Properties.ContentType = contentType;
                            blockBlob.UploadFromStream(streamContents);
                        }
                        else
                        {
                            HttpPostedFile postedFile = context.Request.Files[0];
                            var contentType = postedFile.ContentType;
                            var streamContents = postedFile.InputStream;
                            var blobName = postedFile.FileName;
                            var StoreBlob = "BC_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + user.UserId.ToString() + "_" + blobName;
                            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
                            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                            CloudBlobContainer container = blobClient.GetContainerReference("dev-raw");
                            CloudBlockBlob blockBlob = container.GetBlockBlobReference(StoreBlob);
                            blockBlob.Properties.ContentType = contentType;
                            blockBlob.UploadFromStream(streamContents);
                            BankCorrespondentBL bc = new BankCorrespondentBL();
                            Task task1 = Task.Run(() => bc.ExcelRead(StoreBlob, user.UserId, Constants.ADDMODE, user.BankId.ToString(), blobName, extension));
                        }
                    }

                    catch (Exception ex)
                    {  
                        throw ex;
                      //  string str = ex.Message.ToString();
                      //  businessLogic.SaveError(user.UserId.ToString(), str);
                    }
                }
            
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }
          }
       

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}