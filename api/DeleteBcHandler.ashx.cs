﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for DeleteBcHandler
    /// </summary>
    public class DeleteBcHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;

            if (access == 1)
            {
                //  UserEntity user = businessLogic.AuthenticateUserData(userId);

                string mode = Utils.GetStringValue(context.Request[Constants.MODE]);
                string user = Utils.GetStringValue(context.Request[Constants.USERMODE]);
                string bank = Utils.GetStringValue(context.Request[Constants.BANKMODE]);
                string branch = Utils.GetStringValue(context.Request[Constants.BRANCHMODE]);
                int bcId = Utils.GetIntValue(context.Request["bcId"]);
                int bankId = Utils.GetIntValue(context.Request["bankId"]);
                int productId = Utils.GetIntValue(context.Request["productId"]);
                int userid = Utils.GetIntValue(context.Request["userId"]);
                int corporateId = Utils.GetIntValue(context.Request["corporateId"]);
                int stateid = Utils.GetIntValue(context.Request["stateid"]);
                int grpproductId = Utils.GetIntValue(context.Request["grpproductId"]);
                string apptDateFrom = context.Request[Constants.APPTDATEFROMFILTER];
                string apptDateTo = context.Request[Constants.APPTDATETOFILTER];
                string createDateFrom = context.Request[Constants.CREATEDATEFROMFILTER];
                string createDateTo = context.Request[Constants.CREATEDATETOFILTER];
                //<<<<<<< .mine
                string bankCircleId = context.Request[Constants.BANKCIRCLEID];
                string stateId = context.Request[Constants.STATEID];
                string regionId = context.Request[Constants.BANKREGIONID];
                string branchId = context.Request[Constants.BRANCHID];
                string bankSsa = context.Request[Constants.BANKSSA];
                string branchCategory = context.Request[Constants.BRANCHCATEGORY];
                string zoneId = context.Request[Constants.BANKZONEID];
                string pinCode = context.Request[Constants.PINCODE];
                //||||||| .r567
                //=======
                string IsBlackListed = context.Request["IsBlackListed"];

                Dictionary<string, string> filters = new Dictionary<string, string>();
                if (!String.IsNullOrEmpty(user))
                    filters.Add(Constants.USERMODE, userid.ToString());
                if (!String.IsNullOrEmpty(bank))
                    filters.Add(Constants.BANKMODE, bankId.ToString());
                if (!String.IsNullOrEmpty(branch))
                    filters.Add(Constants.BRANCHMODE, branch.ToString());
                if (!String.IsNullOrEmpty(apptDateFrom))
                    filters.Add(Constants.APPTDATEFROMFILTER, apptDateFrom);
                if (!String.IsNullOrEmpty(apptDateTo))
                    filters.Add(Constants.APPTDATETOFILTER, apptDateTo);
                //<<<<<<< .mine
                if (!String.IsNullOrEmpty(createDateFrom))
                    filters.Add(Constants.CREATEDATEFROMFILTER, createDateFrom);
                //||||||| .r567
                if (!String.IsNullOrEmpty(createDateFrom))
                    filters.Add(Constants.APPTDATETOFILTER, createDateFrom);
                //=======
                if (!String.IsNullOrEmpty(createDateFrom))
                    filters.Add(Constants.CREATEDATEFROMFILTER, createDateFrom);
                //>>>>>>> .r584
                if (!String.IsNullOrEmpty(createDateTo))
                    filters.Add(Constants.CREATEDATETOFILTER, createDateTo);

                if (!String.IsNullOrEmpty(bankCircleId))
                    filters.Add(Constants.BANKCIRCLEID, bankCircleId);
                if (!String.IsNullOrEmpty(branchId))
                    filters.Add(Constants.BRANCHID, branchId);
                if (!String.IsNullOrEmpty(stateId))
                    filters.Add(Constants.STATEID, stateId);
                if (!String.IsNullOrEmpty(regionId))
                    filters.Add(Constants.BANKREGIONID, regionId);
                if (!String.IsNullOrEmpty(branchId))
                    filters.Add(Constants.BRANCHID, branchId);
                if (!String.IsNullOrEmpty(bankSsa))
                    filters.Add(Constants.BANKSSA, bankSsa);
                if (!String.IsNullOrEmpty(branchCategory))
                    filters.Add(Constants.BRANCHCATEGORY, branchCategory);
                if (!String.IsNullOrEmpty(zoneId))
                    filters.Add(Constants.BANKZONEID, zoneId);
                if (!String.IsNullOrEmpty(IsBlackListed))
                    filters.Add(Constants.ISBLACKLISTED, IsBlackListed);

                if (mode == "One" && bcId != Constants.DEFAULTVALUE)
                    filters.Add("bcId", context.Request["bcId"]);


                BankCorrespondentBL bl = new BankCorrespondentBL();
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = Encoding.UTF8;

                if (mode == Constants.BANKMODE)
                {
                    UserBL userBal = new UserBL();
                    userBal.DeleteBank(bankId);
                }
                if (mode == Constants.BANKMODE)
                {
                    UserBL userBal = new UserBL();
                    userBal.DeleteBank(bankId);
                }
                if (mode == Constants.STATESMODE)
                {
                    UserBL userBal = new UserBL();
                    userBal.deleteState(Convert.ToInt32(stateId));
                }


                else if (mode == "Product")
                {
                    UserBL userBal = new UserBL();
                    userBal.deleteProduct(productId);
                }
                else if (mode == "groupProduct")
                {
                    UserBL userBal = new UserBL();
                    userBal.deleteGroupProduct(grpproductId);
                }

                    
                else if (mode == "User")
                {
                    UserBL userBal = new UserBL();
                    userBal.DeleteUsers(userid);
                }
                else if (mode == "BANKc")
                {
                    UserBL userBal = new UserBL();
                    userBal.deleteBC(bcId);
                }

                else if (mode == Constants.CORPORATEMODE)
                {
                    UserBL userBal = new UserBL();
                    userBal.deleteCorporate(corporateId);

                }


                else if (mode == Constants.NOTIFICATIONMODE)
                {
                    List<Notification> allNotifications = bl.GetNotifications(null);
                    context.Response.Write(JsonConvert.SerializeObject(allNotifications));

                }

                else if (mode == Constants.PRODUCTMODE)
                {
                    List<BCProducts> products = bl.GetProducts(bcId, productId);
                    context.Response.Write(JsonConvert.SerializeObject(products));
                }
                else
                {

                   
                }



            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }
        }
        


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}