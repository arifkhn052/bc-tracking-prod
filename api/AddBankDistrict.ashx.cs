﻿using System;
using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Web;
using System.Web.SessionState;
using System.Text;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for AddBankDistrict
    /// </summary>
    public class AddBankDistrict : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;

            if (access == 1)
            {
                try
                {
                    UserEntity userEntity = businessLogic.AuthenticateUserData(userId);
                    if (userEntity != null)
                    {
                        string postedData = context.Request["info"];
                        DistrictList state = JsonConvert.DeserializeObject<DistrictList>(postedData);
                        StateBL logic = new StateBL();

                        string mode = Constants.ADDMODE;
                        if (state.CityId != 0)
                            mode = Constants.UPDATEMODE;

                        if (logic.InsertUpdateBankDistrict(state, mode, userEntity.UserId) != 0)
                            throw new Exception(String.Format("Adding corporate {0} failed", state.DistrictName));
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}