﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCEntities;
using Newtonsoft.Json;
using BCTrackingBL;
using BCTrackingServices;
using System.Web.SessionState;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Data;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for addBcHandler
    /// </summary>
    public class addBcHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
           
            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;

            if (access == 1)
            {
                UserEntity user = businessLogic.AuthenticateUserData(userId);
                string mode = Utils.GetStringValue(context.Request[Constants.MODE]);
                BankCorrespondence bc = new BankCorrespondence();
                if (user != null)
                {

                    try
                    {
                        bool isvalid = true;
                        List<errorclass> errorMsg = new List<errorclass>();
                       
                        DataSet result = new DataSet();
                        var jsonString = String.Empty;
                        context.Request.InputStream.Position = 0;
                        using (var inputStream = new StreamReader(context.Request.InputStream))
                        {
                            jsonString = inputStream.ReadToEnd();
                        }
                        JObject jsonObject = JObject.Parse(jsonString);
                        var bcDatas = jsonObject["bcorr"].ToString();
                        JObject bcObject = JObject.Parse(bcDatas);



                        var userName = validation.required(bcObject["Name"].ToString());
                        if (userName == "")
                        {                        
                            isvalid = false;
                            result = validation.errorFinder("userName");                           
                        }
                        else
                        {
                            bc.Name = userName;   
                        }
                        var Gender = validation.getGendereValidation(bcObject["Gender"].ToString());
                        if (Gender == "false")
                        {
                            isvalid = false;
                            result = validation.errorFinder("Gender");
                        }
                        else
                        {
                            bc.Gender = Gender;
                        }
                        var DOB = validation.required(bcObject["DOB"].ToString());
                        if (DOB == "")
                        {
                            isvalid = false;
                            result = validation.errorFinder("DOB");
                        }
                        else
                        {
                            bc.DOB = DateTime.FromOADate(Utils.GetDoubleValue(DOB)); 
                          //  bc.DOB =  Convert.ToDateTime(DOB);
                        }
                        var District = validation.required(bcObject["District"].ToString());
                        if (District == "")
                        {

                            bc.District = null;   
                        }
                        var Handicap = validation.required(bcObject["Handicap"].ToString());
                        if (Handicap == "")
                        {
                            isvalid = false;
                            result = validation.errorFinder("Handicap");
                        }
                        else
                        {
                            bc.Handicap = Handicap.ToString();
                        }
                     
                        bc.BankCorrespondId = 0;

                        var ImagePath = validation.required(bcObject["ImagePath"].ToString());
                        if (ImagePath == "")
                        {
                            bc.ImagePath = bcObject["Name"].ToString();
                        }
                        else
                        {
                            bc.ImagePath = bcObject["ImagePath"].ToString();
                        }

                        var FatherName = validation.required(bcObject["FatherName"].ToString());
                        if (FatherName == "")
                        {
                            isvalid = false;
                            result = validation.errorFinder("FatherName");
                        }
                        else
                        {
                            bc.FatherName = bcObject["FatherName"].ToString();
                        }

                     
                        bc.SpouseName = bcObject["SpouseName"].ToString();
                        var Category = validation.required(bcObject["Category"].ToString());
                        if (Category == "")
                        {
                            isvalid = false;
                            result = validation.errorFinder("Category");
                        }
                        else
                        {
                            bc.Category = Category.ToString();
                        }
                       
                        var phone1 = validation.IsCorrectMobileNumber(bcObject["PhoneNumber1"].ToString());
                        if (phone1 == "0")
                        {                          
                            isvalid = false;
                            result =  validation.errorFinder("phone1");                           
                        }
                        else
                        {
                            bc.PhoneNumber1 = phone1.ToString();
                        }
                        var PhoneNumber2 = validation.IsCorrectMobileNumber(bcObject["PhoneNumber2"].ToString());
                        if (PhoneNumber2 == "0")
                        {
                            isvalid = false;
                            result = validation.errorFinder("PhoneNumber2");
                        }
                        else
                        {
                            bc.PhoneNumber2 = PhoneNumber2.ToString();
                        }
                        var PhoneNumber3 = validation.IsCorrectMobileNumber(bcObject["PhoneNumber2"].ToString());
                        if (PhoneNumber3 == "0")
                        {
                            isvalid = false;
                            result = validation.errorFinder("PhoneNumber3");
                        }
                        else
                        {
                            bc.PhoneNumber3 = PhoneNumber3.ToString();
                        }
                        var Email = validation.IsValidEmailId(bcObject["Email"].ToString());
                        if (Email == "false")
                        {
                            isvalid = false;
                            result = validation.errorFinder("email");
                        }
                        else
                        {
                            bc.Email = Email.ToString();
                        }
                        var AadharCard = validation.required(bcObject["AadharCard"].ToString());
                        if (AadharCard == "")
                        {
                            isvalid = false;
                            result = validation.errorFinder("AadharCard");
                        }
                        else
                        {
                            int valid = businessLogic.getAadhaar(AadharCard);
                            if (valid==1)
                            {
                                isvalid = false;
                                result = validation.errorFinder("AadharCard1");
                            }
                            else
                            {
                                bc.AadharCard = bcObject["AadharCard"].ToString();
                            }

                          
                        }
                      
                        bc.PanCard = bcObject["PanCard"].ToString();
                        bc.VoterCard = bcObject["VoterCard"].ToString();
                        bc.DriverLicense = bcObject["DriverLicense"].ToString();
                        bc.NregaCard = bcObject["NregaCard"].ToString();
                        bc.RationCard = bcObject["RationCard"].ToString();
                        bc.State = bcObject["State"].ToString();
                        bc.Subdistrict = bcObject["Subdistrict"].ToString();
                        bc.Area = bcObject["Area"].ToString();
                        var PinCode = validation.required(bcObject["PinCode"].ToString());
                        if (PinCode == "")
                        {
                            isvalid = false;
                            result = validation.errorFinder("PinCode");
                        }
                        else
                        {
                            bc.PinCode = bcObject["PinCode"].ToString();
                        }

                        var AlternateOccupationType = validation.getAlternateOccupationType(bcObject["AlternateOccupationType"].ToString());
                        if (AlternateOccupationType == "false")
                        {
                            isvalid = false;
                            result = validation.errorFinder("AlternateOccupationType");
                        }
                        else
                        {
                            bc.AlternateOccupationType = AlternateOccupationType.ToString();
                        }
                        bc.AlternateOccupationDetail = bcObject["AlternateOccupationDetail"].ToString();
                        bc.UniqueIdentificationNumber = bcObject["UniqueIdentificationNumber"].ToString();
                        bc.BankReferenceNumber = bcObject["BankReferenceNumber"].ToString();
                        var Qualification = validation.getQualificationValidation(bcObject["Qualification"].ToString());
                        if (Qualification == "false")
                        {
                            isvalid = false;
                            result = validation.errorFinder("Qualification");
                        }
                        else
                        {
                            bc.Qualification = Qualification.ToString();
                        }
                        bc.OtherQualification = bcObject["OtherQualification"].ToString();
                        bc.CorporateId = Convert.ToInt32(bcObject["CorporateId"]);
                        var isAllocated = validation.required(bcObject["isAllocated"].ToString());
                        if (isAllocated == "")
                        {
                            isvalid = false;
                            result = validation.errorFinder("isAllocated");
                        }
                        else
                        {
                            bc.isAllocated = Convert.ToBoolean(isAllocated.ToString());
                        }
                        if (isAllocated=="true")
                        {
                            var AppointmentDate = validation.required(bcObject["AppointmentDate"].ToString());
                            if (AppointmentDate == "")
                            {
                                isvalid = false;
                                result = validation.errorFinder("AppointmentDate");
                            }
                            else
                            {
                                bc.AppointmentDate = DateTime.FromOADate(Utils.GetDoubleValue(AppointmentDate)); 
                              //  bc.DOB = Convert.ToDateTime(DOB);
                            }
                            bc.AllocationIFSCCode = bcObject["AllocationIFSCCode"].ToString();
                        


                            int AllocationBankId = validation.IsCorrectStringIsNumber(bcObject["AllocationBankId"].ToString());
                            if (AllocationBankId == -1)
                            {
                                isvalid = false;
                                result = validation.errorFinder("AllocationBankId");
                            }
                            else
                            {
                                bc.AllocationBankId = AllocationBankId;
                            }

                            int AllocationBranchId = validation.IsCorrectStringIsNumber(bcObject["AllocationBranchId"].ToString());
                            if (AllocationBranchId == -1)
                            {
                                isvalid = false;
                                result = validation.errorFinder("AllocationBranchId");
                            }
                            else
                            {
                                bc.AllocationBranchId = AllocationBranchId;
                            }


                            var BCType = validation.getBCTypeValidation(bcObject["BCType"].ToString());
                            if (BCType == "false")
                            {
                                isvalid = false;
                                result = validation.errorFinder("BCType");
                            }
                            else
                            {
                                bc.BCType = BCType;
                            }
                            int WorkingDays = validation.IsCorrectStringIsNumber(bcObject["WorkingDays"].ToString());
                            if (WorkingDays == -1)
                            {
                                isvalid = false;
                                result = validation.errorFinder("WorkingDays");
                            }
                            else
                            {
                                bc.WorkingDays = WorkingDays;
                            }
                            int WorkingHours = validation.IsCorrectStringIsNumber(bcObject["WorkingHours"].ToString());
                            if (WorkingHours == -1)
                            {
                                isvalid = false;
                                result = validation.errorFinder("WorkingHours");
                            }
                            else
                            {
                                bc.WorkingHours = WorkingHours;
                            }
                            bc.PLPostalAddress = bcObject["PLPostalAddress"].ToString();
                            bc.PLVillageCode = bcObject["PLVillageCode"].ToString();
                            bc.PLVillageDetail = bcObject["PLVillageDetail"].ToString();
                            int PLStateId = validation.IsCorrectStringIsNumber(bcObject["PLStateId"].ToString());
                            if (PLStateId == -1)
                            {
                                isvalid = false;
                                result = validation.errorFinder("PLStateId");
                            }
                            else
                            {
                                bc.PLStateId = Convert.ToInt32(PLStateId);
                            }
                            bc.PLTaluk = bcObject["PLTaluk"].ToString();
                            bc.PLPinCode = bcObject["PLPinCode"].ToString();
                            int MinimumCashHandlingLimit = validation.IsCorrectStringIsNumber(bcObject["MinimumCashHandlingLimit"].ToString());
                            if (MinimumCashHandlingLimit == -1)
                            {
                                isvalid = false;
                                result = validation.errorFinder("MinimumCashHandlingLimit");
                            }
                            else
                            {
                                bc.MinimumCashHandlingLimit = Convert.ToDouble(MinimumCashHandlingLimit);
                            }
                            int MonthlyFixedRenumeration = validation.IsCorrectStringIsNumber(bcObject["MonthlyFixedRenumeration"].ToString());
                            if (MonthlyFixedRenumeration == -1)
                            {
                                isvalid = false;
                                result = validation.errorFinder("MonthlyFixedRenumeration");
                            }
                            else
                            {
                                bc.MonthlyFixedRenumeration = Convert.ToDouble(MonthlyFixedRenumeration);
                            }
                            int MonthlyVariableRenumeration = validation.IsCorrectStringIsNumber(bcObject["MonthlyFixedRenumeration"].ToString());
                            if (MonthlyVariableRenumeration == -1)
                            {
                                isvalid = false;
                                result = validation.errorFinder("MonthlyVariableRenumeration");
                            }
                            else
                            {
                                bc.MonthlyVariableRenumeration = Convert.ToDouble(MonthlyVariableRenumeration);
                            }
                            
                        }

///////////////////////////////////////////////////////// Certificate\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

                        var certi = jsonObject["allCerts"].ToString();
                        JArray certiarray = JArray.Parse(certi);
                        List<BCCertifications> Certification = null;
                        int rowNum = 1;
                        foreach (var item in certiarray)
                        {
                            BCCertifications cr = new BCCertifications();
                            string DateOfPassing = validation.DateFormat( item["DateOfPassing"].ToString());
                            if(DateOfPassing=="false")
                            {
                                isvalid = false;
                                result = validation.errorFinderdate("date", rowNum, "allCerts");
                            }
                            else
                            {
                               // DateTime Date_Of_Passing = DateTime.ParseExact(string.IsNullOrEmpty(DateOfPassing) ? "01/01/1970" : DateOfPassing.Replace(".", "/"), "dd/MM/yyyy", null);

                                cr.DateOfPassing = DateTime.FromOADate(Utils.GetDoubleValue(DateOfPassing)); 
                            }


                           
                            cr.InstituteName = item["InstituteName"].ToString();
                            cr.CourseName = item["CourseName"].ToString();
                            cr.Grade = item["Grade"].ToString();
                            if (Certification == null)
                            {
                                Certification = new List<BCCertifications>();
                            }
                            Certification.Add(cr);
                            rowNum++;
                        }
                        if (Certification != null)
                        {
                            bc.Certifications = Certification;
                        }

                        ///////////////////////////////////////////////////////// Experience\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\             
                        var Exps = jsonObject["allExps"].ToString();
                        JArray ExpsArray = JArray.Parse(Exps);
                        List<PreviousExperiene> experience = null;
                        int rowNum1 = 1;
                        foreach (var item in ExpsArray)
                        {
                            PreviousExperiene pr = new PreviousExperiene();


                            string FromDate = validation.DateFormat(item["FromDate"].ToString());
                            if (FromDate == "false")
                            {
                                isvalid = false;
                                result = validation.errorFinderdate("date", rowNum1, "allExps");
                            }
                            else
                            {                            
                              //  DateTime From_Date = DateTime.ParseExact(string.IsNullOrEmpty(FromDate) ? "01/01/1970" : FromDate.Replace(".", "/"), "dd/MM/yyyy", null);
                                pr.FromDate =  DateTime.FromOADate(Utils.GetDoubleValue(FromDate)); 
                            }

                            string ToDate = validation.DateFormat(item["ToDate"].ToString());
                            if (ToDate == "false")
                            {
                                isvalid = false;
                                result = validation.errorFinderdate("date", rowNum1, "allExps");
                            }
                            else
                            {  
                              //  DateTime To_Date = DateTime.ParseExact(string.IsNullOrEmpty(ToDate) ? "01/01/1970" : ToDate.Replace(".", "/"), "dd/MM/yyyy", null);
                                pr.ToDate = DateTime.FromOADate(Utils.GetDoubleValue(ToDate)); 
                            }

                            int BankId = validation.IsCorrectStringIsNumber(item["BankId"].ToString());
                            if (BankId == -1)
                            {
                                isvalid = false;
                                result = validation.errorFinderdate("bankId", rowNum1, "allExps");
                            }
                            else
                            {
                                pr.BankId = Convert.ToInt32(item["BankId"]);
                            }

                            int Branchid = validation.IsCorrectStringIsNumber(item["Branchid"].ToString());
                            if (Branchid == -1)
                            {
                                isvalid = false;
                                result = validation.errorFinderdate("Branchid", rowNum1, "allExps");
                            }
                            else
                            {
                                pr.Branchid = Convert.ToInt32(item["Branchid"]);
                            }
                            
                         
                           
                          
                        
                         
                            pr.Reason = item["Reason"].ToString();
                          
                            if (experience == null)
                            {
                                experience = new List<PreviousExperiene>();
                            }
                            rowNum1++;
                        }
                        if (experience != null)
                        {
                            bc.PreviousExperience = experience;
                        }


                        ///////////////////////////////////////////////////////// Areas\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\             
                        List<OperationalAreas> allAreas = ((JArray)jsonObject["allAreas"]).Select(x => new OperationalAreas
                        {
                            VillageCode = (string)x["VillageCode"],
                            VillageDetail = (string)x["VillageDetail"],
                           

                        }).ToList();
                        if (allAreas.Count > 0)
                        {
                            bc.OperationalAreas = allAreas;
                        }


                        ///////////////////////////////////////////////////////// all Devices\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\          
                        var allDevices = jsonObject["allDevices"].ToString();
                        JArray allDevicesArray = JArray.Parse(allDevices);
                        List<BCDevices> Devices = null;
                        int rowNum2 = 1;
                        foreach (var item in allDevicesArray)
                        {
                            BCDevices dc = new BCDevices();

                            string GivenOn = validation.DateFormat(item["GivenOn"].ToString());
                            if (GivenOn == "false")
                            {
                                isvalid = false;
                                result = validation.errorFinderdate("date", rowNum2, "allDevices");
                            }
                            else
                            {                             
                               // DateTime Given_On = DateTime.ParseExact(string.IsNullOrEmpty(GivenOn) ? "01/01/1970" : GivenOn.Replace(".", "/"), "dd/MM/yyyy", null);
                                dc.GivenOn = DateTime.FromOADate(Utils.GetDoubleValue(GivenOn)); 
                            }

                                       
                            dc.Device = item["Device"].ToString();
                            if (Devices == null)
                            {
                                Devices = new List<BCDevices>();
                            }
                            Devices.Add(dc);
                            rowNum2++;
                        }
                        if (Devices != null)
                        {
                             bc.Devices = Devices;
                        }
                        //List<BCDevices> allDevices = ((JArray)jsonObject["allDevices"]).Select(x => new BCDevices
                        //{
                        //    Device = (string)x["Device"],
                        //    GivenOn = (DateTime)x["GivenOn"],
                           

                        //}).ToList();
                        //if (allDevices.Count > 0)
                        //{
                        //    bc.Devices = allDevices;
                        //}


                            
                        ///////////////////////////////////////////////////////// Conns\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\    
                        var allConns = jsonObject["allConns"].ToString();
                        JArray allConnsArray = JArray.Parse(allConns);
                        List<ConnectivityDetails> Connectivity = null;
                        int rowNum3 = 1;
                        foreach (var item in allConnsArray)
                        {
                            ConnectivityDetails cd = new ConnectivityDetails();
                            cd.ConnectivityMode = item["ConnectivityMode"].ToString();
                            cd.ConnectivityProvider = item["ConnectivityProvider"].ToString();

                            string ContactNumber = validation.IsCorrectMobileNumber(item["ContactNumber"].ToString());
                            if (ContactNumber == "0")
                            {
                                isvalid = false;
                                result = validation.errorFinderdate("ContactNumber", rowNum3, "allConns");
                            }
                            else
                            {
                                cd.ContactNumber = item["ContactNumber"].ToString();

                            }

                            if (Connectivity == null)
                            {
                                Connectivity = new List<ConnectivityDetails>();
                            }
                            Connectivity.Add(cd);

                        }
                        if (Connectivity != null)
                        {
                            bc.ConnectivityDetails = Connectivity;
                        }
                        


                        if (isvalid)
                        {
                            BankCorrespondentBL bl = new BankCorrespondentBL();
                            bl.InsertUpdateBC(bc, user.UserId, mode);
                            context.Response.Write(JsonConvert.SerializeObject("BC successfully saved."));

                        }
                        else
                        {
                            string JSONString = string.Empty;
                            JSONString = JsonConvert.SerializeObject(result.Tables[0]);
                            context.Response.Write(JSONString);
                          
                          
                        }
                    }
                    catch (Exception)
                    {
                       
                        throw;
                    }


                    //string bcData = context.Request["bcorr"];

                    //bcData = HttpUtility.UrlDecode(bcData);
                    //BankCorrespondence bc = JsonConvert.DeserializeObject<BankCorrespondence>(bcData);



                    //if (mode != Constants.BLACKLISTMODE && mode != Constants.WHITELISTMODE && mode != Constants.CHANGESTATUSMODE)
                    //{
                    //    string rtnData = context.Request["allCerts"];
                    //    if (!String.IsNullOrEmpty(rtnData))
                    //    {
                    //        rtnData = HttpUtility.UrlDecode(rtnData);
                    //        List<BCCertifications> certs = JsonConvert.DeserializeObject<List<BCCertifications>>(rtnData);
                    //        bc.Certifications = certs;
                    //    }

                    //    string rtnexps = context.Request["allExps"];
                    //    if (!String.IsNullOrEmpty(rtnexps))
                    //    {
                    //        rtnexps = HttpUtility.UrlDecode(rtnexps);//new line
                    //        List<PreviousExperiene> exps = JsonConvert.DeserializeObject<List<PreviousExperiene>>(rtnexps);
                    //        bc.PreviousExperience = exps;
                    //    }
                    //    string rtnareas = context.Request["allAreas"];
                    //    if (!String.IsNullOrEmpty(rtnareas))
                    //    {
                    //        rtnareas = HttpUtility.UrlDecode(rtnareas);//new line
                    //        List<OperationalAreas> areas = JsonConvert.DeserializeObject<List<OperationalAreas>>(rtnareas);
                    //        bc.OperationalAreas = areas;
                    //    }
                    //    string rtndevices = context.Request["allDevices"];
                    //    if (!String.IsNullOrEmpty(rtndevices))
                    //    {
                    //        rtndevices = HttpUtility.UrlDecode(rtndevices);//new line
                    //        List<BCDevices> devices = JsonConvert.DeserializeObject<List<BCDevices>>(rtndevices);
                    //        bc.Devices = devices;
                    //    }
                    //    string rtnconns = context.Request["allConns"];
                    //    if (!String.IsNullOrEmpty(rtnconns))
                    //    {
                    //        rtnconns = HttpUtility.UrlDecode(rtnconns);//new line
                    //        List<ConnectivityDetails> conns = JsonConvert.DeserializeObject<List<ConnectivityDetails>>(rtnconns);
                    //        bc.ConnectivityDetails = conns;
                    //    }
                    //    if (bc.BankCorrespondId == 0)
                    //        mode = Constants.ADDMODE;
                    //    else if (bc.BankCorrespondId != 0)
                    //        mode = Constants.UPDATEMODE;

                    //}
                    //BankCorrespondentBL bl = new BankCorrespondentBL();
                    //bl.InsertUpdateBC(bc, user.UserId, mode);

                }


                //lb.Insert(bc);

            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }


        }
 
        public class errorclass
        {
            public string errCode { get; set; }
            public string message { get; set; }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}