﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCEntities;
using BCTrackingServices;
using BCTrackingBL;
using SBI.Web.Helpers;

namespace BCTrackingWeb
{
    public partial class editEntry : System.Web.UI.Page
    {

        public static ListItem GetDefaultItem()
        {
            ListItem item = new ListItem(Constants.DEFAULTCHOICE, "-1");
            return item;
        }
        protected void fileImage_Change(object sender, EventArgs e)
        {
            int s = 1;

        }
        protected void Page_Load(object sender, EventArgs e)
        {

          //  string userId = Session["userid"].ToString();
            BankCorrespondentBL bl = new BankCorrespondentBL();
            BankBL bankBL = new BankBL();
            UserBL userBal = new UserBL();
            //List<Bank> allBanks = bankBL.GetBanks(null);

            //if (allBanks != null)
            //{

                //   List<Branch> allBranches = allBanks.SelectMany(b => b.Branches).ToList();

                //selPreviousExpBank.DataSource = allBanks;
                //selPreviousExpBank.DataBind();
                //selPreviousExpBank.Items.Insert(0, GetDefaultItem());

                //selAllocationBank.DataSource = allBanks;
                //selAllocationBank.DataBind();
                //selAllocationBank.Items.Insert(0, GetDefaultItem());



                //List<Branch> allBranches = userBal.getBranch(userId);
                //if (allBranches != null)
                //{
                //    selPreviousExpBranch.DataSource = allBranches;
                //    selPreviousExpBranch.DataBind();
                //    selPreviousExpBranch.Items.Insert(0, GetDefaultItem());

                //    selAllocationBranch.DataSource = allBranches;
                //    selAllocationBranch.DataBind();
                //    selAllocationBranch.Items.Insert(0, GetDefaultItem());
                //}
           // }


            List<Institute> allInstitutes = userBal.getInstitue();
            List<Coursess> allCourses = userBal.getCouse();
            if (allInstitutes != null)
            {

                int instituecount = Convert.ToInt32(allInstitutes.Count().ToString());
                SelInstitute.DataSource = allInstitutes;
                SelInstitute.DataBind();
                SelInstitute.Items.Insert(instituecount, new ListItem("Other", "0"));
                SelInstitute.Items.Insert(0, GetDefaultItem());

            }
            else
            {

                SelInstitute.Items.Insert(0, new ListItem("Select", "--select--"));
                SelInstitute.Items.Insert(1, new ListItem("Other", "1"));

            }
            if (allCourses != null)
            {

                int coursecount = Convert.ToInt32(allCourses.Count().ToString());
                SelCourse.DataSource = allCourses;
                SelCourse.DataBind();
                SelCourse.Items.Insert(coursecount, new ListItem("Other", "0"));
                SelCourse.Items.Insert(0, GetDefaultItem());
            }
            else
            {

                SelCourse.Items.Insert(0, new ListItem("Select", "--select--"));
                SelCourse.Items.Insert(1, new ListItem("Other", "1"));

            }
            List<Corporates> allCorporates = bl.GetCorporates(null);

            selCorporate.DataSource = allCorporates;
            selCorporate.DataBind();
            selCorporate.Items.Insert(0, GetDefaultItem());
        }
    }
}