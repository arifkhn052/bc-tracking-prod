﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="c1.aspx.cs" Inherits="BCTrackingWeb.c1" %>
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">List of Corporate</h2>
                                   
                                </div>
                                <!-- /.col-md-6 -->
                                <div class="col-md-6 right-side">
                                    <a href="AddCorporate1.aspx" class="btn bg-black" role="button">Add Corporate</a>
                                </div>
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="home.aspx"><i class="fa fa-home"></i> Home</a></li>
                                   <li><a href="c1.aspx"> Corporate List</a></li>
            							
            						</ul>
                                </div>
                              
                                <!-- /.col-md-6 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                                <div class="row">

                                  
                                    <!-- /.col-md-6 -->

                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                  
                                                </div>
                                            </div>
                                            <div class="panel-body p-20">

                                                <table id="tblCorporateList" class="display dataTable table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Address</th>
                    <th>Contact</th>
                    <th>Edit</th>
                    <th>Delete</th>

                </tr>
            </thead>
            <tbody></tbody>
        </table>

                                                <div class="col-md-12 mt-15 src-code">
                                                    <pre class="language-html">
                                                        <code class="language-html">
&lt;table class="table table-striped table-bordered"&gt;
	&lt;caption&gt;Optional table caption.&lt;/caption&gt;
	&lt;tbody class="text-center line-height-35"&gt;
		&lt;tr&gt;
			&lt;td class="w-10 text-center line-height-35"&gt;&lt;i class="fa fa-close color-danger font-size-18" aria-hidden="true"&gt;&lt;/i&gt;&lt;/td&gt;
			&lt;td class="w-10"&gt;&lt;img src="images/letter/a.png" alt="" class="border-radius-50 img-size-35"&gt;&lt;/td&gt;
			&lt;td class="w-20 line-height-35"&gt;Alepy Macintyre&lt;/td&gt;
            &lt;td class="line-height-35 text-left"&gt;Account for static form controls in form group&lt;/td&gt;
            &lt;td class="w-10 line-height-35"&gt;Resolved&lt;/td&gt;
            &lt;td class="w-10 line-height-35 table-dropdown"&gt;
                &lt;div class="dropdown"&gt;
                	&lt;button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"&gt;
                		&lt;i class="fa fa-bars"&gt;&lt;/i&gt;
                		&lt;span class="caret"&gt;&lt;/span&gt;
                	&lt;/button&gt;
                	&lt;ul class="dropdown-menu"&gt;
                		&lt;li&gt;&lt;a href="#"&gt;&lt;i class="fa fa-undo"&gt;&lt;/i&gt; Quick reply&lt;/a&gt;&lt;/li&gt;
                		&lt;li&gt;&lt;a href="#"&gt;&lt;i class="fa fa-history"&gt;&lt;/i&gt; Full history&lt;/a&gt;&lt;/li&gt;
                		&lt;li&gt;&lt;a href="#"&gt;&lt;i class="fa fa-check color-success"&gt;&lt;/i&gt; Resolve issue&lt;/a&gt;&lt;/li&gt;
                        &lt;li&gt;&lt;a href="#"&gt;&lt;i class="fa fa-close color-danger"&gt;&lt;/i&gt; Close issue&lt;/a&gt;&lt;/li&gt;
                	&lt;/ul&gt;
                &lt;/div&gt;
            &lt;/td&gt;
		&lt;/tr&gt;
		...
	&lt;/tbody&gt;
&lt;/table&gt;
                                                        </code>
                                                    </pre>
                                                </div>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                               

                                    
                                    <!-- /.col-md-8 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>

    <div class="container page-content">


        <div class="loader"></div>

        <div>
            <ul class="breadcrumbs mini">
                <li><a href="Home.aspx"><span class="icon mif-home"></span></a></li>
                <li><a href="CorporateList.aspx"><span>Corporate List</span></a></li>
            </ul>
        </div>
        <br>

                
        <br/>
            <div class="formmargin center-block">
           
                <div class="col-sm-2">
                    <a href="AddCorporate.aspx" class="btn btn-default center-block">Add Corporate</a>
                    <!--<button type="submit" id="btnAddBank" class="btn btn-default center-block">Submit</button>-->
                </div>
        </div>
    
    </div>
    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/getbc.js" type="text/javascript"></script>

