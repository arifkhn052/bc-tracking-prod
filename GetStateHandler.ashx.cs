﻿using System.Web;
using BCEntities;
using BCTrackingBL;
using Newtonsoft.Json;
using BCTrackingServices;
using System.Collections.Generic;
using System;

namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for GetStateHandler
    /// </summary>
    public class GetStateHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context) {
            StateBL stateBl = new StateBL();
            context.Response.ContentType = "application/json";
            try {
                string userId = context.Session[Constants.USERSESSIONID].ToString();
                string queryMode = context.Request[BCTrackingServices.Constants.MODE];
                if (queryMode == "allstates") {
                    List<State> allStates = stateBl.GetStates(null);
                    context.Response.Write(JsonConvert.SerializeObject(allStates));
                }
                else if (queryMode == "state") {
                    string stateId = context.Request["stateId"];
                    string stateObject = context.Request["State"];
                    State stateToEdit = JsonConvert.DeserializeObject<State>(stateObject);
                    context.Response.Write("Got the object!");
                }
            }
            catch (Exception ex) {
                context.Response.Write("Some shit occured!");
            }
            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}