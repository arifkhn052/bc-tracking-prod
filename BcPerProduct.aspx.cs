﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using BCTrackingBL;
using BCEntities;

namespace BCTrackingWeb
{
    public partial class BcPerProduct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string getbcProduct()
        {
            // Paging parameters:
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);

            // Sorting parameters
            var iSortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var iSortDir = HttpContext.Current.Request.Params["sSortDir_0"];
            // Search parameters
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToLower();
            int totalRecords = 0;
            if (System.Web.HttpContext.Current.Session["UserInfo"] != null) ;
            var productId = HttpContext.Current.Request.Params["productId"].ToLower();
            var userid = HttpContext.Current.Request.Params["adminuserid"].ToLower();
            productId = productId.TrimEnd(',');


            var leadList = Common.getbcProduct(iDisplayStart, iDisplayLength, iSortCol, iSortDir, sSearch, out totalRecords, productId, userid);

            Func<BCProducts, object> order = p =>
            {
                if (iSortCol == 0)
                {
                    return p.ProductId;
                }

                else if (iSortCol == 1)
                {
                    return p.ProductName;
                }
                else if (iSortCol == 2)
                {
                    return p.BCId;
                }


                return p.ProductId;
            };

            //    // Define the order direction based on the iSortDir parameter
            if ("desc" == iSortDir)
            {
                leadList = leadList.OrderByDescending(order).ToList<BCProducts>();
            }
            else
            {
                leadList = leadList.OrderBy(order).ToList<BCProducts>();
            }
            var finalResult = leadList
                    .Select(p => new[] { p.ProductId.ToString(), p.ProductName, p.BCId.ToString(),p.BcName});

            int count = finalResult.Count();

            // prepare an anonymous object for JSON serialization
            var result = new
            {
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = finalResult
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            return json;
        }
    }
}