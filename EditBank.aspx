﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="EditBank.aspx.cs" Inherits="BCTrackingWeb.EditBank" %>

     <div  ng-init="GetStates()" >
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-12">
                    <h2 class="title">Edit Bank </h2>

                </div>


                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="#"  ui-sref="home"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="#" ui-sref="bankList"> Bank List</a></li>
                        <li><a >Edit Bank</a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">
                                     <h4>Bank Details</h4>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                                <div class="col-md-12">
                                                    <div class="panel">                                                       
                                                                                                                    
                                                        <div class="panel-body p-20">
                                                            <div class="panel-body" ng-hide="divVIEW">
                                                         
                                                                <div class="row">
                                                                    <div class="col-sm-2">
                                                                        <label for="fileImage" class=" control-label">Name</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                            <label for="fileImage"   id="lblName" placeholder="Name" class=" control-label">{{lblName}}</label>
                                                                    
                                                                    </div>
                                                                     <div class="col-sm-2">
                                                                        <label for="fileImage" class="control-label">Address</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                            <label for="fileImage"   id="lblAddress" placeholder="Name" class=" control-label">{{lblAddress}}</label>
                                                                  
                                                                    </div>
                                                                </div>


                                                             

                                                                <div class="row">
                                                                    <div class="col-sm-2">
                                                                        <label for="fileImage" class="control-label">Contact</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                         <label for="fileImage"   id="lblContact" placeholder="Name" class=" control-label">{{lblContact}}</label>
                                                                 
                                                                    </div>
                                                                          <div class="col-sm-2">
                                                                        <label for="fileImage" class="control-label">
                                                                               Email</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                         <label for="fileImage"   id="lblEmail" placeholder="Name" class=" control-label">{{lblEmail}}</label>
                                                                     
                                                                    </div>
                                                                </div>

                                                           

                                                            </div>
                                                            <div class="panel-body" ng-hide="divEdit">
                                                         
                                                                <div class="row">
                                                                    <div class="col-sm-2">
                                                                        <label for="fileImage" class=" control-label">Name</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                      
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtName" placeholder="Name"
                                                                               parsley-trigger="change" required>
                                                                    </div>
                                                                      <div class="col-sm-2">
                                                                        <label for="fileImage" class="control-label">Address</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight "
                                                                               id="txtAddress" placeholder="Address"
                                                                               parsley-trigger="change" required>
                                                                    </div>
                                                                </div>



                                                                <div class="row">
                                                                    <div class="col-sm-2">
                                                                        <label for="fileImage" class="control-label">Contact</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtContact" placeholder="Contact"
                                                                               parsley-trigger="change" required>
                                                                    </div>
                                                                          <div class="col-sm-2">
                                                                        <label for="fileImage" class="control-label">
                                                                               Email</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtEmail" placeholder="Email"
                                                                               parsley-trigger="change" required>
                                                                    </div>
                                                                </div>

                                                          

                                                            </div>
                                    <div  class="row pull-right">                                     
                                    <div class="col-sm-6">
                                        <a class="btn btn-danger btn-sm" ng-click="deleteBank()">
                                            <span class="fa fa-trash-o" aria-hidden="true"></span>&nbsp;Delete</a>
                                    </div>


                                    <div class="col-sm-2">
                                        <a ng-click="editView()" class="btn btn-primary btn-sm" id="tabedit">
                                            <span class="fa fa-pencil-square-o" aria-hidden="true"></span>&nbsp;{{btnedit}} </a>
                                    </div>
                                </div>
                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                      
                                                        
                                            
                                                    </div>
                                                   
                                                    <!-- /.panel -->
                                                </div>
                                            </div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                             <%--       <li role="presentation" class="active"><a href="#home" aria-controls="home"
                                                                              role="tab" data-toggle="tab"><label
                                            for="exampleInputEmail1">Bank Details</label> </a></li>--%>
                                            <%--<li role="presentation"><a ng-click="displayTab('circle')" aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Circle
                                    </label> </a></li>--%>
                               <%--     <li role="presentation"><a ng-click="displayTab('profile')" aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Branches
                                    </label> </a></li>--%>


                                </ul>

                                <!-- Tab panes -->
                               
                                    <%--<div role="tabpanel" class="tab-pane" id="home">


                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="panel">

                                                        <div class="panel-body p-20">
                                                            <div class="panel-body">

                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class=" control-label">Name</label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtName" placeholder="Name"
                                                                               parsley-trigger="change" required>
                                                                    </div>
                                                                </div>


                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class="control-label">Address</label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight "
                                                                               id="txtAddress" placeholder="Address"
                                                                               parsley-trigger="change" required>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class="control-label">Contact</label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtContact" placeholder="Contact"
                                                                               parsley-trigger="change" required>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class="control-label">
                                                                               Email</label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtEmail" placeholder="Email"
                                                                               parsley-trigger="change" required>
                                                                    </div>
                                                                </div>

                                                            </div>


                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                    </div>
                                                   
                                                    <!-- /.panel -->
                                                </div>
                                            </div>
                                        </div>


                                    </div>--%>

                                    <div role="tabpanel" class="tab-pane"  ng-hide="divcircle">


                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    
                                                    <div class="panel">
                                                        <div class="panel-heading">
                                                            <div class="panel-title">
                                                                Circles
                                                            </div>
                                                        </div>
                                                        <div class="panel-body p-20">

                                                            <div class="panel-body">
                                                                <div class="row">


                                                                    <div class="col-sm-12">
                                                                        <strong>Circle Name</strong>
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtCircleName"
                                                                               placeholder="Circle Name" />
                                                                               
                                                                    </div>


                                                                </div>
                                                                <div class="row pull-right" id="Div2">
                                                                    <div class="col-sm-12">
                                                                        <input type="button"
                                                                               class="btn btn-primary  btn-sm form-control"
                                                                               ng-click="addCircle()"
                                                                               ng-hide=""
                                                                               value="Add More  " id="Button2">
                                                                    </div>
                                                                </div>
                                                                <br/>
                                                                <br/>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <table id="" class="table ">
                                                                            <tr
                                                                            ">
                                                                            <td><strong>Circle</strong>
                                                                            </td>
                                                                            <td><strong>Remove</strong></td>
                                                                            </tr>
                                                                            <tr ng-repeat="item in circles">
                                                                                <td>{{item.CircleName}}</td>

                                                                                <td>
                                                                                    <div ng-click="removeCircle($index)"
                                                                                         class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>


                                                            </div>


                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                    </div>
                                                    <div class="panel">
                                                        <div class="panel-heading">
                                                            <div class="panel-title">
                                                                Zones
                                                            </div>
                                                        </div>
                                                        <div class="panel-body p-20">

                                                            <div class="panel-body">
                                                                <div class="row">

                                                                    <div class="col-sm-6">
                                                                        <strong>Zone Name</strong>
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtZoneName" placeholder="Zone Name">
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <strong>Circle</strong>
                                                                     
                                                                             <select type="text" class="form-control formcontrolheight" id="selZoneCircle"></select>
                                                                    </div>


                                                                </div>
                                                                <div class="row pull-right" id="Div2">
                                                                    <div class="col-sm-12">
                                                                        <input type="button"
                                                                               class="btn btn-primary  btn-sm form-control"
                                                                               ng-click="addZone()"
                                                                               ng-hide=""
                                                                               value="Add More  " id="Button2">
                                                                    </div>
                                                                </div>
                                                                <br/>
                                                                <br/>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <table id="" class="table ">
                                                                            <tr
                                                                            ">
                                                                            <td><strong>Zone</strong>
                                                                            </td>
                                                                            <td><strong>Circle</strong>
                                                                            </td>
                                                                            <td><strong>Remove</strong></td>
                                                                            </tr>
                                                                            <tr ng-repeat="item in zoneList ">
                                                                                <td>{{item.ZoneName}}</td>
                                                                                <td>{{item.CircleName}}</td>
                                                                                <td>
                                                                                    <div ng-click="removeZone($index,item.cName)"
                                                                                         class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>


                                                            </div>


                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                    </div>
                                                    <div class="panel">
                                                        <div class="panel-heading">
                                                            <div class="panel-title">
                                                                Regions
                                                            </div>
                                                        </div>
                                                        <div class="panel-body p-20">

                                                            <div class="panel-body">
                                                                <div class="row">


                                                                    <div class="col-sm-6">
                                                                        <strong>Region Name</strong>
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtRegionName"
                                                                               placeholder="Region Name">
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <strong>Zone</strong>
                                                                       <select type="text" class="form-control formcontrolheight" id="selRegionZone"></select>
                                                                    </div>


                                                                </div>
                                                                <div class="row pull-right" id="Div2">
                                                                    <div class="col-sm-12">
                                                                        <input type="button"
                                                                               class="btn btn-primary  btn-sm form-control"
                                                                               ng-click="addRegion()"
                                                                               ng-hide="!regionList "
                                                                               value="Add More  " id="Button2">
                                                                    </div>
                                                                </div>
                                                                <br/>
                                                                <br/>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <table id="" class="table ">
                                                                            <tr
                                                                            ">
                                                                            <td><strong>Region</strong>
                                                                            </td>
                                                                            <td><strong>Zone</strong>
                                                                            </td>
                                                                            <td><strong>Remove</strong></td>
                                                                            </tr>
                                                                            <tr ng-repeat="item in regionList ">
                                                                                <td>{{item.RegionName}}</td>
                                                                                <td>{{item.ZoneName}}</td>
                                                                                <td>
                                                                                    <div ng-click="removeRegion($index,item.zone)"
                                                                                         class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>


                                                            </div>


                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                    </div>
                                                    <!-- /.panel -->
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div role="tabpanel" class="tab-pane"   ng-hide="divprofile">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                           <div class="col-sm-4">
                                                                            <strong>Branch Code <a style="color:red">*</a></strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchCode"
                                                                                   placeholder="Branch Code">
                                                                        </div>

                                                                        <div class="col-sm-4">
                                                                            <strong>Branch Name <a style="color:red">*</a></strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchName"
                                                                                   placeholder="Branch Name">
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>Region <a style="color:red">*</a></strong>
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBranchRegion"></select>
                                                                        </div>


                                                                        


                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <strong>IFSC Code <a style="color:red">*</a></strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchIFSCCode"
                                                                                   placeholder="IFSC Code">
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <strong>Address</strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchAddress"
                                                                                   placeholder="Branch Address">
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>State <a style="color:red">*</a></strong>
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBranchState"></select>
                                                                        </div>


                                                                        


                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <strong>District <a style="color:red">*</a></strong>
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBranchDistrict"></select>
                                                                        </div>
                                                                 <%--       <div class="col-sm-4">
                                                                            <strong>Taluka</strong>
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBranchTaluka"></select>
                                                                        </div>--%>


                                                                        <div class="col-sm-4">
                                                                            <strong>Sub District <a style="color:red">*</a></strong>
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBranchCity"></select>
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>Village  <a style="color:red">*</a></strong>
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBranchVillage"></select>
                                                                        </div>


                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <strong>Pin Code</strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchPinCode"
                                                                                   placeholder="Pin Code">
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>Contact</strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight "
                                                                                   id="txtBranchContact"
                                                                                   placeholder="Contact">
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>Email<a style="color:red">*</a></strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchEmail"
                                                                                   placeholder="Email">
                                                                        </div>


                                                                    </div>
                                                                    <div class="row pull-right" id="Div2">
                                                                        <div class="col-sm-12">
                                                                            <input type="button"
                                                                                   class="btn btn-primary  btn-sm form-control"
                                                                                   ng-click="addBranch()"
                                                                                   ng-hide="!brancheslist "
                                                                                   value="Add More  " id="Button2">
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                    <br/>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Branch Name</strong>
                                                                                </td>
 <td><strong>Branch Code</strong>
                                                                                </td>
                                                                                    
                                                                                <td><strong>Region</strong></td>
                                                                                <td><strong> Address</strong></td>
                                                                                <td><strong>Contact</strong></td>
                                                                                <td><strong>Email</strong></td>
                                                                                <td><strong>Remove</strong></td>
                                                                                </tr>
                                                                                <tr ng-repeat="item in brancheslist ">
                                                                                    <td>{{item.BranchName}}</td>
                                                                                     <td>{{item.BranchCode}}</td>
                                                                                    <td>{{item.RegionName}}</td>
                                                                                     <td>{{item.Address}}</td>
                                                                                <td>{{item.ContactNumber}}</td>
                                                                               <td>{{item.Email}}</td>
                                                                           
                                                                                    <td>
                                                                                        <div ng-click="removeBranch($index,item.RegionName)"
                                                                                             class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>
                                    <div class="formmargin center-block">
                    <div class="center-block">
                        <button type="button"  ng-click="addBank()" class="btn btn-success center-block">Update</button>
                    </div>
                </div>

                               

                             
                    
                
                                <!-- /.src-code -->

                            </div>
                              
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>
      
        <script src="ot/js/jquery/jquery-2.2.4.min.js"></script>
        

   <%-- <script src="js/jquery-1.12.2.min.js"></script>--%>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/parsley.min.js" defer></script>
    <script src="js/main.js"></script>
    <script src="js/banks.js"></script>

