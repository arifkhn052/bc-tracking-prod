﻿
BCTRACK.controller("singleEntryController", ["$scope", "$location", "$window", "$http", function ($scope, $location, $window, $http) {
    $scope.btnedit = 'Edit';
    $scope.divVIEW = false;
    $scope.divEdit = true;

    $scope.editView = function () {
        
        if ($scope.btnedit == "Edit") {
            $scope.btnedit = 'View';
            $scope.divVIEW = true;
            $scope.divEdit = false;
        }
        else {
            $scope.btnedit = 'Edit';
            $scope.divVIEW = false;
            $scope.divEdit = true;
        }

    }
    var b = $state.params.bankId;
    var bcId = '';
    if (b != "")
        bcId = parseInt(b);


    var baseControlName = '';
    var errorMessage = "";
    var certs = [];
    $scope.certsList = [];
    var exps = [];
    $scope.expsList = [];
    var areas = [];
    $scope.areasList = [];
    var devices = [];
    $scope.devicesList = [];
    var conns = [];
    $scope.connsList = [];
    var bcorrespondene = new Object();
    
    function getParameterByName(name) //courtesy Artem
    {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else {
            if ((results[1].indexOf('?')) > 0)
                return decodeURIComponent(results[1].substring(0, results[1].indexOf('?')).replace(/\+/g, " "));
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
    $scope.AddMoreCerts = function(){ 
        var cert = new Object();
        cert.DateOfPassing = $('#' + baseControlName + 'txtPassingDate').val();
        cert.InstituteId = $('#' + baseControlName + 'SelInstitute').val();
        cert.CourseId = $('#' + baseControlName + 'SelCourse').val();
        cert.Grade = $('#' + baseControlName + 'txtGrades').val();
        certs.push(cert);
        var institueName = $('#'+baseControlName+ 'SelInstitute :selected').text();
        var Course = $('#' + baseControlName + 'SelCourse :selected').text();
        $scope.certsList.push({
            "DateOfPassing": cert.DateOfPassing,
            "InstituteId": institueName,
            "CourseId": Course,
            "Grade": cert.Grade

        });
        $('#' + baseControlName + 'txtPassingDate').val("");
        $('#' + baseControlName + 'SelInstitute').val("");
        $('#' + baseControlName + 'SelCourse').val("");
        $('#' + baseControlName + 'txtGrades').val("");
    };
    $scope.removeCerts = function (index) {
        certs.splice(index, 1);
        $scope.certsList.splice(index, 1);
    }

    $scope.AddMoreExperience = function(){        

        var exp = new Object();
        exp.BankId = $('#' + baseControlName + 'selPreviousExpBank').val();
        exp.Branchid = $('#' + baseControlName + 'selPreviousExpBranch').val();
        // exp.FromDate = $('#' + baseControlName + 'txtFromDateExp').val();
        var frmDate = new Date($('#' + baseControlName + 'txtFromDateExp').val());
        var dtString = frmDate.getFullYear() + '-' + (frmDate.getMonth() + 1) + '-' + frmDate.getDate();
        exp.FromDate = dtString;
        var to_Date = new Date($('#' + baseControlName + 'txtToDateExp').val());
        var dtToString = to_Date.getFullYear() + '-' + (to_Date.getMonth() + 1) + '-' + to_Date.getDate();
        exp.ToDate = dtToString;
        //exp.ToDate = $('#' + baseControlName + 'txtToDateExp').val();
        exp.Reason = $('#' + baseControlName + 'txtReasons').val();
        exps.push(exp);
        $scope.expsList.push({
            "BankId": $('#' + baseControlName + 'selPreviousExpBank :selected').text(),
            "Branchid": $('#' + baseControlName + 'selPreviousExpBranch :selected').text(),
            "FromDate":exp.FromDate,
            "ToDate":  exp.ToDate,
            "Reason" :  exp.Reason
        
        });
        $('#' + baseControlName + 'selPreviousExpBank').val("");
        $('#' + baseControlName + 'selPreviousExpBranch').val("");
        $('#' + baseControlName + 'txtFromDateExp').val("");
        $('#' + baseControlName + 'txtToDateExp').val("");
        $('#' + baseControlName + 'txtReasons').val("");

    };


    $scope.removeExperience = function (index) {
        exps.splice(index, 1);
        $scope.expsList.splice(index, 1);
    }
    
    $scope.AddMoreOpAreas = function () {       
        var area = new Object();
        area.VillageCode = $('#' + baseControlName + 'txtOperationVillageCode').val();
        area.VillageDetail = $('#' + baseControlName + 'txtOperationVillageDetail').val();
        areas.push(area);
        $scope.areasList.push(area);
        
        $('#' + baseControlName + 'txtOperationVillageCode').val("");
        $('#' + baseControlName + 'txtOperationVillageDetail').val("");

    };
    $scope.removeOpAreas = function (index) {
        areas.splice(index, 1);
        $scope.areasList.splice(index, 1);
    }

    $scope.AddMoreDevices = function () {
        var device = new Object();
        device.GivenOn = $('#' + baseControlName + 'txtGivenOn').val();
        device.Device = $('#' + baseControlName + 'selDeviceName').val();
        devices.push(device);
        $scope.devicesList.push({
            "GivenOn": device.GivenOn,
            "Device": $('#' + baseControlName + 'selDeviceName :selected').text()
        })
        $('#' + baseControlName + 'txtGivenOn').val("");
        $('#' + baseControlName + 'selDeviceName').val("");

    };

    $scope.removeDevices = function (index) {
        devices.splice(index, 1);
        $scope.devicesList.splice(index, 1);
    }

    $scope.AddMoreConnectivity = function (index) {
        var conn = new Object();
        conn.ConnectivityMode = $('#' + baseControlName + 'selConnType').val();
        conn.ConnectivityProvider = $('#' + baseControlName + 'txtProvider').val();
        conn.ContactNumber = $('#' + baseControlName + 'txtNumber').val();
        conns.push(conn);
        $scope.connsList.push(conn);


        $('#' + baseControlName + 'selConnType').val("");
        $('#' + baseControlName + 'txtProvider').val("");
        $('#' + baseControlName + 'txtNumber').val("");

    };
    $scope.removeConnectivity = function (index) {
        conns.splice(index, 1);
        $scope.connsList.splice(index, 1);
    }




    function validate() {

        errorMsg = '';

        var email = $('#txtEmail').val();      
        var name = $('#txtName').val();
        var phone = $('#txtPhone1').val();
        var dob = $('#txtDOB').val();
        var MinCash = $('#txtMinCash').val();
        var MonthlyFixed = $('#txtMonthlyFixed').val();
        var MonthlyVariable = $('#txtMonthlyVariable').val();

        

        var result = true;
        if (name == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Name.";
            result = false;
        }
        if (dob == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select Date of Birth";
            result = false;
        }
        if (phone == "" || phone.length > 10 || phone.length < 10 || isNaN(phone)) {
            $('#divPhone').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Valid First Mobile Number.";
            result = false;
        }
        else {
            $("#divPhone").removeClass("form-group has-error");
        }
        if (!IsEmail(email)) {
            $('#txtEmail').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter valid email Id.";
            result = false;
        }
        else {
            $("#divEmail").removeClass("form-group has-error");
        }

      
     
      

      
        if (MinCash == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Minimum Cash Handling Limit.";
            result = false;
        }
        if (MonthlyFixed == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Monthly Fixed Renumeration.";
            result = false;
        }
        if (MonthlyVariable == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Monthly Variable Renumeration.";
            result = false;
        }


        return result;
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    $scope.addSingleEntry = function () {

        if (validate() == true) {
            bcorrespondene.Name = $('#' + baseControlName + 'txtName').val();
            //Handle Duplicate Image Name Issue

            //bcorrespondene.ImagePath = $('#' + baseControlName + 'fileImage.PostedFile.FileName').val();


            //append ts to img name
            var fileName = $('#' + baseControlName + 'fileImage').val();
            var d = new Date();
            var ts = '_' + d.getDate().toString() + '-' + (d.getMonth() + 1).toString() + '-' + d.getFullYear().toString() + '-' + d.getHours().toString() + '-' + d.getMinutes().toString();
            bcorrespondene.ImagePath = (fileName.substring(12, fileName.length - 4)).concat(ts);

            bcorrespondene.Gender = $('#' + baseControlName + 'selGender').val();
            bcorrespondene.DOB = $('#' + baseControlName + 'txtDOB').val();
            bcorrespondene.FatherName = $('#' + baseControlName + 'txtFather').val();
            bcorrespondene.SpouseName = $('#' + baseControlName + 'txtSpouse').val();
            bcorrespondene.Category = $('#' + baseControlName + 'selCategory').val();
            if ($('#' + baseControlName + 'radHandicapNo').checked)
                bcorrespondene.Handicap = $('#' + baseControlName + 'radHandicapNo').val()
            else
                bcorrespondene.Handicap = $('#' + baseControlName + 'radHandicapYes').val()

            // bcorrespondene.Handicap = $('#' + baseControlName + 'radHandicapNo.Checked ? radHandicapNo).val() : radHandicapYes').val();
            bcorrespondene.PhoneNumber1 = $('#' + baseControlName + 'txtPhone1').val();
            bcorrespondene.PhoneNumber2 = $('#' + baseControlName + 'txtPhone2').val();
            bcorrespondene.PhoneNumber3 = $('#' + baseControlName + 'txtPhone3').val();
            bcorrespondene.Email = $('#' + baseControlName + 'txtEmail').val();
            bcorrespondene.AadharCard = $('#' + baseControlName + 'txtAadharCard').val();
            bcorrespondene.PanCard = $('#' + baseControlName + 'txtPanCard').val();
            bcorrespondene.VoterCard = $('#' + baseControlName + 'txtVoterId').val();
            bcorrespondene.DriverLicense = $('#' + baseControlName + 'txtDriverLic').val();
            bcorrespondene.NregaCard = $('#' + baseControlName + 'txtNregaCard').val();
            bcorrespondene.RationCard = $('#' + baseControlName + 'txtRationCard').val();
            bcorrespondene.State = $('#' + baseControlName + 'selAddressState').val();
            bcorrespondene.City = $('#' + baseControlName + 'selAddressCity').val();
            bcorrespondene.District = $('#' + baseControlName + 'selAddressDistrict').val();
            bcorrespondene.Subdistrict = $('#' + baseControlName + 'txtAddressSubDistrict').val();
            bcorrespondene.Area = $('#' + baseControlName + 'txtAddressArea').val();
            bcorrespondene.PinCode = $('#' + baseControlName + 'txtPinCode').val();
            bcorrespondene.AlternateOccupationType = $('#' + baseControlName + 'SelAlternateOccupation').val();
            bcorrespondene.AlternateOccupationDetail = $('#' + baseControlName + 'txtAlternateOccupationDtl').val();
            bcorrespondene.UniqueIdentificationNumber = $('#' + baseControlName + 'txtUniqueId').val();
            bcorrespondene.BankReferenceNumber = $('#' + baseControlName + 'txtBankReferenceNumber').val();
            bcorrespondene.Qualification = $('#' + baseControlName + 'selQualification').val();

            bcorrespondene.OtherQualification = $('#' + baseControlName + 'txtOtherQualification').val();
            bcorrespondene.CorporateId = $('#' + baseControlName + 'selCorporate').val();


            bcorrespondene.Allocation = $('#' + baseControlName + 'radAllocatedNo').checked ? "No" : "Yes";
            bcorrespondene.isAllocated = (bcorrespondene.Allocation == 'Yes');
            if (bcorrespondene.Allocation == 'Yes') {

                bcorrespondene.AppointmentDate = $('#' + baseControlName + 'txtAppointmentDate').val();
                bcorrespondene.AllocationIFSCCode = $('#' + baseControlName + 'ifscCode').val();

                bcorrespondene.AllocationBankId = $('#' + baseControlName + 'selAllocationBank').val();
                bcorrespondene.AllocationBranchId = $('#' + baseControlName + 'selAllocationBranch').val();

                bcorrespondene.BCType = $('#' + baseControlName + 'selBCType').val();
                bcorrespondene.WorkingDays = $('#' + baseControlName + 'txtWorkingDays').val();
                bcorrespondene.WorkingHours = $('#' + baseControlName + 'txtWorkingHours').val();
                bcorrespondene.PLPostalAddress = $('#' + baseControlName + 'txtPostalAddress').val();
                bcorrespondene.PLVillageCode = $('#' + baseControlName + 'txtVillageCode1').val();
                bcorrespondene.PLVillageDetail = $('#' + baseControlName + 'txtVillageDetail1').val();
                bcorrespondene.PLStateId = $('#' + baseControlName + 'selPLState').val();
                //bcorrespondene.PLDistrictId = $('#' + baseControlName + 'selPLDistrict').val();
                bcorrespondene.PLTaluk = $('#' + baseControlName + 'selPLTaluk').val();
                bcorrespondene.PLPinCode = $('#' + baseControlName + 'txtPLPinCode').val();
                bcorrespondene.MinimumCashHandlingLimit = $('#' + baseControlName + 'txtMinCash').val();
                bcorrespondene.MonthlyFixedRenumeration = $('#' + baseControlName + 'txtMonthlyFixed').val();
                bcorrespondene.MonthlyVariableRenumeration = $('#' + baseControlName + 'txtMonthlyVariable').val();
            }


            $.ajax({
                url: "InsertUpdateHandler.ashx",
                method: "POST",
                datatype: 'json',
                data: {
                    "bcorr": JSON.stringify(bcorrespondene), "allCerts": JSON.stringify(certs), "allExps": JSON.stringify(exps),
                    "allAreas": JSON.stringify(areas), "allDevices": JSON.stringify(devices), "allConns": JSON.stringify(conns)
                },
                success: function (data) {
                    //handle img upload on btn click
                    var fileName = $('#' + baseControlName + 'fileImage').val().replace(/^.*[\\\/]/, '');
                    var currFile = $('#' + baseControlName + 'fileImage')[0].files[0];
                    var fieldId = $('#' + baseControlName + 'fileImage').attr('id');
                    var xhr = new XMLHttpRequest();
                    var uploadData = new FormData();
                    var url = 'FileUploader.ashx';
                    uploadData.append('file', currFile);
                    xhr.open("POST", url, true);
                    xhr.send(uploadData);

                    $("#getCodeModal").modal('show');
                    //alert('posted');                
                }
            });

        }

        else {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
    }
  
    $scope.getBcDetails = function () {
        
        if (bcId != '') {
            $('.loader').show();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: "GetBCHandler.ashx?mode=One&bcId=" + bcId,
                async: false,
                success: function (bankc) {
                    //console('Got it!');
                    if (bankc != null) {
                        bcorrespondene = bankc;
                        //Set Controls
                        $scope.lblName = bankc.Name;
                        $scope.lblGender = bankc.Gender;
                        $scope.lblDOB = bankc.dtString;
                        $('#' + baseControlName + 'txtName').val(bankc.Name);
                        $('#' + baseControlName + 'selGender').val(bankc.Gender);                 
                    
                        var dt = new Date(bankc.DOB);
                        //       var dtString = dt.toDateString("YYYY-MM-DD");
                        var dtString = dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate();

                        $('#' + baseControlName + 'txtDOB').val(dtString);
                        
                        $scope.lblFather = bankc.FatherName;
                        $('#' + baseControlName + 'txtFather').val(bankc.FatherName);
                        $scope.lblSpouse = bankc.SpouseName;
                        $('#' + baseControlName + 'txtSpouse').val(bankc.SpouseName);
                        $('#' + baseControlName + 'selCategory').val(bankc.Category);
                        $scope.lblCategory = bankc.Category;
                        if (bankc.Handicap == 'Yes')
                        {
                            $scope.lblhand = "Yes";
                            $('#' + baseControlName + 'radHandicapYes').checked = true;
                        }                           
                        else
                        {
                            $scope.lblhand = "No";
                            $('#' + baseControlName + 'radHandicapNo').checked = true;
                        }
                          

                        $scope.lblPhone1 = bankc.PhoneNumber1;
                        $scope.lblPhone2 = bankc.PhoneNumber2;
                        $scope.lblPhone3 = bankc.PhoneNumber3;
                        $scope.lblEmail = bankc.Email;
                        $('#' + baseControlName + 'txtPhone1').val(bankc.PhoneNumber1);
                        $('#' + baseControlName + 'txtPhone2').val(bankc.PhoneNumber2);
                        $('#' + baseControlName + 'txtPhone3').val(bankc.PhoneNumber3);
                        $('#' + baseControlName + 'txtEmail').val(bankc.Email);
                        $('#' + baseControlName + 'txtAadharCard').val(bankc.AadharCard);
                        $('#' + baseControlName + 'txtPanCard').val(bankc.PanCard);
                        $('#' + baseControlName + 'txtVoterId').val(bankc.VoterCard);
                        $('#' + baseControlName + 'txtDriverLic').val(bankc.DriverLicense);
                        $('#' + baseControlName + 'txtNregaCard').val(bankc.NregaCard);
                        $('#' + baseControlName + 'txtRationCard').val(bankc.RationCard);
                        $('#' + baseControlName + 'selAddressState').val(bankc.State);
                        $('#' + baseControlName + 'selAddressCity').val(bankc.City);
                        $('#' + baseControlName + 'selAddressDistrict').val(bankc.District);
                        $('#' + baseControlName + 'txtAddressSubDistrict').val(bankc.Subdistrict);
                        $('#' + baseControlName + 'txtAddressArea').val(bankc.Area);
                        $('#' + baseControlName + 'txtPinCode').val(bankc.PinCode);
                        $('#' + baseControlName + 'SelAlternateOccupation').val(bankc.AlternateOccupationType);
                        $('#' + baseControlName + 'txtAlternateOccupationDtl').val(bankc.AlternateOccupationDetail);
                        $('#' + baseControlName + 'txtUniqueId').val(bankc.UniqueIdentificationNumber);
                        $('#' + baseControlName + 'txtBankReferenceNumber').val(bankc.BankReferenceNumber);

                        $('#' + baseControlName + 'selQualification').val(bankc.Qualification);

                        $('#' + baseControlName + 'txtOtherQualification').val(bankc.OtherQualification);
                        $('#' + baseControlName + 'selCorporate').val(bankc.CorporateId);

                        if (bankc.isAllocated)
                            $('#radAllocatedYes').prop('checked', true);
                        else {
                            $('#radAllocatedNo').prop('checked', true);
                            $('#divAllocationDetails').hide();
                        }

                        if (bankc.AppointmentDate != null && bankc.AppointmentDate != undefined) {
                            var dt2 = new Date(bankc.AppointmentDate);
                            var dt2String = dt2.getFullYear() + '-' + (dt2.getMonth() + 1) + '-' + dt2.getDate();

                            $('#' + baseControlName + 'txtAppointmentDate').val(dtString);
                        }


                        $('#' + baseControlName + 'ifscCode').val(bankc.AllocationIFSCCode);

                        $('#' + baseControlName + 'selAllocationBank').val(bankc.AllocationBankId);
                        $('#' + baseControlName + 'selAllocationBranch').val(bankc.AllocationBranchId);

                        $('#' + baseControlName + 'selBCType').val(bankc.BCType);
                        $('#' + baseControlName + 'txtWorkingDays').val(bankc.WorkingDays);
                        $('#' + baseControlName + 'txtWorkingHours').val(bankc.WorkingHours);
                        $('#' + baseControlName + 'txtPostalAddress').val(bankc.PLPostalAddress);
                        $('#' + baseControlName + 'txtVillageCode1').val(bankc.PLVillageCode);
                        $('#' + baseControlName + 'txtVillageDetail1').val(bankc.PLVillageDetail);
                        $('#' + baseControlName + 'selPLState').val(bankc.PLStateId);
                        //$('#' + baseControlName + 'selPLDistrict').val(bankc.PLDistrictId);
                        $('#' + baseControlName + 'selPLTaluk').val(bankc.PLTaluk);
                        $('#' + baseControlName + 'txtPLPinCode').val(bankc.PLPinCode);
                        //$('#' + baseControlName + 'selProductsOffered').val(bankc.ProductsOffered);
                        $('#' + baseControlName + 'txtMinCash').val(bankc.MinimumCashHandlingLimit);
                        $('#' + baseControlName + 'txtMonthlyFixed').val(bankc.MonthlyFixedRenumeration);
                        $('#' + baseControlName + 'txtMonthlyVariable').val(bankc.MonthlyVariableRenumeration);

                        $('#lblName').html(bankc.Name);

                        if (bankc.Certifications != null) {
                            $scope.certsList = bankc.Certifications;
                            for (var c = 0; c < bankc.Certifications.length; c++) {
                                var cert = bankc.Certifications[c];
                                certs.push(cert);
                              

                            }
                        }
                        if (bankc.PreviousExperience != null) {
                            for (var c = 0; c < bankc.PreviousExperience.length; c++) {
                                $scope.expsList = bankc.PreviousExperience;
                                var exp = bankc.PreviousExperience[c];                           
                                exps.push(exp);
                               
                            }

                        }


                        if (bankc.OperationalAreas != null) {
                            $scope.areasList = bankc.OperationalAreas;
                            for (var c = 0; c < bankc.OperationalAreas.length; c++) {
                                var oprarea = bankc.OperationalAreas[c];
                                areas.push(oprarea);                                                      
                            }
                        }


                        if (bankc.Devices != null) {
                            $scope.devicesList = bankc.Devices;
                            for (var c = 0; c < bankc.Devices.length; c++) {
                                var device = bankc.Devices[c];
                                devices.push(device);
                              
                            }
                        }


                        if (bankc.ConnectivityDetails != null) {
                            $scope.connsList = bankc.ConnectivityDetails;
                            for (var c = 0; c < bankc.ConnectivityDetails.length; c++) {
                                var conn = bankc.ConnectivityDetails[c];
                                conns.push(conn);
                             
                            }
                        }

                        if (bankc.Products != null) {
                            for (var c = 0; c < bankc.Products.length; c++) {
                                var prod = bankc.AddProducts[c];
                                prods.push(prod);
                                AddProducts(t7, prod.minCashHandlingLimit, prod.MonthlyFixedRenumeration, prod.MonthlyVariableRenumeration);
                                //prods.push(bankc.Products[c]);
                                //AddProducts(t7, bankc.ProductName, bankc.MinimumCashHandlingLimit, bankc.MonthlyFixedRenumeration, bankc.MonthlyVariableRenumeration);
                            }
                        }


                    }

                    $('.loader').hide();

                },
                error: function (err) {
                    alert(err);
                    $('.loader').hide();
                }
            });
        }
    }

  
    


    function ShowDialog(message, height, width, title, padding, icon, showOkButton, link) {

        $("#helloModalMessage").html(message);

        $('#helloModal').modal('show');

        if (showOkButton == false) {
            $("#closemodalbutton").show();
            $("#redirectbutton").hide();
        }
        else {
            if (link != '')
                $("#redirectbutton").attr("href", link);
            $("#closemodalbutton").hide();
            $("#redirectbutton").show();
        }

        //$.Dialog({
        //    overlay: true,
        //    shadow: true,
        //    flat: true,
        //    title: title,
        //    icon: icon,
        //    content: '',
        //    width: width,
        //    padding: padding,
        //    height: height,//600,400
        //    onShow: function (_dialog) {
        //        var content = _dialog.children('.content');
        //        content.html(message);
        //        $.Metro.initInputs();
        //    }
        //});
    }



}]);