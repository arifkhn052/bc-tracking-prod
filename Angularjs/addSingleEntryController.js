﻿

bctrackApp.controller("addSingleEntryController", function ($scope, $state, $timeout) {
    let allBanks = new Array();
    $('#ifscCode').on('change', function () {

        ifsccode = $('#ifscCode').val();
        
        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=IFSC&IfscCode=" + ifsccode;
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {
                if (Banks.length !== 0) {
                    $('#selAllocationBank').empty();
                    $('#selAllocationBranch').empty();
                    $('#selAllocationBank').append($('<option>', {
                        value: Banks[0].BankId,
                        text: Banks[0].BankName
                    }));
                    $('#selAllocationBranch').append($('<option>', {
                        value: Banks[0].BranchId,
                        text: Banks[0].BranchName
                    }));
                }
                else {
                    alert("IFSC Code  does not Exist!!");
                    $('#ifscCode').val('');
                    $('#selAllocationBank').empty();
                    $('#selAllocationBranch').empty();

                }

            },
            error: function (err) {

            }
        });
        return banks;

    });
    $('#txt_ifsccode').on('change', function () {
        var bank_id = $('#selPreviousExpBank').val();
        var ifsccode = $('#txt_ifsccode').val();
        
        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=IFSC&IfscCode=" + ifsccode + "&bankId=" + bank_id;
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {
                if (Banks.length !== 0) {
                    $('#selAllocationBank').empty();
                    $('#selPreviousExpBranch').empty();
                    //$('#selAllocationBank').append($('<option>', {
                    //    value: Banks[0].BankId,
                    //    text: Banks[0].BankName
                    //}));
                    $('#selPreviousExpBranch').append($('<option>', {
                        value: Banks[0].BranchId,
                        text: Banks[0].BranchName
                    }));
                }
                else {
                    alert("IFSC Code  does not Exist!! or Not belongs to this bank!!");
                    $('#txt_ifsccode').val('');
                    // $('#selAllocationBank').empty();
                    $('#selPreviousExpBranch').empty();

                }

            },
            error: function (err) {

            }
        });
        return banks;

    });

    $('#div_bankdtl').hide();
    $('#div_ifsccode').hide();

    $('#selPreviousExpBank').on('change', function () {
      

    });
    $scope.GetBanks = function () {

        var requestUrl = 'api/getBankHandler.ashx?mode=All';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            url: requestUrl,
            async: false,
            success: function (bankList) {
                 
                if (bankList.length != 0) {
                    for (let i = 0; i < bankList.length; i++) {
                        let bank = new Object();
                        bank.BankId = bankList[i].BankId;
                        bank.BankName = bankList[i].BankName;
                        bank.BankCircles = bankList[i].BankCircles;
                        bank.Branches = bankList[i].Branches;
                        allBanks.push(bank);
                    }
                }

                if (allBanks.length != 0) {
                    $('#selPreviousExpBank').append($('<option>', {
                        value: '-1',
                        text: '--Select--'
                    }));
                    $('#selPreviousExpBank').append($('<option>', {
                        value: '0',
                        text: 'Other'
                    }));
                    for (let i = 0; i < allBanks.length; i++) {

                        $('#selPreviousExpBank').append($('<option>', {
                            value: allBanks[i].BankId,
                            text: allBanks[i].BankName
                        }));

                    }

                    $('#selAllocationBank').append($('<option>', {
                        value: '-1',
                        text: '--Select--'
                    }));
                    for (let i = 0; i < allBanks.length; i++) {

                        $('#selAllocationBank').append($('<option>', {
                            value: allBanks[i].BankId,
                            text: allBanks[i].BankName
                        }));

                    }

                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allBanks;
    }

    $('#selPreviousExpBank').on('change', function () {
        var bankid = $('#selPreviousExpBank').val();
        if (bankid == '0') {
            $('#div_bankdtl').show();
            $('#div_ifsccode').hide();
        }
        else if (bankid == '-1') {
            $('#div_bankdtl').hide();
            $('#div_ifsccode').hide();
        }
        else {
            $('#div_bankdtl').hide();
            $('#div_ifsccode').show();
        }
        $('#txt_ifsccode').val('');
        $('#txt_otherbankname').val('');
        $('#txt_otherbranchname').val('');
        $('#selPreviousExpBranch').empty();
        $('#selPreviousExpBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        var selectedBankId = $('#selPreviousExpBank :selected').val();
        if (selectedBankId != 0) {
            let bank = getBank(allBanks, selectedBankId);
            let bankCircles = null, bankBranches = null;
            if (bank.BankCircles != null)
                bankCircles = bank.BankCircles;
            if (bank.Branches != null)
                bankBranches = bank.Branches;
            if (bankCircles != null) {
                for (let i = 0; i < bankBranches.length; i++) {
                    $('#selPreviousExpBranch').append($('<option>', { value: bankBranches[i].BranchId, text: bankBranches[i].BranchName }));
                }
            }
        }
    });
    $('#selAllocationBank').on('change', function () {
         
        var selectedBankId = $('#selAllocationBank :selected').val();
        $scope.bindBrnach(selectedBankId);
    });
    $scope.bindBrnach = function (id) {
        $('#selAllocationBranch').empty();
        $('#selAllocationBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        var selectedBankId = id;
        if (selectedBankId != 0) {
            let bank = getBank(allBanks, selectedBankId);
            let bankCircles = null, bankBranches = null;
            if (bank.BankCircles != null)
                bankCircles = bank.BankCircles;
            if (bank.Branches != null)
                bankBranches = bank.Branches;
            if (bankCircles != null) {
                for (let i = 0; i < bankBranches.length; i++) {
                    $('#selAllocationBranch').append($('<option>', { value: bankBranches[i].BranchId, text: bankBranches[i].BranchName }));
                }
            }
        }
    }

    $("#other_qualfctn").hide();
    $("#selQualification").change(function () {
        
        var val = $('#selQualification').val();
        if (val !== "Others") {
            $("#other_qualfctn").hide();
            $('#Textarea1').val('');

        }
        else {
            $("#other_qualfctn").show();
        }
    });
    function getBank(bankList, bankId) {

        let bank = new Object();
        for (let i = 0; i < bankList.length; i++) {
            if (bankList[i].BankId == bankId) {
                bank = bankList[i];
                break;
            }
        }
        return bank;
    }

    $scope.getProduct = function () {

        let corporates = new Array();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: 'api/GetBCHandler.ashx?mode=userProductS',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (product) {

                if (product.length != 0) {
                    $('#selProductsOffered').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < product.length; i++) {
                        $('#selProductsOffered').append($('<option>', {
                            value: product[i].productId,
                            text: product[i].productName
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('Failed. ' + errorMessage);
            }
        });
        return corporates;
    }
    $scope.datetimeBind = function () {
        //tabing();
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        var firstDay = '01/' + mm + '/' + yyyy;
        $("#txtDOB").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtDOB").val(firstDay);

        $("#txtPassingDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtPassingDate").val(today);
        $scope.txtPassingDate = today;
        $("#txtFromDateExp").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtFromDateExp").val(today);

        $("#txtToDateExp").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtToDateExp").val(today);

        $("#txtAppointmentDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtAppointmentDate").val(today);

        $("#txtGivenOn").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtGivenOn").val(today);

        $("#txtDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtDate").val(today);



    }
    $scope.deleteBc = function () {

        if (confirm("Do you want to delete?")) {

            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                url: "api/DeleteBcHandler.ashx?mode=BANKc&bcId=" + $state.params.BankCorrespondId,
                async: false,
                success: function () {


                },
                error: function (xmlHttpRequest, errorString, errorMessage) {

                    window.location.href = "#/all";

                    alert('BC deleted successfully.');
                }
            });
        }
        return false;
    }


    $scope.divhome = true;
    $scope.divcircle = true;
    $scope.divprofile = true;
    $scope.divsetting = true;
    $scope.divmessage = true;

    $scope.displayTab = function (fn) {
        if (fn == "home") {

            $scope.divhome = false;
            $scope.divcircle = true;
            $scope.divprofile = true;
            $scope.divsetting = true;
            $scope.divmessage = true;
        }
        if (fn == "circle") {

            $scope.divhome = true;
            $scope.divcircle = false;
            $scope.divprofile = true;
            $scope.divsetting = true;
            $scope.divmessage = true;
        }
        if (fn == "profile") {

            $scope.divhome = true;
            $scope.divcircle = true;
            $scope.divprofile = false;
            $scope.divsetting = true;
            $scope.divmessage = true;
        }
        if (fn == "setting") {

            $scope.divhome = true;
            $scope.divcircle = true;
            $scope.divprofile = true;
            $scope.divsetting = false;
            $scope.divmessage = true;
        }
        if (fn == "message") {

            $scope.divhome = true;
            $scope.divcircle = true;
            $scope.divprofile = true;
            $scope.divsetting = true;
            $scope.divmessage = false;
        }
    }









    $scope.btnedit = 'Edit';
    $scope.divVIEW = false;
    $scope.divEdit = true;
    var states;
    var districts;
    $scope.editView = function () {
        
        if ($scope.btnedit == "Edit") {
            $scope.btnedit = 'View';
            $scope.divVIEW = true;
            $scope.divEdit = false;
        }
        else {
            $scope.btnedit = 'Edit';
            $scope.divVIEW = false;
            $scope.divEdit = true;
        }

    }
    
    var b = $state.params.BankCorrespondId;
    var bcId = '';
    if (b != undefined)
        bcId = parseInt(b);


    var baseControlName = '';
    var errorMessage = "";
    var certs = [];
    $scope.certsList = [];
    var exps = [];
    $scope.expsList = [];
    var areas = [];
    $scope.areasList = [];
    var devices = [];
    $scope.devicesList = [];
    var conns = [];
    $scope.connsList = [];
    var bcorrespondene = new Object();
    var ssas = [];
    $scope.ssaList = [];
    function getParameterByName(name) //courtesy Artem
    {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else {
            if ((results[1].indexOf('?')) > 0)
                return decodeURIComponent(results[1].substring(0, results[1].indexOf('?')).replace(/\+/g, " "));
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
    $scope.AddMoreCerts = function () {
         
        var institueName = '';
        var Course = '';
        var cert = new Object();
        //  cert.DateOfPassing = $('#txtPassingDate').val();
        var from = $("#txtPassingDate").val().split("/");
        newdate = from[2] + "-" + from[1] + "-" + from[0];
        cert.DateOfPassing = newdate
        //   cert.InstituteId = $('#' + baseControlName + 'SelInstitute :selected').text();
        //    cert.CourseId = $('#' + baseControlName + 'SelCourse').val();
        var Institute_Name = $('#' + baseControlName + 'SelInstitute :selected').text();
        if (Institute_Name == "Other") {
            cert.InstituteName = $("#Text1").val();
            institueName = $("#Text1").val();
            $timeout(function () { $scope.divOtherrInstitute = true; }, 100);
        }
        else {
            cert.InstituteName = $('#' + baseControlName + 'SelInstitute :selected').text();
            institueName = $('#' + baseControlName + 'SelInstitute :selected').text();

        }

        var Course_Name = $('#' + baseControlName + 'SelInstitute :selected').text();
        if (Course_Name == "Other") {
            cert.CourseName = $("#Text2").val();
            Course = $("#Text2").val();
            $timeout(function () { $scope.divOtherCourse = true; }, 100);
        }
        else {
            cert.CourseName = $('#' + baseControlName + 'SelCourse :selected').text();
            var Course = $('#' + baseControlName + 'SelCourse :selected').text();

        }



        cert.Grade = $('#' + baseControlName + 'txtGrades').val();
        certs.push(cert);


        $scope.certsList.push({
            "DateOfPassing": cert.DateOfPassing,
            "InstituteName": institueName,
            "CourseName": Course,
            "Grade": cert.Grade

        });
        $('#' + baseControlName + 'txtPassingDate').val("");
        $('#' + baseControlName + 'SelInstitute').val("");
        $('#' + baseControlName + 'SelCourse').val("");
        $('#' + baseControlName + 'txtGrades').val("");
    };
    $scope.divOtherrInstitute = true;
    $scope.divOtherCourse = true;

    $('#SelInstitute').on('change', function () {
         
        var institueName = $('#' + baseControlName + 'SelInstitute :selected').text();
        if (institueName == "Other") {
            $timeout(function () { $scope.divOtherrInstitute = false; }, 100);


        }
        else {
            $timeout(function () { $scope.divOtherrInstitute = true; }, 100);

        }

    });

    $('#SelCourse').on('change', function () {
         
        var institueName = $('#' + baseControlName + 'SelCourse :selected').text();
        if (institueName == "Other") {
            $timeout(function () { $scope.divOtherCourse = false; }, 100);

        }
        else {
            $timeout(function () { $scope.divOtherCourse = true; }, 100);

        }

    });
    $scope.removeCerts = function (index) {
        certs.splice(index, 1);
        $scope.certsList.splice(index, 1);
    }

    $scope.AddMoreExperience = function () {
        
        var exp = new Object();
        exp.oBankName = '';
        exp.oBranchName = '';
        exp.brnachId = '';


        BankId = $('#selPreviousExpBank').val();
        if (BankId !== "0") {
            exp.oBankName = 'N/A';
            exp.oBranchName = 'N/A';
            exp.BankId = $('#' + baseControlName + 'selPreviousExpBank').val();
            exp.brnachId = $('#' + baseControlName + 'selPreviousExpBranch').val();


        } else {
            
            exp.oBankName = $('#' + baseControlName + 'txt_otherbankname').val();
            exp.oBranchName = $('#' + baseControlName + 'txt_otherbranchname').val();
            exp.BankId = 195;
            exp.brnachId = 0;


        }


        var from = $('#txtFromDateExp').val().split("/");
        Fromdate = from[2] + "-" + from[1] + "-" + from[0];
        exp.FromDate = Fromdate;


        var toDate = $('#txtToDateExp').val().split("/");
        Todate = toDate[2] + "-" + toDate[1] + "-" + toDate[0];
        exp.ToDate = Todate;



        exp.Reason = $('#' + baseControlName + 'txtReasons').val();
        exps.push(exp);
        $scope.expsList.push({
            "BankId": exp.BankId,
            "Branchid": exp.Branchid,
            "BankName": $('#' + baseControlName + 'selPreviousExpBank :selected').text(),
            "BranchName": $('#' + baseControlName + 'selPreviousExpBranch :selected').text(),
            "oBankName": exp.oBankName,
            "oBranchName": exp.oBranchName,
            "FromDate": exp.FromDate,
            "ToDate": exp.ToDate,
            "Reason": exp.Reason


        });
        $('#' + baseControlName + 'selPreviousExpBank').val("");
        $('#' + baseControlName + 'selPreviousExpBranch').val("");
        $('#' + baseControlName + 'txtFromDateExp').val("");
        $('#' + baseControlName + 'txtToDateExp').val("");
        $('#' + baseControlName + 'txtReasons').val("");

    };


    $scope.removeExperience = function (index) {
        exps.splice(index, 1);
        $scope.expsList.splice(index, 1);
    }

    $scope.AddMoreOpAreas = function () {
        var area = new Object();
        area.VillageCode = $('#' + baseControlName + 'txtOperationVillageCode').val();
        area.VillageDetail = $('#' + baseControlName + 'txtOperationVillageDetail').val();
        areas.push(area);
        $scope.areasList.push(area);

        $('#' + baseControlName + 'txtOperationVillageCode').val("");
        $('#' + baseControlName + 'txtOperationVillageDetail').val("");

    };
    $scope.removeOpAreas = function (index) {
        areas.splice(index, 1);
        $scope.areasList.splice(index, 1);
    }

    $scope.AddMoreDevices = function () {


        var device = new Object();

        var from = $("#txtGivenOn").val().split("/");
        newdate = from[2] + "-" + from[1] + "-" + from[0];

        device.GivenOn = newdate;
        device.Device = $('#' + baseControlName + 'selDeviceName').val();
        device.DeviceCode = $('#txtDeviceCode').val();
        devices.push(device);
        $scope.devicesList.push({
            "GivenOn": device.GivenOn,
            "Device": $('#' + baseControlName + 'selDeviceName :selected').text(),
            "DeviceCode": $('#txtDeviceCode').val()
        })

        $('#' + baseControlName + 'selDeviceName').val("");

    };

    $scope.removeDevices = function (index) {
        devices.splice(index, 1);
        $scope.devicesList.splice(index, 1);
    }

    $scope.AddMoreConnectivity = function (index) {
        var conn = new Object();
        conn.ConnectivityMode = $('#' + baseControlName + 'selConnType').val();
        conn.ConnectivityProvider = $('#' + baseControlName + 'txtProvider').val();
        conn.ContactNumber = $('#' + baseControlName + 'txtNumber').val();
        conns.push(conn);
        $scope.connsList.push(conn);


        $('#' + baseControlName + 'selConnType').val("");
        $('#' + baseControlName + 'txtProvider').val("");
        $('#' + baseControlName + 'txtNumber').val("");

    };
    $scope.removeConnectivity = function (index) {
        conns.splice(index, 1);
        $scope.connsList.splice(index, 1);
    }




    function validate() {

        errorMsg = '';

        var email = $('#txtEmail').val();
        var name = $('#txtName').val();
        var fathersname = $('#txtFather').val();
        var phone = $('#txtPhone1').val();
        var gender = $('#selGender').val();
        var category = $('#selCategory').val();
        var dob = $('#txtDOB').val();
        var MinCash = $('#txtMinCash').val();
        var MonthlyFixed = $('#txtMonthlyFixed').val();
        var MonthlyVariable = $('#txtMonthlyVariable').val();
        var Aadhar = $('#txtAadharCard').val();
        var qualificaiton = $('#Select3').val();
        var state = $('#selAddressState').val();
        var plstate = $('#selPLState').val();
        var district = $('#txtAddressSubDistrict').val();
        var occupationtype = $('#SelAlternateOccupation').val();
        var selAllocationBank = $('#selAllocationBank').val();
        var AllocationBranch = $('#selAllocationBranch').val();
        var pincode = $('#txtPinCode').val();



        var result = true;
        if (name == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Name.";
            result = false;
        }

        if (Aadhar == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Aadhar Card no";
            result = false;
        }
        if (dob == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select Date of Birth";
            result = false;
        }
        if (fathersname == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Father's Name.";
            result = false;
        }
        if (gender == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select  Gender.";
            result = false;
        }
        if (selAllocationBank == "-1") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select Bank";
            result = false;
        }
        if (AllocationBranch == null) {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select Branch";
            result = false;
        }
        if (category == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select  Category.";
            result = false;
        }
        if (pincode == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Pin Code.";
            result = false;
        }
        if (state == "0") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select  State.";
            result = false;
        }
        if (district == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select  District.";
            result = false;
        }
        if (phone == "" || phone.length > 10 || phone.length < 10 || isNaN(phone)) {
            $('#divPhone').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Valid First Mobile Number.";
            result = false;
        }
        else {
            $("#divPhone").removeClass("form-group has-error");
        }
        if (!IsEmail(email)) {
            $('#txtEmail').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter valid email Id.";
            result = false;
        }
        else {
            $("#divEmail").removeClass("form-group has-error");
        }

        if (qualificaiton == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select Qualification";
            result = false;
        }
        if (occupationtype == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select occupation Type";
            result = false;
        }




        if (MinCash == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Minimum Cash Handling Limit.";
            result = false;
        }
        if (MonthlyFixed == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Commission 1";
            result = false;
        }
        if (MonthlyVariable == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Commission 2";
            result = false;
        }


        return result;
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    $scope.addSingleEntry = function () {

        if (validate() == true) {
            var fileUpload = $("#fileImage").get(0);
            var files = fileUpload.files;

            var fileName = $('#' + baseControlName + 'fileImage').val().replace(/^.*[\\\/]/, '');

            bcorrespondene.ImagePath = $('#' + baseControlName + 'txtAadharCard').val() + "_" + fileName;

            bcorrespondene.Gender = $('#' + baseControlName + 'selGender').val();
            var from = $("#txtDOB").val().split("/");
            newdate = from[2] + "-" + from[1] + "-" + from[0];

            bcorrespondene.DOB = newdate;
            bcorrespondene.FatherName = $('#' + baseControlName + 'txtFather').val();
            bcorrespondene.SpouseName = $('#' + baseControlName + 'txtSpouse').val();
            bcorrespondene.Category = $('#' + baseControlName + 'selCategory').val();
            if ($('#' + baseControlName + 'radHandicapNo').checked)
                bcorrespondene.Handicap = $('#' + baseControlName + 'radHandicapNo').val()
            else
                bcorrespondene.Handicap = $('#' + baseControlName + 'radHandicapYes').val()

            // bcorrespondene.Handicap = $('#' + baseControlName + 'radHandicapNo.Checked ? radHandicapNo).val() : radHandicapYes').val();
            bcorrespondene.PhoneNumber1 = $('#' + baseControlName + 'txtPhone1').val();
            bcorrespondene.PhoneNumber2 = $('#' + baseControlName + 'txtPhone2').val();
            bcorrespondene.PhoneNumber3 = $('#' + baseControlName + 'txtPhone3').val();
            bcorrespondene.Email = $('#' + baseControlName + 'txtEmail').val();
            bcorrespondene.AadharCard = $('#' + baseControlName + 'txtAadharCard').val();
            bcorrespondene.PanCard = $('#' + baseControlName + 'txtPanCard').val();
            bcorrespondene.VoterCard = $('#' + baseControlName + 'txtVoterId').val();
            bcorrespondene.DriverLicense = $('#' + baseControlName + 'txtDriverLic').val();
            bcorrespondene.NregaCard = $('#' + baseControlName + 'txtNregaCard').val();
            bcorrespondene.RationCard = $('#' + baseControlName + 'txtRationCard').val();

            bcorrespondene.State = $('#selAddressState').val();
            bcorrespondene.City = 0;
            bcorrespondene.District = $('#selAddressDistrict').val();
            bcorrespondene.Subdistrict = $('#selAddresssubDistrict').val();
            bcorrespondene.Village = $('#selAddressVillage').val();

            bcorrespondene.plState = $('#selPLState').val();
            bcorrespondene.plDistrict = $('#selPLDistrict').val();
            bcorrespondene.plSubdistrict = $('#selPLSubDistrict').val();
            bcorrespondene.plVillage = $('#selPLdVillages').val();

            bcorrespondene.Area = $('#' + baseControlName + 'txtAddressArea').val();
            bcorrespondene.PinCode = $('#' + baseControlName + 'txtPinCode').val();
            bcorrespondene.AlternateOccupationType = $('#' + baseControlName + 'SelAlternateOccupation').val();
            bcorrespondene.AlternateOccupationDetail = $('#' + baseControlName + 'txtAlternateOccupationDtl').val();
            bcorrespondene.UniqueIdentificationNumber = $('#' + baseControlName + 'txtUniqueId').val();
            bcorrespondene.BankReferenceNumber = $('#' + baseControlName + 'txtBankReferenceNumber').val();
            bcorrespondene.Qualification = $('#' + baseControlName + 'selQualification').val();

            bcorrespondene.OtherQualification = $('#' + baseControlName + 'txtOtherQualification').val();
            bcorrespondene.CorporateId = $('#' + baseControlName + 'selCorporate').val();


            bcorrespondene.Allocation = $('#' + baseControlName + 'radAllocatedNo').checked ? "No" : "Yes";
            bcorrespondene.isAllocated = (bcorrespondene.Allocation == 'Yes');
            if (bcorrespondene.Allocation == 'Yes') {
                var from = $("#txtAppointmentDate").val().split("/");
                newdates = from[2] + "-" + from[1] + "-" + from[0];

                bcorrespondene.AppointmentDate = newdates;
                bcorrespondene.AllocationIFSCCode = $('#' + baseControlName + 'ifscCode').val();

                bcorrespondene.AllocationBankId = $('#selAllocationBank').val();
                bcorrespondene.AllocationBranchId = $('#selAllocationBranch').val();

                bcorrespondene.BCType = $('#' + baseControlName + 'selBCType').val();
                bcorrespondene.WorkingDays = $('#' + baseControlName + 'txtWorkingDays').val();
                bcorrespondene.WorkingHours = $('#' + baseControlName + 'txtWorkingHours').val();
                bcorrespondene.PLPostalAddress = $('#' + baseControlName + 'txtPostalAddress').val();
                bcorrespondene.PLVillageCode = $('#' + baseControlName + 'txtVillage').val();
                bcorrespondene.PLVillageDetail = $('#' + baseControlName + 'txtVillageDetail1').val();
                bcorrespondene.PLStateId = $('#' + baseControlName + 'selPLState').val();
                //bcorrespondene.PLDistrictId = $('#' + baseControlName + 'selPLDistrict').val();
                bcorrespondene.PLTaluk = $('#' + baseControlName + 'selPLTaluk').val();
                bcorrespondene.PLPinCode = $('#' + baseControlName + 'txtPLPinCode').val();
                bcorrespondene.MinimumCashHandlingLimit = $('#' + baseControlName + 'txtMinCash').val();
                bcorrespondene.MonthlyFixedRenumeration = $('#' + baseControlName + 'txtMonthlyFixed').val();
                bcorrespondene.MonthlyVariableRenumeration = $('#' + baseControlName + 'txtMonthlyVariable').val();
                bcorrespondene.productid = $('#selProductsOffered').val();

                console.log(conns);
            }



            $.ajax({
                url: "api/InsertUpdateHandler.ashx",
                method: "POST",
                datatype: 'json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                data: {
                    "bcorr": JSON.stringify(bcorrespondene), "allCerts": JSON.stringify(certs), "allExps": JSON.stringify(exps),
                    "allAreas": JSON.stringify(areas), "allDevices": JSON.stringify(devices), "allConns": JSON.stringify(conns),
                    "allSSAs": JSON.stringify(ssas)
                },
                success: function (data) {
                     

                    var fileUpload = $("#fileImage").get(0);
                    var files = fileUpload.files;

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "api/FileUploader.ashx",
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        headers: {
                            "userId": $cookies.get("userId"),
                            "TokenId": $cookies.get("TokenId"),
                            "adhaar": $('#' + baseControlName + 'txtAadharCard').val()

                        },
                        success: function (result) {

                            alert('Data Updated Successfully.')
                            window.location.href = "#/home";

                        },
                        error: function (err) {
                            alert('Data Updated Successfully.')
                            window.location.href = "#/home";

                        }
                    });

                },
                error: function (err) {
                     

                    var fileUpload = $("#fileImage").get(0);
                    var files = fileUpload.files;

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "api/FileUploader.ashx",
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        headers: {
                            "userId": $cookies.get("userId"),
                            "TokenId": $cookies.get("TokenId"),
                            "adhaar": $('#' + baseControlName + 'txtAadharCard').val()

                        },
                        success: function (result) {
                            alert('Data Updated Successfully.')
                            window.location.href = "#/home";
                        },
                        error: function (err) {
                            alert('Data Updated Successfully.')
                            window.location.href = "#/home";
                        }
                    });


                }
            });

        }

        else {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
    }

    $scope.getBcDetails = function () {
        
        if (bcId != '') {
            $('.loader').show();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                url: "api/GetBCHandler.ashx?mode=One&bcId=" + bcId,
                async: false,
                success: function (bankc) {

                     
                    console.log(bankc);
                    //console('Got it!');
                    if (bankc != null) {
                         
                        bcorrespondene = bankc;
                        //Set Controls
                        $scope.ImagePath = bankc.ImagePath;
                        $scope.lblName = bankc.Name;
                        $scope.lblGender = bankc.Gender;
                        $scope.lblDOB = bankc.dateOfBirth;
                        $('#' + baseControlName + 'txtName').val(bankc.Name);
                        $('#' + baseControlName + 'selGender').val(bankc.Gender);

                        var dt = new Date(bankc.DOB);
                        //       var dtString = dt.toDateString("YYYY-MM-DD");
                        var dtString = dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate();

                        $('#txtDOB').val( bankc.dateOfBirth);

                        $scope.lblFather = bankc.FatherName;
                        $('#' + baseControlName + 'txtFather').val(bankc.FatherName);
                        $scope.lblSpouse = bankc.SpouseName;
                        $('#' + baseControlName + 'txtSpouse').val(bankc.SpouseName);
                        $('#' + baseControlName + 'selCategory').val(bankc.Category);
                        $scope.lblCategory = bankc.Category;
                        if (bankc.Handicap == 'Yes') {
                            $scope.lblhand = "Yes";
                            $('#' + baseControlName + 'radHandicapYes').checked = true;
                        }
                        else {
                            $scope.lblhand = "No";
                            $('#' + baseControlName + 'radHandicapNo').checked = true;
                        }


                        $scope.lblPhone1 = bankc.PhoneNumber1;
                        $scope.lblPhone2 = bankc.PhoneNumber2;
                        $scope.lblPhone3 = bankc.PhoneNumber3;
                        $scope.lblEmail = bankc.Email;
                        $('#' + baseControlName + 'txtPhone1').val(bankc.PhoneNumber1);
                        $('#' + baseControlName + 'txtPhone2').val(bankc.PhoneNumber2);
                        $('#' + baseControlName + 'txtPhone3').val(bankc.PhoneNumber3);
                        $('#' + baseControlName + 'txtEmail').val(bankc.Email);
                        $('#' + baseControlName + 'txtAadharCard').val(bankc.AadharCard);
                        $('#' + baseControlName + 'txtPanCard').val(bankc.PanCard);
                        $('#' + baseControlName + 'txtVoterId').val(bankc.VoterCard);
                        $('#' + baseControlName + 'txtDriverLic').val(bankc.DriverLicense);
                        $('#' + baseControlName + 'txtNregaCard').val(bankc.NregaCard);
                        $('#' + baseControlName + 'txtRationCard').val(bankc.RationCard);
                        $('#' + baseControlName + 'selAddressState').val(bankc.State);
                        $scope.BindDistrict(bankc.State);

                        $('#selAddressDistrict').val(bankc.District);
                        $scope.BindSubDistrict(bankc.District);
                        $('#selAddresssubDistrict').val(bankc.Subdistrict);
                        
                        $scope.BindVillage(bankc.Subdistrict);
                        $('#selAddressVillage').val(bankc.Village);
                  
                     
                        $('#' + baseControlName + 'txtAddressArea').val(bankc.Area);                    
                        $('#' + baseControlName + 'txtLongitude').val(bankc.Longitude);
                        $('#' + baseControlName + 'txtLatitude').val(bankc.Latitude);

                        $('#' + baseControlName + 'txtPinCode').val(bankc.PinCode);
                        $('#' + baseControlName + 'SelAlternateOccupation').val(bankc.AlternateOccupationType);
                        $('#' + baseControlName + 'txtAlternateOccupationDtl').val(bankc.AlternateOccupationDetail);
                        $('#' + baseControlName + 'txtUniqueId').val(bankc.UniqueIdentificationNumber);
                        $('#' + baseControlName + 'txtBankReferenceNumber').val(bankc.BankReferenceNumber);

                        $('#' + baseControlName + 'selQualification').val(bankc.Qualification);

                        $('#' + baseControlName + 'txtOtherQualification').val(bankc.OtherQualification);
                        $('#' + baseControlName + 'selCorporate').val(bankc.CorporateId);

                        if (bankc.isAllocated)
                            $('#radAllocatedYes').prop('checked', true);
                        else {
                            $('#radAllocatedNo').prop('checked', true);
                            $('#divAllocationDetails').hide();
                        }

                        if (bankc.AppointmentDate != null && bankc.AppointmentDate != undefined) {
                            var dt2 = new Date(bankc.AppointmentDate);
                            var dt2String = dt2.getFullYear() + '-' + (dt2.getMonth() + 1) + '-' + dt2.getDate();

                            $('#' + baseControlName + 'txtAppointmentDate').val(dtString);
                        }


                        $('#' + baseControlName + 'ifscCode').val(bankc.AllocationIFSCCode);

                        $('#selAllocationBank').val(bankc.AllocationBankId);
                        $scope.bindBrnach(bankc.AllocationBankId);
                        $('#selAllocationBranch').val(bankc.AllocationBranchId);

                        $('#' + baseControlName + 'selBCType').val(bankc.BCType);
                        $('#' + baseControlName + 'txtWorkingDays').val(bankc.WorkingDays);
                        $('#' + baseControlName + 'txtWorkingHours').val(bankc.WorkingHours);
                        $('#' + baseControlName + 'txtPostalAddress').val(bankc.PLPostalAddress);
                        $('#' + baseControlName + 'txtVillageCode1').val(bankc.PLVillageCode);
                        $('#' + baseControlName + 'txtVillageDetail1').val(bankc.PLVillageDetail);
                        $('#' + baseControlName + 'selPLState').val(bankc.PLStateId);
                        //$('#' + baseControlName + 'selPLDistrict').val(bankc.PLDistrictId);
                        $('#' + baseControlName + 'selPLTaluk').val(bankc.PLTaluk);
                        $('#' + baseControlName + 'txtPLPinCode').val(bankc.PLPinCode);
                        //$('#' + baseControlName + 'selProductsOffered').val(bankc.ProductsOffered);
                        $('#' + baseControlName + 'txtMinCash').val(bankc.MinimumCashHandlingLimit);
                        $('#' + baseControlName + 'txtMonthlyFixed').val(bankc.MonthlyFixedRenumeration);
                        $('#' + baseControlName + 'txtMonthlyVariable').val(bankc.MonthlyVariableRenumeration);
                        $('#selProductsOffered').val(bankc.productId);
                        

                        $('#lblName').html(bankc.Name);

                        if (bankc.Certifications != null) {
                            $scope.certsList = bankc.Certifications;
                            for (var c = 0; c < bankc.Certifications.length; c++) {
                                var cert = bankc.Certifications[c];
                                certs.push(cert);


                            }
                        }
                        if (bankc.PreviousExperience != null) {
                            for (var c = 0; c < bankc.PreviousExperience.length; c++) {
                                $scope.expsList = bankc.PreviousExperience;
                                var exp = bankc.PreviousExperience[c];
                                exps.push(exp);

                            }

                        }


                        if (bankc.OperationalAreas != null) {
                            $scope.areasList = bankc.OperationalAreas;
                            for (var c = 0; c < bankc.OperationalAreas.length; c++) {
                                var oprarea = bankc.OperationalAreas[c];
                                areas.push(oprarea);
                            }
                        }


                        if (bankc.Devices != null) {
                            $scope.devicesList = bankc.Devices;
                            for (var c = 0; c < bankc.Devices.length; c++) {
                                var device = bankc.Devices[c];
                                devices.push(device);

                            }
                        }


                        if (bankc.ConnectivityDetails != null) {
                            $scope.connsList = bankc.ConnectivityDetails;
                            for (var c = 0; c < bankc.ConnectivityDetails.length; c++) {
                                var conn = bankc.ConnectivityDetails[c];
                                conns.push(conn);

                            }
                        }

                         
                        if (bankc.SsaDetails != null) {
                            $scope.ssaList = bankc.SsaDetails;
                            for (var c = 0; c < bankc.SsaDetails.length; c++) {
                                var ssa = bankc.SsaDetails[c];
                                ssas.push(conn);

                            }
                        }

                        if (bankc.Products != null) {
                            for (var c = 0; c < bankc.Products.length; c++) {
                                var prod = bankc.AddProducts[c];
                                prods.push(prod);
                                AddProducts(t7, prod.minCashHandlingLimit, prod.MonthlyFixedRenumeration, prod.MonthlyVariableRenumeration);
                                //prods.push(bankc.Products[c]);
                                //AddProducts(t7, bankc.ProductName, bankc.MinimumCashHandlingLimit, bankc.MonthlyFixedRenumeration, bankc.MonthlyVariableRenumeration);
                            }
                        }


                    }

                    $('.loader').hide();

                },
                error: function (err) {
                    alert(err);
                    $('.loader').hide();
                }
            });
        }
    }





    function ShowDialog(message, height, width, title, padding, icon, showOkButton, link) {

        $("#helloModalMessage").html(message);

        $('#helloModal').modal('show');

        if (showOkButton == false) {
            $("#closemodalbutton").show();
            $("#redirectbutton").hide();
        }
        else {
            if (link != '')
                $("#redirectbutton").attr("href", link);
            $("#closemodalbutton").hide();
            $("#redirectbutton").show();
        }

        //$.Dialog({
        //    overlay: true,
        //    shadow: true,
        //    flat: true,
        //    title: title,
        //    icon: icon,
        //    content: '',
        //    width: width,
        //    padding: padding,
        //    height: height,//600,400
        //    onShow: function (_dialog) {
        //        var content = _dialog.children('.content');
        //        content.html(message);
        //        $.Metro.initInputs();
        //    }
        //});
    }

    $('#btnSubmitBlackList').on('click', function () {
        
        var bc = new Object();
        bc.BankCorrespondId = bcId; //From url
        bc.IsBlackListed = true;
        var from = $("#txtDate").val().split("/");
        newdate = from[2] + "-" + from[1] + "-" + from[0];

        bc.BlacklistDate = newdate;
        bc.BlackListReason = $('#txtReason').val();
        bc.BlackListApprovalNumber = $('#txtApprovalNumber').val();

        $.ajax({
            url: "api/InsertUpdateHandler.ashx?mode=BlackList",
            method: "POST",
            datatype: "json",
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            data: { "bcorr": JSON.stringify(bc) },
            success: function () {
                alert('BC Successfully BlackListed.')
                window.location.href = "#/all";
               
            },
            error: function (xmlHttpRequest, errorString, errorMessage) {
                alert('BC Successfully BlackListed.')
                window.location.href = "#/all";
            }
        });
    });

    $('#btnSubmitChangeStatus').on('click', function () {
        
        var bc = new Object();
        bc.BankCorrespondId = bcId; //From url
        bc.Status = 'Inactive';
        bc.CurrentStatusSince = $('#txtDate').val();
        bc.StatusChangeReason = $('#txtReason').val();

        $.ajax({
            url: "api/InsertUpdateHandler.ashx?mode=ChangeStatus",
            method: "POST",
            datatype: "json",
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            data: { "bcorr": JSON.stringify(bc) },
            success: function () {
                alert('BC Terminated successfully.');
                window.location.href = "#/all";
            },
            error: function (xmlHttpRequest, errorString, errorMessage) {
                alert('BC Terminated successfully.');
                window.location.href = "#/all";
            }
        });
    });

    $scope.GetStates = function () {

        $('#selAddressState').empty();
        $('#selPLState').empty();
        $('#selPLDistrict').empty();

        $('#selPLSubDistrict').empty();

        $('#selPLdVillages').empty();


        var allStates = new Array();
        var requestUrl = 'api/state.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (stateList) {
                if (stateList.length != 0) {
                    for (let i = 0; i < stateList.length; i++) {
                        let state = new Object();
                        state.StateId = stateList[i].StateId;
                        state.StateName = stateList[i].StateName;
                        state.StateCode = stateList[i].StateCode;
                        state.StateCircles = stateList[i].StateCircles;
                        state.Branches = stateList[i].Branches;
                        allStates.push(state);
                    }
                }
                states = stateList;
                if (allStates.length != 0) {
                    $('#selAddressState').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selPLState').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selPLDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selPLSubDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selPLdVillages').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selAddressState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }
                if (allStates.length != 0) {
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selPLState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }

            },
            error: function (xhr, errorString, errorMessage) {
               // alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allStates;
    }
    ////////////////////////-----------------------District----------
    $('#selPLState').on('change', function () {
        
        $('#selPLDistrict').empty();
        var allDistrict = new Array();
        var requestUrl = 'api/Districts.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"stateId": "' + $('#selPLState').val() + '"}'
           ,
            async: false,
            success: function (districtList) {
                if (districtList.length != 0) {
                    for (let i = 0; i < districtList.length; i++) {
                        let district = new Object();
                        district.DistrictCode = districtList[i].DistrictCode;
                        district.DistrictName = districtList[i].DistrictName;
                        district.DistrictId = districtList[i].DistrictId;

                        allDistrict.push(district);
                    }
                }
                // states = districtList;
                if (allDistrict.length != 0) {


                    $('#selPLDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allDistrict.length; i++) {
                        $('#selPLDistrict').append($('<option>', {
                            value: allDistrict[i].DistrictId,
                            text: allDistrict[i].DistrictName + '(' + allDistrict[i].DistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
              //  alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allDistrict;
      
    });



    $('#selAddressState').on('change', function () {
     
        var id = $('#selAddressState').val();
        $scope.BindDistrict(id);
    });

    $scope.BindDistrict = function (id) {
        $('#selAddressDistrict').empty();

        var allDistrict = new Array();
        var requestUrl = 'api/Districts.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"stateId": "' + id + '"}'
           ,
            async: false,
            success: function (districtList) {
                if (districtList.length != 0) {
                    for (let i = 0; i < districtList.length; i++) {
                        let district = new Object();
                        district.DistrictCode = districtList[i].DistrictCode;
                        district.DistrictName = districtList[i].DistrictName;
                        district.DistrictId = districtList[i].DistrictId;

                        allDistrict.push(district);
                    }
                }
                // states = districtList;
                if (allDistrict.length != 0) {


                    $('#selAddressDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allDistrict.length; i++) {
                        $('#selAddressDistrict').append($('<option>', {
                            value: allDistrict[i].DistrictId,
                            text: allDistrict[i].DistrictName + '(' + allDistrict[i].DistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allDistrict;
    }

    //////////////////////////////////------------------------------- Subdistrict--------
    $('#selPLDistrict').on('change', function () {

        $('#selPLSubDistrict').empty();

        var allSubDistrict = new Array();
        var requestUrl = 'api/GetSubDistrict.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId"),


            },
            data: '{"distirctId": "' + $('#selPLDistrict').val() + '"}'
           ,
            async: false,
            success: function (subdistrictList) {
                if (subdistrictList.length != 0) {
                    for (let i = 0; i < subdistrictList.length; i++) {
                        let subdistrict = new Object();
                        subdistrict.SubDistrictCode = subdistrictList[i].SubDistrictCode;
                        subdistrict.SubDistrictName = subdistrictList[i].SubDistrictName;
                        subdistrict.SubDistrictId = subdistrictList[i].SubDistrictId;

                        allSubDistrict.push(subdistrict);
                    }
                }
                   $('#selPLSubDistrict').append($('<option>', { value: 0, text: 'Select' }));
                if (allSubDistrict.length != 0) {
              
                    for (let i = 0; i < allSubDistrict.length; i++) {
                        $('#selPLSubDistrict').append($('<option>', {
                            value: allSubDistrict[i].SubDistrictId,
                            text: allSubDistrict[i].SubDistrictName + '(' + allSubDistrict[i].SubDistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allDistrict;
       
    });


    $('#selAddressDistrict').on('change', function () {

        var id = $('#selAddressDistrict').val()
        $scope.BindSubDistrict(id);
    });

    $scope.BindSubDistrict = function (id) {

        $('#selAddresssubDistrict').empty();

        var allSubDistrict = new Array();
        var requestUrl = 'api/GetSubDistrict.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"distirctId": "' + id + '"}'
           ,
            async: false,
            success: function (subdistrictList) {
                if (subdistrictList.length != 0) {
                    for (let i = 0; i < subdistrictList.length; i++) {
                        let subdistrict = new Object();
                        subdistrict.SubDistrictCode = subdistrictList[i].SubDistrictCode;
                        subdistrict.SubDistrictName = subdistrictList[i].SubDistrictName;
                        subdistrict.SubDistrictId = subdistrictList[i].SubDistrictId;

                        allSubDistrict.push(subdistrict);
                    }
                }
                // states = districtList;
                if (allSubDistrict.length != 0) {


                    $('#selAddresssubDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allSubDistrict.length; i++) {
                        $('#selAddresssubDistrict').append($('<option>', {
                            value: allSubDistrict[i].SubDistrictId,
                            text: allSubDistrict[i].SubDistrictName + '(' + allSubDistrict[i].SubDistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
    //    return allDistrict;
    }
    //////////////////////////////////------------------------------- Villagee--------
    $('#selPLSubDistrict').on('change', function () {
        $('#selPLdVillages').empty();

        var allVillage = new Array();
        var requestUrl = 'api/GetVillage.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"subDistrictId": "' + $('#selPLSubDistrict').val() + '"}'
           ,
            async: false,
            success: function (villageList) {
                if (villageList.length != 0) {
                    for (let i = 0; i < villageList.length; i++) {
                        let village = new Object();
                        village.VillageCode = villageList[i].VillageCode;
                        village.VillageName = villageList[i].VillageName;
                        village.VillageId = villageList[i].VillageId;

                        allVillage.push(village);
                    }
                }
                $scope.allVillageList = allVillage;
                $('#selPLdVillages').append($('<option>', { value: 0, text: '--Select--' }));
                if (allVillage.length != 0) {
                    for (let i = 0; i < allVillage.length; i++) {
                        $('#selPLdVillages').append($('<option>', {
                            value: allVillage[i].VillageId,
                            text: allVillage[i].VillageName + '(' + allVillage[i].VillageCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the village list could not be populated!\n' + errorMessage);
            }
        });
        return allVillage;
        // 
        //var StateId = parseInt($('#selPLState').val());
        //var DistrictId = parseInt($('#selPLDistrict').val());
        //var SubDistrictId = parseInt($('#selPLSubDistrict').val());

        //var distrcts = states.filter(function (x) {
        //    return x.StateId === StateId;
        //});

        //var cities = distrcts[0].Districts.filter(function (x) {
        //    return x.DistrictId === DistrictId;
        //});


        //var village = cities[0].Villages


        //$('#selPLdVillages').empty();
        //$('#selPLdVillages').append($('<option>', { value: 0, text: 'Select Village' }));
        //if (village.length >= 1) {
        //    for (i = 0; i < village.length; i++) {
        //        $('#selPLdVillages').append($('<option>', { value: village[i].VillageCode, text: village[i].VillageName + '(' + village[i].VillageCode + ')' }));
        //    }
        //}
    });

    $('#selAddresssubDistrict').on('change', function () {
        var id =$('#selAddresssubDistrict').val();
        $scope.BindVillage(id);
    });
    $scope.BindVillage = function (id) {

         
        $('#selAddressVillage').empty();
        var allVillage = new Array();
        var requestUrl = 'api/GetVillage.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"subDistrictId": "' + id + '"}'
           ,
            async: false,
            success: function (villageList) {
                if (villageList.length != 0) {
                    for (let i = 0; i < villageList.length; i++) {
                        let village = new Object();
                        village.VillageCode = villageList[i].VillageCode;
                        village.VillageName = villageList[i].VillageName;
                        village.VillageId = villageList[i].VillageId;

                        allVillage.push(village);
                    }
                }
                $scope.allVillageList = allVillage;
                $('#selAddressVillage').append($('<option>', { value: 0, text: '--Select--' }));
                if (allVillage.length != 0) {
                    for (let i = 0; i < allVillage.length; i++) {
                        $('#selAddressVillage').append($('<option>', {
                            value: allVillage[i].VillageId,
                            text: allVillage[i].VillageName + '(' + allVillage[i].VillageCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the village list could not be populated!\n' + errorMessage);
            }
        });
      //  return allVillage;
      
    }
    $scope.ssaList = [];
    $scope.AddMoreSSA = function () {
        var ssa = new Object();
        ssa.SSA = $('#' + baseControlName + 'txtSSA1').val();
        ssa.StateName = $('#' + baseControlName + 'selPLState :selected').text();
        ssa.State = $('#' + baseControlName + 'selPLState').val();
        ssa.District = $('#' + baseControlName + 'selPLDistrict').val();
        ssa.DistrictName = $('#' + baseControlName + 'selPLDistrict :selected').text();
        ssa.SubDistrict = $('#' + baseControlName + 'selPLSubDistrict').val();
        ssa.SubDistrictName = $('#' + baseControlName + 'selPLSubDistrict :selected').text();

        var villageId = parseInt($('#' + baseControlName + 'selPLdVillages').val());

        var updatelist = $scope.allVillageList.filter(function (x) {
            return x.VillageId === villageId
        });

        ssa.Village = updatelist[0].VillageCode;
        ssas.push(ssa);

        $scope.ssaList.push(ssa);
        $('#selPLDistrict').empty();
        $('#selPLDistrict').append($('<option>', { value: 0, text: '--Select District--' }));
        $('#selPLSubDistrict').empty();
        $('#selPLSubDistrict').append($('<option>', { value: 0, text: '--Select SubDistrict--' }));
        $('#selPLdVillages').empty();
        $('#selPLdVillages').append($('<option>', { value: 0, text: '--Select Village--' }));
        $scope.GetStates();

    };
    $scope.removeSSA = function (index) {
        ssas.splice(index, 1);
        $scope.ssaList.splice(index, 1);
    }

  

});