﻿$(document).ready(function () {


    $("#txtAddMeetingMeetDate").prop("readonly", true);
    $('#txtAddMeetingMeetDate')
        .datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w"
        });
    $('#txtFromTime').timepicker();

    $('#txtToTime').timepicker();

});

$("#DivBranch").hide();

$("#DivOther").hide();

$("#DivCustomerLead").hide();


$("#divMeet").hide();
$("#ddlSMeetingType").change(function () {

    var ddlSMeetingType = $("#ddlSMeetingType").val();
    if (ddlSMeetingType == "Customer") {
        $("#DivBranch").hide();
        $("#DivOther").hide();
        $("#divMeet").show();
    }
    if (ddlSMeetingType == "Branch") {
        $("#DivBranch").show();
        $("#DivOther").hide();
        $("#divMeet").hide();
    }
    if (ddlSMeetingType == "Other") {
        $("#DivBranch").hide();
        $("#DivOther").show();
        $("#divMeet").hide();

    }
    if (ddlSMeetingType == "TFCPC Meeting") {
        $("#DivBranch").hide();
        $("#DivOther").hide();
        $("#divMeet").hide();

    }
    if (ddlSMeetingType == "P-Review Meeting") {
        $("#DivBranch").hide();
        $("#DivOther").hide();
        $("#divMeet").hide();

    }

});

$("#DdlMeetingWith").change(function () {

    var DdlMeetingWith = $("#DdlMeetingWith").val();
    if (DdlMeetingWith == "Related to Lead Meeting") {
        $("#DivCustomerLead").show();

    }
    if (DdlMeetingWith == "General Meeting") {
        $("#DivCustomerLead").hide();

    }

});


