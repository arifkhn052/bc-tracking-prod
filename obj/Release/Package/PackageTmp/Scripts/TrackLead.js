﻿$(document).ready(function () {
    tabing();
    //NumbersToWords(1911111111);

    //alert(num2words.numberToWords(300000000000).replace("Zero",""));
    GetProductList();
    GetUserDetail($("#hdnEmpId").val());
    GetIndustryMajor();
    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
        (('' + month).length < 2 ? '0' : '') + month + '-' +
        (('' + day).length < 2 ? '0' : '') + day;

    
    $("#txtDateOfAction").datepicker({ dateFormat: 'dd/mm/yy' });
    
    $('#dpDateOfAction input').on("keydown", function (e) { e.preventDefault(); });

    
    $('#txtAddMeetingMeetDate')
        .datepicker({
            dateFormat: 'dd/mm/yy'
        });
    $('#divaddMeetingMeetDate input').on("keydown", function (e) { e.preventDefault(); });
    
    
    $("#txtNextAppointment").datepicker({ dateFormat: 'dd/mm/yy'});

    $('#divNextAppointment input').on("keydown", function (e) { e.preventDefault(); });

    
    $('#txtAddMeetingMeetDate')
        .datepicker({
            dateFormat: 'dd/mm/yy'
        });

    
    $('#txtAddMeetingNextAppointmentDate').datepicker({
        dateFormat: 'dd/mm/yy'
    });
    $('#divaddMeetingNextAppointmentDate input').on("keydown", function (e) { e.preventDefault(); });
    
})

function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
}
function GetProductList() {

    $.ajax({
        url: 'AddLead.aspx/GetProductList',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLProductSold0').append('<option value="' + k + '">' + k + '</option>');
                    $('#DDLProductSold1').append('<option value="' + k + '">' + k + '</option>');
                });
            });
        }
    });
}


$("#DDLProductSold1").change(function () {
    if ($("#DDLProductSold1").val() == 'E-DFS' || $("#DDLProductSold1").val() == 'E-VFS')
        $("#outerDivIndustry1").show();
    else
        $("#outerDivIndustry1").hide();
});

$("#DDLProductSold0").change(function () {
    if ($("#DDLProductSold0").val() == 'E-DFS' || $("#DDLProductSold0").val() == 'E-VFS')
        $("#outerDivIndustry0").show();
    else
        $("#outerDivIndustry0").hide();
});

$(document).on('change', '[type=checkbox]', function (e) {
    var a = '#txt' + $(this).attr('id');
    if (a.indexOf('Type') > 0) {
        $(a).toggle();
    }
    if (a.indexOf('chckMeeting') > 0)
        $("#meetWarning").toggle();
});


$(document).on('click', "#btnAddProduct", function (e) {
    $("input[type='checkbox']:checked").each(function (e) {
        var txtBoxId = '#txt' + $(this).attr('id');
        var txtBoxVal = $(txtBoxId).val();

        if (txtBoxId.indexOf('Type') > 0) {
            var leadId = getParameterByName('lead_id');
            if (txtBoxId.indexOf('Type1') > 0 && txtBoxVal == "") {
                ShowDialog('Please fill the values required for the product selected', 100, 300, 'Message', 10, '<span class="icon-info"></span>',false);
                return false;
            }
            $.ajax({
                url: 'TrackLead.aspx/SaveCrossSellProductSold',
                type: "POST",
                dataType: "json",
                data: "{'product': '" + $(this).attr('id') + "','value': '" + txtBoxVal + "','leadId' : '" + leadId + "'}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var messageContent = "<center><h1>Thank You</h1><h6>" + data.d + " </h6>" +
                           "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                    ShowDialog(messageContent, 100, 300, 'Message', 10, '<span class="icon-info"></span>',true,window.location.href);
                }
            });
        }
    });
});


var errorMessage = '';

$("#DDLStatus").change(function () {
    var leadId = getParameterByName('lead_id');
    $.ajax({
        url: 'TrackLead.aspx/CheckPrevStageDate',
        type: "POST",
        dataType: "json",
        data: "{'leadId': '" + leadId + "','stage' : '" + $("#DDLStatus option:selected").text() + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#hdnPrevStage").val(data.d["Stage"]);
            $("#hdnPrevActionDate").val(data.d["date"]);
        }
    });
    $("#chckMeeting").prop('checked', false);
    $("#divNextAppointmentDate").hide();
    if ($("#DDLStatus").val() > 3)
        $("#divConverted").show();
    else
        $("#divConverted").hide();

    if ($("#DDLStatus").val() == 4)
        $("#divNewEnhancement").show();
    else
        $("#divNewEnhancement").hide();
});

$("#chckMeeting").change(function () {
    $("#divNextAppointmentDate").toggle(this.checked);
});


function validate() {
    
    var status = $("#DDLStatus").val();
    var addMeeting = $("#chckMeeting").val();

    var newEnhance = $("#DDLNewEnhance").val();

    var productSold0 = $("#DDLProductSold0").val();
    var account0 = $("#txtAccount0").val();
    var amount0 = $("#txtAmount0").val();
    var industryMajor0 = $('#DDLIndustryMajor0').val();

    var productSold1 = $("#DDLProductSold1").val();
    var account1 = $("#txtAccount1").val();
    var amount1 = $("#txtAmount1").val();
    var industryMajor1 = $('#DDLIndustryMajor1').val();

    var currentStatus = $("#currentStatus").val();

    var nextAppointmentDate = $("#txtNextAppointment").val();

    var actionDate = $("#txtDateOfAction").val();


    var result = true;

    if (status == "-1") {
        $("#divStatus").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divStatus").removeClass("form-group has-error");
    }

    if (status == 4 && newEnhance == "-1") {
        $("#divNewEnhance").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divNewEnhance").removeClass("form-group has-error");
    }

    if (status > 3 && productSold0 == "-1") {
        $("#divProductSold0").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divProductSold0").removeClass("form-group has-error");
    }

    if (status > 3 && (amount0 == "" || isNaN(amount0))) {
        $("#abc").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#abc").removeClass("form-group has-error");
    }

    if (status == 8 && (account0 == "" || account0.length != 11)) {
        $("#divAccount0").addClass("form-group has-error");
        result = false;
    }
    else if (account0 != "" && account0.length != 11) {
        $("#divAccount0").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divAccount0").removeClass("form-group has-error");
    }



    if (productSold1 != "-1" && (amount1 == "" || isNaN(amount1))) {
        $("#divAmount1").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divAmount1").removeClass("form-group has-error");
    }

    if (productSold1 != "-1" && status == 8 && (account1 == "" || account1.length != 11)) {
        $("#divAccount1").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divAccount1").removeClass("form-group has-error");
    }

    if (actionDate == "") {
        $("#divActionDate").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divActionDate").removeClass("form-group has-error");
        var aDate = new Date(actionDate.substring(6, 10) + '-' + actionDate.substring(3, 5) + '-' + actionDate.substring(0, 2));
        var bDate = new Date($("#hdnPrevActionDate").val().substring(6, 10) + '-' + $("#hdnPrevActionDate").val().substring(3, 5) + '-'
            + $("#hdnPrevActionDate").val().substring(0, 2))
            
       

        errorMessage = "";
        if (aDate <= bDate) {
            result = false;
            $("#divActionDate").removeClass("form-group has-error");
            errorMessage = $("#DDLStatus option:selected").text() + " Date should be greater than " + $("#hdnPrevStage").val() + " date";
        }

    }

    if ((productSold0 == "E-DFS" || productSold0 == "E-VFS") && industryMajor0 == "-1") {
        $("#divIndustryMajor0").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divIndustryMajor0").removeClass("form-group has-error");
    }

    if ((productSold1 == "E-DFS" || productSold1 == "E-VFS") && industryMajor1 == "-1") {
        $("#divIndustryMajor1").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divIndustryMajor1").removeClass("form-group has-error");
    }

    if ((currentStatus != "Sanctioned" && currentStatus != "Disbursed") && status == 8) {
        result = false;
        errorMessage = "Disbursal can be done only after sanctioning.";
    }

    if ((currentStatus != "Submitted for sanctioning" && currentStatus != "Sanctioned" && currentStatus != "Disbursed") && status == 7) {
        result = false;
        errorMessage = "Sanctioning can be done only after submitting for sanctioning.";
    }

    if ((currentStatus != "Submitted for sanctioning" && currentStatus != "Submitted for QC" && currentStatus != "Sanctioned" && currentStatus != "Disbursed") && status == 6) {
        result = false;
        errorMessage = "Submitting for sanctioning can be done only after submitting for QC.";
    }

    if ((currentStatus != "Converted lead" && currentStatus != "Submitted for QC" && currentStatus != "Sanctioned" && currentStatus != "Disbursed" && currentStatus != "Submitted for sanctioning") && status == 5) {
        result = false;
        errorMessage = "Submitting for QC can be done only after converting the lead.";
    }


    return result;

}


$("#btnSubmit").click(function () {
    if (validate() == true) {
        var leadId = getParameterByName('lead_id');

        var status = $("#DDLStatus option:selected").text();
        //var addMeeting = $("#chckMeeting").val();

        var newEnhance = $("DDLNewEnhance").val();

        var productSold0 = $("#DDLProductSold0").val();
        var account0 = $("#txtAccount0").val();
        var amount0 = $("#txtAmount0").val();
        var industryMajor0 = $("#DDLIndustryMajor0").val();

        var productSold1 = $("#DDLProductSold1").val();
        var account1 = $("#txtAccount1").val();
        var amount1 = $("#txtAmount1").val();
        var industryMajor1 = $("#DDLIndustryMajor1").val();

        var nextAppointmentDate = $("#txtNextAppointment").val();

        var actionDate = $("#txtDateOfAction").val();
        var addMeeting = $('input[id="chckMeeting"]:checked').length > 0 ? 'on' : 'off';

        $.ajax({
            url: 'TrackLead.aspx/SaveProcessDetails',
            type: "POST",
            dataType: "json",
            data: "{'leadId': '" + leadId + "','status': '" + status + "','chkMeeting' : '" + addMeeting + "','nextAppointment' : '" + nextAppointmentDate +
                "','newEnhance' : '" + newEnhance +
                "','product0' : '" + productSold0 + "','account0' : '" + account0 + "','amount0' : '" + amount0 +
                "','product1' : '" + productSold1 + "','account1' : '" + account1 + "','amount1' : '" + amount1 +
                "','actionDate' : '" + actionDate + "','comments' : '" + $("#txtComments").val() + "','industryMajor0' : '" + industryMajor0
                + "','industryMajor1' : '" + industryMajor1 + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {


                var messageContent = "<center><h1>Thank You</h1><h6>" + data.d + " </h6>" +
                       "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                ShowDialog(messageContent, 100, 300, 'Message', 10, '<span class="icon-info"></span>',true,window.location.href);
            }
        });

    }
    else {

        ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly.", 100, 300, 'Message', 10, '<span class="icon-info"></span>',false);
    }

});

function ValidateMeeting() {

    var meetingDate = $("#txtAddMeetingMeetDate").val();

    var result = true;

    if (meetingDate == "") {
        $("#divaddMeetingMeetDate").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divaddMeetingMeetDate").removeClass("form-group has-error");
    }

    return result;
}


$("#btnAddMeeting").click(function () {
    if (ValidateMeeting() == true) {
        var leadId = getParameterByName('lead_id');
        var meetingDate = $("#txtAddMeetingMeetDate").val();

        var nextAppointmentDate = $("#txtAddMeetingNextAppointmentDate").val();

        //var actionDate = $("#txtDateOfAction").val();\
	if(meetingDate !="")
        meetingDate = meetingDate.substring(6, 10) + '-' + meetingDate.substring(3, 5) + '-' + meetingDate.substring(0, 2);

	if(nextAppointmentDate !="")
        nextAppointmentDate = nextAppointmentDate.substring(6, 10) + '-' + nextAppointmentDate.substring(3, 5) + '-' + nextAppointmentDate.substring(0, 2);

        $.ajax({
            url: 'TrackLead.aspx/SaveMeetingDetails',
            type: "POST",
            dataType: "json",
            data: "{'meetingDate': '" + meetingDate + "','nextAppointment': '" + nextAppointmentDate + "','comments': '" + $("#txtMeetingDescription").val()
                + "','leadId': '" + leadId + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {

                ShowDialog(data.d, 100, 300, 'Message', 10, '<span class="icon-info"></span>', true, window.location.href);
            }
        });

    }
    else {
        ShowDialog("Please enter all the values correctly.", 100, 300, 'Message', 10, '<span class="icon-info"></span>',false);
    }

});

$("#txtAmount0").focusout(function (event) {

    var num2words = new NumberToWords();
    num2words.setMode("indian");
    if (event.which == 13) {
        event.preventDefault();
    }
    $("#lblAmount0").html(replaceAll("Zero", "", num2words.numberToWords($("#txtAmount0").val())));
});

$("#txtAmount1").focusout(function (event) {
    var num2words = new NumberToWords();
    num2words.setMode("indian");
    if (event.which == 13) {
        event.preventDefault();
    }
    $("#lblAmount1").html(replaceAll("Zero", "", num2words.numberToWords($("#txtAmount1").val())));
});



function NumbersToWords(inputNumber) {
    var inputNo = inputNumber;

    if (inputNo == 0)
        return "Zero";

    var numbers = new Array(4);
    var first = 0;
    var u, h, t;
    var sb = '';

    if (inputNo < 0) {
        sb += "Minus ";
        inputNo = -inputNo;
    }

    var words0 = ["", "One ", "Two ", "Three ", "Four ",
    "Five ", "Six ", "Seven ", "Eight ", "Nine "];
    var words1 = ["Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
    "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen "];
    var words2 = ["Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
    "Seventy ", "Eighty ", "Ninety "];
    var words3 = ["Thousand ", "Lakh ", "Crore "];

    numbers[0] = parseInt(inputNo % 1000); // units
    numbers[1] = parseInt(inputNo / 1000);
    numbers[2] = parseInt(inputNo / 100000);
    numbers[1] = parseInt(numbers[1] - 100 * numbers[2]); // thousands
    numbers[3] = parseInt(inputNo / 10000000); // crores
    numbers[2] = parseInt(numbers[2] - 100 * numbers[3]); // lakhs

    for (var i = 3; i > 0; i--) {
        if (numbers[i] != 0) {
            first = i;
            break;
        }
    }

    for (var i = first; i >= 0; i--) {
        if (numbers[i] == 0) continue;
        u = parseInt(numbers[i] % 10); // ones
        t = parseInt(numbers[i] / 10);
        h = parseInt(numbers[i] / 100); // hundreds
        t = parseInt(t - 10 * h); // tens
        if (h > 0) sb += words0[h] + "Hundred ";
        if (u > 0 || t > 0) {
            if (h > 0 || i == 0) sb += "and ";
            if (t == 0)
                sb += words0[u];
            else if (t == 1)
                sb += words1[u];
            else
                sb += words2[t - 2] + words0[u];
        }
        if (i != 0) sb += words3[i - 1];
    }
    //alert(sb);
    return sb;
}


function GetUserDetail(EmpId) {
    $.ajax({
        url: 'AddUser.aspx/GetUserDetail',
        type: "POST",
        dataType: "json",
        data: "{'empId': '" + $("#hdnEmpId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            var user = data.d;
            if (user["id"] == null) {
                ShowDialog("User does not exist.", 100, 300, 'Message', 10, '<span class="icon-info"></span>',true);
            }
            else {
                if (user["staff_type"] != "Sale Staff" && user["staff_type"] != "Admin") {
                    $("#tabProcess").hide();
                    $("#tabAddMeeting").hide();
                    $("#tabAddProduct").hide();
                }

            }
        }
    });
}

function GetIndustryMajor() {
    $.ajax({
        url: 'AddLead.aspx/GetIndustryMajor',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLIndustryMajor0').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLIndustryMajor1').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}


var tabing = function () {
    var nav = $(".tabs"),
     slideCon = $(".frames");
    slideCon.find(".frame").hide();
    var id = nav.find(".selected").attr("href");
    $(id).show();
    $('a[href*="' + "#" + id + '"]').addClass("selected");
    nav.on("click", "li a", function (e) {
        e.preventDefault();
        var $this = $(this);
        var parentItem = $($this.attr("href"));
        var currentTab = $this.attr("href");
        if (parentItem.is(":visible")) {
            return;
        }
        else {
            slideCon.find(".frame").hide();
            parentItem.show();
            $this.addClass("selected").closest("li").siblings().find("a").removeClass("selected");
            var leadId = getParameterByName('lead_id');
            if (currentTab == "#_page_process") {
                $.ajax({
                    url: 'TrackLead.aspx/GetProcessDetails',
                    type: "POST",
                    dataType: "json",
                    data: "{'leadId': '" + leadId + "'}",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (data) {
                        var obj = JSON.parse(data.d);

                        if ('CurrentStatus' in obj) {
                            $("#DDLStatus option:contains(" + obj['CurrentStatus'] + ")").attr('selected', 'selected');
                            $("#currentStatus").val(obj['CurrentStatus']);
                        }

                        if ($("#DDLStatus").val() == 4)
                        {
                            $("#divNewEnhancement").show();
                            if ('new_enhance' in obj)
                            {
                                $("#DDLNewEnhance").val(obj['new_enhance']);
                            }
                        }

                        if ($("#DDLStatus").val() > 3)
                            $("#divConverted").show();

                        if ('ProductSold0' in obj)
                            $("#DDLProductSold0").val(obj['ProductSold0']);
                        if ('ac_no0' in obj)
                            $("#txtAccount0").val(obj['ac_no0']);
                        if ('amount0' in obj)
                            $("#txtAmount0").val(obj['amount0']);

                        if ('ProductSold1' in obj)
                            $("#DDLProductSold1").val(obj['ProductSold1']);
                        if ('ac_no1' in obj)
                            $("#txtAccount1").val(obj['ac_no1']);
                        if ('amount0' in obj)
                            $("#txtAmount1").val(obj['amount1']);

                        if (obj['ProductSold0'] == 'E-DFS' || obj['ProductSold0'] == 'E-VFS')
                            $('#outerDivIndustry0').show();
                        if ('industry_major0' in obj)
                            $("#DDLIndustryMajor0").val(obj['industry_major0']);

                        if (obj['ProductSold1'] == 'E-DFS' || obj['ProductSold1'] == 'E-VFS')
                            $('#outerDivIndustry1').show();
                        if ('industry_major1' in obj)
                            $("#DDLIndustryMajor1").val(obj['industry_major1']);
                        if ('action_date' in obj)
                            $("#hdnPrevActionDate").val(obj['action_date']);
                    }
                });
            }
            else if (currentTab == "#_page_feed") {
                var feedTable = $('#tblFeed').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 25,
                    "aLengthMenu": [[25, 30, 45, 60], [25, 30, 45, 60]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,//TODO :- Sorting searching to happen on DB side.
                    "bDestroy": true,
                    "sAjaxSource": "TrackLead.aspx/GetFeed",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblFeed").show();
                                }
                        });
                    }
                });
            }
            else if (currentTab == "#_page_products") {
                var productTable = $('#tblProduct').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "TrackLead.aspx/GetProductSold",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblProduct").show();
                                        }
                        });
                    }
                });

                var crossproductTable = $('#tblCrossSellProduct').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "TrackLead.aspx/GetCrossSellProductSold",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblCrossSellProduct").show();
                                        }
                        });
                    }
                });

                //
            }
            else if (currentTab == "#_page_meeting") {
                var productTable = $('#tblMeeting').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "TrackLead.aspx/GetMeetingList",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblMeeting").show();
                                        }
                        });
                    }
                });
            }

            else if (currentTab == "#_page_add_product") {
                $.ajax({
                    url: 'TrackLead.aspx/GetCrossSellProducts',
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (data) {
                        var content = '<h1 class="tab-head">Add Cross Sell Product</h1><div style="margin-left:15px;margin-bottom:20px;">';
                        $.each(data, function () {
                            $.each(this, function (k, v) {
                                content += '<label class="inline-block"><input type="checkbox" id="' + v + '" /><span class="check" style="margin-left:1em"></span>' + k + '</label>';
                                if (v.indexOf('Type1') > 0) {
                                    content += '<input id="txt' + v + '" style="margin-left:5em;display:none;" placeholder="a/c no" autofocus required />'
                                }
                                content += '<br/><br/>';
                            });
                        });

                        content += '<input type="button" class="btn btn-primary btn-sm" value="Add Product" id="btnAddProduct" /></div>';
                        $("#_page_add_product").html(content);
                    }
                });
            }

        }




    });


}


function NumberToWords() {

    var units = ["Zero", "One", "Two", "Three", "Four", "Five", "Six",
      "Seven", "Eight", "Nine", "Ten"];
    var teens = ["Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen",
      "Sixteen", "Seventeen", "Eighteen", "Nineteen", "Twenty"];
    var tens = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty",
      "Seventy", "Eighty", "Ninety"];

    var othersIndian = ["Thousand", "Lakh", "Crore"];

    var othersIntl = ["Thousand", "Million", "Billion", "Trillion"];

    var INDIAN_MODE = "indian";
    var INTERNATIONAL_MODE = "international";
    var currentMode = INDIAN_MODE;

    var getBelowHundred = function (n) {
        if (n >= 100) {
            return "greater than or equal to 100";
        };
        if (n <= 10) {
            return units[n];
        };
        if (n <= 20) {
            return teens[n - 10 - 1];
        };
        var unit = Math.floor(n % 10);
        n /= 10;
        var ten = Math.floor(n % 10);
        var tenWord = (ten > 0 ? (tens[ten] + " ") : '');
        var unitWord = (unit > 0 ? units[unit] : '');
        return tenWord + unitWord;
    };

    var getBelowThousand = function (n) {
        if (n >= 1000) {
            return "greater than or equal to 1000";
        };
        var word = getBelowHundred(Math.floor(n % 100));

        n = Math.floor(n / 100);
        var hun = Math.floor(n % 10);
        word = (hun > 0 ? (units[hun] + " Hundred ") : '') + word;

        return word;
    };

    return {
        numberToWords: function (n) {
            if (isNaN(n)) {
                return "Not a number";
            };

            var word = '';
            var val;

            val = Math.floor(n % 1000);
            n = Math.floor(n / 1000);

            word = getBelowThousand(val);

            if (this.currentMode == INDIAN_MODE) {
                othersArr = othersIndian;
                divisor = 100;
                func = getBelowHundred;
            } else if (this.currentMode == INTERNATIONAL_MODE) {
                othersArr = othersIntl;
                divisor = 1000;
                func = getBelowThousand;
            } else {
                throw "Invalid mode - '" + this.currentMode
                  + "'. Supported modes: " + INDIAN_MODE + "|"
                  + INTERNATIONAL_MODE;
            };

            var i = 0;
            while (n > 0) {
                if (i == othersArr.length - 1) {
                    word = this.numberToWords(n) + " " + othersArr[i] + " "
                      + word;
                    break;
                };
                val = Math.floor(n % divisor);
                n = Math.floor(n / divisor);
                if (val != 0) {
                    word = func(val) + " " + othersArr[i] + " " + word;
                };
                i++;
            };
            return word;
        },
        setMode: function (mode) {
            if (mode != INDIAN_MODE && mode != INTERNATIONAL_MODE) {
                throw "Invalid mode specified - '" + mode
                  + "'. Supported modes: " + INDIAN_MODE + "|"
                  + INTERNATIONAL_MODE;
            };
            this.currentMode = mode;
        }
    }
}