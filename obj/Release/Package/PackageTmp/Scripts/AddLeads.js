﻿


$(document).ready(function () {

    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
        (('' + month).length < 2 ? '0' : '') + month + '-' +
        (('' + day).length < 2 ? '0' : '') + day;
    $("#txtAppointmentDate").prop("readonly", true);
    $("#txtAppointmentDate").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w" });

    $('#datetimepicker10 input').on("keydown", function (e) { e.preventDefault(); });
    GetTMO();
    $('#hdntmo').val($("#txtEmpId").val());
    GetCustomers($("#txtEmpId").val());
    GetProductList();
    GetUserDetail($("#txtEmpId").val());

    $("#divalert").hide();

    
});


$('#chkselfAllocate').prop('checked', true);
$("#divtmomeeting").hide()
$("#Divcustomer").hide()
$("#Divappointment").hide();

$("#chkappointmentdate").change(function () {
    var checkbox = $('#chkappointmentdate').is(':checked');
    if (checkbox == true) {
        $("#Divappointment").show();
    }
    else {
        $("#Divappointment").hide();
    }

});

$("#chkselfAllocate").change(function () {
    var checkbox = $('#chkselfAllocate').is(':checked');
    if (checkbox == false) {
        $('#ddlcustomer').html('');
        $('#ddlcustomer').append('<option value="">' + 'Select' + '</option>');
        $("#divtmomeeting").show();
        $("#Divcustomer").show();
    }
    else {
        $('#hdntmo').val($("#txtEmpId").val());
        GetCustomers($("#txtEmpId").val());
        $("#divtmomeeting").hide();
        $("#Divcustomer").hide();

    }


});

$("#DdlTmo").change(function () {
     
    $('#hdntmo').val($("#DdlTmo").val());
    GetCustomers($("#DdlTmo").val());
});

var customertype = '1';
function GetUserDetail(EmpId) {

    $.ajax({
        url: 'AddUser.aspx/GetUserDetail',
        type: "POST",
        dataType: "json",
        data: "{'empId': '" + EmpId + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            var user = data.d;
            if (user["id"] == null) {
                ShowDialog("User does not exist.", 100, 300, 'Message', 10, '<span class="icon-info"></span>', true);
            }
            else {

                if (user["staff_type"] == "Sale Staff") {
                    $("#divSelfAllocate").show();
                    $("#chkselfAllocate").prop('checked', true);
                    $("#DDLSalesType").val(user["saletype"]);

                }
                else {
                    $("#divSelfAllocate").hide();
                    if (user["staff_type"] == "Admin") {
                        GetUserList();
                        $("#trEnterAs").show();
                    }
                }
            }
        }
    });
}

$("#DeleteProduct1").click(function () {
    
    $("#DDLProductInterest1").find('option:first').attr('selected', 'selected');
    $("#Delete1").hide()
    $("#Product1").hide();
    $("#AddMore1").hide();
    $("#AddMore").show();
    $("#divplain1").hide();
    $("#divSwaps1").hide();

});



function GetTMO() {
    $.ajax({
        url: 'AddLead.aspx/GetTMO',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {
                    $('#DdlTmo').append('<option value="' + v + '">' + k + '</option>');

                });
            });
        }
    });
}

$("#Addproduct").click(function () {

    $("#Product1").show();
    $("#AddMore1").show();
    $("#Delete1").show()
    $("#AddMore").hide();

});
$("#Addproduct1").click(function () {

    if ($("#Product2").css('display') == 'none') {
        $("#Product2").show();
        $("#AddMore2").show();
        $("#Delete1").hide();
        $("#Delete2").show();
        $("#AddMore1").hide();
    }


});
$("#Addproduct2").click(function () {

    if ($("#Product3").css('display') == 'none') {
        $("#Product3").show();
        $("#AddMore3").show();
        $("#Delete2").hide();
        $("#Delete3").show();
        $("#AddMore2").hide();
    }


});

$("#Addproduct3").click(function () {

    if ($("#Product4").css('display') == 'none') {
        $("#Product4").show();
        $("#AddMore4").show();
        $("#Delete3").hide();
        $("#Delete4").show();
        $("#AddMore3").hide();
    }


});

$("#Addproduct4").click(function () {

    if ($("#Product5").css('display') == 'none') {
        $("#Product5").show();
        $("#AddMore5").show();
        $("#Delete4").hide();
        $("#Delete5").show();
        $("#AddMore4").hide();
    }


});

$("#Addproduct5").click(function () {

    if ($("#Product6").css('display') == 'none') {
        $("#Product6").show();
        $("#AddMore6").show();
        $("#Delete5").hide();
        $("#Delete6").show();
        $("#AddMore5").hide();
    }


});

$("#Addproduct6").click(function () {

    if ($("#Product7").css('display') == 'none') {
        $("#Product7").show();
        $("#AddMore7").show();
        $("#Delete6").hide();
        $("#Delete7").show();
        $("#AddMore6").hide();
    }


});

$("#Addproduct7").click(function () {

    if ($("#Product8").css('display') == 'none') {
        $("#Product8").show();
        $("#AddMore8").show();
        $("#Delete7").hide();
        $("#Delete8").show();
        $("#AddMore7").hide();
    }


});

$("#Addproduct8").click(function () {

    if ($("#Product9").css('display') == 'none') {
        $("#Product9").show();
        $("#AddMore9").show();
        $("#Delete8").hide();
        $("#Delete9").show();
        $("#AddMore8").hide();
    }


});

$("#Addproduct9").click(function () {

    if ($("#Product10").css('display') == 'none') {
        $("#Product10").show();
        $("#AddMore10").show();
        $("#Delete9").hide();
        $("#Delete10").show();
        $("#AddMore9").hide();
    }


});


function reply_click(clicked_id) {
    
    hideDelDiv(clicked_id);
}

function ddl_change(clicked_id) {
    manageddlChange(clicked_id);
}

function hideDelDiv(id) {
    
    var idVar = id.substring(13);
    $("#DDLProductInterest" + idVar).find('option:first').attr('selected', 'selected');
    $("#Product" + idVar).hide();
    $("#divplain" + idVar).hide();
    $("#divSwaps" + idVar).hide();
    $("#AddMore" + idVar).hide();
    $("#Delete" + idVar).hide();
    if (idVar > 1) {
        $("#Delete" + (idVar - 1)).show();
        $("#AddMore" + (idVar - 1)).show();
    }



}





function GetProductList() {

    $.ajax({
        url: 'AddLead.aspx/GetProductList',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLProductInterest').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductInterest1').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductInterest2').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductInterest3').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductInterest4').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductInterest5').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductInterest6').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductInterest7').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductInterest8').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductInterest9').append('<option value="' + v + '">' + k + '</option>');
                });


            });
        }
    });
}

function GetUserList() {
    $.ajax({
        url: 'AddUser.aspx/GetUserNames',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLEntryAs').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetCustomers(tmoId) {
    $.ajax({
        url: 'AddLead.aspx/GetCustomers',
        type: "POST",
        dataType: "json",
        data: "{'tmoId': '" + tmoId + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            $('#ddlcustomer').html('');
            $('#ddlcustomer').append('<option value="">' + 'Select' + '</option>');
            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#ddlcustomer').append('<option value="' + k + '">' + v + '</option>');
                });
            });
        }
    });
}

$("#txtPhone").change(function () {

    $.ajax({
        url: 'AddLead.aspx/ValidatePhoneEmail',
        type: "POST",
        dataType: "json",
        data: "{'value' : '" + $("#txtPhone").val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            var list = data.d;
            if (list.length == "0") {
                customertype = '1';
                $("#txtName").prop('disabled', false);
                $("#txtPhoneAlt").prop('disabled', false);
                $("#txtEmail").prop('disabled', false);
                $("#txtEmail").prop('disabled', false);
                $("#txtAddress").prop('disabled', false);
                $("#txtName").val("");
                $("#txtPhoneAlt").val("");
                $("#txtEmail").val("");
                $("#txtAddress").val("");
                $("#divalert").hide();
            }
            else {
                $("#divalert").show();
                $("#txtName").val(list[2]);
                $("#txtPhoneAlt").val(list[5]);
                $("#txtEmail").val(list[6]);
                $("#txtAddress").val(list[3]);
                $("#txtName").prop('disabled', true);
                $("#txtPhoneAlt").prop('disabled', true);
                $("#txtEmail").prop('disabled', true);
                $("#txtEmail").prop('disabled', true);
                $("#txtAddress").prop('disabled', true);
                customertype = '0';
            }


        }
    });
});

$("#txtEmail").change(function () {

    $.ajax({
        url: 'AddLead.aspx/ValidatePhoneEmail',
        type: "POST",
        dataType: "json",
        data: "{'value' : '" + $("#txtEmail").val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            var list = data.d;
            if (list.length == "0") {
                customertype = '1';
                $("#txtName").prop('disabled', false);
                $("#txtPhone").prop('disabled', false);
                $("#txtPhoneAlt").prop('disabled', false);
                $("#txtEmail").prop('disabled', false);

                $("#txtAddress").prop('disabled', false);
                //$("#txtName").val("");
                //$("#txtPhoneAlt").val("");
                //$("#txtPhone").val("");
                $("#txtAddress").val("");
                $("#divalert").hide();
            }
            else {
                $("#divalert").show();
                $("#txtName").val(list[2]);
                $("#txtPhoneAlt").val(list[5]);
                $("#txtPhone").val(list[4]);
                $("#txtAddress").val(list[3]);
                $("#txtName").prop('disabled', true);
                $("#txtPhoneAlt").prop('disabled', true);
                $("#txtPhone").prop('disabled', true);

                $("#txtAddress").prop('disabled', true);
                customertype = '0';
            }


        }
    });
});




var errorMsg;

function validate() {

    var salutation = $("#DDLSalutation").val();
    var salesType = $("#DDLSalesType").val();
    var productInterest = $("#DDLProductInterest").val();
    var circle = $("#DDLCircle").val();
    var network = $("#DDLNetwork").val();
    var branch = $("#DDLBranch").val();
    var name = $('#txtName').val();
    var phone = $('#txtPhone').val();
    var phoneAlt = $('#txtPhoneAlt').val();
    var email = $('#txtEmail').val();
    var address = $('#txtAddress').val();
    var industryMajor = $('#DDLIndustryMajor').val();
    var proposalType = $('#DDLProposalType').val();
    var result = true;
    errorMsg = '';

    if (salutation == "-1") {
        errorMsg = errorMsg + "<br/>Please select salutation.";
        $("#divSalutation").addClass("form-group has-error");
        result = false;

    }
    else {
        $("#divSalutation").removeClass("form-group has-error");
    }

    //if (salesType == "-1") {
    //    errorMsg = errorMsg + "<br/>Please select sales type.";
    //    $("#divSalesType").addClass("form-group has-error");
    //    result = false;
    //}
    //else {
    //    $("#divSalesType").removeClass("form-group has-error");
    //}

    //if (proposalType == "-1") {
    //    errorMsg = errorMsg + "<br/>Please select proposal type.";
    //    $("#divProposalType").addClass("form-group has-error");
    //    result = false;
    //}
    //else {
    //    $("#divProposalType").removeClass("form-group has-error");
    //}

    if (productInterest == "-1") {
        errorMsg = errorMsg + "<br/>Please select product interest.";
        $("#divProductInterest").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divProductInterest").removeClass("form-group has-error");
    }

    if ((productInterest == "3" || productInterest == "4") && industryMajor == "-1") {
        errorMsg = errorMsg + "<br/>Please select industry major.";
        $("#divIndustryMajor").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divIndustryMajor").removeClass("form-group has-error");
    }

    if (circle == "-1") {
        errorMsg = errorMsg + "<br/>Please select circle.";
        $("#divCircle").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divCircle").removeClass("form-group has-error");
    }

    if (network == "-1") {
        errorMsg = errorMsg + "<br/>Please select network.";
        $("#divNetwork").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divNetwork").removeClass("form-group has-error");
    }

    if (branch == "-1") {
        errorMsg = errorMsg + "<br/>Please select branch.";
        $("#divBranch").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divBranch").removeClass("form-group has-error");
    }

    if (name == "") {
        errorMsg = errorMsg + "<br/>Please enter Customer  name.";
        $('#divName').addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divName").removeClass("form-group has-error");
    }

    if (address == "") {
        errorMsg = errorMsg + "<br/>Please enter address.";
        $('#divAddress').addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divAddress").removeClass("form-group has-error");
    }

    if (phone == "" || phone.length > 10 || phone.length < 10 || isNaN(phone)) {
        $('#divPhone').addClass("form-group has-error");
        result = false;
        errorMsg += '<br/>Please enter correct phone number.(Should have 10 digits)';
    }
    else {
        $("#divPhone").removeClass("form-group has-error");
    }

    if (phoneAlt != "") {
        if (phoneAlt.length > 10 || phoneAlt.length < 10 || isNaN(phoneAlt)) {
            $('#divPhoneAlt').addClass("form-group has-error");
            result = false;
        }
        else {
            $("#divPhoneAlt").removeClass("form-group has-error");
        }
    }
    else {
        $("#divPhoneAlt").removeClass("form-group has-error");
    }
    if (!IsEmail(email)) {
        $('#divEmail').addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divEmail").removeClass("form-group has-error");
    }



    return result;
}
function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function validatecustomer() {
    var name = $('#txtName').val();
    var phone = $('#txtPhone').val();
    var phoneAlt = $('#txtPhoneAlt').val();
    var email = $('#txtEmail').val();
    var address = $('#txtAddress').val();
    var result = true;
    errorMsg = '';

    if (address == "") {
        errorMsg = errorMsg + "<br/>Please enter address.";
        $('#divAddress').addClass("form-group has-error");
        result = false;

    }
    else {
        $("#divAddress").removeClass("form-group has-error");
    }

    if (phone == "" || phone.length > 10 || phone.length < 10 || isNaN(phone)) {
        $('#divPhone').addClass("form-group has-error");
        result = false;
        errorMsg += '<br/>Please enter correct phone number.(Should have 10 digits)';
    }
    else {
        $("#divPhone").removeClass("form-group has-error");
    }

    if (phoneAlt != "") {
        if (phoneAlt.length > 10 || phoneAlt.length < 10 || isNaN(phoneAlt)) {
            $('#divPhoneAlt').addClass("form-group has-error");
            result = false;
        }
        else {
            $("#divPhoneAlt").removeClass("form-group has-error");
        }
    }
    else {
        $("#divPhoneAlt").removeClass("form-group has-error");
    }
    if (!IsEmail(email)) {
        $('#divEmail').addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divEmail").removeClass("form-group has-error");
    }
    return result
}
var errorMessage = '';

function Validatelead() {

    var result = true;
    var ddlcustomer = $("#ddlcustomer").val();
    //var AppointmentDate = $("#txtAppointmentDate").val();
    var Comments = $("#txtComments").val();
    if (ddlcustomer == "") {
        errorMsg = errorMsg + "<br/>Select customer";
        $("#ddlcustomer").addClass("form-group has-error");
        result = false;
    } else {
        $("#ddlcustomer").removeClass("form-group has-error");

    }
    //if (AppointmentDate == "") {
    //    errorMsg = errorMsg + "<br/>Enter Appointment Date";
    //    $("#txtAppointmentDate").addClass("form-group has-error");
    //    result = false;
    //} else {
    //    $("#txtAppointmentDate").removeClass("form-group has-error");

    //}

    if (Comments == "") {
        errorMsg = errorMsg + "<br/>Enter comment";
        $("#txtComments").addClass("form-group has-error");
        result = false;
    } else {
        $("#txtComments").removeClass("form-group has-error");

    }

    return result;
}
$("#btnAddLead").click(function () {
    
    if (Validatelead() == true) {
        window.SBICommon.sendAjaxCall({
            url: 'AddLead.aspx/CreateLead',
            data: "{'employeeId': '" + $("#hdntmo").val() + "','customerId': '" + $("#ddlcustomer").val() + "','salesType' : '" + 'tmo' +
                "','productInterest' : '" + $('#DDLProductInterest').val() +
                "','productInterest1' : '" + $('#DDLProductInterest1').val() + "','productInterest2' : '" + $('#DDLProductInterest2').val() + "','productInterest3' : '" + $('#DDLProductInterest3').val() +
               "','productInterest4' : '" + $('#DDLProductInterest4').val() + "','productInterest5' : '" + $('#DDLProductInterest5').val() + "','productInterest6' : '" + $('#DDLProductInterest6').val() +
               "','productInterest7' : '" + $('#DDLProductInterest7').val() + "','productInterest8' : '" + $('#DDLProductInterest8').val() + "','productInterest9' : '" + $('#DDLProductInterest9').val() +
               "','nextappointmentdate' : '" + $('#txtAppointmentDate').val() + "','comment' : '" + $('#txtComments').val() + "','Plain' : '" + $('#DDLPlain').val() + "','Swaps' : '" + $('#DDLSwaps').val() +
               "','Plain1' : '" + $('#DDLPlain1').val() + "','Swaps1' : '" + $('#DDLSwaps1').val() +
                  "','Plain2' : '" + $('#DDLPlain2').val() + "','Swaps2' : '" + $('#DDLSwaps2').val() +
                     "','Plain3' : '" + $('#DDLPlain3').val() + "','Swaps3' : '" + $('#DDLSwaps3').val() +
                        "','Plain4' : '" + $('#DDLPlain4').val() + "','Swaps4' : '" + $('#DDLSwaps4').val() +
                           "','Plain5' : '" + $('#DDLPlain5').val() + "','Swaps5' : '" + $('#DDLSwaps5').val() +
                              "','Plain6' : '" + $('#DDLPlain6').val() + "','Swaps6' : '" + $('#DDLSwaps6').val() +
                                 "','Plain7' : '" + $('#DDLPlain7').val() + "','Swaps7' : '" + $('#DDLSwaps7').val() +
                                    "','Plain8' : '" + $('#DDLPlain8').val() + "','Swaps8' : '" + $('#DDLSwaps8').val() +
                                      "','Plain9' : '" + $('#DDLPlain9').val() + "','Swaps9' : '" + $('#DDLSwaps9').val() +

               "'}",
            requestType: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: true,
            success: function (data) {

                $("#btnAddLead").attr({ disabled: false });
                if (data.d == "This Lead already exists.")
                    ShowDialog(data.d, 400, 600, 'Message', 10, '<span class="icon-info"></span>', false);
                else {
                    var arr = (data.d).split(",");
                    var leadval = arr.shift();
                    var messageContent = "<center><h1>Thank You</h1><p>Your lead has been successfully submitted</p><hr><p><h6>" + leadval + " </h6></p>" +
              "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";

                    ShowDialog(messageContent, 400, 600, 'Message', 10, '<span class="icon-info"></span>', true, 'AddLead.aspx');
                }

            },

            showLoader: true
    , complete: function (xhr, data) {
        console.log(xhr.status);
        if (xhr.status == 401)
            window.location.href = '/home.aspx';
    }
        });
    } else {

        ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    }

});


$("#btnaddcustomer").click(function () {

    if (validatecustomer() == true) {
        var action;

        if (typeof google == 'undefined') {
            ShowDialog("Please check the internet connectivity, Vijaypath is unable to contact Google maps for the location of the lead.", 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
            return;
        }

        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({
            address: $("#txtAddress").val(),
            region: 'no'
        },
            function (results, status) {
                if (status.toLowerCase() == 'ok') {
                    // Get center
                    var coords = new google.maps.LatLng(
                        results[0]['geometry']['location'].lat(),
                        results[0]['geometry']['location'].lng()
                    );
                    //alert('Latitute: ' + coords.lat() + '    Longitude: ' + coords.lng());

                    $('#divAddress').removeClass("form-group has-error");


                    window.SBICommon.sendAjaxCall({
                        url: 'AddNewCustomer.aspx/CreateCustomer',
                        data: "{'employeeId': '" + $("#txtEmpId").val() +
                            "','branch' : '" + $('#DDLBranch').val() +
                            "','circle' : '" + $('#DDLCircle').val() +
                            "','network' : '" + $('#DDLNetwork').val() +
                            "','address' : '" + $('#txtAddress').val() +
                           "','name' : '" + $('#txtName').val() +
                           "','phone' : '" + $('#txtPhone').val() +
                           "','phoneAlt' : '" + $('#txtPhoneAlt').val() +
                           "','email' : '" + $('#txtEmail').val() +
                           "','latitude' : '" + coords.lat() +
                           "','longitude' : '" + coords.lng() + "'}",
                        requestType: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        async: true,
                        success: function (data) {
                            if (data.d == "This Lead already exists.")
                                ShowDialog(data.d, 400, 600, 'Message', 10, '<span class="icon-info"></span>', false);
                            else {
                                var arr = (data.d).split(",");
                                var leadval = arr.shift();
                                var messageContent = "<center><h1>Thank You</h1><p>Customer  has been save successfully </p><hr><p><h6>" + leadval + " </h6></p>" +
                          "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                                //#chkselfAllocate
                                //$('input[name="chk[]"]:checked').length > 0 ? 'on' : 'false'
                                ShowDialog(messageContent, 400, 600, 'Message', 10, '<span class="icon-info"></span>', true);
                            }

                        },

                        showLoader: true
           , complete: function (xhr, data) {
               console.log(xhr.status);
               if (xhr.status == 401)
                   window.location.href = '/home.aspx';
           }
                    });
                }
                else {
                    $('#divAddress').addClass("form-group has-error");
                    ShowDialog("Enter valid address", 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
                    return;
                }

            });


    }
    else {
        ShowDialog("Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    }

});

$("#divplain").hide();
$("#divSwaps").hide();
$("#divplain1").hide();
$("#divSwaps1").hide();
$("#divplain2").hide();
$("#divSwaps2").hide();
$("#divplain3").hide();
$("#divSwaps3").hide();
$("#divplain4").hide();
$("#divSwaps4").hide();
$("#divplain5").hide();
$("#divSwaps5").hide();
$("#divplain6").hide();
$("#divSwaps6").hide();
$("#divplain7").hide();
$("#divSwaps7").hide();
$("#divplain8").hide();
$("#divSwaps8").hide();
$("#divplain9").hide();
$("#divSwaps9").hide();
$("#DDLProductInterest").change(function () {
    var Product_id = $('#DDLProductInterest').val()
    if (Product_id == '1025') {
        $("#divplain").show();
        PalinVanilaOptionshow("#DDLPlain");
    }
    else {
        $("#divplain").hide();
        $("#divSwaps").hide();
        PalinVanilaOptionHide("#DDLPlain");
        Swapshide("DDLSwaps");
    }
});
$("#DDLPlain").change(function () {
    var Product_id = $('#DDLPlain').val()
    var id = "#DDLSwaps";
    if (Product_id == 'Swaps') {
        $("#divSwaps").show();
        Swapsshow(id);
    }
    else {
        $("#divSwaps").hide();
        Swapshide(id);
    }
});


$("#DDLProductInterest1").change(function () {
    var Product_id = $('#DDLProductInterest1').val()
    if (Product_id == '1025') {
        $("#divplain1").show();
        PalinVanilaOptionshow("#DDLPlain1");

    }
    else {
        $("#divplain1").hide();
        $("#divSwaps1").hide();
        PalinVanilaOptionHide("#DDLPlain1");
        Swapshide("DDLSwaps1");
    }
});
$("#DDLPlain1").change(function () {
    var Product_id = $('#DDLPlain1').val()
    var id = "#DDLSwaps1";
    if (Product_id == 'Swaps') {
        $("#divSwaps1").show();
        Swapsshow(id);
    }
    else {
        $("#divSwaps1").hide();
        Swapshide(id);
    }
});

$("#DDLProductInterest2").change(function () {
    var Product_id = $('#DDLProductInterest2').val()
    if (Product_id == '1025') {
        $("#divplain2").show();
        PalinVanilaOptionshow("#DDLPlain2");
    }
    else {
        $("#divplain2").hide();
        $("#divSwaps2").hide();
        PalinVanilaOptionHide("#DDLPlain2");
        Swapshide("DDLSwaps2");
    }
});
$("#DDLPlain2").change(function () {
    var Product_id = $('#DDLPlain2').val()
    var id = "#DDLSwaps2";
    if (Product_id == 'Swaps') {
        $("#divSwaps2").show();
        Swapsshow(id);
    }
    else {
        $("#divSwaps2").hide();
        Swapshide(id);
    }
});



$("#DDLProductInterest3").change(function () {
    var Product_id = $('#DDLProductInterest3').val()
    if (Product_id == '1025') {
        $("#divplain3").show();
        PalinVanilaOptionshow("#DDLPlain3");
    }
    else {
        $("#divplain3").hide();
        $("#divSwaps3").hide();
        PalinVanilaOptionHide("#DDLPlain3");
        Swapshide("DDLSwaps3");
    }
});
$("#DDLPlain3").change(function () {
    var Product_id = $('#DDLPlain3').val()
    var id = "#DDLSwaps3";
    if (Product_id == 'Swaps') {
        $("#divSwaps3").show();
        Swapsshow(id);
    }
    else {
        $("#divSwaps3").hide();
        Swapshide(id);
    }
});

$("#DDLProductInterest4").change(function () {
    var Product_id = $('#DDLProductInterest4').val()
    if (Product_id == '1025') {
        $("#divplain4").show();
        PalinVanilaOptionshow("#DDLPlain4");
    }
    else {
        $("#divplain4").hide();
        $("#divSwaps4").hide();
        PalinVanilaOptionHide("#DDLPlain4");
        Swapshide("DDLSwaps4");
    }
});
$("#DDLPlain4").change(function () {
    var Product_id = $('#DDLPlain4').val()
    var id = "#DDLSwaps4";
    if (Product_id == 'Swaps') {
        $("#divSwaps4").show();
        Swapsshow(id);
    }
    else {
        $("#divSwaps4").hide();
        Swapshide(id);
    }
});
$("#DDLProductInterest5").change(function () {
    var Product_id = $('#DDLProductInterest5').val()
    if (Product_id == '1025') {
        $("#divplain5").show();
        PalinVanilaOptionshow("#DDLPlain5");
    }
    else {
        $("#divplain5").hide();
        $("#divSwaps5").hide();
        PalinVanilaOptionHide("#DDLPlain5");
        Swapshide("DDLSwaps5");
    }
});
$("#DDLPlain5").change(function () {
    var Product_id = $('#DDLPlain5').val()
    var id = "#DDLSwaps5";
    if (Product_id == 'Swaps') {
        $("#divSwaps5").show();
        Swapsshow(id);
    }
    else {
        $("#divSwaps5").hide();
        Swapshide(id);
    }
});

$("#DDLProductInterest6").change(function () {
    var Product_id = $('#DDLProductInterest6').val()
    if (Product_id == '1025') {
        $("#divplain6").show();
        PalinVanilaOptionshow("#DDLPlain6");
    }
    else {
        $("#divplain6").hide();
        $("#divSwaps6").hide();
        PalinVanilaOptionHide("#DDLPlain6");
        Swapshide("DDLSwaps6");
    }
});
$("#DDLPlain6").change(function () {
    var Product_id = $('#DDLPlain6').val()
    var id = "#DDLSwaps6";
    if (Product_id == 'Swaps') {
        $("#divSwaps6").show();
        Swapsshow(id);
    }
    else {
        $("#divSwaps6").hide();
        Swapshide(id);
    }
});
$("#DDLProductInterest7").change(function () {
    var Product_id = $('#DDLProductInterest7').val()
    if (Product_id == '1025') {
        $("#divplain7").show();
        PalinVanilaOptionshow("#DDLPlain7");
    }
    else {
        $("#divplain7").hide();
        $("#divSwaps7").hide();
        PalinVanilaOptionHide("#DDLPlain7");
        Swapshide("DDLSwaps7");
    }
});
$("#DDLPlain7").change(function () {
    var Product_id = $('#DDLPlain7').val()
    var id = "#DDLSwaps7";
    if (Product_id == 'Swaps') {
        $("#divSwaps7").show();
        Swapsshow(id);
    }
    else {
        $("#divSwaps7").hide();
        Swapshide(id);
    }
});

$("#DDLProductInterest8").change(function () {
    var Product_id = $('#DDLProductInterest8').val()
    if (Product_id == '1025') {
        $("#divplain8").show();
        PalinVanilaOptionshow("#DDLPlain8");
    }
    else {
        $("#divplain8").hide();
        $("#divSwaps8").hide();
        PalinVanilaOptionHide("#DDLPlain8");
        Swapshide("DDLSwaps8");
    }
});
$("#DDLPlain8").change(function () {
    var Product_id = $('#DDLPlain8').val()
    var id = "#DDLSwaps8";
    if (Product_id == 'Swaps') {
        $("#divSwaps8").show();
        Swapsshow(id);
    }
    else {
        $("#divSwaps8").hide();
        Swapshide(id);
    }
});
$("#DDLProductInterest9").change(function () {
    var Product_id = $('#DDLProductInterest9').val()
    if (Product_id == '1025') {
        $("#divplain9").show();
        PalinVanilaOptionshow("#DDLPlain9");
    }
    else {
        $("#divplain9").hide();
        $("#divSwaps9").hide();
        PalinVanilaOptionHide("#DDLPlain9");
        Swapshide("DDLSwaps9");
    }
});
$("#DDLPlain9").change(function () {
    var Product_id = $('#DDLPlain9').val()
    var id = "#DDLSwaps9";
    if (Product_id == 'Swaps') {
        $("#divSwaps9").show();
        Swapsshow(id);
    }
    else {
        $("#divSwaps9").hide();
        Swapshide(id);
    }
});



function PalinVanilaOptionshow(id) {
   
    $(id).append('<option value=" Plain vanilla Option">' + ' Plain vanilla Option' + '</option>');
    $(id).append('<option value="Cost Reduction structure">' + 'Cost Reduction structure' + '</option>');
    $(id).append('<option value="Zero cost structure">' + 'Zero cost structure' + '</option>');
    $(id).append('<option value="Exotic structure">' + 'Exotic structure' + '</option>');
    $(id).append('<option value="Risk Reversal">' + 'Risk Reversal' + '</option>');
    $(id).append('<option value="Swaps">' + 'Swaps' + '</option>');


}

function PalinVanilaOptionHide(id) {
    
    $(id).html('select');
    $(id).append('<option value="">' + 'select' + '</option>');


}

function Swapsshow(id) {
    $(id).append('<option value="IRS">' + 'IRS' + '</option>');
    $(id).append('<option value="POS">' + 'POS' + '</option>');
    $(id).append('<option value="COS">' + 'COS' + '</option>');
    $(id).append('<option value="CIRS">' + 'CIRS' + '</option>');

}

function Swapshide(id) {
    
    $(id).html('');
    $(id).append('<option value="">' + 'select' + '</option>');


}

$("#btnaddnewcustomer").click(function () {
    window.location.href = 'tCustomer.aspx';

});