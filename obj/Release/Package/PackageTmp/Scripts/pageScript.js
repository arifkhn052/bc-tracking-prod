$(document).ready(function (e) {
    var setThres = false;
    $('ul.sideBar li').click(function (e) {
            var tabNumber = $(this).index();
            var parameter = $(".subList ul li.highlight").html();
            //console.log('ul li click'+$(this).attr("id"));
            if ($(this).attr("id") == "setThresholdSideBar" ) {
                //console.log('setThres');
                var circleChkId = '';
                $('.checkbox1:checked').each(function () {
                    circleChkId += $(this).val() + ",";
                });

                if (circleChkId == '') {
                    $("#smsSuccess").removeClass("popUp-success").addClass("popUp-success").find("#dynTxt").html("Please choose a circle!");
                    $(".rmv").hide();
                    popUp("#smsLayout", "#smsSuccess");
                    e.preventDefault();
                    return false;
                }

                var monthChkId = '';
                $('.monthCheckbox:checked').each(function () {
                    monthChkId += $(this).val() + ",";
                });
                monthChkId = monthChkId.slice(0, -1);
                var headervalue = $(".secMenu ul li.active > .secTab").html();
                if (monthChkId == '' && CheckParameter1() == false) {
					$("#smsSuccess").removeClass("popUp-success").addClass("popUp-success");
                    popUp("#smsLayout", "#smsSuccess");
					$("#smsSuccess").removeClass("popUp-success").addClass("popUp-success").find("#dynTxt").html("Please choose month!");
					$(".rmv").hide();
					popUp("#smsLayout", "#smsSuccess");
                    e.preventDefault();
                    return false;
                }
                var modeChkId = '';
                $('.modeCheckbox:checked').each(function () {
                    modeChkId += $(this).val() + ",";
                });
                //console.log(modeChkId);
                if (modeChkId == '') {
					$("#smsSuccess").removeClass("popUp-success").addClass("popUp-success").find("#dynTxt").html("Please choose SMS/Email!");
					$(".rmv").hide();
					popUp("#smsLayout", "#smsSuccess");
					e.preventDefault();
                    return false;
                }

                var parameter = $(".subList ul li.highlight").html();
                var perf = $(".subtabData ul li.active").data('alerttype');
                
                ////console.log(perf);
                if (parameter == undefined) {
                    $("#smsSuccess").removeClass("popUp-success").addClass("popUp-success").find("#dynTxt").html("Please choose a parameter!");
                    $(".rmv").hide();
                    popUp("#smsLayout", "#smsSuccess");
                    e.preventDefault();
                    return false;
                }
                if ((perf == '' || perf == undefined) && CheckParameter()==false) {
                    $("#smsSuccess").removeClass("popUp-success").addClass("popUp-success").find("#dynTxt").html("Please choose performance alert!");
                    $(".rmv").hide();
                    popUp("#smsLayout", "#smsSuccess");
                    e.preventDefault();
                    return false;
                }
                if ((headervalue == '' || headervalue == undefined) ) {
                    $("#smsSuccess").removeClass("popUp-success").addClass("popUp-success").find("#dynTxt").html("Please choose staff type!");
                    $(".rmv").hide();
                    popUp("#smsLayout", "#smsSuccess");
                    e.preventDefault();
                    return false;
                }


                    $(this).addClass('active').siblings().removeClass('active');
                    $('.sideContentInside > li:visible').fadeOut('fast', function () {
                        $('.thresholdTabContent').fadeIn('fast');
                    }).stop(true, true);
                
                setThres = true;
            }
            

            if ($(this).attr("id") == "setParameterSideBar") {
                ////console.log('setParam');
               
                $("#previewSideBar").show();
                $(".nextBtn").show();
                    $(this).addClass('active').siblings().removeClass('active');
                    $('.sideContentInside > li:visible').fadeOut('fast', function () {
                        $('.parameterTabContent').fadeIn('fast');
                    }).stop(true, true);
                
                setThres = false;
            }

            if ((setThres === true && $(this).attr("id") !== "setThresholdSideBar")) {
                ////console.log('id='+$(this).attr("id") + ' sett'+setThres);
                    $(this).addClass('active').siblings().removeClass('active');

                    $('.sideContentInside > li:visible').fadeOut('fast', function () {
                        $('.previewTabContent').fadeIn('fast');
                    }).stop(true, true);
                }


        });
    setTimeout(function () {
        SetSUDSlider(); 
    }, 2000);
   

    function SetSUDSlider() {
        var staff = $(".secMenu ul li.active > .secTab").html();
        //console.log('staff - ' + staff);
        if (staff == "CGM") {
            $('#sud-tooltip').noUiSlider({
                start: [50],
                step: 1,
                range: {
                    'min': [0],
                    'max': [200]
                }
            }, true);

        }
        if (staff == "GM") {
            $('#sud-tooltip').noUiSlider({
                start: [25],
                step: 1,
                range: {
                    'min': [0],
                    'max': [100]
                }
            }, true);
        }
        if (staff == "DGM") {
            $('#sud-tooltip').noUiSlider({
                start: [10],
                step: 1,
                range: {
                    'min': [0],
                    'max': [50]
                }
            }, true);
        }
        if (staff == "RM") {
            $('#sud-tooltip').noUiSlider({
                start: [5],
                step: 1,
                range: {
                    'min': [0],
                    'max': [25]
                }
            }, true);
        }
        if (staff == "RMSE") {
            $('#sud-tooltip').noUiSlider({
                start: [1],
                step: 1,
                range: {
                    'min': [0],
                    'max': [5]
                }
            }, true);
        }
        if (staff == "RMME") {
            $('#sud-tooltip').noUiSlider({
                start: [2],
                step: 1,
                range: {
                    'min': [0],
                    'max': [10]
                }
            }, true);
        }

        $("#sud-tooltip").noUiSlider_pips({
            mode: 'positions',
            values: [0, 20, 40, 60, 80, 200],
            density: 4,
            stepped: true
        });

        $("#sud-tooltip").Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {
            $(this).html(
                '<strong>' + value + '</strong>'
            );
        });
    }
        $('.secMenu ul li').click(function(e) {
            // ////console.log("clicked");
            if ($(this).attr('class') === 'disabled permdisabled') {
                e.preventDefault();
                return false;
            };
            if ($(this).attr('class') === 'permdisabled') {
                e.preventDefault();
                return false;
            };

            if ($(this).attr('class') === 'disabled') {
                e.preventDefault();
                return false;
            };
           


            $(this).addClass('active').siblings().removeClass('active');
            SetSUDSlider();

        });
        prevNext()
 });

// Prev Next Btn Slide
 function prevNext(){
   var nextBtn = $(".nextBtn"),
       prevBtn = $(".prevBtn "),
       sideBar =$(".sideBar"),
       slideBarLi =$(".sideBar> li"),
       contenTabWrap =$("ul.sideContentInside"),
       contenTabWrapLi = $("ul.sideContentInside > li");
    
        nextBtn.on("click", function() {
        //////console.log("Clicked");
    	var current = contenTabWrap.find("> li:visible");
    	var next= contenTabWrap.find("> li:visible").next();
    	var indexNum= next.index();
    	
    	//current.hide();
    	//next.show();
    	//slideBarLi.removeClass("active");
            //slideBarLi.eq(indexNum).addClass("active");
    	
    	$("ul.sideBar li").eq(indexNum).click();
    	//console.log($("ul.sideBar li").eq(indexNum));


    });

        prevBtn.on("click",function(){

        	var current = contenTabWrap.find("> li:visible");	
        		var prev= contenTabWrap.find("> li:visible").prev();
    	var indexNum= prev.index();

    	//current.hide();
            //prev.show();
    	$("ul.sideBar li").eq(indexNum).click();
    	//console.log($("ul.sideBar li").eq(indexNum));
     
    	//slideBarLi.removeClass("active");
    	//slideBarLi.eq(indexNum).addClass("active");

        });

 

 }



 function popUp(a, b) {
     var layout = a,
         popUp = b,
         close = $(b).find(".cls"),
         btn = $("#customMessageOk");
     margL = $(b).width() / 2,
     margT = $(b).height() / 2;

     $(layout).fadeIn();
     $(popUp).css({ "margin-left": -margL, "margin-top": -margT }).fadeIn();
     $(close).click(function (e) {
         e.preventDefault();
         $(this).parent().fadeOut();
         $(a).fadeOut();
     });
     $(btn).click(function (e) {
         e.preventDefault();
         $(close).parent().fadeOut();
         $(a).fadeOut();
     })
	 $(".popUp-success .btn").on('click', function (e) {
         e.preventDefault();
         $(close).parent().fadeOut();
         $(a).fadeOut();
     });
 
 }
 