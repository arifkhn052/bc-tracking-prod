$(document).ready(function () { 

    $("#meetings-tooltip").noUiSlider({
	start: 7,
	step:1,
	range: {
		'min': 0,
		'max': 15
	}

    });





    $("#meetings-tooltip").noUiSlider_pips({
	mode: 'positions',
	values: [0,20,40,60, 80,100],
	density: 4,
	stepped: true,
	format: wNumb({
		suffix: '%'
	})

});
    $("#meetings-tooltip").Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {

	// The tooltip HTML is 'this', so additional
	// markup can be inserted here.
	$(this).html(
		'<strong>' + value + '</strong>'
	);
});


    $("#risk-tooltip").noUiSlider({
	start: 50,
	step:1,
	range: {
		'min': 0,
		'max': 100
	}

});

    $("#risk-tooltip").noUiSlider_pips({
	mode: 'positions',
	values: [0,20,40,60, 80,100],
	density: 4,
	stepped: true
});



    $("#risk-tooltip").Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {

	// The tooltip HTML is 'this', so additional
	// markup can be inserted here.
	$(this).html(
		'<strong>' + value + '</strong>'
	);
});



    $("#tat-tooltip").noUiSlider({
        start: 35,
        step: 1,
        range: {
            'min': 0,
            'max': 90
        }

    });

    $("#tat-tooltip").noUiSlider_pips({
        mode: 'positions',
        values: [0, 20, 40, 60, 80, 100],
        density: 4,
        stepped: true
    });



    $("#tat-tooltip").Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {

        // The tooltip HTML is 'this', so additional
        // markup can be inserted here.
        $(this).html(
            '<strong>' + value + '</strong>'
        );
    });




    $("#sanctions-tooltip").noUiSlider({
        start: 50,
        step: 1,
        range: {
            'min': 0,
            'max': 100
        }

    });

    $("#sanctions-tooltip").noUiSlider_pips({
        mode: 'positions',
        values: [0, 20, 40, 60, 80, 100],
        density: 4,
        stepped: true
    });



    $("#sanctions-tooltip").Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {

        // The tooltip HTML is 'this', so additional
        // markup can be inserted here.
        $(this).html(
            '<strong>' + value + '</strong>'
        );
    });




    $("#disbursal-tooltip").noUiSlider({
        start: 50,
        step: 1,
        range: {
            'min': 0,
            'max': 100
        }

    });

    $("#disbursal-tooltip").noUiSlider_pips({
        mode: 'positions',
        values: [0, 20, 40, 60, 80, 100],
        density: 4,
        stepped: true
    });



    $("#disbursal-tooltip").Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {

        // The tooltip HTML is 'this', so additional
        // markup can be inserted here.
        $(this).html(
            '<strong>' + value + '</strong>'
        );
    });



    $("#league-tooltip").noUiSlider({
        start: 50,
        step: 1,
        range: {
            'min': 0,
            'max': 100
        }

    });

    $("#league-tooltip").noUiSlider_pips({
        mode: 'positions',
        values: [0, 20, 40, 60, 80, 100],
        density: 4,
        stepped: true
    });



    $("#league-tooltip").Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {

        // The tooltip HTML is 'this', so additional
        // markup can be inserted here.
        $(this).html(
            '<strong>' + value + '</strong>'
        );
    });


    //$("#sud-tooltip").noUiSlider({
    //    start: 50,
    //    step: 1,
    //    range: {
    //        'min': 0,
    //        'max': 100
    //    }

    //});

    //$("#sud-tooltip").noUiSlider_pips({
    //    mode: 'positions',
    //    values: [0, 20, 40, 60, 80, 100],
    //    density: 4,
    //    stepped: true
    //});

    //$("#sud-tooltip").Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {

    //    // The tooltip HTML is 'this', so additional
    //    // markup can be inserted here.
    //    $(this).html(
    //        '<strong>' + value + '</strong>'
    //    );
    //});




    //$("#hero-tooltip").noUiSlider({
    //    start: 7,
    //    step: 1,
    //    range: {
    //        'min': 3,
    //        'max': 20
    //    }

    //});

    //$("#hero-tooltip").noUiSlider_pips({
    //    mode: 'positions',
    //    values: [0, 20, 40, 60, 80, 100],
    //    density: 4,
    //    stepped: true
    //});



    //$("#hero-tooltip").Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {

    //    // The tooltip HTML is 'this', so additional
    //    // markup can be inserted here.
    //    $(this).html(
    //        '<strong>' + value + '</strong>'
    //    );
    //});



    //$("#zero-tooltip").noUiSlider({
    //    start: 50,
    //    step: 1,
    //    range: {
    //        'min': 0,
    //        'max': 100
    //    }

    //});

    //$("#zero-tooltip").noUiSlider_pips({
    //    mode: 'positions',
    //    values: [0, 20, 40, 60, 80, 100],
    //    density: 4,
    //    stepped: true
    //});



    //$("#zero-tooltip").Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {

    //    // The tooltip HTML is 'this', so additional
    //    // markup can be inserted here.
    //    $(this).html(
    //        '<strong>' + value + '</strong>'
    //    );
    //});





    $("#truck-premium-slider").on({
     
        change: function () {
            $("#slider-value").html(450);
        }
    });





});