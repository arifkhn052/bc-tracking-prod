﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="addstates.aspx.cs" Inherits="BCTrackingWeb.addstates" %>

     <div class="container-fluid" >
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">Add State</h2>
            </div>
        </div>

        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                     <li><a ui-sref="addHierarchy">Add  Branch</a></li>
                    <li><a >Add State</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <section class="section" ng-init="GetBankState()">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                            </div>
                        </div>
                        <div class="panel-body p-20">
                            <div class="panel-body">
                                <div class="formmargin">
                                    <label message class="col-sm-2 control-label">State Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control formcontrolheight" id="txtStateName" placeholder="Enter State Name"
                                            parsley-trigger="change" required>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" id="btnAddstate" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">

            <div class="row">


                <!-- /.col-md-6 -->

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                            </div>
                        </div>
                        <div class="panel-body p-20">

                            <table id="tblProduct" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>State Name</th>
                                       <%--  <th>Edit</th>     --%>   
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->


                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
    </section>

