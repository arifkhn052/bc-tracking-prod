﻿bctrackApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
    // For any unmatched url, send to /business

    $urlRouterProvider.otherwise('/');

    $stateProvider
         .state("logout", {
             url: "/logout",
             templateUrl: "Logout.aspx",
             controller: "logoutController"  
             ,
             resolve: {
        'user': ['$q', 'authenticationService', function ($q, authenticationService) {
            var user = authenticationService.getLoggedInUser();
            console.log(user);
            if (!!user) {
                return $q.when(user);
            } else {
                return $q.reject({
                    authenticated: false
                });
            }
        }]
    }
             

         })

           .state("/", {
               url: "/",
               templateUrl: "Login.aspx",
               controller: 'loginController',
               onEnter: function ($rootScope) {
                   $rootScope.$broadcast('hideNavigation');
                   $rootScope.$broadcast('hideHeader');

               },
               resolve: {
                   'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                       var user = authenticationService.getLoggedInUser();
                       console.log(user);
                       if (!user) {
                           return $q.when(user);
                       } else {
                           return $q.reject({
                               authenticated: true
                           });
                       }
                   }]
               }

           })

          .state("/login", {
              url: "/login",
              templateUrl: "Login.aspx",
              controller: 'loginController',
              onEnter: function ($rootScope) {
                  $rootScope.$broadcast('hideNavigation');
                  $rootScope.$broadcast('hideHeader');

              },
              resolve: {
                  'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                      var user = authenticationService.getLoggedInUser();
                      console.log(user);
                      if (!user) {
                          return $q.when(user);
                      } else {
                          return $q.reject({
                              authenticated: true
                          });
                      }
                  }]
              }

          })

       .state("home", {
           url: "/home",
           templateUrl: "bcHome.aspx",
           controller: "homeController",
           onEnter: function ($rootScope) {
               $rootScope.$broadcast('showNavigation');
               $rootScope.$broadcast('showHeader');
               $rootScope.$broadcast('loadMenu');
           },
           resolve: {
               'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                   var user = authenticationService.getLoggedInUser();
                   if (!!user) {
                       return $q.when(user);
                   } else {
                       return $q.reject({
                           authenticated: false
                       });
                   }
               }]
           }
           
       })
         .state("bankcode", {
             url: "/bankcode",
             templateUrl: "bankRecord.aspx",
            // controller: "homeController",
             onEnter: function ($rootScope) {
                 $rootScope.$broadcast('showNavigation');
                 $rootScope.$broadcast('showHeader');
                 $rootScope.$broadcast('loadMenu');
             },
             resolve: {
                 'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                     var user = authenticationService.getLoggedInUser();
                     if (!!user) {
                         return $q.when(user);
                     } else {
                         return $q.reject({
                             authenticated: false
                         });
                     }
                 }]
             }

         })
       .state("bulkUpload", {
           url: "/bulkUpload",
           templateUrl: "../bcBulkUpload.aspx",
           controller: "bulkUploadController",
           onEnter: function ($rootScope) {
               $rootScope.$broadcast('showNavigation');
               $rootScope.$broadcast('showHeader');
               $rootScope.$broadcast('loadMenu');
           },
           resolve: {
               'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                   var user = authenticationService.getLoggedInUser();
                   if (!!user) {
                       return $q.when(user);
                   } else {
                       return $q.reject({
                           authenticated: false
                       });
                   }
               }]
           }
       })
        //.state("singleentry", {
        //    url: "/entry",
        //    templateUrl: "../sEntry.aspx",
        //    controller: "singleController.js"
        //})
        .state("bclistall", {
            url: "/list",
            templateUrl: "../BCListAll.aspx",
            controller: "uploadController.js",
            onEnter: function ($rootScope) {
                $rootScope.$broadcast('showNavigation');
                $rootScope.$broadcast('showHeader');
                $rootScope.$broadcast('loadMenu');
            },
            resolve: {
                'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                    var user = authenticationService.getLoggedInUser();
                    if (!!user) {
                        return $q.when(user);
                    } else {
                        return $q.reject({
                            authenticated: false
                        });
                    }
                }]
            }
        })
        .state("bclistbankwise", {
            url: "/listbankwise",
            templateUrl: "../bclistBankWise.aspx",
            controller: "uploadController.js",
            onEnter: function ($rootScope) {
                $rootScope.$broadcast('showNavigation');
                $rootScope.$broadcast('showHeader');
                $rootScope.$broadcast('loadMenu');
            },
            resolve: {
                'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                    var user = authenticationService.getLoggedInUser();
                    if (!!user) {
                        return $q.when(user);
                    } else {
                        return $q.reject({
                            authenticated: false
                        });
                    }
                }]
            }
        })
        .state("bclistbylocation", {
            url: "/listlocationwise",
            templateUrl: "../bclistLocationWise.aspx",
            controller: "uploadController.js",
            onEnter: function ($rootScope) {
                $rootScope.$broadcast('showNavigation');
                $rootScope.$broadcast('showHeader');
                $rootScope.$broadcast('loadMenu');
            },
            resolve: {
                'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                    var user = authenticationService.getLoggedInUser();
                    if (!!user) {
                        return $q.when(user);
                    } else {
                        return $q.reject({
                            authenticated: false
                        });
                    }
                }]
            }
        })
        .state("bclistunallocated", {
            url: "/unallocated",
            templateUrl: "../bclistUnallocated.aspx",
            controller: "uploadController",
            onEnter: function ($rootScope) {
                $rootScope.$broadcast('showNavigation');
                $rootScope.$broadcast('showHeader');
                $rootScope.$broadcast('loadMenu');
            },
            resolve: {
                'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                    var user = authenticationService.getLoggedInUser();
                    if (!!user) {
                        return $q.when(user);
                    } else {
                        return $q.reject({
                            authenticated: false
                        });
                    }
                }]
            }
        })
        .state("addstate", {
            url: "/addstate",
            templateUrl: "../uAddState.aspx",
            controller: "editStateController",
            onEnter: function ($rootScope) {
                $rootScope.$broadcast('showNavigation');
                $rootScope.$broadcast('showHeader');
                $rootScope.$broadcast('loadMenu');
            },
            resolve: {
                'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                    var user = authenticationService.getLoggedInUser();
                    if (!!user) {
                        return $q.when(user);
                    } else {
                        return $q.reject({
                            authenticated: false
                        });
                    }
                }]
            }
        })
        .state("statelist", {
            url: "/statelist",
            templateUrl: "../uStateList.aspx",
            controller: "stateListController",
            onEnter: function ($rootScope) {
                $rootScope.$broadcast('showNavigation');
                $rootScope.$broadcast('showHeader');
                $rootScope.$broadcast('loadMenu');
            },
            resolve: {
                'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                    var user = authenticationService.getLoggedInUser();
                    if (!!user) {
                        return $q.when(user);
                    } else {
                        return $q.reject({
                            authenticated: false
                        });
                    }
                }]
            }
        })
         .state("editState", {
             url: "/edit/:stateId",
             templateUrl: "../editState.aspx",
             controller: "editStateController",
             onEnter: function ($rootScope) {
                 $rootScope.$broadcast('showNavigation');
                 $rootScope.$broadcast('showHeader');
                 $rootScope.$broadcast('loadMenu');
             },
             resolve: {
                 'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                     var user = authenticationService.getLoggedInUser();
                     if (!!user) {
                         return $q.when(user);
                     } else {
                         return $q.reject({
                             authenticated: false
                         });
                     }
                 }]
             }
         })
          .state("editbank", {
              url: "/editbank/:bankId",
              templateUrl: "../EditBank.aspx",
              controller: "addBankController",
              onEnter: function ($rootScope) {
                  $rootScope.$broadcast('showNavigation');
                  $rootScope.$broadcast('showHeader');
                  $rootScope.$broadcast('loadMenu');
              },
              resolve: {
                  'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                      var user = authenticationService.getLoggedInUser();
                      if (!!user) {
                          return $q.when(user);
                      } else {
                          return $q.reject({
                              authenticated: false
                          });
                      }
                  }]
              }
          })
            .state("editUser", {
                url: "/editUser/:userId",
                templateUrl: "../AddUser.aspx",
                controller: "userController",
                onEnter: function ($rootScope) {
                    $rootScope.$broadcast('showNavigation');
                    $rootScope.$broadcast('showHeader');
                    $rootScope.$broadcast('loadMenu');
                },
                resolve: {
                    'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                        var user = authenticationService.getLoggedInUser();
                        if (!!user) {
                            return $q.when(user);
                        } else {
                            return $q.reject({
                                authenticated: false
                            });
                        }
                    }]
                }
            })
          .state("editCorporate", {
              url: "/editCorporate/:corporateId",
              templateUrl: "../editCorporate.aspx",
              controller: "addCorporateController",
              onEnter: function ($rootScope) {
                  $rootScope.$broadcast('showNavigation');
                  $rootScope.$broadcast('showHeader');
                  $rootScope.$broadcast('loadMenu');
              },
              resolve: {
                  'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                      var user = authenticationService.getLoggedInUser();
                      if (!!user) {
                          return $q.when(user);
                      } else {
                          return $q.reject({
                              authenticated: false
                          });
                      }
                  }]
              }
          })
     .state("editallCorresponds", {
         url: "/editallCorresponds/:BankCorrespondId",
         templateUrl: "../editEntry.aspx",
         controller: "addSingleEntryController",
         onEnter: function ($rootScope) {
             $rootScope.$broadcast('showNavigation');
             $rootScope.$broadcast('showHeader');
             $rootScope.$broadcast('loadMenu');
         },
         resolve: {
             'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                 var user = authenticationService.getLoggedInUser();
                 if (!!user) {
                     return $q.when(user);
                 } else {
                     return $q.reject({
                         authenticated: false
                     });
                 }
             }]
         }
     })

          .state("viewallCorresponds", {
              url: "/viewallCorresponds/:BankCorrespondId",
              templateUrl: "../ViewEntry.aspx",
              controller: "viewSingleEntryController",
              onEnter: function ($rootScope) {
                  $rootScope.$broadcast('showNavigation');
                  $rootScope.$broadcast('showHeader');
                  $rootScope.$broadcast('loadMenu');
              },
              resolve: {
                  'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                      var user = authenticationService.getLoggedInUser();
                      if (!!user) {
                          return $q.when(user);
                      } else {
                          return $q.reject({
                              authenticated: false
                          });
                      }
                  }]
              }
          })

          .state("viewallProspectiveRegistry", {
              url: "/viewallProspectiveRegistry/:prospectiveid",
              templateUrl: "../ViewProspectiveRegistryEntry.aspx",
              controller: "viewProspectiveRegistryEntryController",
              onEnter: function ($rootScope) {
                  $rootScope.$broadcast('showNavigation');
                  $rootScope.$broadcast('showHeader');
                  $rootScope.$broadcast('loadMenu');
              },
              resolve: {
                  'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                      var user = authenticationService.getLoggedInUser();
                      if (!!user) {
                          return $q.when(user);
                      } else {
                          return $q.reject({
                              authenticated: false
                          });
                      }
                  }]
              }
          })
           .state("blacklistallCorresponds", {
               url: "/blacklistallCorresponds/:BankCorrespondId",
               templateUrl: "../BlackListBC.aspx",
               controller: "addSingleEntryController",
               onEnter: function ($rootScope) {
                   $rootScope.$broadcast('showNavigation');
                   $rootScope.$broadcast('showHeader');
                   $rootScope.$broadcast('loadMenu');
               },
               resolve: {
                   'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                       var user = authenticationService.getLoggedInUser();
                       if (!!user) {
                           return $q.when(user);
                       } else {
                           return $q.reject({
                               authenticated: false
                           });
                       }
                   }]
               }
           })
            .state("chnageStatusallCorresponds", {
                url: "/chnageStatusallCorresponds/:BankCorrespondId",
                templateUrl: "../ChangeStatus.aspx",
                controller: "addSingleEntryController",
                onEnter: function ($rootScope) {
                    $rootScope.$broadcast('showNavigation');
                    $rootScope.$broadcast('showHeader');
                    $rootScope.$broadcast('loadMenu');
                },
                resolve: {
                    'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                        var user = authenticationService.getLoggedInUser();
                        if (!!user) {
                            return $q.when(user);
                        } else {
                            return $q.reject({
                                authenticated: false
                            });
                        }
                    }]
                }
            })


     
       .state("notifications", {
           url: "/notifications",
           templateUrl: "../notifications.aspx",
           controller: "notificationController",
           onEnter: function ($rootScope) {
               $rootScope.$broadcast('showNavigation');
               $rootScope.$broadcast('showHeader');
               $rootScope.$broadcast('loadMenu');
           },
           resolve: {
               'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                   var user = authenticationService.getLoggedInUser();
                   if (!!user) {
                       return $q.when(user);
                   } else {
                       return $q.reject({
                           authenticated: false
                       });
                   }
               }]
           }
       })
          .state("chnagepassword", {
              url: "/chnagepassword",
              templateUrl: "../changePassword.aspx",
              controller: "changePasswordController",
              onEnter: function ($rootScope) {
                  $rootScope.$broadcast('showNavigation');
                  $rootScope.$broadcast('showHeader');
                  $rootScope.$broadcast('loadMenu');
              },
              resolve: {
                  'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                      var user = authenticationService.getLoggedInUser();
                      if (!!user) {
                          return $q.when(user);
                      } else {
                          return $q.reject({
                              authenticated: false
                          });
                      }
                  }]
              }
          })
          .state("searchvillage", {
              url: "/searchvillage",
              templateUrl: "../searchVillage.aspx",
              controller: "searchvillageController",
              onEnter: function ($rootScope) {
                  $rootScope.$broadcast('showNavigation');
                  $rootScope.$broadcast('showHeader');
                  $rootScope.$broadcast('loadMenu');
              },
              resolve: {
                  'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                      var user = authenticationService.getLoggedInUser();
                      if (!!user) {
                          return $q.when(user);
                      } else {
                          return $q.reject({
                              authenticated: false
                          });
                      }
                  }]
              }
          })
          .state("searchlog", {
              url: "/searchlog",
              templateUrl: "../searchLog.aspx",
              controller: "searchlogController",
              onEnter: function ($rootScope) {
                  $rootScope.$broadcast('showNavigation');
                  $rootScope.$broadcast('showHeader');
                  $rootScope.$broadcast('loadMenu');
              },
              resolve: {
                  'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                      var user = authenticationService.getLoggedInUser();
                      if (!!user) {
                          return $q.when(user);
                      } else {
                          return $q.reject({
                              authenticated: false
                          });
                      }
                  }]
              }
          })
      .state("addUser", {
          url: "/adduser",
          templateUrl: "../AddUser.aspx",
          controller: "userController",
          onEnter: function ($rootScope) {
              $rootScope.$broadcast('showNavigation');
              $rootScope.$broadcast('showHeader');
              $rootScope.$broadcast('loadMenu');
          },
          resolve: {
              'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                  var user = authenticationService.getLoggedInUser();
                  if (!!user) {
                      return $q.when(user);
                  } else {
                      return $q.reject({
                          authenticated: false
                      });
                  }
              }]
          }
      })
      .state("userList", {
          url: "/userlist",
          templateUrl: "../UserList.aspx",
          controller: "userController",
          onEnter: function ($rootScope) {
              $rootScope.$broadcast('showNavigation');
              $rootScope.$broadcast('showHeader');
              $rootScope.$broadcast('loadMenu');
          },
          resolve: {
              'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                  var user = authenticationService.getLoggedInUser();
                  if (!!user) {
                      return $q.when(user);
                  } else {
                      return $q.reject({
                          authenticated: false
                      });
                  }
              }]
          }
      })
     .state("corporateList", {
         url: "/corporatelist",
         templateUrl: "../CorporateList.aspx",
         controller: "corporateController",
         onEnter: function ($rootScope) {
             $rootScope.$broadcast('showNavigation');
             $rootScope.$broadcast('showHeader');
             $rootScope.$broadcast('loadMenu');
         },
         resolve: {
             'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                 var user = authenticationService.getLoggedInUser();
                 if (!!user) {
                     return $q.when(user);
                 } else {
                     return $q.reject({
                         authenticated: false
                     });
                 }
             }]
         }
     })

          .state("addCorporate", {
              url: "/addcorporate",
              templateUrl: "../AddCorporate.aspx",
              controller: "corporateController",
              onEnter: function ($rootScope) {
                  $rootScope.$broadcast('showNavigation');
                  $rootScope.$broadcast('showHeader');
                  $rootScope.$broadcast('loadMenu');
              },
              resolve: {
                  'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                      var user = authenticationService.getLoggedInUser();
                      if (!!user) {
                          return $q.when(user);
                      } else {
                          return $q.reject({
                              authenticated: false
                          });
                      }
                  }]
              }
          })



      .state("bankList", {
          url: "/banklist",
          templateUrl: "../BankList.aspx",
          controller: "bankController",
          onEnter: function ($rootScope) {
              $rootScope.$broadcast('showNavigation');
              $rootScope.$broadcast('showHeader');
              $rootScope.$broadcast('loadMenu');
          },
          resolve: {
              'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                  var user = authenticationService.getLoggedInUser();
                  if (!!user) {
                      return $q.when(user);
                  } else {
                      return $q.reject({
                          authenticated: false
                      });
                  }
              }]
          }
      })

       .state("addBank", {
           url: "/addbank",
           templateUrl: "../AddBank.aspx",
           controller: "addBankController",
           onEnter: function ($rootScope) {
               $rootScope.$broadcast('showNavigation');
               $rootScope.$broadcast('showHeader');
               $rootScope.$broadcast('loadMenu');
           },
           resolve: {
               'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                   var user = authenticationService.getLoggedInUser();
                   if (!!user) {
                       return $q.when(user);
                   } else {
                       return $q.reject({
                           authenticated: false
                       });
                   }
               }]
           }
       })

         .state("addHierarchy", {
             url: "/addHierarchy",
             templateUrl: "../Hierarchy.aspx",
             controller: "HierarchyController",
             onEnter: function ($rootScope) {
                 $rootScope.$broadcast('showNavigation');
                 $rootScope.$broadcast('showHeader');
                 $rootScope.$broadcast('loadMenu');
             },
             resolve: {
                 'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                     var user = authenticationService.getLoggedInUser();
                     if (!!user) {
                         return $q.when(user);
                     } else {
                         return $q.reject({
                             authenticated: false
                         });
                     }
                 }]
             }
         })
       //.state("bulkUpload", {
       //    url: "/bulkupload",
       //    templateUrl: "../bulkupload.aspx",
       //  //  controller: "bankController"
       //})
    
      .state("singleEntry", {
          url: "/singleEntry",
          templateUrl: "../sEntry.aspx",
          controller: "singleEntryc",
          onEnter: function ($rootScope) {
              $rootScope.$broadcast('showNavigation');
              $rootScope.$broadcast('showHeader');
              $rootScope.$broadcast('loadMenu');
          },
          resolve: {
              'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                  var user = authenticationService.getLoggedInUser();
                  if (!!user) {
                      return $q.when(user);
                  } else {
                      return $q.reject({
                          authenticated: false
                      });
                  }
              }]
          }
      })
         .state("addProspectiveBCRegistry", {
             url: "/addProspectiveBCRegistry",
             templateUrl: "../addProspectiveBCRegistry.aspx",
             controller: "addProspectiveBCRegistryController",
             onEnter: function ($rootScope) {
                 $rootScope.$broadcast('showNavigation');
                 $rootScope.$broadcast('showHeader');
                 $rootScope.$broadcast('loadMenu');
             },
             resolve: {
                 'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                     var user = authenticationService.getLoggedInUser();
                     if (!!user) {
                         return $q.when(user);
                     } else {
                         return $q.reject({
                             authenticated: false
                         });
                     }
                 }]
             }
         })
         .state("all", {
             url: "/all",
             templateUrl: "../bcBCListAlls.aspx.aspx",
             controller: "getAllBcCorresponds",
             onEnter: function ($rootScope) {
                 $rootScope.$broadcast('showNavigation');
                 $rootScope.$broadcast('showHeader');
                 $rootScope.$broadcast('loadMenu');
             },
             resolve: {
                 'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                     var user = authenticationService.getLoggedInUser();
                     if (!!user) {
                         return $q.when(user);
                     } else {
                         return $q.reject({
                             authenticated: false
                         });
                     }
                 }]
             }
         })

             .state("ProspectiveBCRegistryList", {
                 url: "/ProspectiveBCRegistryList",
                 templateUrl: "../ProspectiveBCRegistryList.aspx",
                 controller: "getAllProspectiveBCRegistryList",
                 onEnter: function ($rootScope) {
                     $rootScope.$broadcast('showNavigation');
                     $rootScope.$broadcast('showHeader');
                     $rootScope.$broadcast('loadMenu');
                 },
                 resolve: {
                     'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                         var user = authenticationService.getLoggedInUser();
                         if (!!user) {
                             return $q.when(user);
                         } else {
                             return $q.reject({
                                 authenticated: false
                             });
                         }
                     }]
                 }
             })

      .state("byBranches", {
          url: "/bybranches",
          templateUrl: "../bcbclistBankWise.aspx",
          controller: "bclistLocationWiseContro",
          onEnter: function ($rootScope) {
              $rootScope.$broadcast('showNavigation');
              $rootScope.$broadcast('showHeader');
              $rootScope.$broadcast('loadMenu');
          },
          resolve: {
              'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                  var user = authenticationService.getLoggedInUser();
                  if (!!user) {
                      return $q.when(user);
                  } else {
                      return $q.reject({
                          authenticated: false
                      });
                  }
              }]
          }
      })
          .state("branchlist", {
              url: "/branchlist",
              templateUrl: "../BranchList.aspx",
              controller: "BranchListContro",
              onEnter: function ($rootScope) {
                  $rootScope.$broadcast('showNavigation');
                  $rootScope.$broadcast('showHeader');
                  $rootScope.$broadcast('loadMenu');
              },
              resolve: {
                  'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                      var user = authenticationService.getLoggedInUser();
                      if (!!user) {
                          return $q.when(user);
                      } else {
                          return $q.reject({
                              authenticated: false
                          });
                      }
                  }]
              }
          })
    
        .state("bclistLocationWise", {
            url: "/bclistLocationWise",
            templateUrl: "../bclistLocationWise.aspx",
            controller: "bcListAddressWiseContro",
            onEnter: function ($rootScope) {
                $rootScope.$broadcast('showNavigation');
                $rootScope.$broadcast('showHeader');
                $rootScope.$broadcast('loadMenu');
            },
            resolve: {
                'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                    var user = authenticationService.getLoggedInUser();
                    if (!!user) {
                        return $q.when(user);
                    } else {
                        return $q.reject({
                            authenticated: false
                        });
                    }
                }]
            }
        })
      .state("bclistUnallocated", {
          url: "/bclistUnallocated",
          templateUrl: "../bclistUnallocated.aspx",
          controller: "getUnallocatedAll",
          onEnter: function ($rootScope) {
              $rootScope.$broadcast('showNavigation');
              $rootScope.$broadcast('showHeader');
              $rootScope.$broadcast('loadMenu');
          },
          resolve: {
              'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                  var user = authenticationService.getLoggedInUser();
                  if (!!user) {
                      return $q.when(user);
                  } else {
                      return $q.reject({
                          authenticated: false
                      });
                  }
              }]
          }
      })
       .state("bclistStatus", {
           url: "/bclistStatus",
           templateUrl: "../bclistStatus.aspx",
           controller: "getBcByStatus",
           onEnter: function ($rootScope) {
               $rootScope.$broadcast('showNavigation');
               $rootScope.$broadcast('showHeader');
               $rootScope.$broadcast('loadMenu');
           },
           resolve: {
               'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                   var user = authenticationService.getLoggedInUser();
                   if (!!user) {
                       return $q.when(user);
                   } else {
                       return $q.reject({
                           authenticated: false
                       });
                   }
               }]
           }
       })
       .state("reportbetween", {
           url: "/reportbetween",
           templateUrl: "../reportbetween.aspx",
           controller: "summaryChartController",
           onEnter: function ($rootScope) {
               $rootScope.$broadcast('showNavigation');
               $rootScope.$broadcast('showHeader');
               $rootScope.$broadcast('loadMenu');
           },
           resolve: {
               'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                   var user = authenticationService.getLoggedInUser();
                   if (!!user) {
                       return $q.when(user);
                   } else {
                       return $q.reject({
                           authenticated: false
                       });
                   }
               }]
           }
       })
       .state("summaryUnallocated", {
           url: "/summaryUnallocated",
           templateUrl: "../unallocated.aspx",
           controller: "summaryChartController",
           onEnter: function ($rootScope) {
               $rootScope.$broadcast('showNavigation');
               $rootScope.$broadcast('showHeader');
               $rootScope.$broadcast('loadMenu');
           },
           resolve: {
               'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                   var user = authenticationService.getLoggedInUser();
                   if (!!user) {
                       return $q.when(user);
                   } else {
                       return $q.reject({
                           authenticated: false
                       });
                   }
               }]
           }
       })
     .state("blacklistedBcGraph", {
         url: "/blacklistedBcGraph",
         templateUrl: "../blacklistedBcGraph.aspx",
         controller: "summaryChartController",
         onEnter: function ($rootScope) {
             $rootScope.$broadcast('showNavigation');
             $rootScope.$broadcast('showHeader');
             $rootScope.$broadcast('loadMenu');
         },
         resolve: {
             'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                 var user = authenticationService.getLoggedInUser();
                 if (!!user) {
                     return $q.when(user);
                 } else {
                     return $q.reject({
                         authenticated: false
                     });
                 }
             }]
         }
     })

     .state("activeBcGraph", {
         url: "/activeBcGraph",
         templateUrl: "../activeBcGraph.aspx",
         controller: "summaryChartController",
         onEnter: function ($rootScope) {
             $rootScope.$broadcast('showNavigation');
             $rootScope.$broadcast('showHeader');
             $rootScope.$broadcast('loadMenu');
         },
         resolve: {
             'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                 var user = authenticationService.getLoggedInUser();
                 if (!!user) {
                     return $q.when(user);
                 } else {
                     return $q.reject({
                         authenticated: false
                     });
                 }
             }]
         }
     })
      .state("areaWiseAllocationBetweenDate", {
          url: "/areaWiseAllocationBetweenDate",
          templateUrl: "../areaWiseAllocationBetweenDate.aspx",
          controller: "summaryChartController",
          onEnter: function ($rootScope) {
              $rootScope.$broadcast('showNavigation');
              $rootScope.$broadcast('showHeader');
              $rootScope.$broadcast('loadMenu');
          },
          resolve: {
              'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                  var user = authenticationService.getLoggedInUser();
                  if (!!user) {
                      return $q.when(user);
                  } else {
                      return $q.reject({
                          authenticated: false
                      });
                  }
              }]
          }
      })

       .state("listason", {
           url: "/listason",
           templateUrl: "../listason.aspx",
           controller: "reportListController",
           onEnter: function ($rootScope) {
               $rootScope.$broadcast('showNavigation');
               $rootScope.$broadcast('showHeader');
               $rootScope.$broadcast('loadMenu');
           },
           resolve: {
               'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                   var user = authenticationService.getLoggedInUser();
                   if (!!user) {
                       return $q.when(user);
                   } else {
                       return $q.reject({
                           authenticated: false
                       });
                   }
               }]
           }
       })
      .state("CorporateBC", {
          url: "/CorporateBC",
          templateUrl: "../CorporateBC.aspx",
          controller: "reportListController",
          onEnter: function ($rootScope) {
              $rootScope.$broadcast('showNavigation');
              $rootScope.$broadcast('showHeader');
              $rootScope.$broadcast('loadMenu');
          },
          resolve: {
              'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                  var user = authenticationService.getLoggedInUser();
                  if (!!user) {
                      return $q.when(user);
                  } else {
                      return $q.reject({
                          authenticated: false
                      });
                  }
              }]
          }
      })
       .state("BranchesLastUpdated", {
           url: "/BranchesLastUpdated",
           templateUrl: "../BranchesLastUpdated.aspx",
           controller: "BranchesLastUpdatedController",
           onEnter: function ($rootScope) {
               $rootScope.$broadcast('showNavigation');
               $rootScope.$broadcast('showHeader');
               $rootScope.$broadcast('loadMenu');
           },
           resolve: {
               'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                   var user = authenticationService.getLoggedInUser();
                   if (!!user) {
                       return $q.when(user);
                   } else {
                       return $q.reject({
                           authenticated: false
                       });
                   }
               }]
           }
       })
      .state("AllocationBasedList", {
          url: "/AllocationBasedList",
          templateUrl: "../AllocationBasedList.aspx",
          controller: "allocationBasedContro",
          onEnter: function ($rootScope) {
              $rootScope.$broadcast('showNavigation');
              $rootScope.$broadcast('showHeader');
              $rootScope.$broadcast('loadMenu');
          },
          resolve: {
              'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                  var user = authenticationService.getLoggedInUser();
                  if (!!user) {
                      return $q.when(user);
                  } else {
                      return $q.reject({
                          authenticated: false
                      });
                  }
              }]
          }
      })
          .state("fixedVSMobile", {
              url: "/fixedVSMobile",
           //   templateUrl: "../mobileVSfixedBC.aspx",
           //   controller: "mobilevsfixedBcController",
              onEnter: function ($rootScope) {
                  $rootScope.$broadcast('showNavigation');
                  $rootScope.$broadcast('showHeader');
                  $rootScope.$broadcast('loadMenu');
              },
              resolve: {
                  'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                      var user = authenticationService.getLoggedInUser();
                      if (!!user) {
                          return $q.when(user);
                      } else {
                          return $q.reject({
                              authenticated: false
                          });
                      }
                  }]
              }
          })
       .state("areaWiseAllocation", {
           url: "/areaWiseAllocation",
           templateUrl: "../areaWiseAllocation.aspx",
           controller: "areaWiseAllocationController",
           onEnter: function ($rootScope) {
               $rootScope.$broadcast('showNavigation');
               $rootScope.$broadcast('showHeader');
               $rootScope.$broadcast('loadMenu');
           },
           resolve: {
               'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                   var user = authenticationService.getLoggedInUser();
                   if (!!user) {
                       return $q.when(user);
                   } else {
                       return $q.reject({
                           authenticated: false
                       });
                   }
               }]
           }
       })

       .state("BcPerProduct", {
           url: "/BcPerProduct",
           templateUrl: "../BcPerProduct.aspx",
           controller: "getbcProductController",
           onEnter: function ($rootScope) {
               $rootScope.$broadcast('showNavigation');
               $rootScope.$broadcast('showHeader');
               $rootScope.$broadcast('loadMenu');
           },
           resolve: {
               'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                   var user = authenticationService.getLoggedInUser();
                   if (!!user) {
                       return $q.when(user);
                   } else {
                       return $q.reject({
                           authenticated: false
                       });
                   }
               }]
           }
       })

         .state("BcMultiroduct", {
             url: "/BcMultiroduct",
             templateUrl: "../BcMultiroduct.aspx",
             controller: "getBcMultiproduct",
             onEnter: function ($rootScope) {
                 $rootScope.$broadcast('showNavigation');
                 $rootScope.$broadcast('showHeader');
                 $rootScope.$broadcast('loadMenu');
             },
             resolve: {
                 'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                     var user = authenticationService.getLoggedInUser();
                     if (!!user) {
                         return $q.when(user);
                     } else {
                         return $q.reject({
                             authenticated: false
                         });
                     }
                 }]
             }
         })

       .state("BlacklistedBCs", {
           url: "/BlacklistedBCs",
           templateUrl: "../BlacklistedBCs.aspx",
           controller: "AllocationBasedListController",
           onEnter: function ($rootScope) {
               $rootScope.$broadcast('showNavigation');
               $rootScope.$broadcast('showHeader');
               $rootScope.$broadcast('loadMenu');
           },
           resolve: {
               'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                   var user = authenticationService.getLoggedInUser();
                   if (!!user) {
                       return $q.when(user);
                   } else {
                       return $q.reject({
                           authenticated: false
                       });
                   }
               }]
           }
       })
        .state("addproduct", {
            url: "/addproduct",
            templateUrl: "../bcAddProduct.aspx",
            controller: "addProductController",
            onEnter: function ($rootScope) {
                $rootScope.$broadcast('showNavigation');
                $rootScope.$broadcast('showHeader');
                $rootScope.$broadcast('loadMenu');
            },
            resolve: {
                'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                    var user = authenticationService.getLoggedInUser();
                    if (!!user) {
                        return $q.when(user);
                    } else {
                        return $q.reject({
                            authenticated: false
                        });
                    }
                }]
            }
        })

         .state("groupproduct", {
             url: "/groupproduct",
             templateUrl: "../bcGroupProduct.aspx",
             controller: "groupProductController",
             onEnter: function ($rootScope) {
                 $rootScope.$broadcast('showNavigation');
                 $rootScope.$broadcast('showHeader');
                 $rootScope.$broadcast('loadMenu');
             },
             resolve: {
                 'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                     var user = authenticationService.getLoggedInUser();
                     if (!!user) {
                         return $q.when(user);
                     } else {
                         return $q.reject({
                             authenticated: false
                         });
                     }
                 }]
             }
         })
         .state("editgroupProduct", {
             url: "/editgroupProduct/:groupId",
             templateUrl: "../bcUpdateGroupProduct.aspx",
             controller: "groupProductController",
             onEnter: function ($rootScope) {
                 $rootScope.$broadcast('showNavigation');
                 $rootScope.$broadcast('showHeader');
                 $rootScope.$broadcast('loadMenu');
             },
             resolve: {
                 'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                     var user = authenticationService.getLoggedInUser();
                     if (!!user) {
                         return $q.when(user);
                     } else {
                         return $q.reject({
                             authenticated: false
                         });
                     }
                 }]
             }
         })
   .state("editProduct", {
       url: "/editProduct/:productId",
       templateUrl: "../editProduct.aspx",
       controller: "addProductController",
       onEnter: function ($rootScope) {
           $rootScope.$broadcast('showNavigation');
           $rootScope.$broadcast('showHeader');
           $rootScope.$broadcast('loadMenu');
       },
       resolve: {
           'user': ['$q', 'authenticationService', function ($q, authenticationService) {
               var user = authenticationService.getLoggedInUser();
               if (!!user) {
                   return $q.when(user);
               } else {
                   return $q.reject({
                       authenticated: false
                   });
               }
           }]
       }
   })
     .state("findbc", {
         url: "/findbc",
         templateUrl: "../bcsearchBC.aspx.aspx",
         controller: "findBcController",
         onEnter: function ($rootScope) {
             $rootScope.$broadcast('showNavigation');
             $rootScope.$broadcast('showHeader');
             $rootScope.$broadcast('loadMenu');
         },
         resolve: {
             'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                 var user = authenticationService.getLoggedInUser();
                 if (!!user) {
                     return $q.when(user);
                 } else {
                     return $q.reject({
                         authenticated: false
                     });
                 }
             }]
         }
     })

     .state("addBankState", {
         url: "/addBankState/:Bankid",
         templateUrl: "../addstates.aspx",
         controller: "addStateController",
         onEnter: function ($rootScope) {
             $rootScope.$broadcast('showNavigation');
             $rootScope.$broadcast('showHeader');
             $rootScope.$broadcast('loadMenu');
             $rootScope.$broadcast('showLoader');
         },
        
         resolve: {
             'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                 var user = authenticationService.getLoggedInUser();
                 if (!!user) {
                     return $q.when(user);
                 } else {
                     return $q.reject({
                         authenticated: false
                     });
                 }
             }]
         }
     })
     .state("addBankCity", {
         url: "/addBankCity/:StateId",
         templateUrl: "../addBankCity.aspx",
         controller: "addBankCityController",
         onEnter: function ($rootScope) {
             $rootScope.$broadcast('showNavigation');
             $rootScope.$broadcast('showHeader');
             $rootScope.$broadcast('loadMenu');
         },
         resolve: {
             'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                 var user = authenticationService.getLoggedInUser();
                 if (!!user) {
                     return $q.when(user);
                 } else {
                     return $q.reject({
                         authenticated: false
                     });
                 }
             }]
         }
     })
      .state("addBankDistrict", {
          url: "/addBankDistrict/:CityID",
          templateUrl: "../addBankDistrict.aspx",
          controller: "addBankDistrictController",
          onEnter: function ($rootScope) {
              $rootScope.$broadcast('showNavigation');
              $rootScope.$broadcast('showHeader');
              $rootScope.$broadcast('loadMenu');
          },
          resolve: {
              'user': ['$q', 'authenticationService', function ($q, authenticationService) {
                  var user = authenticationService.getLoggedInUser();
                  if (!!user) {
                      return $q.when(user);
                  } else {
                      return $q.reject({
                          authenticated: false
                      });
                  }
              }]
          }
      })


    
    
}]);



bctrackApp.run(["$rootScope", "$location", "$state", function ($rootScope, $location, $state) {
    $rootScope.$on("$stateChangeStart", function () {
        console.log("Route Change Event Started");
        console.log('Start Loading');
        $rootScope.$broadcast('showLoader');

    });
    $rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
        console.log('Route Change Error Event fired');
        console.log(error);
        if (error.authenticated === false) {
            $location.path("/");

        }
        else {
            $location.path("/home");
        }

        console.log('Stopped Loading');
        event.preventDefault()
    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        console.log('Stopped Loading');
        $rootScope.$broadcast('hideLoader');
        if (toState.resolve) {
            /*$scope.hideSpinner();*/
        }
    });
}]);


