﻿

bctrackApp.controller("addProductController", function ($scope, $state, $cookies) {
    $scope.getProduct = function(){
    

    var leadTable = $('#tblProduct').dataTable({
        "oLanguage": {
            "sZeroRecords": "No records to display",
            "sSearch": "Search "
        },
        "sDom": 'T<"clear">lfrtip',
        "tableTools": {

            "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
        },
        "iDisplayLength": 15,
        "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
        "bSortClasses": false,
        "bStateSave": false,
        "bPaginate": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "bcAddProduct.aspx/getProduct",
        "columnDefs": [

            {
                "orderable": false,
                "targets": -1,
                "render": function (data, type, full, meta) {
                    return '<a  class="btn btn-success btn-sm btn-labeled btn-rounded"  href="#/editProduct/' + full[0] + '" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                }
            }
        ],
        "bDeferRender": true,
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success":
                            function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblProduct").show();
                            }
            });
        }
    });
    leadTable.fnSetFilteringDelay(300);
    }

    var errorMessage = "";
    $scope.deleteProduct = function () {

        if (confirm("Do you want to delete?")) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: "api/DeleteBcHandler.ashx?mode=Product&productId=" + $state.params.productId,
                async: false,
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                success: function () {
         

                },
                error: function (xmlHttpRequest, errorString, errorMessage) {

                    window.location.href = "#/addproduct";
                    $scope.getProduct();
                    alert('Product details deleted successfully.');
                }
            });

        }
        return false;
    }

 
    var b = $state.params.productId;
    var productId = 0;
    if (b != undefined)
        productId = b;
    var defaultOption = '--Select--';
    var defaultOptionValue = -1;   

    if (productId != 0) {
        
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: "api/GetBCHandler.ashx?mode=userProduct&productId=" + productId,
            async: false,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            success: function (user) {
                
            
                $('#txtProductName').val(user[0].productName);
              

            }
        });
    }

  

    function validate() {

        errorMsg = '';       
        var name = $('#txtProductName').val();
        var result = true;


        if (name == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Product Name.";
            result = false;
        }
        else {
            if (name.match(/\d+/g) != null) {
                $('#divName').addClass("form-group has-error");
                result = false;
            }
            else {
                $("#divName").removeClass("form-group has-error");
            }

        }
    

        return result;
    }



    $('#btnAddProduct').on('click', function () {
        
        if (validate() == true) {
            var user = new Object();
            user.productId = 0;
            user.productName = $('#txtProductName').val();

            $.ajax({
                url: "api/InserUpdateProductHandler.ashx",
                method: "POST",
                datatype: "json",
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                data: { "Products": JSON.stringify(user) },
                success: function () {
                    $('#txtProductName').val('');
                    window.location.href = "#/addproduct";
                    $scope.getProduct();
                    alert('Product details saved successfully.');
                   
                },
                error: function (xmlHttpRequest, errorString, errorMessage) {
                    $('#txtProductName').val('');
                    window.location.href = "#/addproduct";
                    $scope.getProduct();
                    alert('Product details saved successfully.');
                }
            });
        }
        else
        {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
    });

    $('#btnPrdoctUpdate').on('click', function () {
        
        if (validate() == true) {
            var user = new Object();
            user.productId = $state.params.productId;
            user.productName = $('#txtProductName').val();

            $.ajax({
                url: "api/InserUpdateProductHandler.ashx",
                method: "POST",
                datatype: "json",
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                data: { "Products": JSON.stringify(user) },
                success: function () {
                    $('#txtProductName').val('');
                    window.location.href = "#/addproduct";
                    $scope.getProduct();
                    alert('Product details updated successfully.');

                },
                error: function (xmlHttpRequest, errorString, errorMessage) {
                    $('#txtProductName').val('');
                    window.location.href = "#/addproduct";
                    $scope.getProduct();
                    alert('Product details saved successfully.');
                }
            });
        }
        else {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
    });

    
});
