﻿
bctrackApp.controller("searchvillageController", function ($scope, $state, $cookies) {

    console.log('searchvillageController Called.');

    $scope.GetStates = function () {

        $('#selAddressState').empty();
        $('#selAddressDistrict').empty();
        $('#selAddresssubDistrict').empty();

      
        var allStates = new Array();
        var requestUrl = 'api/state.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (stateList) {
                if (stateList.length != 0) {
                    for (let i = 0; i < stateList.length; i++) {
                        let state = new Object();
                        state.StateId = stateList[i].StateId;
                        state.StateName = stateList[i].StateName;
                        state.StateCode = stateList[i].StateCode;
                        state.StateCircles = stateList[i].StateCircles;
                        state.Branches = stateList[i].Branches;
                        allStates.push(state);
                    }
                }
                states = stateList;
                if (allStates.length != 0) {
                    $('#selAddressState').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selAddressDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selAddresssubDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                   
               
                }
                if (allStates.length != 0) {
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selAddressState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }

            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allStates;

    }
    ////////////////////////-----------------------District----------
    $('#selAddressState').on('change', function () {
        
        $('#selAddressDistrict').empty();
        $('#selAddresssubDistrict').empty();
        var allDistrict = new Array();
        var requestUrl = 'api/Districts.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"stateId": "' + $('#selAddressState').val() + '"}'
           ,
            async: false,
            success: function (districtList) {
                if (districtList.length != 0) {
                    for (let i = 0; i < districtList.length; i++) {
                        let district = new Object();
                        district.DistrictCode = districtList[i].DistrictCode;
                        district.DistrictName = districtList[i].DistrictName;
                        district.DistrictId = districtList[i].DistrictId;

                        allDistrict.push(district);
                    }
                }
                // states = districtList;
                if (allDistrict.length != 0) {


                    $('#selAddressDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selAddresssubDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allDistrict.length; i++) {
                        $('#selAddressDistrict').append($('<option>', {
                            value: allDistrict[i].DistrictId,
                            text: allDistrict[i].DistrictName + '(' + allDistrict[i].DistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {

            }
        });
        return allDistrict;

    });



    

    //////////////////////////////////------------------------------- Subdistrict--------
    $('#selAddressDistrict').on('change', function () {
        
        $('#selAddresssubDistrict').empty();
        var allSubDistrict = new Array();
        var requestUrl = 'api/GetSubDistrict.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"distirctId": "' + $('#selAddressDistrict').val() + '"}'
           ,
            async: false,
            success: function (subdistrictList) {
                if (subdistrictList.length != 0) {
                    for (let i = 0; i < subdistrictList.length; i++) {
                        let subdistrict = new Object();
                        subdistrict.SubDistrictCode = subdistrictList[i].SubDistrictCode;
                        subdistrict.SubDistrictName = subdistrictList[i].SubDistrictName;
                        subdistrict.SubDistrictId = subdistrictList[i].SubDistrictId;

                        allSubDistrict.push(subdistrict);
                    }
                }
                $('#selAddresssubDistrict').append($('<option>', { value: 0, text: 'Select' }));
                if (allSubDistrict.length != 0) {

                    for (let i = 0; i < allSubDistrict.length; i++) {
                        $('#selAddresssubDistrict').append($('<option>', {
                            value: allSubDistrict[i].SubDistrictId,
                            text: allSubDistrict[i].SubDistrictName + '(' + allSubDistrict[i].SubDistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allSubDistrict;

    });


    $scope.SearchVillage = function () {
        
        StateId = $('#selAddressState').val();
        DistrictId = $('#selAddressDistrict').val();
        SubDistrictId = $('#selAddresssubDistrict').val();
        if (StateId == undefined || StateId == null || StateId == 0) {
            StateId = -1;
        }
        if (DistrictId == undefined || DistrictId == null || DistrictId == 0) {
            DistrictId = -1;
        }
        if (SubDistrictId == undefined || SubDistrictId == 0 || SubDistrictId==0) {
            SubDistrictId = -1;
        }
        var leadTable = $('#tblsearchVillage').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "searchVillage.aspx/getVillages",
            "columnDefs": [

                //{
                //    "orderable": false,
                //    "targets": -1,
                //    "render": function (data, type, full, meta) {
                //        return '<a    href="#/chnageStatusallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i></a>';
                //    }
                //}               
                     
            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
            
                aoData["StateId"] = StateId;
                aoData["DistrictId"] = DistrictId;
                aoData["SubDistrictId"] = SubDistrictId;
              
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblsearchVillage").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);


    }
});
