﻿bctrackApp.controller("addBankController", function ($scope, $state, $cookies) {
    $scope.divhome = false;
    $scope.divcircle = true;
    $scope.divprofile = true;
    $scope.displayTab = function (fn) {
        if (fn == "home")
        {
            $scope.divhome = false;
            $scope.divcircle = true;
            $scope.divprofile = true;
        }
        if (fn == "circle") {
            $scope.divhome = true;
            $scope.divcircle = false;
            $scope.divprofile = true;
        }
        if (fn == "profile") {
            $scope.divhome = true;
            $scope.divcircle = true;
            $scope.divprofile = false;
        }
    }






    $scope.btnedit = 'Edit';
    $scope.divVIEW = false;
    $scope.divEdit = true;

    $scope.editView = function () {

        if ($scope.btnedit == "Edit") {
            $scope.btnedit = 'View';
            $scope.divVIEW = true;
            $scope.divEdit = false;
        }
        else {
            $scope.btnedit = 'Edit';
            $scope.divVIEW = false;
            $scope.divEdit = true;
        }

    }
    var errorMessage = "";

    $scope.brancheslist = [];
    $scope.circles = [];
    $scope.zoneList = [];
    $scope.regionList = [];
    var zones = [];
    var regions = [];
    var regions = [];
    var branches = [];
    
    var b = $state.params.bankId;
    var bankId = '';
    if (b != undefined)
        bankId = parseInt(b);
    else
        bankId = 0;

    if (bankId != 0) {
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: "api/GetBCHandler.ashx?mode=Bank&bankId=" + bankId,
            async: false,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            success: function (bank) {
                 
                $('#txtName').val(bank.BankName);
                $('#txtAddress').val(bank.Address);
                $('#txtContact').val(bank.ContactNumber);
                $('#txtEmail').val(bank.Email);
                $('#lblName').val(bank.BankName);
                $scope.lblName = bank.BankName
                $scope.lblAddress = bank.Address;
                $scope.lblContact = bank.ContactNumber;
                $scope.lblEmail = bank.Email;


                if (bank.BankCircles != null) {
                    for (var c = 0; c < bank.BankCircles.length; c++) {
                        var circle = bank.BankCircles[c];
                        
                        //   circles.push(circle);
                        $scope.circles.push(circle);

                        if (circle.CircleZones != null) {
                            for (var z = 0; z < circle.CircleZones.length; z++) {
                                var zone = circle.CircleZones[z];
                                zones.push(zone);
                                $scope.zoneList.push(zone);
                                if (zone.ZoneRegions != null) {
                                    for (var r = 0; r < zone.ZoneRegions.length; r++) {
                                        var region = zone.ZoneRegions[r];
                                        delete region.RegionBranches;
                                        region.RegionBranches = [];
                                        regions.push(region);
                                        $scope.regionList.push(region);

                                      
                                    }

                                } // if (zone.ZoneRegions
                            }
                        }
                    }
                    if (bank.Branches != null) {
                        for (var b = 0; b < bank.Branches.length; b++) {
                            var branch = bank.Branches[b];
                            branches.push(branch);
                            //    AddBranch(branch, region.RegionName);
                            $scope.brancheslist.push(branch);
                        }

                    }

                }

                PopulateCircles();
                Populatezone();
                PopulateRegions();


            }
        });
    }

    $scope.deleteBank = function () {

        if (confirm("Do you want to delete?")) {

            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: "api/DeleteBcHandler.ashx?mode=Bank&bankId=" + $state.params.bankId,
                async: false,
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                success: function () {


                },
                error: function (xmlHttpRequest, errorString, errorMessage) {

                    window.location.href = "#/banklist";

                    alert('Bank deleted successfully.');
                }
            });
        }
        return false;
    }
    
    $scope.addCircle = function () {

        var circle = new Object();
        circle.CircleName = $('#txtCircleName').val();
        circle.CircleZones = [];

        $scope.circles.push(circle);
        $('#txtCircleName').val('');
        PopulateCircles();
    }


    $scope.addZone = function () {

        var zone = new Object();
        zone.ZoneName = $('#txtZoneName').val();
        zone.CircleId = $('#selZoneCircle').val();
        zone.ZoneRegions = [];
        var circleName = $("#selZoneCircle option:selected").text();
        $scope.zoneList.push({
            "ZoneName": zone.ZoneName,
            "CircleName": circleName
        });

        zones.push(zone);
        //Add to relevant Cirlce
        for (var c = 0; c < $scope.circles.length; c++) {
            var circ = $scope.circles[c];
            if (circ.CircleName == circleName) {
                circ.CircleZones.push(zone);

            }

        }

        $('#txtZoneName').val('');
        Populatezone();
    }
    $scope.addRegion = function () {

         

        var region = new Object();
        region.RegionName = $('#txtRegionName').val();
        region.ZoneId = $("#selRegionZone").val();
        region.RegionBranches = [];
        var zoneName = $("#selRegionZone option:selected").text();

        $scope.regionList.push({
            "RegionName": region.RegionName,
            "ZoneName": zoneName
        })
        regions.push(region);
        PopulateRegions();

        for (var z = 0; z < zones.length; z++) {
            var zon = zones[z];
            if (zon.ZoneName == zoneName) {
                zon.ZoneRegions.push(region);

            }

        }
        $('#txtRegionName').val('');

    }
    function PopulateRegions() {
        $('#selBranchRegion').empty();
        for (var c = 0; c < regions.length; c++) {
            $('#selBranchRegion').append($('<option>', {
                value: 0,
                text: regions[c].RegionName
            }));


        }
    }
    $scope.removeCircle = function (index) {

        $scope.circles.splice(index, 1);
    }
    $scope.removeZone = function (index, cName) {

        $scope.zoneList.splice(index, 1);

        for (var c = 0; c < $scope.circles.length; c++) {
            var circ = $scope.circles[c];
            if (circ.CircleName == cName) {
                circ.CircleZones.splice(index, 1);

            }

        }

    }
    $scope.removeRegion = function (index, zoneName) {
        $scope.regionList.splice(index, 1);
        for (var z = 0; z < zones.length; z++) {
            var zon = zones[z];
            if (zon.ZoneName == zoneName) {
                zon.ZoneRegions.splice(index, 1);

            }

        }
    }

    function PopulateCircles() {

        $('#selZoneCircle').empty();
        for (var c = 0; c < $scope.circles.length; c++) {
            $('#selZoneCircle').append($('<option>', {
                value: 0,
                text: $scope.circles[c].CircleName
            }));
        }
    }
    function Populatezone() {
        
        $('#selRegionZone').empty();
        for (var c = 0; c < $scope.zoneList.length; c++) {
            $('#selRegionZone').append($('<option>', {
                value: 0,
                text: $scope.zoneList[c].ZoneName
            }));
        }
    }



    function validate() {

        errorMsg = '';

        var email = $('#txtEmail').val();
        var empId = $('#txtAddress').val();
        var name = $('#txtName').val();
        var phone = $('#txtContact').val();

        var result = true;



        if (!IsEmail(email)) {
            $('#txtEmail').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter valid email Id.";
            result = false;
        }
        else {
            $("#divEmail").removeClass("form-group has-error");
        }

        if (name == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Name.";
            result = false;
        }
        else {
            if (name.match(/\d+/g) != null) {
                $('#divName').addClass("form-group has-error");
                result = false;
            }
            else {
                $("#divName").removeClass("form-group has-error");
            }

        }

        if (phone == "" || phone.length > 10 || phone.length < 10 || isNaN(phone)) {
            $('#divPhone').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Valid Mobile Number.";
            result = false;
        }
        else {
            $("#divPhone").removeClass("form-group has-error");
        }

        return result;
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    $scope.addBank = function () {
         
        if (validate() == true) {
            var bank = new Object();
            bank.BankId = bankId;
            bank.BankName = $('#txtName').val();
            bank.Address = $('#txtAddress').val();
            bank.ContactNumber = $('#txtContact').val();
            bank.Email = $('#txtEmail').val();
            bank.BankCircles = $scope.circles;

            $.ajax({
                url: "api/InsertUpdateBankHandler.ashx",
                method: "POST",
                datatype: "json",
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                data: { "bank": JSON.stringify(bank) },
                success: function () {
                    alert('Bank details saved successfully.');
                    window.location.href = "#/banklist";
                },
                error: function (xmlHttpRequest, errorString, errorMessage) {
                    alert('Bank details saved successfully.');
                    window.location.href = "#/banklist";
                }
            });

        }

        else {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
    }

    $scope.addBranch = function () {
         
        var branch = new Object();

        
        branch.BranchCode = $('#txtBranchCode').val();
        branch.BranchName = $('#txtBranchName').val();
        branch.IFSCCode = $('#txtBranchIFSCCode').val();        
        branch.Address = $('#txtBranchAddress').val();
        branch.RegionId = $('#selBranchRegion').val();
        branch.StateId = $('#selBranchState').val();
        branch.DistrictId = $('#selBranchDistrict').val();
        branch.CityId = $('#selBranchCity').val();
        branch.Subdistrict = $('#selBranchCity').val();
        branch.VillageId = $('#selBranchVillage').val();

        branch.TalukaId = 0;    
        branch.PinCode = $('#txtBranchPinCode').val();
        branch.ContactNumber = $('#txtBranchContact').val();
        branch.Email = $('#txtBranchEmail').val();

        branch.RegionName = $("#selBranchRegion option:selected").text();
        var RegionName = $("#selBranchRegion option:selected").text();
        branches.push(branch);
        $scope.brancheslist.push(branch);
        for (var r = 0; r < regions.length; r++) {
            var reg = regions[r];
            if (reg.RegionName == RegionName) {

                reg.RegionBranches.push(branch);

            }

        }
        var s = $scope.circles;
     


    };
    $scope.removeBranch = function (index, RegionName) {

        for (var r = 0; r < regions.length; r++) {
            var reg = regions[r];
            if (reg.RegionName == RegionName) {
                reg.RegionBranches.push(branch);

            }

        }
        $scope.brancheslist.splice(index, 1);
    }
    $scope.isNumber = function (evt)
    {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

    function ShowDialog(message, height, width, title, padding, icon, showOkButton, link) {

        $("#helloModalMessage").html(message);

        $('#helloModal').modal('show');

        if (showOkButton == false) {
            $("#closemodalbutton").show();
            $("#redirectbutton").hide();
        }
        else {
            if (link != '')
                $("#redirectbutton").attr("href", link);
            $("#closemodalbutton").hide();
            $("#redirectbutton").show();
        }

        //$.Dialog({
        //    overlay: true,
        //    shadow: true,
        //    flat: true,
        //    title: title,
        //    icon: icon,
        //    content: '',
        //    width: width,
        //    padding: padding,
        //    height: height,//600,400
        //    onShow: function (_dialog) {
        //        var content = _dialog.children('.content');
        //        content.html(message);
        //        $.Metro.initInputs();
        //    }
        //});
    }


    $scope.GetStates = function () {

        $('#selBranchDistrict').empty();
        $('#selBranchCity').empty();      
        $('#selBranchVillage').empty();

      


        var allStates = new Array();
        var requestUrl = 'api/state.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (stateList) {
                if (stateList.length != 0) {
                    for (let i = 0; i < stateList.length; i++) {
                        let state = new Object();
                        state.StateId = stateList[i].StateId;
                        state.StateName = stateList[i].StateName;
                        state.StateCode = stateList[i].StateCode;
                        state.StateCircles = stateList[i].StateCircles;
                        state.Branches = stateList[i].Branches;
                        allStates.push(state);
                    }
                }
                states = stateList;
                if (allStates.length != 0) {
                    $('#selBranchState').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selBranchDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selBranchCity').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selBranchVillage').append($('<option>', { value: 0, text: '--Select--' }));               
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selBranchState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }
          

            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allStates;
    }
    $scope.sDistricts = [];
    $scope.sCities = [];
    $('#selBranchState').on('change', function () {
        $('#selBranchDistrict').empty();

        var allDistrict = new Array();
        var requestUrl = 'api/Districts.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")

            },
            data: '{"stateId": "' + $('#selBranchState').val() + '"}'
           ,
            async: false,
            success: function (districtList) {
                if (districtList.length != 0) {
                    for (let i = 0; i < districtList.length; i++) {
                        let district = new Object();
                        district.DistrictCode = districtList[i].DistrictCode;
                        district.DistrictName = districtList[i].DistrictName;
                        district.DistrictId = districtList[i].DistrictId;

                        allDistrict.push(district);
                    }
                }
                // states = districtList;
                if (allDistrict.length != 0) {


                    //$('#selPLdVillages').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allDistrict.length; i++) {
                        $('#selBranchDistrict').append($('<option>', {
                            value: allDistrict[i].DistrictId,
                            text: allDistrict[i].DistrictName + '(' + allDistrict[i].DistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allDistrict;
        //let allDistricts = getDistricts($('#selPLState').val(), states);
        //if (allDistricts.length >= 1) {
        //    $('#selPLDistrict').empty();
        //    $('#selPLDistrict').append($('<option>', { value: 0, text: 'Select District' }));
        //    for (let i = 0; i < allDistricts.length; i++) {
        //        $('#selPLDistrict').append($('<option>', { value: allDistricts[i].DistrictId, text: allDistricts[i].DistrictName + '(' + allDistricts[i].DistrictCode + ')' }));
        //    }
        //}
    });

    $('#selBranchDistrict').on('change', function () {
        $('#selBranchCity').empty();

        var allSubDistrict = new Array();
        var requestUrl = 'api/GetSubDistrict.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"distirctId": "' + $('#selBranchDistrict').val() + '"}'
           ,
            async: false,
            success: function (subdistrictList) {
                if (subdistrictList.length != 0) {
                    for (let i = 0; i < subdistrictList.length; i++) {
                        let subdistrict = new Object();
                        subdistrict.SubDistrictCode = subdistrictList[i].SubDistrictCode;
                        subdistrict.SubDistrictName = subdistrictList[i].SubDistrictName;
                        subdistrict.SubDistrictId = subdistrictList[i].SubDistrictId;

                        allSubDistrict.push(subdistrict);
                    }
                }
                // states = districtList;
                if (allSubDistrict.length != 0) {


                    $('#selBranchCity').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allSubDistrict.length; i++) {
                        $('#selBranchCity').append($('<option>', {
                            value: allSubDistrict[i].SubDistrictId,
                            text: allSubDistrict[i].SubDistrictName + '(' + allSubDistrict[i].SubDistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allDistrict;
        // 
        //let subDistrict = new Array();
        //let distrcts = getDistricts($('#selPLState').val(), states);
        //subDistrict = getCities($('#selPLDistrict').val(), distrcts);
        //console.log(JSON.stringify(subDistrict));
        //$('#selPLSubDistrict').empty();
        //$('#selPLSubDistrict').append($('<option>', {
        //    value: 0,
        //    text: 'Select Sub District'
        //}));
        //if (subDistrict.length >= 1) {
        //    for (let i = 0; i < subDistrict.length; i++) {
        //        $('#selPLSubDistrict').append($('<option>', { value: subDistrict[i].SubDistrictId, text: subDistrict[i].SubDistrictName + '(' + subDistrict[i].SubDistrictCode + ')' }));
        //    }
        //}
    });
    $('#selBranchCity').on('change', function () {
        $('#selBranchVillage').empty();
        
        var allVillage = new Array();
        var requestUrl = 'api/GetVillage.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"subDistrictId": "' + $('#selBranchCity').val() + '"}'
           ,
            async: false,
            success: function (villageList) {
                if (villageList.length != 0) {
                    for (let i = 0; i < villageList.length; i++) {
                        let village = new Object();
                        village.VillageCode = villageList[i].VillageCode;
                        village.VillageName = villageList[i].VillageName;
                        village.VillageId = villageList[i].VillageId;

                        allVillage.push(village);
                    }
                }
                $scope.allVillageList = allVillage;
                $('#selBranchVillage').append($('<option>', { value: 0, text: '--Select--' }));
                if (allVillage.length != 0) {
                    for (let i = 0; i < allVillage.length; i++) {
                        $('#selBranchVillage').append($('<option>', {
                            value: allVillage[i].VillageId,
                            text: allVillage[i].VillageName + '(' + allVillage[i].VillageCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the village list could not be populated!\n' + errorMessage);
            }
        });
        return allVillage;
       
    });

})