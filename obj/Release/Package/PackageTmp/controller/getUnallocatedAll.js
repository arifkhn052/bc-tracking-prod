﻿

bctrackApp.controller("getUnallocatedAll", function ($scope, $state, $cookies) {
    var leadTable = $('#tbl_User_List').dataTable({
        "oLanguage": {
            "sZeroRecords": "No records to display",
            "sSearch": "Search "
        },
        "sDom": 'T<"clear">lfrtip',
        "tableTools": {

            "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
        },
        "iDisplayLength": 15,
        "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
        "bSortClasses": false,
        "bStateSave": false,
        "bPaginate": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "bclistUnallocated.aspx/getUnallocated",
        "columnDefs": [

             {
                 "orderable": false,
                 "targets": -1,
                 "render": function (data, type, full, meta) {
                     return '<a class="btn btn-danger  btn-sm"    href="#/chnageStatusallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> Terminate</a>';
                 }
             },
              {
                  "orderable": false,
                  "targets": -2,
                  "render": function (data, type, full, meta) {

                      if (full[6] == "True")
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i> Blacklisted</a>';
                      else
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Blacklist</a>';
                  }
              },
                {
                    "orderable": false,
                    "targets": -3,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-primary  btn-sm"   href="#/viewallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>';
                    }
                },
                  {
                      "orderable": false,
                      "targets": -4,
                      "render": function (data, type, full, meta) {
                          return '<a class="btn btn-primary  btn-sm"   href="#/editallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Edit</a>';
                      }
                  }

        ],
        "bDeferRender": true,
        "fnServerData": function (sSource, aoData, fnCallback) {
                      
              
            aoData["adminuserid"] = $cookies.get("userId"),
               
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success":
                            function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tbl_User_List").show();
                            }
            });
        }
    });
    leadTable.fnSetFilteringDelay(300);


});