﻿bctrackApp.controller("userController", function ($scope, $state, $cookies) {
    var leadTable = $('#tbl_User_List').dataTable({
        "oLanguage": {
            "sZeroRecords": "No records to display",
            "sSearch": "Search "
        },
        "sDom": 'T<"clear">lfrtip',
        "tableTools": {

            "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
        },
        "iDisplayLength": 15,
        "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
        "bSortClasses": false,
        "bStateSave": false,
        "bPaginate": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "UserList.aspx/getUser",
        "columnDefs": [

            {
                "orderable": false,
                "targets": -1,
                "render": function (data, type, full, meta) {
                    return '<a  class="btn btn-success btn-sm btn-labeled btn-rounded"  href="#/editUser/' + full[0] + '" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                }
            }
        ],
        "bDeferRender": true,
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success":
                            function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tbl_User_List").show();
                            }
            });
        }
    });
    //  leadTable.fnSetFilteringDelay(300);


    function deleteUser(userId) {

        if (confirm("Do you want to delete?")) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                url: "api/DeleteBcHandler.ashx?mode=User&userId=" + userId,
                async: false,
                success: function (user) {
                    window.location.reload();

                }
            });

        }
        return false;
    }


    var b = $state.params.userId;
    var userId = 0;
    if (b == undefined) {
        userId = 0;
        $('#btnUpdate').hide();
        $('#btnDelete').hide();
    }

    else {
        userId = parseInt(b)
        $('#btnSubmitUser').hide();

    }




    var defaultOption = '--Select--';
    var defaultOptionValue = -1;
    GetBanks();

    if (userId != 0) {

        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: "api/GetBCHandler.ashx?mode=User&userId=" + userId,
            async: false,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            success: function (user) {

                debugger;
                $('#txtUserName').val(user.UserName);
                $('#txtFirstName').val(user.FirstName);
                $('#txtLastName').val(user.LastName);
                $('#selRole').val(user.RoleId);
                $('#selBank').val(user.BankId);
                $('#txtphone').val(user.Phone);
                $('#txtEmail').val(user.Email);

            }
        });
    }

    function getParameterByName(name) //courtesy Artem
    {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else {
            if ((results[1].indexOf('?')) > 0)
                return decodeURIComponent(results[1].substring(0, results[1].indexOf('?')).replace(/\+/g, " "));
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    function GetBanks() {

        var requrl = "api/getBankHandler.ashx?mode=Bank";

        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            url: requrl,
            async: false,
            success: function (Banks) {
                //console('Got it!');

                for (var cnt = 0; cnt < Banks.length; cnt++) {

                    $('#selBank').append($('<option>', {
                        value: Banks[cnt].BankId,
                        text: Banks[cnt].BankName
                    }));

                }

                $('.loader').hide();

            },
            error: function (err) {
                //   alert(err);
                $('.loader').hide();
            }

        });

    }
    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    $scope.filterValue = function ($event) {
        if (isNaN(String.fromCharCode($event.keyCode))) {
            $event.preventDefault();
        }
    };
    var errorMessage = '';
    function validate() {

        errorMsg = '';


        var empId = $('#txtUserName').val();
        var name = $('#txtFirstName').val();
        var LastName = $('#txtLastName').val();
        //var phone = $('#txtphone').val();
      //  var email = $('#txtEmail').val();
        var role = $('#selRole').val();
        var Bank = $('#selBank').val();


        var result = true;



        if (empId == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter UserId.";
            result = false;
        }

        else {
            $("#divName").removeClass("form-group has-error");
        }





        if (name == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter First Name.";
            result = false;
        }
        else {
            if (name.match(/\d+/g) != null) {
                errorMsg = errorMsg + "<br/>Enter First Name.";
                result = false;
            }
            else {
                $("#divName").removeClass("form-group has-error");
            }

        }

        if (LastName == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Last Name.";
            result = false;
        }
        else {
            if (LastName.match(/\d+/g) != null) {
                errorMsg = errorMsg + "<br/>Enter Last Name.";
                result = false;
            }
            else {
                $("#divName").removeClass("form-group has-error");
            }

        }
        //if (phone == "" || phone.length > 10 || phone.length < 10 || isNaN(phone)) {
        //    $('#divPhone').addClass("form-group has-error");
        //    errorMsg = errorMsg + "<br/>Enter Valid First Mobile Number.";
        //    result = false;
        //}
        //else {
        //    $("#divPhone").removeClass("form-group has-error");
        //}
         if (!IsEmail(email)) {
            $('#txtemail').addclass("form-group has-error");
            errormsg = errormsg + "<br/>enter valid email id.";
            result = false;
        }
        else {
            $("#divemail").removeclass("form-group has-error");
        }

        if (role == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>select Role";
            result = false;
        }
        if (Bank == "" && role == "3") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>select Bank";
            result = false;
        }



        return result;
    }



    $('#btnSubmitUser').on('click', function () {

        if (validate() == true) {
            var user = new Object();
            user.userId = 0;
            user.UserName = $('#txtUserName').val();
            user.FirstName = $('#txtFirstName').val();
            user.LastName = $('#txtLastName').val();
            user.Phone = $('#txtphone').val();
            user.Email = $('#txtEmail').val();
            if ($('#selRole').val() == "1") {
                user.BankId = 1;
            }
            else {
                user.BankId = $('#selBank').val();
            }

            user.UserPassword = $.md5($('#txtPassword').val());
            user.mode = 0;

            user.UserRoles = [];
            var userRole = new Object();
            userRole.RoleId = $('#selRole').val();
            user.UserRoles.push(userRole);

            console.log(user)
            $.ajax({
                url: "api/InsertUpdateUserHandler.ashx",
                method: "POST",
                datatype: "json",
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                data: { "user": JSON.stringify(user) },
                success: function (data) {
                    if (data == 'UserId already exists.') {
                        alert(data);
                    }
                    else {
                        window.location.href = "#/userlist";
                        alert('User Saved!');
                    }

                },
                error: function (xmlHttpRequest, errorString, errorMessage) {
                    //window.location.href = "#/userlist";
                    //alert('User Saved!');
                }

            });
        } else {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
    });
    function ShowDialog(message, height, width, title, padding, icon, showOkButton, link) {

        $("#helloModalMessage").html(message);

        $('#helloModal').modal('show');

        if (showOkButton == false) {
            $("#closemodalbutton").show();
            $("#redirectbutton").hide();
        }
        else {
            if (link != '')
                $("#redirectbutton").attr("href", link);
            $("#closemodalbutton").hide();
            $("#redirectbutton").show();
        }

        //$.Dialog({
        //    overlay: true,
        //    shadow: true,
        //    flat: true,
        //    title: title,
        //    icon: icon,
        //    content: '',
        //    width: width,
        //    padding: padding,
        //    height: height,//600,400
        //    onShow: function (_dialog) {
        //        var content = _dialog.children('.content');
        //        content.html(message);
        //        $.Metro.initInputs();
        //    }
        //});
    }

    $('#btnDelete').on('click', function () {
        if (confirm("Do you want to delete?")) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                url: "api/DeleteBcHandler.ashx?mode=User&userId=" + userId,
                async: false,
                success: function (user) {
                    window.location.href = "#/userlist";
                    alert('User deleted successfully.');

                },
                error: function (xmlHttpRequest, errorString, errorMessage) {
                    window.location.href = "#/userlist";
                    alert('User deleted successfully.');
                }

            });

        }
        return false;
    });
    $('#btnUpdate').on('click', function () {

        if (validate() == true) {
            var user = new Object();
            user.userId = userId;
            user.UserName = $('#txtUserName').val();
            user.FirstName = $('#txtFirstName').val();
            user.LastName = $('#txtLastName').val();
            user.Phone = $('#txtphone').val();
            user.Email = $('#txtEmail').val();
            if ($('#selRole').val() == "1") {
                user.BankId = 1;
            }
            else {
                user.BankId = $('#selBank').val();
            }
            user.UserPassword = $.md5($('#txtPassword').val());
            user.mode = 1;

            user.UserRoles = [];
            var userRole = new Object();
            userRole.RoleId = $('#selRole').val();
            user.UserRoles.push(userRole);

            console.log(user)
            $.ajax({
                url: "api/InsertUpdateUserHandler.ashx",
                method: "POST",
                datatype: "json",
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                data: { "user": JSON.stringify(user) },
                success: function (data) {
                    if (data == 'UserId already exists.') {
                        alert(data);
                    }
                    else {
                        window.location.href = "#/userlist";
                        alert('User Updated successfully.');
                    }

                },
                error: function (xmlHttpRequest, errorString, errorMessage) {
                    //window.location.href = "#/userlist";
                    //alert('User Saved!');
                }

            });
        } else {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
    });

});
