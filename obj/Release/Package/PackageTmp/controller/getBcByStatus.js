﻿

bctrackApp.controller("getBcByStatus", function ($scope, $state, $cookies) {
    $scope.datetimeBind = function () {
        //tabing();
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        var firstDay = '01/' + mm + '/' + yyyy;
        $("#dtStartDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: firstDay
        });
        $("#dtStartDate").val(firstDay);

        $("#dtEndDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#dtEndDate").val(today);
    }

    $scope.getListByStatus = function () {
        startDate = $('#dtStartDate').val();
        endDate = $('#dtEndDate').val();
        var leadTable = $('#tblbclistStatus').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "bclistStatus.aspx/getBCByStatus",
            "columnDefs": [

                {
                    "orderable": false,
                    "targets": -1,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-danger  btn-sm"    href="#/chnageStatusallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> Terminate</a>';
                    }
                },
              {
                  "orderable": false,
                  "targets": -2,
                  "render": function (data, type, full, meta) {

                      if (full[6] == "True")
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i> Blacklisted</a>';
                      else
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Blacklist</a>';
                  }
              },
                {
                    "orderable": false,
                    "targets": -3,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-primary  btn-sm"   href="#/viewallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>';
                    }
                },
                  {
                      "orderable": false,
                      "targets": -4,
                      "render": function (data, type, full, meta) {
                          return '<a class="btn btn-primary  btn-sm"   href="#/editallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Edit</a>';
                      }
                  }

            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["adminuserid"] = $cookies.get("userId");
                
                 aoData["startDate"] = startDate;
                aoData["endDate"] = endDate;
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblbclistStatus").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);

    }

    $scope.getListByStatusFilter = function () {
        
        startDate = $('#dtStartDate').val();
        endDate = $('#dtEndDate').val();
        let status = $('#drpStatus').val();
        var itype;

        if (status == "All") {
            itype = 2;
        }
        else {
            itype = 3;
        }

        var leadTable = $('#tblbclistStatus').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "bclistStatus.aspx/getBCByStatusFilter",
            "columnDefs": [

                {
                    "orderable": false,
                    "targets": -1,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-danger  btn-sm"    href="#/chnageStatusallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> Terminate</a>';
                    }
                },
              {
                  "orderable": false,
                  "targets": -2,
                  "render": function (data, type, full, meta) {

                      if (full[6] == "True")
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i> Blacklisted</a>';
                      else
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Blacklist</a>';
                  }
              },
                {
                    "orderable": false,
                    "targets": -3,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-primary  btn-sm"   href="#/viewallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>';
                    }
                },
                  {
                      "orderable": false,
                      "targets": -4,
                      "render": function (data, type, full, meta) {
                          return '<a class="btn btn-primary  btn-sm"   href="#/editallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Edit</a>';
                      }
                  }

            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["itype"] = itype;
                aoData["startDate"] = startDate;            
                aoData["endDate"] = endDate;
                aoData["status"] = status;
                aoData["adminuserid"] = $cookies.get("userId");
               
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblbclistStatus").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);

    }
   


});