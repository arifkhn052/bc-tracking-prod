﻿

bctrackApp.controller("bcListAddressWiseContro", function ($scope, $state, $cookies, $window) {

    $scope.GetStates = function () {
        
        $('#selAddressState').empty();
        $('#selAddressVillage').empty();
        $('#selAddressDistrict').empty();

        $('#selAddresssubDistrict').empty();

        $('#selAddressVillage').empty();
   
        $('#selAddressState').append($('<option>', { value: 0, text: '--Select--' }));
        $('#selAddressDistrict').append($('<option>', { value: 0, text: '--Select--' }));
        $('#selAddresssubDistrict').append($('<option>', { value: 0, text: '--Select--' }));
        $('#selAddressVillage').append($('<option>', { value: 0, text: '--Select--' }));

        var allStates = new Array();
        var requestUrl = 'api/state.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (stateList) {
                if (stateList.length != 0) {
                    for (let i = 0; i < stateList.length; i++) {
                        let state = new Object();
                        state.StateId = stateList[i].StateId;
                        state.StateName = stateList[i].StateName;
                        state.StateCode = stateList[i].StateCode;
                        state.StateCircles = stateList[i].StateCircles;
                        state.Branches = stateList[i].Branches;
                        allStates.push(state);
                    }
                }
                states = stateList;
                if (allStates.length != 0) {
                  
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selAddressState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }
                if (allStates.length != 0) {
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selPLState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }

            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allStates;
    }
    ////////////////////////-----------------------District----------
    $('#selPLState').on('change', function () {

        $('#selPLDistrict').empty();
        var allDistrict = new Array();
        var requestUrl = 'api/Districts.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"stateId": "' + $('#selPLState').val() + '"}'
           ,
            async: false,
            success: function (districtList) {
                if (districtList.length != 0) {
                    for (let i = 0; i < districtList.length; i++) {
                        let district = new Object();
                        district.DistrictCode = districtList[i].DistrictCode;
                        district.DistrictName = districtList[i].DistrictName;
                        district.DistrictId = districtList[i].DistrictId;

                        allDistrict.push(district);
                    }
                }
                // states = districtList;
                if (allDistrict.length != 0) {


                    $('#selPLDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allDistrict.length; i++) {
                        $('#selPLDistrict').append($('<option>', {
                            value: allDistrict[i].DistrictId,
                            text: allDistrict[i].DistrictName + '(' + allDistrict[i].DistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                
            }
        });
        return allDistrict;

    });



    $('#selAddressState').on('change', function () {

        var id = $('#selAddressState').val();
        $scope.BindDistrict(id);
    });

    $scope.BindDistrict = function (id) {
        $('#selAddressDistrict').empty();

        var allDistrict = new Array();
        var requestUrl = 'api/Districts.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"stateId": "' + id + '"}'
           ,
            async: false,
            success: function (districtList) {
                if (districtList.length != 0) {
                    for (let i = 0; i < districtList.length; i++) {
                        let district = new Object();
                        district.DistrictCode = districtList[i].DistrictCode;
                        district.DistrictName = districtList[i].DistrictName;
                        district.DistrictId = districtList[i].DistrictId;

                        allDistrict.push(district);
                    }
                }
                // states = districtList;
                if (allDistrict.length != 0) {


                    $('#selAddressDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allDistrict.length; i++) {
                        $('#selAddressDistrict').append($('<option>', {
                            value: allDistrict[i].DistrictId,
                            text: allDistrict[i].DistrictName + '(' + allDistrict[i].DistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allDistrict;
    }

    //////////////////////////////////------------------------------- Subdistrict--------
    $('#selPLDistrict').on('change', function () {

        $('#selPLSubDistrict').empty();

        var allSubDistrict = new Array();
        var requestUrl = 'api/GetSubDistrict.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"distirctId": "' + $('#selPLDistrict').val() + '"}'
           ,
            async: false,
            success: function (subdistrictList) {
                if (subdistrictList.length != 0) {
                    for (let i = 0; i < subdistrictList.length; i++) {
                        let subdistrict = new Object();
                        subdistrict.SubDistrictCode = subdistrictList[i].SubDistrictCode;
                        subdistrict.SubDistrictName = subdistrictList[i].SubDistrictName;
                        subdistrict.SubDistrictId = subdistrictList[i].SubDistrictId;

                        allSubDistrict.push(subdistrict);
                    }
                }
                $('#selPLSubDistrict').append($('<option>', { value: 0, text: 'Select' }));
                if (allSubDistrict.length != 0) {

                    for (let i = 0; i < allSubDistrict.length; i++) {
                        $('#selPLSubDistrict').append($('<option>', {
                            value: allSubDistrict[i].SubDistrictId,
                            text: allSubDistrict[i].SubDistrictName + '(' + allSubDistrict[i].SubDistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allDistrict;

    });


    $('#selAddressDistrict').on('change', function () {

        var id = $('#selAddressDistrict').val()
        $scope.BindSubDistrict(id);
    });

    $scope.BindSubDistrict = function (id) {

        $('#selAddresssubDistrict').empty();

        var allSubDistrict = new Array();
        var requestUrl = 'api/GetSubDistrict.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"distirctId": "' + id + '"}'
           ,
            async: false,
            success: function (subdistrictList) {
                if (subdistrictList.length != 0) {
                    for (let i = 0; i < subdistrictList.length; i++) {
                        let subdistrict = new Object();
                        subdistrict.SubDistrictCode = subdistrictList[i].SubDistrictCode;
                        subdistrict.SubDistrictName = subdistrictList[i].SubDistrictName;
                        subdistrict.SubDistrictId = subdistrictList[i].SubDistrictId;

                        allSubDistrict.push(subdistrict);
                    }
                }
                // states = districtList;
                if (allSubDistrict.length != 0) {


                    $('#selAddresssubDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allSubDistrict.length; i++) {
                        $('#selAddresssubDistrict').append($('<option>', {
                            value: allSubDistrict[i].SubDistrictId,
                            text: allSubDistrict[i].SubDistrictName + '(' + allSubDistrict[i].SubDistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        //    return allDistrict;
    }
    //////////////////////////////////------------------------------- Villagee--------
    $('#selPLSubDistrict').on('change', function () {
        $('#selPLdVillages').empty();

        var allVillage = new Array();
        var requestUrl = 'api/GetVillage.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"subDistrictId": "' + $('#selPLSubDistrict').val() + '"}'
           ,
            async: false,
            success: function (villageList) {
                if (villageList.length != 0) {
                    for (let i = 0; i < villageList.length; i++) {
                        let village = new Object();
                        village.VillageCode = villageList[i].VillageCode;
                        village.VillageName = villageList[i].VillageName;
                        village.VillageId = villageList[i].VillageId;

                        allVillage.push(village);
                    }
                }
                $scope.allVillageList = allVillage;
                $('#selPLdVillages').append($('<option>', { value: 0, text: '--Select--' }));
                if (allVillage.length != 0) {
                    for (let i = 0; i < allVillage.length; i++) {
                        $('#selPLdVillages').append($('<option>', {
                            value: allVillage[i].VillageId,
                            text: allVillage[i].VillageName + '(' + allVillage[i].VillageCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the village list could not be populated!\n' + errorMessage);
            }
        });
        return allVillage;
      
    });

    $('#selAddresssubDistrict').on('change', function () {
        var id = $('#selAddresssubDistrict').val();
        $scope.BindVillage(id);
    });
    $scope.BindVillage = function (id) {

         
        $('#selAddressVillage').empty();
        var allVillage = new Array();
        var requestUrl = 'api/GetVillage.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"subDistrictId": "' + id + '"}'
           ,
            async: false,
            success: function (villageList) {
                if (villageList.length != 0) {
                    for (let i = 0; i < villageList.length; i++) {
                        let village = new Object();
                        village.VillageCode = villageList[i].VillageCode;
                        village.VillageName = villageList[i].VillageName;
                        village.VillageId = villageList[i].VillageId;

                        allVillage.push(village);
                    }
                }
                $scope.allVillageList = allVillage;
                $('#selAddressVillage').append($('<option>', { value: 0, text: '--Select--' }));
                if (allVillage.length != 0) {
                    for (let i = 0; i < allVillage.length; i++) {
                        $('#selAddressVillage').append($('<option>', {
                            value: allVillage[i].VillageId,
                            text: allVillage[i].VillageName + '(' + allVillage[i].VillageCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the village list could not be populated!\n' + errorMessage);
            }
        });
        //  return allVillage;

    }

    $scope.ListOfLocationWIse = function () {
     
        State = $('#selAddressState').val();
        District = $('#selAddressDistrict').val();
        SubDistrict = $('#selAddresssubDistrict').val();
        Village = $('#selAddressVillage').val();
        if (State == null || State == '')
        {
            State = "0";
        }
        if (State == null || State == '') {
            State = "0";
        }
        if (District == null || District == '') {
            District = "0";
        }
        if (SubDistrict == null || SubDistrict == '') {
            SubDistrict = "0";
        }
        if (Village == null || Village == '') {
            Village = "0";
        }

        var leadTable = $('#tblbclistLocationWise').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "bclistLocationWise.aspx/getBCLocationWise",
            "columnDefs": [

               {
                   "orderable": false,
                   "targets": -1,
                   "render": function (data, type, full, meta) {
                       return '';// '<a class="btn btn-danger  btn-sm"    href="#/chnageStatusallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> Terminate</a>';
                   }
               },
              {
                  "orderable": false,
                  "targets": -2,
                  "render": function (data, type, full, meta) {

                      if (full[6] == "True")
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i> Blacklisted</a>';
                      else
                          return '';// '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Blacklist</a>';
                  }
              },
                {
                    "orderable": false,
                    "targets": -3,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-primary  btn-sm"   href="#/viewallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>';
                    }
                },
                  {
                      "orderable": false,
                      "targets": -4,
                      "render": function (data, type, full, meta) {
                          return '<a class="btn btn-primary  btn-sm"   href="#/editallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Edit</a>';
                      }
                  }
            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["adminuserid"] = $cookies.get("userId");
                aoData["State"] = State;
                aoData["District"] = District;
                aoData["SubDistrict"] = SubDistrict;
                aoData["Village"] = Village;

                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblbclistLocationWise").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);


    }
 
     $scope.userRole = $cookies.get("Role");
    if ($scope.userRole == "1") {
        $scope.hidebtnDiv = true;
    }
    else {
        $scope.hidebtnDiv = false;
    }


    $scope.redirectToDashBoard = function () {
        $window.open('http://bcregistrybi.herokuapp.com/public/question/49c9190f-3e84-463c-9e7d-e2bdb499c657');
    };
    $scope.redirectToTopList = function () {
        $window.open('https://bcregistrybi.herokuapp.com/question/3');
    };
});
