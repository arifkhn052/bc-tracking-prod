﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="CorporateBC.aspx.cs" Inherits="BCTrackingWeb.CorporateBC" %>
  
        <div class="container-fluid" ng-init="GetCorporates();getCorporateBc();GetStates()">
            <div class="row page-title-div">
                <div class="col-md-6">
                    <h2 class="title">List Corporate BC</h2>

                </div>

                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="home.aspx"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="CorporateBC.aspx"><span>Corporate BC</span></a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
              <div class="col-md-8 pull-right" style="margin-top:15px" ng-show="hidebtnDiv">
             
                                        <button type="button" style="background-color: #e15349;"  ng-click="redirectToDashBoard()" class="btn btn-primary btn-lg">
                                           Global Corporate Count
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </button>
                  
                                        <button type="button"  ng-click="redirectToTopList()" class="btn btn-primary btn-lg">
                                           Corporate Count with Filter
                                              <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </button>

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">

                    <div class="col-md-12">
                        <div class="panel">

                            <div class="panel-body p-20">

                                <div class="panel-body">

                                    <div class="formmargin">


                                        <%--<div class="col-md-4">
                                              <label for="exampleInputName2">Start Date</label>
              
                                              <input type='text' class="form-control" id="dtStartDate"   placeholder="dd/mm/yyyy" autocomplete="off" />
                                        </div>
                                        <div class="col-md-4">
                                              <label for="exampleInputName2">End Date</label>
              
                                              <input type='text' class="form-control" id="dtEndDate"   placeholder="dd/mm/yyyy" autocomplete="off" />
                                        </div>--%>
                                           <div class="col-md-5">
                                             <strong>Corporate</strong>
                    <select id="selCorporate" class="form-control formcontrolheight" >
                      
                    </select>

                                        </div>
                                    <%--     <div class="col-md-3">
                                        <strong>State</strong>
                                        <select runat="server" class="form-control"
                                            id="selAddressState">
                                        </select>

                                    </div>
                                    <div class="col-md-3">

                                        <strong>District</strong>
                                      
                                            <select class="form-control"
                                                id="selAddressDistrict">
                                            </select>
                                      

                                    </div>

                                    <div class="col-md-3">
                                        <strong>Sub District</strong>
                                        <select class="form-control"
                                            id="selAddresssubDistrict">
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <strong>Villages</strong>
                                     
                                            <select class="form-control"
                                                id="selAddressVillage">
                                            </select>
                                        
                                    </div>
                                      --%>
                                     

                                    </div>
                                  

                                    
                                </div>
                                <div class="panel-body">

                                   <%-- <div class="formmargin">


                                        <div class="col-md-2">
                                         <select class="form-control" id="allBanks">
                <option>Select Bank</option>
            </select>

                                        </div>
                                        <div class="col-md-2">
                                           <select class="form-control" id="bankCircle">
                <option value="0">Select Circle</option>
            </select>

                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" id="bankZone">
                <option value="0">Select Zone</option>
            </select>

                                        </div>

                                        <div class="col-md-2">
                                            <select class="form-control" id="bankRegion">
                <option value="0">Select Region</option>
            </select>

                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" id="bankBranch">
                <option value="0">Select Branch</option>
                <!--
                <option class="dissolv" value="5">Branch1</option>
                <option class="dissolv" value="32">branch2</option>
                    -->
            </select>
                                        </div>
                                          <div class="col-md-2">
                                            <select class="form-control" id="bankCategory">
                <option value="0">Select Category</option>
                <option class="dissolv" value="5">Urban</option>
                <option class="dissolv" value="32">Semi Urban</option>
            </select>
                                        </div>


                                    </div>
                                    <br/>
                                    <br/>--%>


                                    <div class="formmargin">
                                        
                                      
                                      <%--  <div class="col-md-2">
                                           <select class="form-control" id="bankSsa">
                <option value="0">Select SSA</option>
                <option class="dissolv" value="5">SSA1</option>
                <option class="dissolv" value="32">SSA3</option>
            </select>
                                        </div>
                                        <div class="col-md-2">
                                             <select class="form-control" id="bankState">
                <option value="0">Select State</option>
            </select>
                                        </div>--%>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-success" ng-click="getCorporateBc()">Show List</button>
                                        </div>
                                    
                                    </div>
                                </div>


                                <hr/>
                                 <table id="tblCorporateBC" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
               <th>Bc Code</th>
            <th>ID</th>
            <th>Name</th>
            <th>Phone</th>
         
            <th>Certificates</th>
            <th>Villages</th>
            <th>Action</th>
            <th></th>
            <th></th>
            <th></th>

        </tr>
        </thead>
        <tbody></tbody>
    </table>


                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>

                </div>
            </div>

        </section>


    
    
    <%--<script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/report.js" type="text/javascript"></script>
    <script src="js/bc.js" type="text/javascript"></script>
    <script src="js/getbc.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            var t = $('#example').DataTable({});


        });
    </script>--%>

