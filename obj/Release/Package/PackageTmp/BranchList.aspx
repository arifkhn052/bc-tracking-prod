﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="BranchList.aspx.cs" Inherits="BCTrackingWeb.BranchList" %>
     <div class="container-fluid" ng-init="getBrnahList();GetBanks()">
            <div class="row page-title-div">
                <div class="col-md-6">
                    <h2 class="title">List of Branch</h2>

                </div>

                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                       <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                        <li><a ><span>Branch</span></a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">


                    <!-- /.col-md-6 -->

                    <div class="col-md-12">
                        <div class="panel">
                           
                            <div class="panel-body p-20">
                                <div class="panel-body">

                                    <div class="formmargin">

                                        <div class="col-sm-3">
                                                   <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBank"></select>
                                        </div>
                                        <div class="col-sm-3">
                                          <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selState"></select>
                                        </div>
                                          <div class="col-sm-3">
                                        <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selCity"></select>
                                        </div>
                                          <div class="col-sm-3">
                                             <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selDistrict"></select>
                                        </div>
                                  
                                   
                                  

                                    </div>
                                 

                                </div>
                           
                                <div class="panel-body">

                                         <div class="formmargin pull right">
                                                     <div class="col-sm-3">
                                            <button  type="button" ng-click="getBrnahListFilter()" class="btn btn-success">Show List
                                            </button>
                                        </div>
                                          </div>    


                                </div>
                                     <hr />
                                <table id="tblBranchList" class="display" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                         <th>Branch Name </th>
                                        <th>IFSC Code</th>
                                        <th>Contact</th>                                 
                                        <th>Address</th>
                                      
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>


                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-8 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->


    
    <%--<script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/report.js" type="text/javascript"></script>
    <script src="js/getbc.js" type="text/javascript"></script>--%>

