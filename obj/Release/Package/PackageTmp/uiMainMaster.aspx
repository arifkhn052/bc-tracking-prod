﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uiMainMaster.aspx.cs" Inherits="BCTrackingWeb.uiMainMaster" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>BC Track</title>
  <style>
      .avicss{
          margin-top:5px;
      }
      /* Absolute Center Spinner */
      .loading {
          position: fixed;
          z-index: 999;
          height: 2em;
          width: 2em;
          overflow: show;
          margin: auto;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
      }

          /* Transparent Overlay */
          .loading:before {
              content: '';
              display: block;
              position: fixed;
              top: 0;
              left: 0;
              width: 100%;
              height: 100%;
              background-color: rgba(0,0,0,0.8);
          }

          /* :not(:required) hides these rules from IE9 and below */
          .loading:not(:required) {
              /* hide "loading..." text */
              font: 0/0 a;
              color: transparent;
              text-shadow: none;
              background-color: transparent;
              border: 0;
          }

              .loading:not(:required):after {
                  content: '';
                  display: block;
                  font-size: 10px;
                  width: 1em;
                  height: 1em;
                  margin-top: -0.5em;
                  /*-webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;*/
              }

      .sk-cube-grid {
          width: 40px;
          height: 40px;
          margin: 100px auto;
      }

          .sk-cube-grid .sk-cube {
              width: 33%;
              height: 33%;
              background-color: #00a8ff;
              float: left;
              -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
              animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
          }

          .sk-cube-grid .sk-cube1 {
              -webkit-animation-delay: 0.2s;
              animation-delay: 0.2s;
          }

          .sk-cube-grid .sk-cube2 {
              -webkit-animation-delay: 0.3s;
              animation-delay: 0.3s;
          }

          .sk-cube-grid .sk-cube3 {
              -webkit-animation-delay: 0.4s;
              animation-delay: 0.4s;
          }

          .sk-cube-grid .sk-cube4 {
              -webkit-animation-delay: 0.1s;
              animation-delay: 0.1s;
          }

          .sk-cube-grid .sk-cube5 {
              -webkit-animation-delay: 0.2s;
              animation-delay: 0.2s;
          }

          .sk-cube-grid .sk-cube6 {
              -webkit-animation-delay: 0.3s;
              animation-delay: 0.3s;
          }

          .sk-cube-grid .sk-cube7 {
              -webkit-animation-delay: 0s;
              animation-delay: 0s;
          }

          .sk-cube-grid .sk-cube8 {
              -webkit-animation-delay: 0.1s;
              animation-delay: 0.1s;
          }

          .sk-cube-grid .sk-cube9 {
              -webkit-animation-delay: 0.2s;
              animation-delay: 0.2s;
          }

  </style>
</head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BC Track</title>

<!-- ========== COMMON STYLES ========== -->
<link rel="stylesheet" href="ot/css/bootstrap.min.css" media="screen">
<link rel="stylesheet" href="ot/css/font-awesome.min.css" media="screen">
<link rel="stylesheet" href="ot/css/animate-css/animate.min.css" media="screen">
<link rel="stylesheet" href="ot/css/lobipanel/lobipanel.min.css" media="screen">

<!-- ========== PAGE STYLES ========== -->
<link rel="stylesheet" href="ot/css/prism/prism.css" media="screen"> <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
<link rel="stylesheet" href="ot/css/toastr/toastr.min.css" media="screen">
<link rel="stylesheet" href="ot/css/icheck/skins/line/blue.css">
<link rel="stylesheet" href="ot/css/icheck/skins/line/red.css">
<link rel="stylesheet" href="ot/css/icheck/skins/line/green.css">
<link rel="stylesheet" href="ot/css/bootstrap-tour/bootstrap-tour.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!--<link rel="stylesheet" href="/resources/demos/style.css">-->

<!-- ========== THEME CSS ========== -->
<link rel="stylesheet" href="ot/css/main.css" media="screen">

<!-- ========== MODERNIZR ========== -->
<script src="ot/js/modernizr/modernizr.min.js"></script>
<script src="public/angular/angular.min.js"></script>
<script src="js/ui-bootstrap-tpls-0.14.3.js"></script>
<!--<script src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.14.3.js"></script>-->
<!--<script src="https://mbenford.github.io/ngTagsInput/js/ng-tags-input.min.js"></script>-->
<script src="js/ng-tags-input.min.js"></script>
<!--<link rel="stylesheet" href="https://mbenford.github.io/ngTagsInput/css/ng-tags-input.min.css" />-->
<link href="css/ng-tags-input.min.css" rel="stylesheet" />
<script src="public/angular/angular-cookies.min.js"></script>
<script src="public/angular/angular-ui-router.min.js"></script>
<script src="Angularjs/mainApp.js"></script>
<script src="Angularjs/config.js"></script>
<script src="Angularjs/MainModule.js"></script>

  <!--controllers-->
<script src="Service/authenticationService.js"></script>
<script src="Service/PasswordService.js"></script>
<script src="controller/loginController.js"></script>
<script src="Angularjs/addBankController.js"></script>
<script src="controller/uploadController.js"></script>
<script src="js/stateController.js"></script>
<script src="controller/singleController.js"></script>
<script src="controller/bcListBankWise.js"></script>
<script src="controller/bcListStatus.js"></script>
<script src="controller/listAsOn.js"></script>
<script src="controller/corporateController.js"></script>
<script src="controller/userController.js"></script>
<script src="controller/bankController.js"></script>
<script src="controller/notificationController.js"></script>
<script src="controller/singleEntryContro.js"></script>
<script src="controller/getbcController.js"></script>
<script src="controller/addProspectiveBCRegistryController.js"></script>
<script src="controller/viewProspectiveRegistryEntryController.js"></script>
<script src="controller/getAllProspectiveBCRegistryList.js"></script>
<script src="controller/report.js"></script>
<script src="controller/summaryChartController.js"></script>
<script src="controller/reportListController.js"></script>
<script src="controller/BranchesLastUpdatedController.js"></script>
<script src="controller/AllocationBasedListController.js"></script>
<script src="controller/stateListController.js"></script>
<script src="controller/editStateController.js"></script>
<script src="controller/addBankController.js"></script>
<script src="controller/addCorporate.js"></script>
<script src="controller/getAllBcCorresponds.js"></script>
<script src="controller/addSingleEntryController.js"></script>
<script src="controller/getbcProductController.js"></script>
<script src="controller/allocationBasedContro.js"></script>
<script src="controller/areaWiseAllocationController.js"></script>
<script src="controller/mainController.js"></script>
<script src="controller/getUnallocatedAll.js"></script>
<script src="controller/getBcByStatus.js"></script>
<script src="controller/addProductController.js"></script>
<script src="controller/bulkUploadController.js"></script>
<script src="controller/logoutController.js"></script>
<script src="controller/findBcController.js"></script>
<script src="controller/viewSingleEntryController.js"></script>
<script src="controller/bclistLocationWiseContro.js"></script>
<script src="controller/bcListAddressWiseContro.js"></script>
<script src="controller/HierarchyController.js"></script>
<script src="controller/BranchListContro.js"></script>
<script src="controller/addStateController.js"></script>
<script src="controller/addBankCityController.js"></script>
<script src="controller/addBankDistrictController.js"></script>
<script src="Angularjs/homeController.js"></script>
<script src="controller/changePasswordController.js"></script>
<script src="controller/searchvillageController.js"></script>
<script src="controller/groupProductController.js"></script>
<script src="controller/getBcMultiproduct.js"></script>
<script src="controller/redierctToHomePageController.js"></script>
<script src="controller/searchlogController.js"></script>
<script src="controller/mobilevsfixedBcController.js"></script>
    
<script src="js/jquery-1.12.2.min.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/metro.min.js"></script>
<!--<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>-->
<script type="text/javascript" src="js/parsley.min.js" defer></script>
<!--<script src="js/jquery.dataTables.min.js"></script>-->


<!-- date time picker -->
<script src="Scripts/Common.js"></script>
<script src="Scripts/jquery.timepicker.js"></script>
<script src="Scripts/jquery.timepicker.min.js"></script>


<script src="Scripts/jquery/jquery.dataTables.js"></script>
<script src="Scripts/jquery/fnSetFilteringDelay.js"></script>
<script src="Scripts/jquery/dataTables.tableTools.js"></script>
<link href="Scripts/css/dataTables.tableTools.css" rel="stylesheet" />
<!--<link href="Scripts/css/dataTables.jqueryui.css" rel="stylesheet" />-->
<script src="Scripts/jquery.md5.js"></script>
<!--<script src="js/main.js"></script>-->

<link href="css/jquery.dataTables.min.css" rel="stylesheet">
<body >
    <div class="top-navbar-fixed" ng-app="bctrackApp" ng-controller="mainController"  ng-cloak>
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
            <!-- ========== TOP NAVBAR ========== -->
            <nav class="navbar top-navbar bg-white box-shadow" ng-show="showHeader">
                <div class="container-fluid">
                    <div class="row">
                        <div class="navbar-header no-padding">
                            <a class="navbar-brand" href="#" ui-sref="home">
                                <img src="ot/images/transparent_text_effect.png" style="margin-left: 20px;" alt="Options - Admin Template" class="logo">
                            </a>
                            <span class="small-nav-handle hidden-sm hidden-xs"><i class="fa fa-outdent"></i></span>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <i class="fa fa-ellipsis-v"></i>
                            </button>
                            <button type="button" class="navbar-toggle mobile-nav-toggle">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>
                        <!-- /.navbar-header -->

                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            <ul class="nav navbar-nav" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                <!--<li class="hidden-sm hidden-xs">
                                    <a href="#" class="user-info-handle">
                                        <i class="fa fa-user"></i>
                                    </a>
                                </li>-->
                                <li class="hidden-sm hidden-xs">
                                    <a href="#" class="full-screen-handle">
                                        <i class="fa fa-arrows-alt"></i>
                                    </a>
                                </li>

                            </ul>
                            <!-- /.nav navbar-nav -->
                            <a href="Helpers/userManualup.docx" class="btn btn-success" role="button" style="margin-top:6px"><i class="fa fa-download" aria-hidden="true"></i>Download Manual</a>

                            <ul class="nav navbar-nav navbar-right" data-dropdown-in="fadeIn"
                                data-dropdown-out="fadeOut">
                                <!-- /.dropdown -->
                                <!--<li><a href="Helpers/userManualup.docx" class="btn btn-success btn-sm" role="button" style="margin-top:6px;padding-top:10px"><i class="fa fa-download" aria-hidden="true"></i>App Document</a></li>-->
                                <li class="dropdown tour-two">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false">{{username}}<span class="caret"></span></a>

                                </li>
                                <li><a ui-sref="logout"> <img style=" margin-top: -5px; margin-left: 0px;" src="ot/images/power-btn.png" id="btnUserName" alt="Logout" class="img-responsive"> </a></li>
                                <!-- /.dropdown -->

                            </ul>
                            <!-- /.nav navbar-nav navbar-right -->
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </nav>

            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                    <div class="left-sidebar fixed-sidebar bg-black-300 box-shadow tour-three" ng-show="showNavigation">
                        <div class="sidebar-content">
                            <div class="user-info closed">
                                <img src="#"
                                     class="img-circle profile-img">
                                <h6 class="title"></h6>
                                <small class="info"></small>
                            </div>
                            <!-- /.user-info -->

                            <div class="sidebar-nav">
                                <ul class="side-nav color-gray">
                                    <li class="nav-header" style="opacity:1">
                                        <span class=""></span>
                                        BC Record
                                    </li>
                                    <li style="padding-left:25px" class="has-children" ng-repeat="menulist in navMenuOption" ng-hide="menulist.hidden">
                                        <a href="#" ng-click="navigateToRootMenu(menulist,'top')">
                                            <i class="{{menulist.icon}}"></i> <span>{{menulist.name}}</span> <i class="fa fa-angle-right arrow"></i>
                                        </a>
                                        <ul class="child-nav">
                                            <li ng-repeat="submenulist in menulist.subMenu" ng-hide="submenulist.hidden">
                                                <a ui-sref="{{submenulist.state}}" ng-click="navigateToRootMenu(submenulist,'lower')">
                                                    <i class="{{submenulist.icon}}"></i>
                                                    <span>{{submenulist.name}}</span>
                                                </a>
                                            </li>


                                        </ul>
                                    </li>

                                    <li class="nav-header" style="opacity:1">
                                        <span class=""></span>
                                       Reports
                                    </li>
                                    <!--adasdasdas-->
                                  
                                    <li style="padding-left:25px" class="has-children" ng-repeat="menulist in navMenuOption2" ng-hide="menulist.hidden">
                                        <a href="#" ng-click="navigateToRootMenu(menulist,'top')">
                                            <i class="{{menulist.icon}}"></i> <span>{{menulist.name}}</span> <i class="fa fa-angle-right arrow"></i>
                                        </a>
                                        <ul class="child-nav">
                                            <li ng-repeat="submenulist in menulist.subMenu" ng-hide="submenulist.hidden">
                                                <a ui-sref="{{submenulist.state}}" ng-click="navigateToRootMenu(submenulist,'lower')">
                                                    <i class="{{submenulist.icon}}"></i>
                                                    <span>{{submenulist.name}}</span>
                                                </a>
                                            </li>


                                        </ul>
                                    </li>
                                
                                    <li style="padding-left:25px" class="has-children" ng-show="hideBIResources">
                                        <a href="#" ng-click="redirectArrowaiBi()">
                                            <i class="fa fa-external-link"></i> <span>More Reports</span> <i class="fa fa-angle-right arrow"></i>
                                        </a>

                                    </li>
                                    <li class="nav-header" style="opacity:1">
                                        <span class=""></span>
                                        Administration
                                    </li>
                                   <!--adasdasdas-->
                                    <li style="padding-left:25px" class="has-children" ng-repeat="menulist in navMenuOption1" ng-hide="menulist.hidden">
                                        <a href="#" ng-click="navigateToRootMenu(menulist,'top')">
                                            <i class="{{menulist.icon}}"></i> <span>{{menulist.name}}</span> <i class="fa fa-angle-right arrow"></i>
                                        </a>
                                        <ul class="child-nav">
                                            <li ng-repeat="submenulist in menulist.subMenu" ng-hide="submenulist.hidden">
                                                <a ui-sref="{{submenulist.state}}" ng-click="navigateToRootMenu(submenulist,'lower')">
                                                    <i class="{{submenulist.icon}}"></i>
                                                    <span>{{submenulist.name}}</span>
                                                </a>
                                            </li>


                                        </ul>
                                    </li>
                                    <li class="nav-header" style="opacity:1">
                                        <span class=""></span>
                                        Others
                                    </li>
                                    <!--adasdasdas-->
                                    <li style="padding-left:25px" class="has-children" ng-repeat="menulist in navMenuOption3" ng-hide="menulist.hidden">
                                        <a href="#" ng-click="navigateToRootMenu(menulist,'top')">
                                            <i class="{{menulist.icon}}"></i> <span>{{menulist.name}}</span> <i class="fa fa-angle-right arrow"></i>
                                        </a>
                                        <ul class="child-nav">
                                            <li ng-repeat="submenulist in menulist.subMenu" ng-hide="submenulist.hidden">
                                                <a ui-sref="{{submenulist.state}}" ng-click="navigateToRootMenu(submenulist,'lower')">
                                                    <i class="{{submenulist.icon}}"></i>
                                                    <span>{{submenulist.name}}</span>
                                                </a>
                                            </li>


                                        </ul>
                                    </li>
                               
                                    <!--<li class="has-children">
                                        <a href="#">
                                            <i class="fa fa-university"></i> <span>Business Correspondents</span> <i class="fa fa-angle-right arrow"></i>
                                        </a>
                                        <ul class="child-nav">
                                            <li>
                                                <a href="#" ui-sref="bulkUpload">
                                                    <i class="fa fa-upload"></i>
                                                    <span>Bulk Upload</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="singleEntry">
                                                    <i class="fa fa-list"></i>
                                                    <span>Single Entry</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>
                                    <li class="has-children">
                                        <a href="#">
                                            <i class="fa fa-list"></i> <span>Business Correspondents</span> <i class="fa fa-angle-right arrow"></i>
                                        </a>
                                        <ul class="child-nav">
                                            <li>
                                                <a href="#" ui-sref="all"><i class="fa fa-bank"></i> <span>All</span></a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="byBranches"><i class="fa fa-leaf"></i> <span>By Branches</span></a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="bclistLocationWise"><i class="fa fa-map-marker"></i> <span>By BC Address</span></a>
                                            </li>

                                            <li>
                                                <a href="#" ui-sref="bclistUnallocated"><i class="fa fa-times"></i> <span>Unallocated</span></a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="bclistStatus">
                                                    <i class="fa fa-info"></i>
                                                    <span>By Status</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="has-children">
                                        <a href="#">
                                            <i class="fa fa-map-signs"></i> <span>Summary Report</span> <i class="fa fa-angle-right arrow"></i>
                                        </a>
                                        <ul class="child-nav">
                                            <li>
                                                <a href="#" ui-sref="reportbetween"><i class="fa fa-line-chart"></i> <span>Allotment between dates</span></a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="summaryUnallocated"><i class="fa fa-line-chart"></i> <span>Former BC</span></a>
                                            </li>
                                            <li>
                                                <a ui-sref="blacklistedBcGraph"><i class="fa fa-line-chart"></i> <span>Black listed BC</span></a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="activeBcGraph"><i class="fa fa-line-chart"></i> <span>Active/Inactive BC</span></a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="areaWiseAllocationBetweenDate"><i class="fa fa-line-chart"></i> <span>Area wise BC Between Dates</span></a>
                                            </li>

                                        </ul>
                                    </li>


                                    <li class="has-children">
                                        <a href="#">
                                            <i class="fa fa-file-text"></i>Report List <span></span> <i class="fa fa-angle-right arrow"></i>
                                        </a>
                                        <ul class="child-nav">
                                            <li>
                                                <a href="#" ui-sref="listason"><i class="fa fa-calendar"></i> <span>List as on date</span></a>
                                            </li>
                                            <li>
                                                <a ui-sref="CorporateBC"><i class="fa fa-plane"></i> <span>Corporate BC's</span></a>
                                            </li>

                                            <li>
                                                <a href="#" ui-sref="BranchesLastUpdated"><i class="fa fa-calendar-plus-o"></i> <span>Branches Last Updated</span></a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="AllocationBasedList"><i class="fa fa-road"></i> <span>Allocation Based List</span></a>
                                            </li>

                                            <li>
                                                <a href="#" ui-sref="areaWiseAllocation"><i class="fa fa-map-marker"></i> <span>Area wise BC allocation</span></a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="BcPerProduct"><i class="fa fa-product-hunt"></i> <span>BC per products</span></a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="BlacklistedBCs"><i class="fa fa-ban"></i> <span>BlackListed BC</span></a>
                                            </li>

                                        </ul>
                                    </li>

                                    <li class="has-children">
                                        <a href="#">
                                            <i class="fa fa-building-o"></i>Corporate <span></span> <i class="fa fa-angle-right arrow"></i>
                                        </a>
                                        <ul class="child-nav">
                                            <li>
                                                <a href="#" ui-sref="corporateList"><i class="fa fa-list-alt"></i> <span>Corporate List</span></a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="addCorporate"><i class="fa fa-plus"></i> <span>Add Corporate</span></a>
                                            </li>


                                        </ul>
                                    </li>

                                    <li class="has-children">
                                        <a href="#">
                                            <i class="fa fa-user"></i>Users<span></span> <i class="fa fa-angle-right arrow"></i>
                                        </a>
                                        <ul class="child-nav">
                                            <li>
                                                <a href="#" ui-sref="userList">
                                                    <i class="fa fa-list-alt"></i>
                                                    <span>Users List</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="addUser"><i class="fa fa-plus"></i> <span>Add Users</span></a>
                                            </li>


                                        </ul>
                                    </li>
                                    <li class="has-children">
                                        <a href="#">
                                            <i class="fa fa-university"></i>Bank<span></span> <i class="fa fa-angle-right arrow"></i>
                                        </a>
                                        <ul class="child-nav">
                                            <li>
                                                <a href="#" ui-sref="bankList">
                                                    <i class="fa fa-list-alt"></i>
                                                    <span>Bank List</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="addBank"><i class="fa fa-plus"></i> <span>Add Bank</span></a>
                                            </li>


                                        </ul>
                                    </li>
                                    <li class="has-children">
                                        <a href="#">
                                            <i class="fa fa-bars"></i>State<span></span> <i class="fa fa-angle-right arrow"></i>
                                        </a>
                                        <ul class="child-nav">
                                            <li>
                                                <a href="#" ui-sref="statelist">
                                                    <i class="fa fa-list-alt"></i>
                                                    <span>State List</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" ui-sref="addstate">
                                                    <i class="fa fa-plus"></i>
                                                    <span>Add State</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>

                                    <li>
                                        <a ui-sref="notifications">
                                            <i class="fa fa-bell"></i>
                                            <span>Notification</span>
                                        </a>
                                    </li>-->
                                </ul>
                                <!-- /.side-nav -->

                            </div>
                            <!-- /.sidebar-nav -->
                        </div>
                        <!-- /.sidebar-content -->
                    </div>
                    <!-- /.left-sidebar -->
                   
                    <div ng-show="loaderClass">
                        <div class="loading">
                            <!-- Loading&#8230; --> <img src="/public/images/ripple.svg" style="margin: 0px auto; position: fixed; left: 40%; right: 40%; top: 35%;bottom: 40%; height: 150px; " alt="">
                        </div>
                    </div>
                    <div data-ui-view="">


                    </div>

                    <!-- /.main-page -->
                    <!-- /.right-sidebar -->

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
            <div class="modal fade" id="helloModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Message</h4>
                        </div>
                        <div class="modal-body" id="helloModalMessage">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="closemodalbutton">Close</button>
                            <a id="redirectbutton" role="button" class="btn btn-primary">ok</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.main-wrapper -->
        <!-- ========== COMMON JS FILES ========== -->
        <!--<script src="ot/js/jquery/jquery-2.2.4.min.js"></script>-->

        <script src="ot/js/jquery-ui/jquery-ui.min.js"></script>
        <script src="ot/js/bootstrap/bootstrap.min.js"></script>
        <script src="ot/js/pace/pace.min.js"></script>
        <script src="ot/js/lobipanel/lobipanel.min.js"></script>
        <script src="ot/js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="ot/js/prism/prism.js"></script>
        <script src="ot/js/waypoint/waypoints.min.js"></script>
        <script src="ot/js/counterUp/jquery.counterup.min.js"></script>
        <script src="ot/js/amcharts/amcharts.js"></script>
        <script src="ot/js/amcharts/serial.js"></script>
        <script src="ot/js/amcharts/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="ot/js/amcharts/plugins/export/export.css" type="text/css" media="all" />
        <script src="ot/js/amcharts/themes/light.js"></script>
        <script src="ot/js/toastr/toastr.min.js"></script>
        <script src="ot/js/icheck/icheck.min.js"></script>
        <script src="ot/js/bootstrap-tour/bootstrap-tour.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="ot/js/main.js"></script>
        <script src="ot/js/production-chart.js"></script>
        <script src="ot/js/traffic-chart.js"></script>
        <script src="ot/js/task-list.js"></script>


    </div>
</body>

</html>
