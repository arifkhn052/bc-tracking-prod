﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="allocate.aspx.cs" Inherits="BCTrackingWeb.allocate" %>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/metro.min.css">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.min.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
       <br /><br /><br /><br /><br />

    <div class="container page-content">
        
        <div class="loader"></div>

        <div>
            <ul class="breadcrumbs mini">
              <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                <li><a href="BCListHome.aspx"><span>Business  Correspondent Lists</span></a></li>
                <li><a href="bclistUnallocated.html"><span>Unnallocated Business  Correspondant</span></a></li>
                <li><a href="Allocate.html"><span>Allocate</span></a></li>
            </ul>
        </div>
        <br>

        <div class="example">

			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Personal Details</a></li>
				<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Associate Bank Detail</a></li>
				<li role="presentation" class="active"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Allotted Area details</a></li>
				<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Certification Details</a></li>
				<li role="presentation"><a href="#others" aria-controls="settings" role="tab" data-toggle="tab">Other Action</a></li>
			</ul>

			<form class="form-horizontal">
			<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane" id="home">
						<br>
						<div class="form-group">
		                    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
		                    <div class="col-sm-10">
		                        <input type="text" class="form-control" id="inputEmail3" placeholder="Name" value="Bc 1">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputPassword3" class="col-sm-2 control-label">Phone Number</label>
		                    <div class="col-sm-10">
		                        <input type="text" class="form-control" id="inputPassword3" placeholder="Phone Number" value="1233123112">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail4" class="col-sm-2 control-label">Email</label>
		                    <div class="col-sm-10">
		                        <input type="email" class="form-control" id="inputEmail4" placeholder="Email" value="a@b.co">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail7" class="col-sm-2 control-label">Date Of Birth</label>
		                    <div class="col-sm-10">
		                        <input type="date" class="form-control" id="inputEmail7" placeholder="Date of Birth" value="1992-03-26">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail5" class="col-sm-2 control-label">Aadhar Card</label>
		                    <div class="col-sm-10">
		                        <input type="text" class="form-control" id="inputEmail5" placeholder="Aadhar Card Number" value="AA1231A21">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Pan Card</label>
		                    <div class="col-sm-10">
		                        <input type="text" class="form-control" id="inputEmail6" placeholder="Pan Card Number" value="SASDWQ123A">
		                    </div>
		                </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="profile">
						<br>
						<div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">IFSC Code</label>
		                    <div class="col-sm-10">
		                        <input type="text" class="form-control" id="inputEmail6" placeholder="IFSC Code" value="SBIN4421232" disabled>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Bank Name</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option>State Bank of India</option>
		                            <option>Bank of Baroda</option>
		                            <option>Axis Bank</option>
		                            <option>Union Bank of India</option>
		                        </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Circle</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option class="dissolv" value="1">Navi Mumbai</option>
		                            <option class="dissolv" value="6">Thane</option>
		                            <option class="dissolv" value="9">Mumbai</option>
		                            <option class="dissolv" value="13">Pune</option>
		                            <option class="dissolv" value="16">Nanded</option>
		                            <option class="dissolv" value="18">Nashik</option>
		                            <option class="dissolv" value="21">Sindhudurg</option>
		                            <option class="dissolv" value="23">UDUPI</option>
		                            <option class="dissolv" value="26">Raigad</option>
		                            <option class="dissolv" value="28">Ahmednagar</option>
		                            <option class="dissolv" value="30">Aurangabad</option>
		                            <option class="dissolv" value="33">Ahmedabad</option>
		                            <option class="dissolv" value="3242">Varanasi</option>
		                        </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">State</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option class="dissolv" value="2">Maharashtra</option>
		                        </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Zone</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option class="dissolv" value="3">NMZ</option>
		                        </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Region</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option class="dissolv" value="4">Navi Mumbai</option>
		                        </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Category</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option class="dissolv" value="5">Urban</option>
		                            <option class="dissolv" value="32">Semi Urban</option>
		                        </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Branch</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option class="dissolv" value="5">Branch1</option>
		                            <option class="dissolv" value="32">branch2</option>
		                        </select>
		                    </div>
		                </div>
					</div>
					<div role="tabpanel" class="tab-pane active" id="messages">
						<br>
						<div class="panel panel-default">
		                    <div class="panel-body">
		                        <div class="form-group">
		                            <label for="inputEmail6" class="col-sm-2 control-label">Village Code</label>
		                            <div class="col-sm-10">
		                                <input type="text" class="form-control" id="villageCode" placeholder="Enter Village Code">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label for="inputEmail6" class="col-sm-2 control-label">Village Detail</label>
		                            <div class="col-sm-10">
		                                <textarea name="villageDetail" id="villageDetail" class="form-control" rows="4" disabled placeholder="Village Detail"></textarea>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <div class="col-sm-3 col-sm-offset-9">
		                                <input type="button" class="pull-right btn btn-danger" id="addMoreButton" value="Add More">
		                            </div>
		                        </div>
		                    </div>
		                </div>

		                <div class="form-group">
		                    <table id="example" class="display" cellspacing="0" width="100%">
		                        <thead>
		                            <tr>
		                                <th>Village Code</th>
		                                <th>Village Detail</th>
		                                <th>Action</th>
		                            </tr>
		                        </thead>
		                    </table>
		                </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="settings">
						<br>
						<div class="panel panel-default">
		                    <div class="panel-body">
		                        <div class="form-group">
		                            <label for="inputEmail6" class="col-sm-2 control-label">Certification Date</label>
		                            <div class="col-sm-10">
		                                <input type="date" class="form-control" id="certificateDate" placeholder="Enter Certification Date">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label for="inputEmail6" class="col-sm-2 control-label">Certification Authority</label>
		                            <div class="col-sm-10">
		                                <select class="form-control" id="certificateAuth">
		                                    <option class="dissolv" value="Authority 1">Authority 1</option>
		                                    <option class="dissolv" value="Authority 2">Authority 2</option>
		                                    <option class="dissolv" value="Authority 3">Authority 3</option>
		                                </select>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <div class="col-sm-3 col-sm-offset-9">
		                                <input type="button" class="pull-right btn btn-danger" id="addMoreButton2" value="Add More">
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <table id="tblBCList" class="display" cellspacing="0" width="100%">
		                        <thead>
		                            <tr>
		                                <th>Certification Date</th>
		                                <th>Certification Authority</th>
		                                <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                           
		                        </tbody>
		                    </table>
		                </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="others">
						<br>
						<div class="form-group">
		                    <label for="inputEmail6" class="col-sm-4 control-label text-left">Black List BC</label>
		                    <div class="col-sm-8">
		                        <input type="button" class="form-control btn btn-danger" id="inputEmail6" value="BlackList Business Correspondent">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-4 control-label text-left">Move to Unallotted</label>
		                    <div class="col-sm-8">
		                        <input type="button" class="form-control btn btn-danger" id="inputEmail6" value="Move to unallotted">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-4 control-label text-left">Transfer to different Branch</label>
		                    <div class="col-sm-8">
		                        <input type="button" class="form-control btn btn-danger" id="inputEmail6" value="Chose New Branch">
		                    </div>
		                </div>
					</div>
				</div>
			</form>

		</div>
        
        


        <br>

    </div>

    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>

    <script src="js/main.js" type="text/javascript"></script>

    <script src="js/getbc.js" type="text/javascript"></script>




    <script>
        $(document).ready(function () {
            var t = $('#example').DataTable({
                "paging": false,
                "ordering": false,
                "info": false,
                "searching": false
            });

            var t2 = $('#example2').DataTable({
                "paging": false,
                "ordering": false,
                "info": false,
                "searching": false
            });

            $("#villageCode").on('blur', function () {
                $("#villageDetail").text('village 1, district 1, state');
            }).on('focus', function () {
                $("#villageDetail").text('');

            });

            $('#addMoreButton2').on('click', function () {
                t2.row.add([
                    $("#certificateDate").val(),
                    $("#certificateAuth").val(),
                    'delete'
                ]).draw(false);
                $("#certificateDate").val("");

            });
            $('#addMoreButton').on('click', function () {
                t.row.add([
                    $("#villageCode").val(),
                    $("#villageDetail").text(),
                    'delete'
                ]).draw(false);
                $("#villageCode").val("");
                $("#villageDetail").text("");
            });
        });
    </script>

