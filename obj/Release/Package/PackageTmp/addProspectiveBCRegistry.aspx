﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="addProspectiveBCRegistry.aspx.cs" Inherits="BCTrackingWeb.addProspectiveBCRegistry" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>BC Tracking System</title>       
       <script type="text/javascript">
         var specialKeys = new Array();
         specialKeys.push(8); //Backspace
         function IsNumeric(e) {
             var keyCode = e.which ? e.which : e.keyCode
             var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
             document.getElementById("error").style.display = ret ? "none" : "inline";
             return ret;
         }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div  ng-init="GetStates();datetimeBind();GetBanks()">
        <div class="container-fluid" >
            <div class="row page-title-div">
                <div class="col-md-12">
                    <h2 class="title">Add Prospective BC</h2>

                </div>


                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="home.aspx"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="javascript:void(0)">Add Prospective BC</a></li>


                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">

                                </div>
                            </div>
                            <div class="panel-body">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a ng-click="displayTab('home')"  aria-controls="home"
                                                                              role="tab" data-toggle="tab"><label
                                            for="exampleInputEmail1"> Basic Information</label> </a></li>
                                    <li role="presentation"><a ng-click="displayTab('circle')"" aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Qualification</label> </a></li>
                                   

                                </ul>

                                <!-- Tab panes -->
                              </div>   
                                    <div role="tabpanel" class="tab-pane active" ng-hide="divhome">


                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="panel">                                                    
                                                        <div class="panel-body p-20">
                                                            <div class="panel-body">
                                                               
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class=" control-label">First Name<a style="color:red">*</a></label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtFName"
                                                                             required    placeholder="Name"
                                                                               parsley-trigger="change" />
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class=" control-label">Last Name<a style="color:red">*</a></label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtLName"
                                                                             required    placeholder="Name"
                                                                               parsley-trigger="change" />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class="control-label">Upload
                                                                            Image</label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input type="file" id="fileImage" runat="server"
                                                                               class="uploaderCss"
                                                                               accept='.png,.jpg,.gif'>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class="control-label">Gender<a style="color:red">*</a></label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                        <select runat="server" class="form-control"
                                                                                  id="selGender">
                                                                            <option value="">--Select-- </option>
                                                                            <option value="Male">Male</option>
                                                                            <option value="Female">Female</option>
                                                                            <option value="Other">Other
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class="control-label">Date
                                                                            Of Birth<a style="color:red">*</a></label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                  <%--      <input runat="server" type="date" required
                                                                               class="form-control" id="txtDOB"
                                                                                 placeholder="Date of Birth">--%>
                                                                          <input type='text' class="form-control" id="txtDOB"  placeholder="dd/mm/yyyy"  />
                                                                    </div>
                                                                </div>


                                                                <div class="row">
                                                                    <label for="txtFather"
                                                                           class="col-sm-3 control-label">Father / Mother
                                                                        Name<a style="color:red">*</a></label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtFather"
                                                                               placeholder="Name" />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtSpouse"
                                                                           class="col-sm-3 control-label">Spouse
                                                                        Name</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtSpouse"
                                                                               placeholder="Name" />
                                                                    </div>
                                                                </div>

                                                              <%--  <div class="row">
                                                                    <label for="selCategory"
                                                                           class="col-sm-3 control-label">Category</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <select runat="server" class="form-control"
                                                                                id="selCategory">
                                                                            <option value="General">General</option>
                                                                            <option value="OBC">OBC</option>
                                                                            <option value="SC">SC</option>
                                                                            <option value="ST">ST</option>
                                                                        </select>
                                                                    </div>

                                                                </div>--%>

                                                                <%--   <div class="row">
                                                                    <label for="txtGender"
                                                                           class="col-sm-3 control-label">Physically
                                                                        Handicapped</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input type="radio" id="radHandicapNo"
                                                                               name="Handicap" value="No" checked/>&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;&nbsp;

                                                                        <input type="radio" id="radHandicapYes"
                                                                               name="Handicap" value="Yes"/>&nbsp;&nbsp;Yes

                                                                    </div>

                                                                </div>--%>
                                                        <div class="row">
                                                                    <label for="txtPhone1"
                                                                           class="col-sm-3 control-label">Residential address</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <textarea runat="server" type="text" required maxlength="10"
                                                                               class="form-control" id="Text6"
                                                                               placeholder="Residential address" />
                                                                    </div>
                                                                </div>

                                                        <div class="row">
                                                                    <label for="txtPhone1"
                                                                           class="col-sm-3 control-label">Pin Code
                                                                        <a style="color:red">*</a></label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text" required maxlength="10"
                                                                               class="form-control" id="pincode" ng-keypress="filterValue($event)"
                                                                               placeholder="Pin Code" />
                                                                    </div>
                                                                </div>
                                                        
                                                                <div class="row">
                                                                    <label for="txtPhone1"
                                                                           class="col-sm-3 control-label">Mobile
                                                                        Number<a style="color:red">*</a></label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text" required maxlength="10"
                                                                               class="form-control" id="txtPhone1" ng-keypress="filterValue($event)"
                                                                               placeholder="Mobile Number" />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtPhone1"
                                                                           class="col-sm-3 control-label">Mobile
                                                                        Number 2</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text" maxlength="10"
                                                                               class="form-control" id="txtPhone2" ng-keypress="filterValue($event)"
                                                                               placeholder="Mobile Number 2" />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtPhone1"
                                                                           class="col-sm-3 control-label">Mobile
                                                                        Number 3</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text" maxlength="10"
                                                                               class="form-control" id="txtPhone3" ng-keypress="filterValue($event)"
                                                                               placeholder="Mobile Number 3" />
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <label for="txtPhone1"
                                                                           class="col-sm-3 control-label">Reference Contact
                                                                        Number</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text" maxlength="10"
                                                                               class="form-control" id="txtReferencePhone" ng-keypress="filterValue($event)"
                                                                               placeholder="Reference Contact Number" />
                                                                    </div>
                                                                </div>
                                                                <%--   <div class="row">
                                                                    <label for="txtPhone1"
                                                                           class="col-sm-3 control-label">Contact Person</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text" 
                                                                               class="form-control" id="txtContactPerson" 
                                                                               placeholder="Contact Person">
                                                                    </div>
                                                                </div>--%>
                                                                <%--   <div class="row">
                                                                    <label for="txtPhone1"
                                                                           class="col-sm-3 control-label">Contact Designation</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text" 
                                                                               class="form-control" id="txtContactDesignation" 
                                                                               placeholder="Contact Designation">
                                                                    </div>
                                                                </div>--%>
                                                                <%--   <div class="row">
                                                                    <label for="txtPhone1"
                                                                           class="col-sm-3 control-label">No of Complaint</label>
                                                                    <div class="col-sm-9 form-group">
                                                                          <input runat="server" type="text" maxlength="10"
                                                                               class="form-control" id="txtNoofComplaint" ng-keypress="filterValue($event)"
                                                                               placeholder="No of Complaint">
                                                                   
                                                                    </div>
                                                                </div>--%>

                                                                <div class="row">
                                                                    <label for="txtEmail"
                                                                           class="col-sm-3 control-label">Email</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="email"
                                                                               class="form-control" id="txtEmail" required
                                                                               placeholder="Email">
                                                                    </div>
                                                                </div>

                                                                

                                                                <div class="row">
                                                                    <label for="txtPanCard"
                                                                           class="col-sm-3 control-label">Pan
                                                                        Card</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text" maxlength="10"
                                                                               class="form-control" id="txtPanCard"
                                                                               placeholder="Pan Card Number" />
                                                                    </div>
                                                                </div>
                       
                                                                <div class="row">
                                                                    <label for="txtAadharCard1"
                                                                           class="col-sm-3 control-label">Aadhar
                                                                        Card</label>
                                                                    <div class="col-sm-9 form-group">
                                                                          <input runat="server" type="text"
                                                                               class="form-control" id="txtAadharCard"  maxlength="10" ng-keypress="filterValue($event)"
                                                                               placeholder="Aadhar Card Number" />
                                                             </div>
                                                                          </div>
                                              
                                                                <div class="row">
                                                                    <label for="txtVoterId"
                                                                           class="col-sm-3 control-label">Voter Id
                                                                        Card</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtVoterId"
                                                                               placeholder="Voter Id Card" />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtDriverLic"
                                                                           class="col-sm-3 control-label">Drivers
                                                                        License</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtDriverLic"
                                                                               placeholder="Drivers License" />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtNregaCard"
                                                                           class="col-sm-3 control-label">NREGA
                                                                        Card</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtNregaCard"
                                                                               placeholder="NREGA Card" />
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtRationCard"
                                                                           class="col-sm-3 control-label">Passport
                                                                        Number</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtRationCard"
                                                                               placeholder="Passport Number" />
                                                                    </div>
                                                                </div>

                                                                <%--  <div class="row">
                                                                    <label for="selAddressState"
                                                                           class="col-sm-3 control-label">Address
                                                                        State<a style="color:red">*</a></label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <select runat="server" class="form-control"
                                                                                id="selAddressState">
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="selAddressDistrict"
                                                                           class="col-sm-3 control-label">Address
                                                                        District<a style="color:red">*</a></label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <select class="form-control"
                                                                                id="selAddressDistrict">
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                  <div class="row">
                                                                    <label for="selAddressDistrict"
                                                                           class="col-sm-3 control-label">Address Sub
                                                                        District<a style="color:red">*</a></label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <select class="form-control"
                                                                                id="selAddresssubDistrict">
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                   <div class="row">
                                                                    <label for="selAddressDistrict"
                                                                           class="col-sm-3 control-label">Address Villages<a style="color:red">*</a></label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <select class="form-control"
                                                                                id="selAddressVillage">
                                                                        </select>
                                                                    </div>
                                                                </div>--%>
                                                          
                                                                <%--      <div class="row">
                                                                    <label for="txtAddressSubDistrict"
                                                                           class="col-sm-3 control-label">Address Sub
                                                                        District</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control"
                                                                               id="txtAddressSubDistrict"
                                                                               placeholder="Address Sub District">
                                                                    </div>
                                                                </div>--%>

                                                                <%--     <div class="row">
                                                                    <label for="selAddressCity"
                                                                           class="col-sm-3 control-label">Address
                                                                        City</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <select class="form-control"
                                                                                id="selAddressCity">
                                                                        </select>
                                                                    </div>
                                                                </div>--%>

                                                                <%--                                                                <div class="row">
                                                                    <label for="txtAddressArea"
                                                                           class="col-sm-3 control-label">Address
                                                                        Area</label>
                                                                    <div class="col-sm-9 form-group">
                            <textarea runat="server" class="form-control" id="txtAddressArea"
                                      placeholder="Address Area"/>
                                                                    </div>
                                                                </div>
                                                                    <div class="row">
                                                                    <label for="txtPinCode2"
                                                                           class="col-sm-3 control-label">Latitude</label>
                                                                    <div class="col-sm-3 form-group">

                                                                        <input runat="server" type="text" 
                                                                               class="form-control" id="txtLatitude"
                                                                               placeholder="Enter Latitude">
                                                                    </div>

                                                                         <label for="txtPinCode3"
                                                                           class="col-sm-3 control-label">Longitude</label>
                                                                    <div class="col-sm-3 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtLongitude"
                                                                               placeholder="Enter Longitude">
                                                                    </div>
                                                                </div>--%>

                                                                <%-- <div class="row">
                                                                    <label for="txtPinCode1"
                                                                           class="col-sm-3 control-label">Pin
                                                                        Code <a style="color:red">*</a></label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtPinCode"
                                                                               placeholder="Pin Code" />
                                                                    </div>
                                                                </div>--%>

                                                                <%--  <div class="row">
                                                                    <label for="SelAlternateOccupation"
                                                                           class="col-sm-3 control-label">Alternate
                                                                        Occupation
                                                                        Type<a style="color:red">*</a></label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <select class="form-control"
                                                                                id="SelAlternateOccupation"
                                                                                runat="server">
                                                                             <option value="">--Select-- </option>
                                                                            <option value="Government">Government
                                                                            </option>
                                                                            <option value="Public Sector">Public
                                                                                Sector
                                                                            </option>
                                                                            <option value="Private">Private</option>
                                                                            <option value="Self Employed">Self
                                                                                Employed
                                                                            </option>
                                                                            <option value="Any Other">Any Other</option>


                                                                        </select>
                                                                    </div>

                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtAlternateOccupationDtl"
                                                                           class="col-sm-3 control-label">Alternate
                                                                        Occupation
                                                                        Detail</label>
                                                                    <div class="col-sm-9 form-group">
                            <textarea runat="server" class="form-control" id="txtAlternateOccupationDtl"
                                      placeholder="Alternate Occupation Detail"/>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtUniqueId"
                                                                           class="col-sm-3 control-label">Account no</label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtUniqueId"
                                                                               placeholder="Account no">
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtBankReferenceNumber"
                                                                           class="col-sm-3 control-label">Bank Reference
                                                                        Number <a style="color:red">*</a></label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control"
                                                                               id="txtBankReferenceNumber"
                                                                               placeholder="Bank Reference Number">
                                                                    </div>
                                                                </div>--%>
                                              
                                                              <div class="row">
                                                                    <label for="txtBankReferenceNumber"
                                                                           class="col-sm-3 control-label">Identification
                                                                        Number <a style="color:red">*</a></label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control"
                                                                               id="txtUniqueId"
                                                                               placeholder="Unique Identification Number" />
                                                                    </div>
                                                                </div>


                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                    </div>
                                                    <!-- /.panel -->
                                                </div>
                                            </div>
                                        </div>


                                 
                                    <div role="tabpanel" class="tab-pane" ng-hide="divcircle">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                                    BC Certification
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-sm-3">
                                                                            <strong>Date of Passing</strong>
                                                                     <%--       <input runat="server" type="date"
                                                                                   class="form-control"
                                                                                   id="txtPassingDate"
                                                                                    ng-model="txtPassingDate"
                                                                                   placeholder="Date of Passing">--%>

                                                                              <input type='text' class="form-control"   ng-model="txtPassingDate" id="txtPassingDate"  placeholder="dd/mm/yyyy" autocomplete="off" />
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <strong>Institute Name</strong>
                                                                            <select runat="server" class="form-control"
                                                                                    id="SelInstitute"
                                                                                    datatextfield="InstituteName"
                                                                                    datavaluefield="InstituteName"
                                                                                    
                                                                                >
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <strong>Course</strong>
                                                                            <select runat="server" class="form-control"
                                                                                    id="SelCourse"
                                                                                    datatextfield="Course"
                                                                                    datavaluefield="Course">
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3">

                                                                            <strong>Grades</strong>
                                                                            <input runat="server" type="text"
                                                                                   class="form-control" id="txtGrades"
                                                                                   placeholder="Grades">
                                                                        </div>
                                                                    </div>
                                                                     <div class="row">
                                                                         <div class="col-sm-6" ng-hide="divOtherrInstitute">

                                                                            <strong>Other Institute Name</strong>
                                                                            <input runat="server" type="text"
                                                                                   class="form-control" id="Text1"
                                                                                   placeholder="Other Institute Name">
                                                                        </div>
                                                                         <div class="col-sm-6" ng-hide="divOtherCourse">

                                                                            <strong>Other Course</strong>
                                                                            <input runat="server" type="text"
                                                                                   class="form-control" id="Text2"
                                                                                   placeholder="Other Course">
                                                                        </div>
                                                                         </div>
                                                                    <div class="row pull-right" id="Div2">
                                                                        <div class="col-sm-12">
                                                                            <input type="button"
                                                                                   class="btn btn-primary  btn-sm form-control"
                                                                                   ng-click="AddMoreCerts()"
                                                                                   ng-disabled="!txtPassingDate "
                                                                                   value="Add More  " id="Button2">
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                    <br/>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Date of Passing</strong>
                                                                                </td>
                                                                                <td><strong>Institute Name</strong></td>
                                                                                <td><strong> Course</strong></td>
                                                                                <td><strong>Grades</strong></td>
                                                                                <td><strong>Remove</strong></td>
                                                                                </tr>
                                                                                <tr ng-repeat="item in certsList ">
                                                                                    <td>{{item.DateOfPassing}}</td>
                                                                                    <td>{{item.InstituteName}}</td>
                                                                                     <td>{{item.CourseName}}</td>
                                                                                     <td>{{item.Grade}}</td>
                                                                                    <td>
                                                                                        <div ng-click="removeCerts($index)"
                                                                                             class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <%--<div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                                    Educational Qualifications
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row">


                                                                        <div class="col-sm-6">
                                                                            <strong>Qualification</strong>
                                                                            <select runat="server"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selQualification">
                                                                                <option value="SSC">SSC</option>
                                                                                <option value="HSC">HSC</option>
                                                                                <option value="Graduate">Graduate
                                                                                </option>
                                                                                <option value="Others">Others</option>
                                                                            </select>
                                                                        </div>


                                                                        <div class="col-sm-6">
                                                                            <strong>If Other Qualification</strong>
                                                                            <textarea runat="server"
                                                                                      class="form-control"
                                                                                      id="txtOtherQualification"
                                                                                      placeholder="Other Qualification"/>
                                                                        </div>


                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>--%>
                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                                    Educational Qualifications
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row">


                                                                        <div class="col-sm-6">
                                                                            <strong>Qualification<a style="color:red">*</a></strong>
                                                                            <select runat="server"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selQualification">
                                                                                 <option value="">--Select-- </option>
                                                                                <option value="SSC">SSC</option>
                                                                                <option value="HSC">HSC</option>
                                                                                <option value="Graduate">Graduate
                                                                                </option>
                                                                                <option value="Others">Others</option>
                                                                            </select>
                                                                        </div>


                                                                        <div  id="other_qualfctn" class="col-sm-6">
                                                                            <strong>If Other Qualification</strong>
                                                                            <textarea runat="server"
                                                                                      class="form-control"
                                                                                      id="Textarea1"
                                                                                      placeholder="Other Qualification"/>
                                                                        </div>


                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>
                                                        
                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                                    Previous Experience as BC (If any)
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row">


                                                                        <div class="col-sm-2">
                                                                            <strong>Bank Name</strong>                                                                    
                                                                               <select  runat="server" class="form-control"
                                                                                id="selPreviousExpBank">
                                                                        </select>
                                                                        
                                                                        </div>

                                                                        <div class="col-sm-2" id="div_ifsccode">
                                                                            <strong>IFSC Code</strong>
                                                                            <input runat="server" type="text"
                                                                               class="form-control" id="ifscCode"
                                                                               placeholder="IFSC Code"/>
                                                                        </div>
                                                                        <div class="col-sm-2" style="display:none">
                                                                            <strong>Branch</strong>
                                                                            <select runat="server"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selAllocationBranch"
                                                                                    datatextfield="BranchName"
                                                                                    datavaluefield="BranchId">
                                                                            </select>
                                                                        </div>


                                                                        <div class="col-sm-2">
                                                                            <strong>From Date</strong>
                                                                             <%--  <input runat="server" type="date"
                                                                                class="form-control formcontrolheight"
                                                                                   id="txtFromDateExp"
                                                                                   placeholder="Experience From Date">--%>
                                                                                  <input type='text' class="form-control" id="txtFromDateExp"  placeholder="dd/mm/yyyy" autocomplete="off" />
                                                                        </div>


                                                                        <div class="col-sm-2">

                                                                            <strong>To Date</strong>
                                                                            <input type='text' class="form-control" id="txtToDateExp"  placeholder="dd/mm/yyyy" autocomplete="off" />
                                                                      <%--      <input runat="server" type="date"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtToDateExp"
                                                                                   placeholder="Experience To Date">--%>
                                                                        </div>
                                                                        <div class="col-sm-4">

                                                                            <strong>Reasons for Leaving</strong>
                                                                            <textarea runat="server"
                                                                                      class="form-control"
                                                                                      id="Textarea2"
                                                                                      placeholder="Reasons"/>
                                                                        </div>
                                                                    </div>
                                                                 
                                                                     <div class="row" id="div_bankdtl">
                                                                        <div class="col-sm-6">
                                                                             <strong>Other Bank Name</strong>  
                                                                           <input runat="server" type="text"
                                                                               class="form-control" id="Text4"
                                                                               placeholder="Other Bank Name"/>
                                                                        </div>
                                                                  
                                                                     
                                                                        <div class="col-sm-6">
                                                                             <strong>Other Branch Name</strong>  
                                                                           <input runat="server" type="text"
                                                                               class="form-control" id="Text5"
                                                                               placeholder="Other Branch Name"/>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row pull-right" id="Div2">
                                                                        <div class="col-sm-12">
                                                                            <input type="button"
                                                                                   class="btn btn-primary  btn-sm form-control"
                                                                                   ng-click="AddMoreExperience()"
                                                                                 
                                                                                   value="Add More  " id="Button2">
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                    <br/>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr>
                                                                                
                                                                                <td><strong>Bank Name</strong></td>
                                                                                <td><strong>Branch</strong></td>
                                                                                <td><strong> From Date</strong></td>
                                                                                <td><strong>To Date</strong></td>
                                                                                <td><strong>Reasons for Leaving</strong></td>
                                                                                <td><strong>Remove</strong></td>
                                                                                </tr>
                                                                                <tr ng-repeat="item in expsList">
                                                                                    <td ng-hide="item.BankName=='Other'">{{item.BankName}}</td>
                                                                                    <td ng-hide="item.oBankName=='N/A'">{{item.oBankName}}</td>
                                                                               
                                                                                      <td ng-hide="item.BranchName==''">{{item.BranchName}}</td>
                                                                                     <td ng-hide="item.oBranchName=='N/A'">{{item.oBranchName}}</td>
                                                                                   <td>{{item.FromDate}}</td>
                                                                                    <td>{{item.ToDate}}</td>
                                                                                  <td>{{item.Reason}}</td>
                                                                                    <td>
                                                                                        <div ng-click="removeExperience($index)"
                                                                                             class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>
                                   
                                                        
                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                            <div class="row">
                                                                    <label for="SelAlternateOccupation"
                                                                           class="col-sm-3 control-label">Alternate
                                                                        Occupation
                                                                        Type<a style="color:red">*</a></label>
                                                                    <div class="col-sm-9 form-group">
                                                                        <select class="form-control"
                                                                                id="SelAlternateOccupation"
                                                                                runat="server">
                                                                             <option value="">--Select-- </option>
                                                                            <option value="Government">Government
                                                                            </option>
                                                                            <option value="Public Sector">Public
                                                                                Sector
                                                                            </option>
                                                                            <option value="Private">Private</option>
                                                                            <option value="Self Employed">Self
                                                                                Employed
                                                                            </option>
                                                                            <option value="Any Other">Any Other</option>


                                                                        </select>
                                                                    </div>

                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtAlternateOccupationDtl"
                                                                           class="col-sm-3 control-label">Alternate
                                                                        Occupation
                                                                        Detail</label>
                                                                    <div class="col-sm-9 form-group">
                            <textarea runat="server" class="form-control" id="txtAlternateOccupationDtl"
                                      placeholder="Alternate Occupation Detail"/>
                                                                    </div>
                                                                </div>

                                                           
                                    </div>
                            
                                    <div class="formmargin center-block">
                    <div class="center-block">
                        <button type="button" ng-click="addSingleEntry()" class="btn btn-success center-block">Submit</button>
                    </div>
                </div>
                            
                                     
                             


                                <!-- /.src-code -->

                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->
                                                      

          
            <!-- /.container-fluid -->
        </section>
        
        <!-- /.section -->

    </div>
        </form>
</body>
