﻿var markers = []; // Create a marker array to hold your markers
var map;
var bounds;
//var bclocation = [
//    ['BC 1', 51.503454, -0.119562, 1231],
//    ['BC 2', 51.499633, -0.124755, 123123]
//];
var bclocation = [

];

var bcList;




function hidePopUp() {
    $('[data-popup="' + "popup-1" + '"]').fadeOut(350);
}
function myFunction() {
    var stateId = $('#selAddressState').val();
    var districtid = $('#selAddressDistrict').val();
    var subdistrictid = $('#selAddresssubDistrict').val();
    var village = $('#selAddressVillage').val();
    if (districtid == null || districtid == "0") {
        districtid = -1;
    }
    if (subdistrictid == null || subdistrictid == "0") {
        subdistrictid = -1;
    }
    if (village == null || village == "0") {
        village = -1;
    }

    $.ajax({
        url: "../api/getBcByState.ashx?mode=state",
        method: "POST",
        datatype: "json",
        headers: {
            "userId": 'admin',
            "TokenId": '36e58be1-9dd3-4a0b-b469-73ae94e54d4f'
        },
        data: '{"stateId": "' + stateId + '","district": "' + districtid + '","subdistrict": "' + subdistrictid + '","village": "' + village + '"}',
        success: function (Bclist) {

            bcList = Bclist;
            $('#tbDetails tbody > tr').remove();
            $('#listOfBC').hide();
            $('#detailOfBC').hide();
            $('#searchBc').hide();

            var list = new Array();
            if (Bclist==null) {
                $('[data-popup="' + "popup-1" + '"]').fadeIn(350);
                $('#listOfBC').hide();
                $('#searchBc').show();
                $('#detailOfBC').hide();
            }
            else {
                for (var i = 0; i < Bclist.length; i++) {
                    var Latitude = parseFloat(Bclist[i].Latitude);
                    var Longitude = parseFloat(Bclist[i].Longitude);
                    if (Latitude == '' || Latitude == null || isNaN(Latitude)) { }
                    else if (Longitude == '' || Longitude == null || isNaN(Longitude)) { }
                    else {
                        list.push(new Array(Bclist[i].Name, Latitude, Longitude, Bclist[i].BankCorrespondId))
                        //list[i] = new Array(Bclist[i].Name, Latitude, Longitude, Bclist[i].BankCorrespondId);
                    }
                    //  list[i].push(Bclist[i].Name, Bclist[i].Latitude, Bclist[i].Longitude, Bclist[i].BankCorrespondId)
                }

                for (var i = 0; i < Bclist.length; i++) {
                    $("#tbDetails").append("<tr id=" + Bclist[i].BankCorrespondId + "><td>" + "<a href='#'>" + Bclist[i].BankCorrespondId + "</a></td><td>" + Bclist[i].Name + "</td><td>" + Bclist[i].Area + "</td></tr>");

                }
                if (!$("#tbDetails tbody").html()) {
                    $('#listOfBC').hide();
                    $('#searchBc').hide();

                }
                else {
                    $('#listOfBC').show();

                }

                bclocation = list;
                console.log(bclocation);
                reloadMarkers();
            }
            //end of else
        },
        error: function (xmlHttpRequest, errorString, errorMessage) {

        }
    });

}


function initMap() {
    var myLatLng = { lat: 18.906, lng: 72.814 };
    bounds = new google.maps.LatLngBounds();

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 7,
        center: myLatLng,
        mapTypeId: 'roadmap'
    });

    addBulkMarkers(bclocation);
}

function addBulkMarkers(locationlist) {

    for (i = 0; i < locationlist.length; i++) {
        var position = new google.maps.LatLng(locationlist[i][1], locationlist[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            bcid: locationlist[i][3],
            title: locationlist[i][0]
        });
        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
        markers.push(marker);

        marker.addListener('click', function () {
        
            var id = marker.bcid;

            $.ajax({
                url: "../api/GetBCHandler.ashx?mode=publicbcdetails&bcId=" + id,
                method: "POST",
                datatype: "json",
                headers: {
                    "userId": 'admin',
                    "TokenId": '36e58be1-9dd3-4a0b-b469-73ae94e54d4f'
                },
             
                success: function (Bclist) {
                  
                    document.getElementById('lblName').innerHTML = Bclist[0].Name;
                    document.getElementById('lblcontactNo').innerHTML = Bclist[0].PhoneNumber1;
                    document.getElementById('lblbankName').innerHTML = Bclist[0].BankName;
                    document.getElementById('lblstate').innerHTML = Bclist[0].State;
                    document.getElementById('lbldistrict').innerHTML = Bclist[0].District;
                    document.getElementById('lblsubdistrict').innerHTML = Bclist[0].Subdistrict;
                    document.getElementById('lblproduct').innerHTML = Bclist[0].productName;
                    
                    $("#searchBc").hide();
                    $("#listOfBC").hide();
                    $("#detailOfBC").show();

                },
                error: function (xmlHttpRequest, errorString, errorMessage) {

                }
            });

        });
    }
}

function reloadMarkers() {

    // Loop through markers and set map to null for each
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers = [];
    // Call set markers to re-add markers
    addBulkMarkers(bclocation);
}

function addMarker(location, map) {
    var marker = new google.maps.Marker({
        position: location,
        label: "BC",
        map: map
    });
}