﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="editCorporate.aspx.cs" Inherits="BCTrackingWeb.editCorporate" %>


    <%--  <div class="main-page">--%>
    <div class="container-fluid">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">Edit Corporate </h2>

            </div>


            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a ui-sref="corporateList">Corporate List</a></li>
                    <li><a>Edit Corporate</a></li>

                </ul>
            </div>

            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <section class="section">
        <div class="container-fluid">

            <div class="row">


                <!-- /.col-md-6 -->

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                            </div>
                        </div>
                        <div class="panel-body p-20">

                            <div class="panel-body">
                                  <div class="row">
                                    <label class="col-sm-2 control-label">Corporate Name <a style="color:red">*</a></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control formcontrolheight" id="txtName" placeholder="Name">
                                    </div>
                                </div>
                                   <div class="row">
                                    <label class="col-sm-2 control-label">Pan No <a style="color:red">*</a></label>
                                    <div class="col-sm-10">
                                        <input type="text" maxlength="10" class="form-control formcontrolheight" id="txtPanNO" placeholder="Pan no" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 control-label">Email <a style="color:red">*</a></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control formcontrolheight" id="txtEmail" placeholder="Email">
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 control-label">Type <a style="color:red">*</a></label>
                                    <div class="col-sm-10">
                                        <select runat="server" class="form-control formcontrolheight" style="margin-bottom: 10px"
                                            id="Select1">
                                            <option value="-1">Select Type</option>
                                            <option value="National">National</option>
                                            <option value="Regional">Regional</option>

                                        </select>


                                    </div>
                                </div>
                              
                                <div class="row">
                                    <label class="col-sm-2 control-label">Contact Number <a style="color:red">*</a></label>
                                    <div class="col-sm-10">
                                      
                                        <input runat="server" type="text" maxlength="10"
                                                                               class="form-control" id="txtContactNo" ng-keypress="filterValue($event)"
                                                                               placeholder="Contact Number" />
                                        
                                    </div>
                                </div>
                                  <div class="row">
                                    <label class="col-sm-2 control-label">Address</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control formcontrolheight" id="txtAddress" placeholder="Address" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 control-label">Contact Person</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control formcontrolheight" id="txtContactPerson" placeholder="Contact Person" />
                                    </div>
                                </div>
                               
                                <div class="row">
                                    <label class="col-sm-2 control-label">Contact Designation</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control formcontrolheight" id="txtContactDesignation" placeholder="Contact Designation" />
                                    </div>
                                </div>
                            <%--    <div class="row">
                                    <label class="col-sm-2 control-label">No of complaint</label>
                                    <div class="col-sm-10">
                                           <input runat="server" type="text" 
                                                                               class="form-control" id="txtNoOfComplaint" ng-keypress="filterValue($event)"
                                                                               maxlength="3" placeholder="No Of Complaint" />
                                        
                                    </div>
                                </div>--%>
                                <%--<div class="row">
                                    <label class="col-sm-2 control-label">Pan No</label>
                                    <div class="col-sm-10">
                                        <input type="text" maxlength="10" class="form-control formcontrolheight" id="txtPanNO" placeholder="Pan no" />
                                    </div>
                                </div>--%>

                                  <div class="row">
                                    <label class="col-sm-2 control-label">Website</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control formcontrolheight" id="txtWebsite" placeholder="Website" />
                                    </div>
                                </div>

                                <div class="formmargin">
                                    <div class="col-sm-1">
                                       <button type="button" id="btnAddCorporate" class="btn btn-success center-block">{{btnName}}</button>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="button" id="btnDelete" class="btn btn-danger center-block">Delete</button>
                                    </div>

                                </div>

                            </div>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->




                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.section -->

    <%-- </div>

   
    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/bc.js" type="text/javascript"></script>--%>

