﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="searchVillage.aspx.cs" Inherits="BCTrackingWeb.searchVillage" %>

<!DOCTYPE html>

   <div class="container-fluid">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">List of BC Address Wise</h2>

            </div>

            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href=""><span>BC Address Wise</span></a></li>

                </ul>
            </div>

            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <section class="section" ng-init="GetStates();SearchVillage()">
        <div class="container-fluid">

            <div class="row">


                <!-- /.col-md-6 -->

                <div class="col-md-12">
                    <div class="panel">

                        <div class="panel-body p-20">
                            <div class="panel-body">

                                 <div class="formmargin">


                                    <div class="col-md-3">
                                        <strong>State</strong>
                                        <select runat="server" class="form-control"
                                            id="selAddressState">
                                        </select>

                                    </div>
                                    <div class="col-md-3">

                                        <strong>District</strong>
                                      
                                            <select class="form-control"
                                                id="selAddressDistrict">
                                            </select>
                                      

                                    </div>

                                    <div class="col-md-3">
                                        <strong>Sub District</strong>
                                        <select class="form-control"
                                            id="selAddresssubDistrict">
                                        </select>
                                    </div>
                                   

                                </div>


                            </div>

                            <div class="panel-body">

                                <div class="formmargin">

                                    <div class="col-md-3 pull-right">
                                        <strong></strong>
                                        <button type="button" class="btn btn-success" ng-click="SearchVillage()">Show Village</button>
                                    </div>

                                </div>

                            </div>
                            <hr />
                            <table id="tblsearchVillage" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Village Code</th>
                                         <th>Village Name</th>
                                         <th>Sub District</th>
                                         <th>District</th>
                                         <th>State</th>
                                 
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->


                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.section -->