﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BcMultiroduct.aspx.cs" Inherits="BCTrackingWeb.BcMultiroduct" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
     
    <div class="container-fluid" ng-init="getProductList();getProduct()">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">List BC Per Multi Product</h2>

            </div>

            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="BcPerProduct.html"><span>BC Per Product</span></a></li>

                </ul>
            </div>

            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <section class="section">
        <div class="container-fluid">

            <div class="row">

                <div class="col-md-12">
                    <div class="panel">

                        <div class="panel-body p-20">


                            <div class="panel-body">

                                <div class="formmargin">


                                  

                                       <div class="col-md-6">

                                      
                                        
                                    <tags-input ng-model="product" display-property="name">
                                <auto-complete source="loadTags($query)"></auto-complete>
                            </tags-input>
                            </tags-input>

                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-success" ng-click="getProductList()">
                                            Show
                                                List
                                        </button>
                                    </div>
                                 
                                </div>
                                <br />
                                <br />


                                <div class="formmargin">
                                  
                                </div>
                            </div>


                            <hr />
                            <table id="tblProducts" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Product ID</th>
                                        <th>Product Name</th>
                                        <th>BC Code</th>
                                         <th>Name</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>

            </div>
        </div>

    </section>


  
    </form>
</body>
</html>
