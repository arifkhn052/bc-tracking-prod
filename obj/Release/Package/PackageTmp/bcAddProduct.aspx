﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bcAddProduct.aspx.cs" Inherits="BCTrackingWeb.bcAddProduct" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    
    <div class="container-fluid" ng-init="getProduct()">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">Add Activity</h2>
            </div>
        </div>

        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a ui-sref="addproduct">Add Activity</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <section class="section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                            </div>
                        </div>
                        <div class="panel-body p-20">
                            <div class="panel-body">
                                <div class="formmargin">
                                    <label message class="col-sm-2 control-label">Activity Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control formcontrolheight" id="txtProductName" placeholder="Enter Activity Name"
                                            parsley-trigger="change" required>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" id="btnAddProduct" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">

            <div class="row">


                <!-- /.col-md-6 -->

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                            </div>
                        </div>
                        <div class="panel-body p-20">

                            <table id="tblProduct" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Activity Name</th>
                                         <th>Created by</th>
                                         <th>Edit</th>        
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->


                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
    </section>
    </form>
</body>
</html>
