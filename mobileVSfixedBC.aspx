﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="mobileVSfixedBC.aspx.cs" Inherits="BCTrackingWeb.mobileVSfixedBC" %>

   <div class="container-fluid">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h3 class="title">Location wise fixed vs mobile BC's </h3>

            </div>
            
            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href=""><span>Location wise fixed vs mobile BC's</span></a></li>

                </ul>
            </div>
            
            <!-- /.col-md-6 -->
        </div>
       
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <section class="section" ng-init="ListOfFixedvsMobile();GetStates()">
        <div class="container-fluid">

            <div class="row">


                <!-- /.col-md-6 -->

                <div class="col-md-12">
                    <div class="panel">

                        <div class="panel-body p-20">
                            <div class="panel-body">

                                 <div class="formmargin">


                                    <div class="col-md-3">
                                        <strong>State</strong>
                                        <select runat="server" class="form-control"
                                            id="selAddressState">
                                        </select>

                                    </div>
                                    <div class="col-md-3">

                                        <strong>District</strong>
                                      
                                            <select class="form-control"
                                                id="selAddressDistrict">
                                            </select>
                                      

                                    </div>

                                    <div class="col-md-3">
                                        <strong>Sub District</strong>
                                        <select class="form-control"
                                            id="selAddresssubDistrict">
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <strong>Villages</strong>
                                     
                                            <select class="form-control"
                                                id="selAddressVillage">
                                            </select>
                                        
                                    </div>

                                </div>


                            </div>

                            <div class="panel-body">

                                <div class="formmargin">

                                    <div class="col-md-3 pull-right">
                                        <strong></strong>
                                        <button type="button" class="btn btn-success" ng-click="ListOfFixedvsMobile()">Show List</button>
                                    </div>






                                </div>


                            </div>
                            <hr />
                            <table id="tblMobilevsFixedtype0" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>State</th>
                                        <th>No of Mobile BC</th>
                                        <th>No of Fixed BC</th>
                                        <th>Both</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <table id="tblMobilevsFixedtype1" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>State</th>
                                          <th>District</th>
                                        <th>No of Mobile BC</th>
                                        <th>No of Fixed BC</th>
                                        <th>Both</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <table id="tblMobilevsFixedtype2" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>State</th>
                                        <th>District</th>
                                        <th>Sub District</th>
                                        <th>No of Mobile BC</th>
                                        <th>No of Fixed BC</th>
                                        <th>Both</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <table id="tblMobilevsFixedtype3" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>State</th>
                                        <th>District</th>
                                        <th>Sub District</th>
                                        <th>Village</th>
                                        <th>No of Mobile BC</th>
                                        <th>No of Fixed BC</th>
                                        <th>Both</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->


                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.section -->
