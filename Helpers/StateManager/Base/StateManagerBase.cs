﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateManagerBase.cs" company="BCG" author="Nagarro">
//   All rights reserved. Copyright (c) 2014.
// </copyright>
// <summary>
//   Represents abstract base class for State managers, Author : BCG
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SBI.Web
{
    using System;

    /// <summary>
    /// Represents abstract base class for State managers, Author : BCG
    /// </summary>
    /// <typeparam name="T">
    /// Type of class whose reference is o be stored in session
    /// </typeparam>
    public abstract class StateManagerBase<T> : IStateManager<T>
        where T : StateEntityBase, new()
    {
        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="StateManagerBase{T}" /> class. Initializes a new instance of the <see
        ///    cref="StateManagerBase&lt;T&gt;" /> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors", Justification = "Required in this case")]
        internal StateManagerBase()
        {
            // mStateEntity = new T();
            // mKey = mStateEntity.GetType().Name + "_" + mStateEntity.GetType().GUID.ToString();
            // this.StateEntity = mStateEntity;
            this.Initialize(true);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets key for state entity.
        /// </summary>
        /// <value> The key. </value>
        public string Key { get; private set; }

        /// <summary>
        ///   Gets or sets the state entity.
        /// </summary>
        /// <value> The state entity. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations",
            Justification = "Exception is thrown intentially")]
        public virtual T StateEntity
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public virtual void Clear()
        {
            // this._stateEntity = null;
        }

        /// <summary>
        /// Generates the key.
        /// </summary>
        /// <returns>
        /// The generate key. 
        /// </returns>
        public string GenerateKey()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Generates the key on the basis of entity.
        /// </summary>
        /// <param name="stateEntity">
        /// The state entity. 
        /// </param>
        /// <returns>
        /// The generate key. 
        /// </returns>
        public string GenerateKey(T stateEntity)
        {
            return stateEntity.Key;
        }

        /// <summary>
        /// Initializes the State Entity.
        /// </summary>
        /// <param name="isFirstTime">
        /// if set to <c>true</c> then generates the key else not. 
        /// </param>
        public void Initialize(bool isFirstTime)
        {
            var stateEntity = new T();
            if (isFirstTime)
            {
                this.Key = this.GenerateKey(stateEntity);
            }

            this.StateEntity = stateEntity;
        }

        #endregion
    }
}