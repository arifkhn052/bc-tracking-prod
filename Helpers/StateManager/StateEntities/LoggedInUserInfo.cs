﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CrossPriceRules.cs" company="BCG" author="Nagarro">
//   All rights reserved. Copyright (c) 2014.
// </copyright>
// <summary>
//   The Class will store cross price rules state info used for download or uplaod
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SBI.Web
{
    /// <summary>
    /// The Class will store cross user information 
    /// </summary>
    public class LoggedInUserInfo : StateEntityBase
    {

        /// <summary>
        /// Gets or sets the name of the group.
        /// </summary>
        /// <value>
        /// The name of the group.
        /// </value>
        public string lg_id { get; set; }
        public string staff_type { get; set; }
        public string userrole { get; set; }
        public string saletype { get; set; }
        public string name { get; set; }
        public string branch_id { get; set; }
        public string employee_id { get; set; }
        public string email { get; set; }
        public string branch_name { get; set; }
        public int userrole_id { get; set; }
        public string phone_no { get; set; }
        public string circle { get; set; }
        public string network { get; set; }
        public string zone { get; set; }
        public string region { get; set; }
        public string isActive { get; set; }
        public string rtmu_id { get; set; }
        
    }

}