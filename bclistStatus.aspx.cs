﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using BCTrackingBL;
using BCEntities;

namespace BCTrackingWeb
{
    public partial class bclistStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string getBCByStatus()
        {
            // Paging parameters:
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);

            // Sorting parameters
            var iSortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var iSortDir = HttpContext.Current.Request.Params["sSortDir_0"];
            // Search parameters
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToLower();
            int totalRecords = 0;
            if (System.Web.HttpContext.Current.Session["UserInfo"] != null) ;
            var userId = HttpContext.Current.Request.Params["adminuserid"].ToLower();
            var startDate = HttpContext.Current.Request.Params["startDate"].ToLower();
            var endDate = HttpContext.Current.Request.Params["endDate"].ToLower();

            var leadList = Common.getBCByStatus(iDisplayStart, iDisplayLength, iSortCol, iSortDir, sSearch, out totalRecords, "1", "", startDate,endDate, userId);

            Func<BankCorrespondence, object> order = p =>
            {
                if (iSortCol == 0)
                {
                    return p.BankCorrespondId;
                }

                else if (iSortCol == 1)
                {
                    return p.Name;
                }
                else if (iSortCol == 2)
                {
                    return p.PhoneNumber1;
                }
                else if (iSortCol == 3)
                {
                    return p.AadharCard;
                }
                else if (iSortCol == 4)
                {
                    return p.Qualification;
                }
                else if (iSortCol == 5)
                {
                    return p.PLVillageDetail;
                }



                return p.BankCorrespondId;
            };

            //    // Define the order direction based on the iSortDir parameter
            if ("desc" == iSortDir)
            {
                leadList = leadList.OrderByDescending(order).ToList<BankCorrespondence>();
            }
            else
            {
                leadList = leadList.OrderBy(order).ToList<BankCorrespondence>();
            }
            var finalResult = leadList
                    .Select(p => new[] { p.BankCorrespondId.ToString(), p.BCCode, p.Name, p.PhoneNumber1, p.Qualification, p.PLVillageDetail, p.IsBlackListeds.ToString() });

            int count = finalResult.Count();

            // prepare an anonymous object for JSON serialization
            var result = new
            {
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = finalResult
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            return json;
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string getBCByStatusFilter()
        {
            // Paging parameters:
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);

            // Sorting parameters
            var iSortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var iSortDir = HttpContext.Current.Request.Params["sSortDir_0"];
            // Search parameters
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToLower();
            int totalRecords = 0;
            if (System.Web.HttpContext.Current.Session["UserInfo"] != null) ;
            var itype = HttpContext.Current.Request.Params["itype"].ToLower();
            var startDate = HttpContext.Current.Request.Params["startDate"].ToLower();
            var endDate = HttpContext.Current.Request.Params["endDate"].ToLower();
            var status = HttpContext.Current.Request.Params["status"].ToLower();
            var userId = HttpContext.Current.Request.Params["adminuserid"].ToLower();

            var leadList = Common.getBCByStatus(iDisplayStart, iDisplayLength, iSortCol, iSortDir, sSearch, out totalRecords, itype.ToString(), status, startDate,endDate, userId);

            Func<BankCorrespondence, object> order = p =>
            {
                if (iSortCol == 0)
                {
                    return p.BankCorrespondId;
                }

                else if (iSortCol == 1)
                {
                    return p.Name;
                }
                else if (iSortCol == 2)
                {
                    return p.PhoneNumber1;
                }
                else if (iSortCol == 3)
                {
                    return p.AadharCard;
                }
                else if (iSortCol == 4)
                {
                    return p.Qualification;
                }
                else if (iSortCol == 5)
                {
                    return p.PLVillageDetail;
                }



                return p.BankCorrespondId;
            };

            //    // Define the order direction based on the iSortDir parameter
            if ("desc" == iSortDir)
            {
                leadList = leadList.OrderByDescending(order).ToList<BankCorrespondence>();
            }
            else
            {
                leadList = leadList.OrderBy(order).ToList<BankCorrespondence>();
            }
            var finalResult = leadList
                    .Select(p => new[] { p.BankCorrespondId.ToString(), p.Name, p.PhoneNumber1, p.AadharCard, p.Qualification, p.PLVillageDetail, p.IsBlackListeds.ToString() });

            int count = finalResult.Count();

            // prepare an anonymous object for JSON serialization
            var result = new
            {
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = finalResult
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            return json;
        }
    }
}