﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCTrackingWeb
{
    public partial class uiMainMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = Request.RawUrl.ToString();

            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
            if (url.Contains("?admin") == true)
            {
              // Response.Redirect(baseUrl+ "?admin/#/login");
            }
            else
            {
                Response.Redirect(baseUrl + "public/index.html");
            }
        }
    }
}