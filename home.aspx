﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="BCTrackingWeb.home" %>

      <div >
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Home</h2>
                                   
                                </div>
                             
                             
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="home.aspx"><i class="fa fa-home"></i> Home</a></li>
                                
            							
            						</ul>
                                </div>
                              
                                <!-- /.col-md-6 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                            <section class="section">
                            <div class="container-fluid">
                                <div class="row">
                                    <%--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat bg-primary" href="#">
                                            <span class="number counter">100</span>
                                            <span class="name">BC</span>
                                            <span class="bg-icon"><i class="fa fa-comments"></i></span>
                                        </a>
                                        <!-- /.dashboard-stat -->

                                        
                                        <!-- /.src-code -->
                                    </div>--%>
                                    <!-- /.col-lg-3 col-md-3 col-sm-6 col-xs-12 -->

                                   <%-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat bg-danger" href="#">
                                            <span class="number counter">322</span>
                                            <span class="name">Total BC</span>
                                            <span class="bg-icon"><i class="fa fa-ticket"></i></span>
                                        </a>
                                        <!-- /.dashboard-stat -->

                                        
                                        <!-- /.src-code -->
                                    </div>--%>
                                    <!-- /.col-lg-3 col-md-3 col-sm-6 col-xs-12 -->

<%--                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat bg-warning" href="#">
                                            <span class="number counter">18</span>
                                            <span class="name">Total Bank </span>
                                            <span class="bg-icon"><i class="fa fa-bank"></i></span>
                                        </a>
                                        <!-- /.dashboard-stat -->

                                        
                                        <!-- /.src-code -->
                                    </div>--%>
                                    <!-- /.col-lg-3 col-md-3 col-sm-6 col-xs-12 -->

                                 <%--   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat bg-success" href="#">
                                            <span class="number counter">1600</span>
                                            <span class="name">Total Branch</span>
                                            <span class="bg-icon"><i class="fa fa-bank"></i></span>
                                        </a>
                                        <!-- /.dashboard-stat -->

                                        
                                        <!-- /.src-code -->
                                    </div>--%>
                                    <!-- /.col-lg-3 col-md-3 col-sm-6 col-xs-12 -->

                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
        <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/bc.js" type="text/javascript"></script>

