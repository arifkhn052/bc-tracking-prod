﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using BCTrackingBL;
using BCEntities;


namespace BCTrackingWeb
{
    public partial class notifications : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string getNotification()
        {
            // Paging parameters:
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);

            // Sorting parameters
            var iSortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var iSortDir = HttpContext.Current.Request.Params["sSortDir_0"];
            // Search parameters
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToLower();
            int totalRecords = 0;
            if (System.Web.HttpContext.Current.Session["UserInfo"] != null) ;
            var leadList = Common.getNotification(iDisplayStart, iDisplayLength, iSortCol, iSortDir, sSearch, out totalRecords);

            Func<Notification, object> order = p =>
            {
                if (iSortCol == 0)
                {
                    return p.NotificationId;
                }

                else if (iSortCol == 1)
                {
                    return p.NotificationType;
                }
                else if (iSortCol == 2)
                {
                    return p.NotificationText;
                }
                else if (iSortCol == 3)
                {
                    return p.NotificationDate;
                }
                
                return p.NotificationId;
            };

            //    // Define the order direction based on the iSortDir parameter
            if ("desc" == iSortDir)
            {
                leadList = leadList.OrderByDescending(order).ToList<Notification>();
            }
            else
            {
                leadList = leadList.OrderBy(order).ToList<Notification>();
            }
            var finalResult = leadList
                    .Select(p => new[] { p.NotificationId.ToString(), p.NotificationType, p.NotificationText, p.NotificationDate});

            int count = finalResult.Count();

            // prepare an anonymous object for JSON serialization
            var result = new
            {
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = finalResult
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            return json;
        }
    }
}