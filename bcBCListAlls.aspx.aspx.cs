﻿using BCTrackingBL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCTrackingWeb
{
    public partial class bcBCListAlls_aspx : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            string UserId = HiddenField1.Value;

            DataTable dt = Common.ListofProposal(UserId);

            int count = dt.Rows.Count;
            if (count == 0)
            {
                gvDetails.Visible = false;
            }
            else
            {

                gvDetails.DataSource = dt;
                gvDetails.DataBind();

                //..................................... Export Data Logic ..................................

                Response.ClearContent();

                Response.Buffer = true;

                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ListofBC" + DateTime.Now.ToShortDateString() + ".xls"));

                Response.ContentType = "application/ms-excel";

                StringWriter sw = new StringWriter();

                HtmlTextWriter htw = new HtmlTextWriter(sw);

                gvDetails.AllowPaging = false;

                //gvDetails.HeaderRow.Style.Add(
                //Change the Header Row back to white color

                gvDetails.HeaderRow.Style.Add("background-color", "#FFFFFF");

                //Applying stlye to gridview header cells

                for (int i = 0; i < gvDetails.HeaderRow.Cells.Count; i++)
                {

                    gvDetails.HeaderRow.Cells[i].Style.Add("background-color", "#507CD1");

                }

                int j = 1;

                //Set alternate row color

                foreach (GridViewRow gvrow in gvDetails.Rows)
                {

                    gvrow.BackColor = System.Drawing.Color.White;

                    if (j <= gvDetails.Rows.Count)
                    {

                        if (j % 2 != 0)
                        {

                            for (int k = 0; k < gvrow.Cells.Count; k++)
                            {

                                gvrow.Cells[k].Style.Add("background-color", "#faf9ee");

                            }

                        }

                    }

                    j++;

                }

                gvDetails.RenderControl(htw);

                Response.Write(sw.ToString());

                Response.End();

                //..................................... End Logic ..........................................
            }

        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }
    }
}