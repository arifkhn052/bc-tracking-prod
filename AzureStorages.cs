﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure; //Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob;
using System.Security.Cryptography; // Namespace for Blob storage types

namespace BCTrackingBL
{
   public class AzureStorages
    {
       public void storageAccount()
       {
           CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
           CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
           // Retrieve a reference to a container.
           CloudBlobContainer container = blobClient.GetContainerReference("dev-raw");
           // Create the container if it doesn't already exist.

           // Retrieve reference to a blob named "myblob".
           CloudBlockBlob blockBlob = container.GetBlockBlobReference("20170719193348754_2_BulkUpload.xlsx");

           // Create or overwrite the "myblob" blob with contents from a local file.
           using (var fileStream = System.IO.File.OpenRead(@"C:\Users\arif\Downloads\WhatsApp Image 2017-04-14 at 6.50.50 PM.jpeg"))
           {
               blockBlob.UploadFromStream(fileStream);
           }
           //using (var fileStream = System.IO.File.OpenWrite(@"D:\project\bctracking-prod\BCTrackingWeb\FileUpload\20170719193348754_2_BulkUpload.xlsx"))
           //{
           //    blockBlob.DownloadToStream(fileStream);
           //}



       }

       public static string MD5Hash(string text)
       {
           MD5 md5 = new MD5CryptoServiceProvider();

           //compute hash from the bytes of text
           md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

           //get hash result after compute it
           byte[] result = md5.Hash;

           StringBuilder strBuilder = new StringBuilder();
           for (int i = 0; i < result.Length; i++)
           {
               //change it into 2 hexadecimal digits
               //for each byte
               strBuilder.Append(result[i].ToString("x2"));
           }

           return strBuilder.ToString();
       }

       public void ReadExcel(string fileName, int userID, string mode)
       {
           List<string> errorMsg = new List<string>();
           int k=0;
           for (int i = 0; i < 10; i++)
           {
                k = i++;
           }
          
       }

     


       }


      
        
}

