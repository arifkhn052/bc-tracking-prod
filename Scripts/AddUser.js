﻿$(document).ready(function () {
   
    $("#txtEmpId").prop("disabled", false);
    var EmpId = getParameterByName('EmpId');
    $("#DivRtmo").hide();
    $("#DivRtmu").hide();
    GetStaffType();
    if (EmpId != "") {
        
        $("#btnAddUser").val("Modify User");
        GetCircle();
        GetUserDetail(EmpId);
        $("#formHeader").html("Modify User");
    }
    else {
        $("#btnAddUser").val("Add User");
      
        
    }
 
});
$('#txtPhone').val("");
var errorMsg;
var errorMessage = '';
$("#DDLLoginType").change(function () {
    if ($("#DDLLoginType").val() == "Sale Staff")
        $("#divSaleType").show();
    else
        $("#divSaleType").hide();

});

$("#DDLSalesType").change(function () {
    if ($("#DDLSalesType").val() == "RMME")
        $("#trRMMEType").show();
    else
        $("#trRMMEType").hide();

});




$("#btnAddUser").click(function () {
    
    $("#btnAddUser").prop("disabled", true);
    if (validate() == true) {
        var action;
        if ($("#btnAddUser").val() == "Modify User")
            action = "Modify";
        else
            action = "Insert";


        window.SBICommon.sendAjaxCall({
            url: 'AddUser.aspx/CreateUser',
            data: "{'action': '" + action + "','employeeId': '" + $("#txtEmpId").val() + "','staffType' : '" + $('#DDLStaffType').val() + "','loginType' : '" + $('#DDLLoginType').val() +
               "','salesType' : '" + $('#DDLSalesType').val() + "','branch' : '" + $('#DDLBranch').val() + "','circle' : '" + $('#DDLCircle').val() +
               "','network' : '" + $('#DDLNetwork').val() + "','zone' : '" + $('#DDLZone').val() + "','region' : '" + $('#DDLRegion').val() +
               "','name' : '" + $('#txtName').val() + "','email' : '" + $('#txtEmail').val() + "','phone' : '" + $('#txtPhone').val()
               + "','lgid' : '" + $('#hdnlgid').val() + "','rmmeType' : '" + $('#DDLRmmeType').val() + "','rtmo' : '" + $('#DDLRtmo').val() + "','rtmu' : '" + $('#DDLRtmou').val() + "','password1' : '" + $('#txtPassword').val() + "'}",
            requestType: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data) {
                $("#btnAddUser").prop("disabled", false);
                if (data.d == "Email Id already exists." || data.d == "Employee Id already exists.")
                    ShowDialog(data.d, 400, 600, 'Message', 10, '<span class="icon-info"></span>', false);
                else {
                    var messageContent = "<center><h1>Thank You</h1><h6>" + data.d + " </h6>" +
                          "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                    ShowDialog(messageContent, 400, 600, 'Message', 10, '<span class="icon-info"></span>', true,  'AddUser.aspx');
                }

            },

        
        });

        
      
    }

    else {
        ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    }

});



function validate() {
     
    errorMsg = '';
    var staffType = $("#DDLStaffType").val();
   
    var email = $('#txtEmail').val();
    var empId = $('#txtEmpId').val();
    var name = $('#txtName').val();
    var phone = $('#txtPhone').val();

    var result = true;

    //if (staffType == "") {
    //    $("#divStaffType").addClass("form-group has-error");
    //    result = false;
    //}
    //else {
    //    $("#divStaffType").removeClass("form-group has-error");
    //}
   
 
    if (!IsEmail(email)) {
        $('#txtEmail').addClass("form-group has-error");
        errorMsg = errorMsg + "<br/>Enter valid email Id.";
        result = false;
    }
    else {
        $("#divEmail").removeClass("form-group has-error");
    }
    if (empId == "") {
        $('#divEmpId').addClass("form-group has-error");
        errorMsg = errorMsg + "<br/>Enter pf number.";
        result = false;
    }
    else {
        $("#divEmpId").removeClass("form-group has-error");
    }
    if (name == "") {
        $('#divName').addClass("form-group has-error");
        errorMsg = errorMsg + "<br/>Enter Name.";
        result = false;
    }
    else {
        if (name.match(/\d+/g) != null) {
            $('#divName').addClass("form-group has-error");
            result = false;
        }
        else {
            $("#divName").removeClass("form-group has-error");
        }
        
    }
    
    //if (phone == "" || phone.length > 10 || phone.length < 10 || isNaN(phone)) {
    //    $('#divPhone').addClass("form-group has-error");
    //    result = false;
    //}
    //else {
    //    $("#divPhone").removeClass("form-group has-error");
    //}
    
    return result;
}

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function GetStaffType() {
    $.ajax({
        url: 'AddUser.aspx/GetStaffType',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            
            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLStaffType').append('<option value="' + k + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetCircle() {
    $.ajax({
        url: 'AddUser.aspx/GetCircleName',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async:false,
        success: function (data) {
            
            $.each(data, function () {
                $.each(this, function (k, v) {
                    
                    $('#DDLCircle').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetNetwork() {
    $.ajax({
        url: 'AddUser.aspx/GetNetwork',
        type: "POST",
        dataType: "json",
        data: "{'circle': '" + $("#DDLCircle").val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            
            $.each(data, function () {
                $.each(this, function (k, v) {
                    
                    $('#DDLNetwork').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetZone() {
    $.ajax({
        url: 'AddUser.aspx/GetZone',
        type: "POST",
        dataType: "json",
        data: "{'circle': '" + $("#DDLCircle").val() + "','network' : '" + $('#DDLNetwork').val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            
            $.each(data, function () {
                $.each(this, function (k, v) {
                    
                    $('#DDLZone').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetRegion() {
    $.ajax({
        url: 'AddUser.aspx/GetRegion',
        type: "POST",
        dataType: "json",
        data: "{'circle': '" + $("#DDLCircle").val() + "','network' : '" + $('#DDLNetwork').val() + "','zone' : '" + $('#DDLZone').val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            
            $.each(data, function () {
                $.each(this, function (k, v) {
                    
                    $('#DDLRegion').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetBranch() {
    $.ajax({
        url: 'AddUser.aspx/GetBranch',
        type: "POST",
        dataType: "json",
        data: "{'circle': '" + $("#DDLCircle").val() + "','network' : '" + $('#DDLNetwork').val() + "','zone' : '" + $('#DDLZone').val() + "','region' : '" + $('#DDLRegion').val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            
            $.each(data, function () {
                $.each(this, function (k, v) {
                    $('#DDLBranch').append('<option value="' + k + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetUserDetail(EmpId) {
    
    $.ajax({
        url: 'AddUser.aspx/GetUserDetail',
        type: "POST",
        dataType: "json",
        data: "{'empId': '" + EmpId + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            var user = data.d;
            if (user["id"] == null) {
                ShowDialog("User does not exist.", 100, 300, 'Message', 10, '<span class="icon-info"></span>',true);
            }
            else {
                
                $("#txtEmpId").val(user["employee_id"]);
                $("#txtEmpId").prop("disabled", true);
                
                $("#DDLStaffType").val(user["userrole_id"]);
                if (user["userrole_id"] == 1 || user["userrole_id"] == 2 || user["userrole_id"] == 3 || user["userrole_id"] == 4 || user["userrole_id"] == 6)
                {
                    $("#DivRtmu").hide();
                    $("#DivRtmo").hide();
                }
                if (user["userrole_id"] == 5 ) {
                    $("#DivRtmu").show();
                    $("#DivRtmo").hide();
                    $("#DDLRtmou").val(user["rtmu_id"]);
                    
                }
                if (user["userrole_id"] == 7) {
                    GetRTMO();
                    $("#DivRtmo").show();
                    $("#DivRtmu").hide();
                    $("#DDLRtmo").val(user["rtmo_id"]);
                  
                }

                
               
                $("#txtName").val(user["name"]);
                $("#txtEmail").val(user["email"]);
                $("#txtPhone").val(user["phone_no"]);
            }
        }
    });
}

$("#DDLStaffType").change(function () {
    
    var salesstaff = $("#DDLStaffType").val()
    if (salesstaff == '7')
        {
        $("#DivRtmo").show();
        $("#DivRtmu").hide();
        GetRTMO();
    }
    if (salesstaff == '5') {
        $("#DivRtmu").show();
        $("#DivRtmo").hide();
        $("#DDLRtmo").html('');
        $('#DDLRtmo').append('<option value="">' + 'select' + '</option>');
    }
    if (salesstaff == '6') {
        $("#DivRtmu").show();
        $("#DivRtmo").hide();
        $("#DDLRtmo").html('');
        $('#DDLRtmo').append('<option value="">' + 'select' + '</option>');
    }
    if (salesstaff == '1') {
        $("#DivRtmu").hide();
        $("#DivRtmo").hide();
        $("#DDLRtmo").html('');
        $('#DDLRtmo').append('<option value="">' + 'select' + '</option>');
    }
    if (salesstaff == '2') {
        $("#DivRtmu").hide();
        $("#DivRtmo").hide();
        $("#DDLRtmo").html('');
        $('#DDLRtmo').append('<option value="">' + 'select' + '</option>');
    }
    if (salesstaff == '3') {
        $("#DivRtmu").hide();
        $("#DivRtmo").hide();
        $("#DDLRtmo").html('');
        $('#DDLRtmo').append('<option value="">' + 'select' + '</option>');
    }
    if (salesstaff == '4') {
        $("#DivRtmu").hide();
        $("#DivRtmo").hide();
        $("#DDLRtmo").html('');
        $('#DDLRtmo').append('<option value="">' + 'select' + '</option>');
    }


    
        
});



function GetRTMO() {

    $.ajax({
        url: 'AddUser.aspx/GetRTMO',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLRtmo').append('<option value="' + k + '">' + v + '</option>');
                });
            });
        }
    });
}
