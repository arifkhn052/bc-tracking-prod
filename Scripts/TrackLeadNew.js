﻿$("#A7").click(function () {
    window.location.href = "Home.aspx";
});

$(document).ready(function () {
    $("#divlost").hide();
    $("#divjunk").show();

    var leadstatus = $("#hdnStatus").val();
    tabing();
    $("#UL2").hide();
    var Pid = getParameterByName('pid');
    
    if (Pid != "1") {
   
        $("#UL2").hide();
        if (leadstatus == "Disbursed") {
            $("#DDLStatus").prop("disabled", true);
            $("#chckMeeting").prop("disabled", true);

            // hide controls
            if ($("#hdnStaffType").val() != 'Admin') {
                $("#DDLNewEnhance").prop("disabled", true);
                $("#Addproduct").prop("disabled", true);
                $("#txtDateOfAction").prop("disabled", true);
                $("#txtComments").prop("disabled", true);
                $("#btnSubmit").prop("disabled", true);
            }
            else {
                $("#DDLNewEnhance").prop("disabled", false);
                $("#Addproduct").prop("disabled", false);
                $("#txtDateOfAction").prop("disabled", false);
                $("#txtComments").prop("disabled", false);
                $("#btnSubmit").prop("disabled", false);
            }

        }
        else {
            $("#DDLStatus").prop("disabled", false);
            $("#chckMeeting").prop("disabled", false);

            
        }
    }
    else {
        if (leadstatus != "To Initiate") {
            $("#UL2").hide();
            $("#lblMsg").show();
        }
        else {
            $("#lblMsg").hide();
            $("#UL2").show();
        }
    }
    //NumbersToWords(1911111111);

    //alert(num2words.numberToWords(300000000000).replace("Zero",""));
    GetProductList();
    GetUserDetail($("#hdnEmpId").val());
    GetIndustryMajor();
    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
        (('' + month).length < 2 ? '0' : '') + month + '-' +
        (('' + day).length < 2 ? '0' : '') + day;

    $("#txtDateOfAction").prop("readonly", true);
    $("#txtDateOfAction").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w" });
    
    $('#dpDateOfAction input').on("keydown", function (e) { e.preventDefault(); });

    $("#txtAddMeetingMeetDate").prop("readonly", true);
    $('#txtAddMeetingMeetDate')
        .datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w"
        });
    $('#divaddMeetingMeetDate input').on("keydown", function (e) { e.preventDefault(); });
    
    $("#txtNextAppointment").prop("readonly", true);
    $("#txtNextAppointment").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w" });

    $('#divNextAppointment input').on("keydown", function (e) { e.preventDefault(); });

    $("#txtAddMeetingMeetDate").prop("readonly", true);
    $('#txtAddMeetingMeetDate')
        .datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w"
        });

    $("#txtAddMeetingNextAppointmentDate").prop("readonly", true);
    $('#txtAddMeetingNextAppointmentDate').datepicker({
        dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w"
    });
    $('#divaddMeetingNextAppointmentDate input').on("keydown", function (e) { e.preventDefault(); });
    $("#_page_lead_details").show();
    
})


function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
}
function GetProductList() {

    $.ajax({
        url: 'AddLead.aspx/GetProductList',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLProductSold0').append('<option value="' + k + '">' + k + '</option>');
                    $('#DDLProductSold1').append('<option value="' + k + '">' + k + '</option>');
                    $('#DDLProductSold2').append('<option value="' + k + '">' + k + '</option>');
                    $('#DDLProductSold3').append('<option value="' + k + '">' + k + '</option>');
                    $('#DDLProductSold4').append('<option value="' + k + '">' + k + '</option>');
                    $('#DDLProductSold5').append('<option value="' + k + '">' + k + '</option>');
                    $('#DDLProductSold6').append('<option value="' + k + '">' + k + '</option>');
                    $('#DDLProductSold7').append('<option value="' + k + '">' + k + '</option>');
                    $('#DDLProductSold8').append('<option value="' + k + '">' + k + '</option>');
                    $('#DDLProductSold9').append('<option value="' + k + '">' + k + '</option>');
                });
            });
        }
    });
}

$("#DDLProductSold0").change(function () {

    if ($("#DDLProductSold0").val() == 'E-DFS' || $("#DDLProductSold0").val() == 'E-VFS')
        $("#outerDivIndustry0").show();
    else
        $("#outerDivIndustry0").hide();

    if ($("#DDLProductSold0").val() == 'Fleet Financing')
        $("#outerDivVendor0").show();

    else
        $("#outerDivVendor0").hide();
});

$("#DDLProductSold1").change(function () {
    if ($("#DDLProductSold1").val() == 'E-DFS' || $("#DDLProductSold1").val() == 'E-VFS')
        $("#outerDivIndustry1").show();

    else
        $("#outerDivIndustry1").hide();

    if ($("#DDLProductSold1").val() == 'Fleet Financing')
        $("#outerDivVendor1").show();

    else
        $("#outerDivVendor1").hide();

    
});

$("#DDLProductSold2").change(function () {
    if ($("#DDLProductSold2").val() == 'E-DFS' || $("#DDLProductSold2").val() == 'E-VFS')
        $("#outerDivIndustry2").show();

    else
        $("#outerDivIndustry2").hide();

    if ($("#DDLProductSold2").val() == 'Fleet Financing')
        $("#outerDivVendor2").show();

    else
        $("#outerDivVendor2").hide();


});

$("#DDLProductSold3").change(function () {
    if ($("#DDLProductSold3").val() == 'E-DFS' || $("#DDLProductSold3").val() == 'E-VFS')
        $("#outerDivIndustry3").show();

    else
        $("#outerDivIndustry3").hide();

    if ($("#DDLProductSold3").val() == 'Fleet Financing')
        $("#outerDivVendor3").show();

    else
        $("#outerDivVendor3").hide();


});

$("#DDLProductSold4").change(function () {
    if ($("#DDLProductSold4").val() == 'E-DFS' || $("#DDLProductSold4").val() == 'E-VFS')
        $("#outerDivIndustry4").show();

    else
        $("#outerDivIndustry4").hide();

    if ($("#DDLProductSold4").val() == 'Fleet Financing')
        $("#outerDivVendor4").show();

    else
        $("#outerDivVendor4").hide();


});

$("#DDLProductSold5").change(function () {
    if ($("#DDLProductSold5").val() == 'E-DFS' || $("#DDLProductSold5").val() == 'E-VFS')
        $("#outerDivIndustry5").show();

    else
        $("#outerDivIndustry5").hide();

    if ($("#DDLProductSold5").val() == 'Fleet Financing')
        $("#outerDivVendor5").show();

    else
        $("#outerDivVendor5").hide();


});

$("#DDLProductSold6").change(function () {
    if ($("#DDLProductSold6").val() == 'E-DFS' || $("#DDLProductSold6").val() == 'E-VFS')
        $("#outerDivIndustry6").show();

    else
        $("#outerDivIndustry6").hide();

    if ($("#DDLProductSold6").val() == 'Fleet Financing')
        $("#outerDivVendor6").show();

    else
        $("#outerDivVendor6").hide();


});

$("#DDLProductSold7").change(function () {
    if ($("#DDLProductSold7").val() == 'E-DFS' || $("#DDLProductSold7").val() == 'E-VFS')
        $("#outerDivIndustry7").show();

    else
        $("#outerDivIndustry7").hide();

    if ($("#DDLProductSold7").val() == 'Fleet Financing')
        $("#outerDivVendor7").show();

    else
        $("#outerDivVendor7").hide();


});

$("#DDLProductSold8").change(function () {
    if ($("#DDLProductSold8").val() == 'E-DFS' || $("#DDLProductSold8").val() == 'E-VFS')
        $("#outerDivIndustry8").show();

    else
        $("#outerDivIndustry8").hide();

    if ($("#DDLProductSold8").val() == 'Fleet Financing')
        $("#outerDivVendor8").show();

    else
        $("#outerDivVendor8").hide();


});

$("#DDLProductSold9").change(function () {
    if ($("#DDLProductSold9").val() == 'E-DFS' || $("#DDLProductSold9").val() == 'E-VFS')
        $("#outerDivIndustry9").show();

    else
        $("#outerDivIndustry9").hide();

    if ($("#DDLProductSold9").val() == 'Fleet Financing')
        $("#outerDivVendor9").show();

    else
        $("#outerDivVendor9").hide();


});

$(document).on('change', '[type=checkbox]', function (e) {
    var a = '#txt' + $(this).attr('id');
    if (a.indexOf('Type') > 0) {
        $(a).toggle();
    }
    if (a.indexOf('chckMeeting') > 0)
        $("#meetWarning").toggle();
});


//$(document).on('click', "#btnAddProduct", function (e) {
//    $("input[type='checkbox']:checked").each(function (e) {
//        var txtBoxId = '#txt' + $(this).attr('id');
//        var txtBoxVal = $(txtBoxId).val();

//        if (txtBoxId.indexOf('Type') > 0) {
//            var leadId = getParameterByName('lead_id');
//            if (txtBoxId.indexOf('Type1') > 0 && txtBoxVal == "") {
//                ShowDialog('Please fill the values required for the product selected', 100, 300, 'Message', 10, '<span class="icon-info"></span>',false);
//                return false;
//            }
//            $.ajax({
//                url: 'TrackLead.aspx/SaveCrossSellProductSold',
//                type: "POST",
//                dataType: "json",
//                data: "{'product': '" + $(this).attr('id') + "','value': '" + txtBoxVal + "','leadId' : '" + leadId + "'}",
//                contentType: "application/json; charset=utf-8",
//                success: function (data) {
//                    var messageContent = "<center><h1>Thank You</h1><h6>" + data.d + " </h6>" +
//                           "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
//                    ShowDialog(messageContent, 100, 300, 'Message', 10, '<span class="icon-info"></span>',true,window.location.href);
//                }
//            });
//        }
//    });
//});


var errorMessage = '';

$("#DDLStatus").change(function () {
    
  
    var leadId = getParameterByName('Customer_id');
    var ddlstatus = $("#DDLStatus").val();
    $.ajax({
        url: 'TrackLead.aspx/CheckPrevStageDate',
        type: "POST",
        dataType: "json",
        data: "{'leadId': '" + leadId + "','stage' : '" + $("#DDLStatus option:selected").text() + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#hdnPrevStage").val(data.d["Stage"]);
            $("#hdnPrevActionDate").val(data.d["date"]);
        }
    });
    $("#chckMeeting").prop('checked', false);
    $("#divNextAppointmentDate").hide();

    if (ddlstatus == "0") {
        $("#divCif").show();
    }
    else 
    {
        $("#divCif").hide();
       
    }

    if (ddlstatus == "1") {
        $("#divConverted").show();
    }
    else{
        $("#divConverted").hide();
   
    }
    if (ddlstatus == "2") {
     
        $("#divlost").show();
    }
    else
    {
        $("#divlost").hide();
    }
    if (ddlstatus == "3") {
        $("#divjunk").show();
    }
    else {
      
        $("#divjunk").hide();
}
    if (ddlstatus == "4") {
        $("#divNewEnhancement").show();}
    else
    {
        $("#divNewEnhancement").hide();
       
    }

   
});

$("#chckMeeting").change(function () {
    $("#divNextAppointmentDate").toggle(this.checked);
});

var errorMsg;
function validate() {
    var status = $("#DDLStatus").val();
    var addMeeting = $("#chckMeeting").val();

    var cifno = $("#txtCif").val();

    var newEnhance = $("#DDLNewEnhance").val();

    var productSold0 = $("#DDLProductSold0").val();
    var account0 = $("#txtAccount0").val();
    var amount0 = $("#txtAmount0").val();
    var industryMajor0 = $('#DDLIndustryMajor0').val();

    var productSold1 = $("#DDLProductSold1").val();
    var account1 = $("#txtAccount1").val();
    var amount1 = $("#txtAmount1").val();
    var industryMajor1 = $('#DDLIndustryMajor1').val();

    var currentStatus = $("#currentStatus").val();

    var nextAppointmentDate = $("#txtNextAppointment").val();

    var actionDate = $("#txtDateOfAction").val();

    var comment = $("#txtComments").val();

    var vendorName0 = $("#DDLVendor0").val();

    var vendorName1 = $("#DDLVendor1").val();

    errorMsg = '';
    var result = true;

    if (status == "8" && cifno == "") {
        errorMsg = errorMsg + "<br/>Please enter cif no.";
        $("#divCifno").addClass("form-group has-error");
        result = false;
    }

    else {
        $("#divCifno").removeClass("form-group has-error");
    }

    if (status == "-1") {
        errorMsg = errorMsg + "<br/>Please select status.";
        $("#divStatus").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divStatus").removeClass("form-group has-error");
    }

    if (status == 4 && newEnhance == "-1") {
        errorMsg = errorMsg + "<br/>Please select new/enhancment.";
        $("#divNewEnhance").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divNewEnhance").removeClass("form-group has-error");
    }

    if (status > 3 && productSold0 == "-1") {
        errorMsg = errorMsg + "<br/>Please select product.";
        $("#divProductSold0").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divProductSold0").removeClass("form-group has-error");
    }

    if (status > 3 && (amount0 == "" || isNaN(amount0))) {
        errorMsg = errorMsg + "<br/>Please enter amount.";
        $("#abc").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#abc").removeClass("form-group has-error");
    }

    if (status == 8 && (account0 == "" || account0.length != 11)) {
        errorMsg = errorMsg + "<br/>Please enter account no.";
        $("#divAccount0").addClass("form-group has-error");
        result = false;
    }
    else if (account0 != "" && account0.length != 11) {
        errorMsg = errorMsg + "<br/>Please enter valid account no.";
        $("#divAccount0").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divAccount0").removeClass("form-group has-error");
    }



    if (status > 3 && productSold1 != "-1" && (amount1 == "" || isNaN(amount1))) {
        errorMsg = errorMsg + "<br/>Please enter amount.";
        $("#divAmount1").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divAmount1").removeClass("form-group has-error");
    }

    if (status > 3 && productSold1 != "-1" && status == 8 && (account1 == "" || account1.length != 11)) {
        errorMsg = errorMsg + "<br/>Please select product.";
        $("#divAccount1").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divAccount1").removeClass("form-group has-error");
    }

    if (actionDate == "") {
        errorMsg = errorMsg + "<br/>Please select date.";
        $("#divActionDate").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divActionDate").removeClass("form-group has-error");
        var aDate = new Date(actionDate.substring(6, 10) + '-' + actionDate.substring(3, 5) + '-' + actionDate.substring(0, 2));
        var bDate = new Date($("#hdnPrevActionDate").val().substring(6, 10) + '-' + $("#hdnPrevActionDate").val().substring(3, 5) + '-'
            + $("#hdnPrevActionDate").val().substring(0, 2))
            
       

        errorMessage = "";
        if (aDate <= bDate) {
            result = false;
            $("#divActionDate").removeClass("form-group has-error");
            errorMessage =  $("#DDLStatus option:selected").text() + " Date should be greater than " + $("#hdnPrevStage").val() + " date";
        }

    }

    if ((productSold0 == "E-DFS" || productSold0 == "E-VFS") && industryMajor0 == "-1") {
        errorMsg = errorMsg + "<br/>Please select industry major.";
        $("#divIndustryMajor0").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divIndustryMajor0").removeClass("form-group has-error");
    }

    if ((productSold0 == "Fleet Financing") && vendorName0 == "-1") {
        errorMsg = errorMsg + "<br/>Please enter vendor name.";
        $("#divVendor0").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divVendor0").removeClass("form-group has-error");
    }

    if ((productSold1 == "E-DFS" || productSold1 == "E-VFS") && industryMajor1 == "-1") {
        errorMsg = errorMsg + "<br/>Please select industry major.";
        $("#divIndustryMajor1").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divIndustryMajor1").removeClass("form-group has-error");
    }

    if ((productSold1 == "Fleet Financing") && vendorName1 == "-1") {
        errorMsg = errorMsg + "<br/>Please enter vendor name.";
        $("#divVendor1").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divVendor1").removeClass("form-group has-error");
    }

    if ((currentStatus != "Sanctioned" && currentStatus != "Disbursed") && status == 8) {
        result = false;
        errorMessage = "Disbursal can be done only after sanctioning.";
    }

    if ((currentStatus != "Submitted for sanctioning" && currentStatus != "Sanctioned" && currentStatus != "Disbursed") && status == 7) {
        result = false;
        errorMessage = "Sanctioning can be done only after submitting for sanctioning.";
    }

    if ((currentStatus != "Submitted for sanctioning" && currentStatus != "Submitted for QC" && currentStatus != "Sanctioned" && currentStatus != "Disbursed") && status == 6) {
        result = false;
        errorMessage = "Submitting for sanctioning can be done only after submitting for QC.";
    }

    if ((currentStatus != "Converted lead" && currentStatus != "Submitted for QC" && currentStatus != "Sanctioned" && currentStatus != "Disbursed" && currentStatus != "Submitted for sanctioning") && status == 5) {
        result = false;
        errorMessage = "Submitting for QC can be done only after converting the lead.";
    }

    if (comment == "") {
        errorMsg = errorMsg + "<br/>Please enter comment.";
        $("#divComments").addClass("form-group has-error");
        result = false;
    }

    else {
        $("#divComments").removeClass("form-group has-error");
    }
    return result;

}

$("#Addproduct").click(function () {

    for (i = 3; i < 11; i++)
    {
        var a=(i-1);
        if ($("#DDLProductSold" + a).val() == "-1")
        {
            $("#divproduct" + i).show();
            break;
        }
        
    }

});

function reply_click(clicked_id) {
    hideDelDiv(clicked_id);
};

function hideDelDiv(id) {
    var idVar = id.substring(9);
    $("#DDLProductSold" + (idVar - 1)).find('option:first').attr('selected', 'selected');
    $("#DDLIndustryMajor" + (idVar - 1)).find('option:first').attr('selected', 'selected');
    $("#DDLVendor" + (idVar - 1)).val('');
    $("#txtAccount" + (idVar - 1)).val('');
    $("#txtAmount" + (idVar - 1)).val('');
    $("#divproduct" + idVar).hide();
    
};

$("#btnSubmit").click(function () {
    
    if (validate() == true) {
        var leadId = getParameterByName('lead_id');

        var status = $("#DDLStatus option:selected").text();
        var cifnumber = $("#txtCif").val();

        var newEnhance = $("#DDLNewEnhance").val();

        var productSold0 = $("#DDLProductSold0").val();
        var account0 = $("#txtAccount0").val();
        var amount0 = $("#txtAmount0").val();
        var industryMajor0 = $("#DDLIndustryMajor0").val();
        var vendorName0 = $("#DDLVendor0").val();

        var productSold1 = $("#DDLProductSold1").val();
        var account1 = $("#txtAccount1").val();
        var amount1 = $("#txtAmount1").val();
        var industryMajor1 = $("#DDLIndustryMajor1").val();
        var vendorName1 = $("#DDLVendor1").val();

        var productSold2 = $("#DDLProductSold2").val();
        var account2 = $("#txtAccount2").val();
        var amount2 = $("#txtAmount2").val();
        var industryMajor2 = $("#DDLIndustryMajor2").val();
        var vendorName2 = $("#DDLVendor2").val();

        var productSold3 = $("#DDLProductSold3").val();
        var account3 = $("#txtAccount3").val();
        var amount3 = $("#txtAmount3").val();
        var industryMajor3 = $("#DDLIndustryMajor3").val();
        var vendorName3 = $("#DDLVendor3").val();

        var productSold4 = $("#DDLProductSold4").val();
        var account4 = $("#txtAccount4").val();
        var amount4 = $("#txtAmount4").val();
        var industryMajor4 = $("#DDLIndustryMajor4").val();
        var vendorName4 = $("#DDLVendor4").val();

        var productSold5 = $("#DDLProductSold5").val();
        var account5 = $("#txtAccount5").val();
        var amount5 = $("#txtAmount5").val();
        var industryMajor5 = $("#DDLIndustryMajor5").val();
        var vendorName5 = $("#DDLVendor5").val();

        var productSold6 = $("#DDLProductSold6").val();
        var account6 = $("#txtAccount6").val();
        var amount6 = $("#txtAmount6").val();
        var industryMajor6 = $("#DDLIndustryMajor6").val();
        var vendorName6 = $("#DDLVendor6").val();

        var productSold7 = $("#DDLProductSold7").val();
        var account7 = $("#txtAccount7").val();
        var amount7 = $("#txtAmount7").val();
        var industryMajor7 = $("#DDLIndustryMajor7").val();
        var vendorName7 = $("#DDLVendor7").val();

        var productSold8 = $("#DDLProductSold8").val();
        var account8 = $("#txtAccount8").val();
        var amount8 = $("#txtAmount8").val();
        var industryMajor8 = $("#DDLIndustryMajor8").val();
        var vendorName8 = $("#DDLVendor8").val();

        var productSold9 = $("#DDLProductSold9").val();
        var account9 = $("#txtAccount9").val();
        var amount9 = $("#txtAmount9").val();
        var industryMajor9 = $("#DDLIndustryMajor9").val();
        var vendorName9 = $("#DDLVendor9").val();

        var nextAppointmentDate = $("#txtNextAppointment").val();

        var actionDate = $("#txtDateOfAction").val();
        var addMeeting = $('input[id="chckMeeting"]:checked').length > 0 ? 'on' : 'off';

        $.ajax({
            url: 'TrackLead.aspx/SaveProcessDetails',
            type: "POST",
            dataType: "json",
            data: "{'leadId': '" + leadId + "','status': '" + status + "','chkMeeting' : '" + addMeeting + "','nextAppointment' : '" + nextAppointmentDate +
                "','newEnhance' : '" + newEnhance +
                "','product0' : '" + productSold0 + "','account0' : '" + account0 + "','amount0' : '" + amount0 + "','product1' : '" + productSold1 + "','account1' : '" + account1 + "','amount1' : '" + amount1 +
                "','product2' : '" + productSold2 + "','account2' : '" + account2 + "','amount2' : '" + amount2 + "','product3' : '" + productSold3 + "','account3' : '" + account3 + "','amount3' : '" + amount3 +
                "','product4' : '" + productSold4 + "','account4' : '" + account4 + "','amount4' : '" + amount4 + "','product5' : '" + productSold5 + "','account5' : '" + account5 + "','amount5' : '" + amount5 +
                "','product6' : '" + productSold6 + "','account6' : '" + account6 + "','amount6' : '" + amount6 + "','product7' : '" + productSold7 + "','account7' : '" + account7 + "','amount7' : '" + amount7 +
                "','product8' : '" + productSold8 + "','account8' : '" + account8 + "','amount8' : '" + amount8 + "','product9' : '" + productSold9 + "','account9' : '" + account9 + "','amount9' : '" + amount9 +
                "','actionDate' : '" + actionDate + "','comments' : '" + $("#txtComments").val() +
                "','industryMajor0' : '" + industryMajor0 + "','industryMajor1' : '" + industryMajor1 + "','industryMajor2' : '" + industryMajor2 + "','industryMajor3' : '" + industryMajor3 +
                "','industryMajor4' : '" + industryMajor4 + "','industryMajor5' : '" + industryMajor5 + "','industryMajor6' : '" + industryMajor6 + "','industryMajor7' : '" + industryMajor7 +
                "','industryMajor8' : '" + industryMajor8 + "','industryMajor9' : '" + industryMajor9 + "','vendorName0' : '" + vendorName0 + "','vendorName1' : '" + vendorName1 +
                "','vendorName2' : '" + vendorName2 + "','vendorName3' : '" + vendorName3 + "','vendorName4' : '" + vendorName4 + "','vendorName5' : '" + vendorName5 +
                "','vendorName6' : '" + vendorName6 + "','vendorName7' : '" + vendorName7 + "','vendorName8' : '" + vendorName8 + "','vendorName9' : '" + vendorName9 + "','cifNo' : '" + cifnumber + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {


                var messageContent = "<center><h1>Thank You</h1><h6>" + data.d + " </h6>" +
                       "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                ShowDialog(messageContent, 100, 300, 'Message', 10, '<span class="icon-info"></span>',true,window.location.href);
            }
        });

    }
    else {

        ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    }

});

function ValidateMeeting() {

    var meetingDate = $("#txtAddMeetingMeetDate").val();

    var result = true;

    if (meetingDate == "") {
        $("#divaddMeetingMeetDate").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divaddMeetingMeetDate").removeClass("form-group has-error");
    }

    return result;
}


$("#btnAddMeeting").click(function () {
    if (ValidateMeeting() == true) {
        var leadId = getParameterByName('lead_id');
        var meetingDate = $("#txtAddMeetingMeetDate").val();

        var nextAppointmentDate = $("#txtAddMeetingNextAppointmentDate").val();

        //var actionDate = $("#txtDateOfAction").val();\
	if(meetingDate !="")
        meetingDate = meetingDate.substring(6, 10) + '-' + meetingDate.substring(3, 5) + '-' + meetingDate.substring(0, 2);

	if(nextAppointmentDate !="")
        nextAppointmentDate = nextAppointmentDate.substring(6, 10) + '-' + nextAppointmentDate.substring(3, 5) + '-' + nextAppointmentDate.substring(0, 2);

        $.ajax({
            url: 'TrackLead.aspx/SaveMeetingDetails',
            type: "POST",
            dataType: "json",
            data: "{'meetingDate': '" + meetingDate + "','nextAppointment': '" + nextAppointmentDate + "','comments': '" + $("#txtMeetingDescription").val()
                + "','leadId': '" + leadId + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {

                ShowDialog(data.d, 100, 300, 'Message', 10, '<span class="icon-info"></span>', true, window.location.href);
            }
        });

    }
    else {
        ShowDialog("Please enter all the values correctly.", 100, 300, 'Message', 10, '<span class="icon-info"></span>',false);
    }

});

$("#txtAmount0").focusout(function (event) {

    var num2words = new NumberToWords();
    num2words.setMode("indian");
    if (event.which == 13) {
        event.preventDefault();
    }
    $("#lblAmount0").html(replaceAll("Zero", "", num2words.numberToWords($("#txtAmount0").val())));
});

$("#txtAmount1").focusout(function (event) {
    var num2words = new NumberToWords();
    num2words.setMode("indian");
    if (event.which == 13) {
        event.preventDefault();
    }
    $("#lblAmount1").html(replaceAll("Zero", "", num2words.numberToWords($("#txtAmount1").val())));
});



function NumbersToWords(inputNumber) {
    var inputNo = inputNumber;

    if (inputNo == 0)
        return "Zero";

    var numbers = new Array(4);
    var first = 0;
    var u, h, t;
    var sb = '';

    if (inputNo < 0) {
        sb += "Minus ";
        inputNo = -inputNo;
    }

    var words0 = ["", "One ", "Two ", "Three ", "Four ",
    "Five ", "Six ", "Seven ", "Eight ", "Nine "];
    var words1 = ["Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
    "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen "];
    var words2 = ["Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
    "Seventy ", "Eighty ", "Ninety "];
    var words3 = ["Thousand ", "Lakh ", "Crore "];

    numbers[0] = parseInt(inputNo % 1000); // units
    numbers[1] = parseInt(inputNo / 1000);
    numbers[2] = parseInt(inputNo / 100000);
    numbers[1] = parseInt(numbers[1] - 100 * numbers[2]); // thousands
    numbers[3] = parseInt(inputNo / 10000000); // crores
    numbers[2] = parseInt(numbers[2] - 100 * numbers[3]); // lakhs

    for (var i = 3; i > 0; i--) {
        if (numbers[i] != 0) {
            first = i;
            break;
        }
    }

    for (var i = first; i >= 0; i--) {
        if (numbers[i] == 0) continue;
        u = parseInt(numbers[i] % 10); // ones
        t = parseInt(numbers[i] / 10);
        h = parseInt(numbers[i] / 100); // hundreds
        t = parseInt(t - 10 * h); // tens
        if (h > 0) sb += words0[h] + "Hundred ";
        if (u > 0 || t > 0) {
            if (h > 0 || i == 0) sb += "and ";
            if (t == 0)
                sb += words0[u];
            else if (t == 1)
                sb += words1[u];
            else
                sb += words2[t - 2] + words0[u];
        }
        if (i != 0) sb += words3[i - 1];
    }
    //alert(sb);
    return sb;
}


function GetUserDetail(EmpId) {
    $.ajax({
        url: 'AddUser.aspx/GetUserDetail',
        type: "POST",
        dataType: "json",
        data: "{'empId': '" + $("#hdnEmpId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            var user = data.d;
            if (user["id"] == null) {
                ShowDialog("User does not exist.", 100, 300, 'Message', 10, '<span class="icon-info"></span>',true);
            }
            else {
                if (user["staff_type"] != "Sale Staff" && user["staff_type"] != "Admin") {
                    $("#tabProcess").hide();
                    $("#tabAddMeeting").hide();
                    $("#tabAddProduct").hide();
                }

            }
        }
    });
}

function GetVendorName() {
    $.ajax({
        url: 'TrackLead.aspx/GetVendorName',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLVendor0').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLVendor1').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLVendor2').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLVendor3').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLVendor4').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLVendor5').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLVendor6').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLVendor7').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLVendor8').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLVendor9').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetIndustryMajor() {
    $.ajax({
        url: 'AddLead.aspx/GetIndustryMajor',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLIndustryMajor0').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLIndustryMajor1').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLIndustryMajor2').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLIndustryMajor3').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLIndustryMajor4').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLIndustryMajor5').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLIndustryMajor6').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLIndustryMajor7').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLIndustryMajor8').append('<option value="' + v + '">' + v + '</option>');
                    $('#DDLIndustryMajor9').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}


$("#liDeleteLead").click(function () {
    
    var leadid = $("#hdnLeadId").val();
    var holderid = $('#hdnAssignId').val();
    var holderemail = $('#hdnEmployeeid').val();
    var checkstr = confirm('Are you sure you want to delete this lead?');
    if (checkstr == true) {
        $.ajax({
            url: 'TrackLead.aspx/LeadDelete',
            type: "POST",
            dataType: "json",
            data: "{'leadId': '" + leadid + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "Try again. Lead deletion failed.")
                    ShowDialog(data.d, 400, 600, 'Message', 10, '<span class="icon-info"></span>', false);
                else {
                    var messageContent = "<center><h1>Thank You</h1><hr> <p><h2>" + leadid + " </h2></p><br/>" +
                        "<p><h6>" + data.d + " </h6></p>" +
              "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                    ShowDialog(messageContent, 400, 600, 'Message', 10, '<span class="icon-info"></span>', true);
                }

            }
        });
    }
    else {
        return false;
    }
        
    
});

var tabing = function () {
    var nav = $(".tabs"),
     slideCon = $(".frames");
    slideCon.find(".frame").hide();
    var id = nav.find(".selected").attr("href");
    $(id).show();
    $('a[href*="' + "#" + id + '"]').addClass("selected");
    nav.on("click", "li a", function (e) {
        e.preventDefault();
        var $this = $(this);
        var parentItem = $($this.attr("href"));
        var currentTab = $this.attr("href");
        if (parentItem.is(":visible")) {
            return;
        }
        else {
            slideCon.find(".frame").hide();
            parentItem.show();
            $this.addClass("selected").closest("li").siblings().find("a").removeClass("selected");
            var leadId = getParameterByName('lead_id');
            if (currentTab == "#_page_process") {
                $.ajax({
                    url: 'TrackLead.aspx/GetProcessDetails',
                    type: "POST",
                    dataType: "json",
                    data: "{'leadId': '" + leadId + "'}",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (data) {
                      
                        var obj = JSON.parse(data.d);
                        console.log(obj);
                        if ('CurrentStatus' in obj) {
                            $("#DDLStatus option:contains(" + obj['CurrentStatus'] + ")").attr('selected', 'selected');
                            $("#currentStatus").val(obj['CurrentStatus']);
                        }
                        if ('new_enhance' in obj)
                        {
                            $("#DDLNewEnhance option:contains(" + obj['new_enhance'] + ")").attr('selected', 'selected');
                            $("#DDLNewEnhance").val(obj['new_enhance']);
                        }
                        if ($("#DDLStatus").val() > 3) {
                                $("#divNewEnhancement").show();
                        }

                        if ($("#DDLStatus").val() > 3)
                            $("#divConverted").show();

                        if ('ProductSold0' in obj)
                            $("#DDLProductSold0").val(obj['ProductSold0']);
                        if ('ac_no0' in obj)
                            $("#txtAccount0").val(obj['ac_no0']);
                        if ('amount0' in obj)
                            $("#txtAmount0").val(obj['amount0']);

                        if ('ProductSold1' in obj)
                            $("#DDLProductSold1").val(obj['ProductSold1']);
                        if ('ac_no1' in obj)
                            $("#txtAccount1").val(obj['ac_no1']);
                        if ('amount1' in obj)
                            $("#txtAmount1").val(obj['amount1']);

                        if ('ProductSold2' in obj) {
                            $("#divproduct3").show();
                            $("#DDLProductSold2").val(obj['ProductSold2']);
                        }
                        if ('ac_no2' in obj)
                            $("#txtAccount2").val(obj['ac_no2']);
                        if ('amount2' in obj)
                            $("#txtAmount2").val(obj['amount2']);

                        if ('ProductSold3' in obj) {
                            $("#divproduct4").show();
                            $("#DDLProductSold3").val(obj['ProductSold3']);
                        }
                        if ('ac_no3' in obj)
                            $("#txtAccount3").val(obj['ac_no3']);
                        if ('amount3' in obj)
                            $("#txtAmount3").val(obj['amount3']);

                        if ('ProductSold4' in obj) {
                            $("#divproduct5").show();
                            $("#DDLProductSold4").val(obj['ProductSold4']);
                        }
                        if ('ac_no4' in obj)
                            $("#txtAccount4").val(obj['ac_no4']);
                        if ('amount4' in obj)
                            $("#txtAmount4").val(obj['amount4']);

                        if ('ProductSold5' in obj) {
                            $("#divproduct6").show();
                            $("#DDLProductSold5").val(obj['ProductSold5']);
                        }
                        if ('ac_no5' in obj)
                            $("#txtAccount5").val(obj['ac_no5']);
                        if ('amount5' in obj)
                            $("#txtAmount5").val(obj['amount5']);

                        if ('ProductSold6' in obj) {
                            $("#divproduct7").show();
                            $("#DDLProductSold6").val(obj['ProductSold6']);
                        }
                        if ('ac_no6' in obj)
                            $("#txtAccount6").val(obj['ac_no6']);
                        if ('amount6' in obj)
                            $("#txtAmount6").val(obj['amount6']);

                        if ('ProductSold7' in obj) {
                            $("#divproduct8").show();
                            $("#DDLProductSold7").val(obj['ProductSold7']);
                        }
                        if ('ac_no7' in obj)
                            $("#txtAccount7").val(obj['ac_no7']);
                        if ('amount7' in obj)
                            $("#txtAmount7").val(obj['amount7']);

                        if ('ProductSold8' in obj) {
                            $("#divproduct9").show();
                            $("#DDLProductSold8").val(obj['ProductSold8']);
                        }
                        if ('ac_no8' in obj)
                            $("#txtAccount8").val(obj['ac_no8']);
                        if ('amount8' in obj)
                            $("#txtAmount8").val(obj['amount8']);

                        if ('ProductSold9' in obj) {
                            $("#divproduct10").show();
                            $("#DDLProductSold9").val(obj['ProductSold9']);
                        }
                        if ('ac_no9' in obj)
                            $("#txtAccount9").val(obj['ac_no9']);
                        if ('amount9' in obj)
                            $("#txtAmount9").val(obj['amount9']);

                        if (obj['ProductSold0'] == 'E-DFS' || obj['ProductSold0'] == 'E-VFS')
                            $('#outerDivIndustry0').show();
                        if ('industry_major0' in obj)
                            $("#DDLIndustryMajor0").val(obj['industry_major0']);

                        if (obj['ProductSold0'] == 'Fleet Financing')
                            $('#outerDivVendor0').show();
                        if ('vendor_name0' in obj)
                            $("#DDLVendor0").val(obj['vendor_name0']);

                        if (obj['ProductSold1'] == 'E-DFS' || obj['ProductSold1'] == 'E-VFS')
                            $('#outerDivIndustry1').show();
                        if ('industry_major1' in obj)
                            $("#DDLIndustryMajor1").val(obj['industry_major1']);

                        if (obj['ProductSold1'] == 'Fleet Financing')
                            $('#outerDivVendor1').show();
                        if ('vendor_name1' in obj)
                            $("#DDLVendor1").val(obj['vendor_name1']);

                        if (obj['ProductSold2'] == 'E-DFS' || obj['ProductSold2'] == 'E-VFS')
                            $('#outerDivIndustry2').show();
                        if ('industry_major2' in obj)
                            $("#DDLIndustryMajor2").val(obj['industry_major2']);

                        if (obj['ProductSold2'] == 'Fleet Financing')
                            $('#outerDivVendor2').show();
                        if ('vendor_name2' in obj)
                            $("#DDLVendor2").val(obj['vendor_name2']);

                        if (obj['ProductSold3'] == 'E-DFS' || obj['ProductSold3'] == 'E-VFS')
                            $('#outerDivIndustry3').show();
                        if ('industry_major3' in obj)
                            $("#DDLIndustryMajor3").val(obj['industry_major3']);

                        if (obj['ProductSold3'] == 'Fleet Financing')
                            $('#outerDivVendor3').show();
                        if ('vendor_name3' in obj)
                            $("#DDLVendor3").val(obj['vendor_name3']);

                        if (obj['ProductSold4'] == 'E-DFS' || obj['ProductSold4'] == 'E-VFS')
                            $('#outerDivIndustry4').show();
                        if ('industry_major4' in obj)
                            $("#DDLIndustryMajor4").val(obj['industry_major4']);

                        if (obj['ProductSold4'] == 'Fleet Financing')
                            $('#outerDivVendor4').show();
                        if ('vendor_name4' in obj)
                            $("#DDLVendor4").val(obj['vendor_name4']);

                        if (obj['ProductSold5'] == 'E-DFS' || obj['ProductSold5'] == 'E-VFS')
                            $('#outerDivIndustry5').show();
                        if ('industry_major5' in obj)
                            $("#DDLIndustryMajor5").val(obj['industry_major5']);

                        if (obj['ProductSold5'] == 'Fleet Financing')
                            $('#outerDivVendor5').show();
                        if ('vendor_name5' in obj)
                            $("#DDLVendor5").val(obj['vendor_name5']);

                        if (obj['ProductSold6'] == 'E-DFS' || obj['ProductSold6'] == 'E-VFS')
                            $('#outerDivIndustry6').show();
                        if ('industry_major6' in obj)
                            $("#DDLIndustryMajor6").val(obj['industry_major6']);

                        if (obj['ProductSold6'] == 'Fleet Financing')
                            $('#outerDivVendor6').show();
                        if ('vendor_name6' in obj)
                            $("#DDLVendor6").val(obj['vendor_name6']);

                        if (obj['ProductSold7'] == 'E-DFS' || obj['ProductSold7'] == 'E-VFS')
                            $('#outerDivIndustry7').show();
                        if ('industry_major7' in obj)
                            $("#DDLIndustryMajor7").val(obj['industry_major7']);

                        if (obj['ProductSold7'] == 'Fleet Financing')
                            $('#outerDivVendor7').show();
                        if ('vendor_name7' in obj)
                            $("#DDLVendor7").val(obj['vendor_name7']);

                        if (obj['ProductSold8'] == 'E-DFS' || obj['ProductSold8'] == 'E-VFS')
                            $('#outerDivIndustry8').show();
                        if ('industry_major8' in obj)
                            $("#DDLIndustryMajor8").val(obj['industry_major8']);

                        if (obj['ProductSold8'] == 'Fleet Financing')
                            $('#outerDivVendor8').show();
                        if ('vendor_name8' in obj)
                            $("#DDLVendor8").val(obj['vendor_name8']);

                        if (obj['ProductSold9'] == 'E-DFS' || obj['ProductSold9'] == 'E-VFS')
                            $('#outerDivIndustry9').show();
                        if ('industry_major9' in obj)
                            $("#DDLIndustryMajor9").val(obj['industry_major9']);

                        if (obj['ProductSold9'] == 'Fleet Financing')
                            $('#outerDivVendor9').show();
                        if ('vendor_name9' in obj)
                            $("#DDLVendor9").val(obj['vendor_name9']);

                        if ('action_date' in obj)
                            $("#hdnPrevActionDate").val(obj['action_date']);


                        if ($("#DDLStatus").val() > 0) {
                            $("#divCif").show();
                            if ($("#txtCif").val(obj['cif_no']) == "" || $("#txtCif").val(obj['cif_no']) == null) {
                                $("#txtCif").prop('disabled', false);
                            }
                            else {
                                $("#txtCif").val(obj['cif_no']);
                                $("#txtCif").prop('disabled', false);
                            }
                            if ($("#hdnStatus").val() == "Disbursed") {
                                $("#txtCif").prop('disabled', true);
                            }
                            else {
                                $("#txtCif").prop('disabled', false);
                            }
                        }
                        else {
                            $("#divCif").hide();

                        }
                    }
                });
            }
            else if (currentTab == "#_page_feed") {
                var feedTable = $('#tblFeed').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 25,
                    "aLengthMenu": [[25, 30, 45, 60], [25, 30, 45, 60]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,//TODO :- Sorting searching to happen on DB side.
                    "bDestroy": true,
                    "sAjaxSource": "TrackLead.aspx/GetFeed",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblFeed").show();
                                }
                        });
                    }
                });
            }
            else if (currentTab == "#_page_products") {
                var productTable = $('#tblProduct').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "TrackLead.aspx/GetProductSold",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblProduct").show();
                                        }
                        });
                    }
                });

                var crossproductTable = $('#tblCrossSellProduct').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "TrackLead.aspx/GetCrossSellProductSold",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblCrossSellProduct").show();
                                        }
                        });
                    }
                });

                //
            }
            else if (currentTab == "#_page_meeting") {
                var productTable = $('#tblMeeting').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "TrackLead.aspx/GetMeetingList",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblMeeting").show();
                                        }
                        });
                    }
                });
            }

            else if (currentTab == "#_page_add_product") {
                $.ajax({
                    url: 'TrackLead.aspx/GetCrossSellProducts',
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (data) {
                        var content = '<h1 class="tab-head">Add Cross Sell Product</h1><div style="margin-left:15px;margin-bottom:20px;">';
                        $.each(data, function () {
                            $.each(this, function (k, v) {
                                content += '<label class="inline-block"><input type="checkbox" id="' + v + '" /><span class="check" style="margin-left:1em"></span>' + k + '</label>';
                                if (v.indexOf('Type1') > 0) {
                                    content += '<input id="txt' + v + '" style="margin-left:5em;display:none;" placeholder="a/c no" autofocus required />'
                                }
                                content += '<br/><br/>';
                            });
                        });

                        content += '<input type="button" class="btn btn-primary btn-sm" value="Add Product" id="btnAddProduct" /></div>';
                        $("#_page_add_product").html(content);
                    }
                });
            }

        }




    });


}


function NumberToWords() {

    var units = ["Zero", "One", "Two", "Three", "Four", "Five", "Six",
      "Seven", "Eight", "Nine", "Ten"];
    var teens = ["Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen",
      "Sixteen", "Seventeen", "Eighteen", "Nineteen", "Twenty"];
    var tens = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty",
      "Seventy", "Eighty", "Ninety"];

    var othersIndian = ["Thousand", "Lakh", "Crore"];

    var othersIntl = ["Thousand", "Million", "Billion", "Trillion"];

    var INDIAN_MODE = "indian";
    var INTERNATIONAL_MODE = "international";
    var currentMode = INDIAN_MODE;

    var getBelowHundred = function (n) {
        if (n >= 100) {
            return "greater than or equal to 100";
        };
        if (n <= 10) {
            return units[n];
        };
        if (n <= 20) {
            return teens[n - 10 - 1];
        };
        var unit = Math.floor(n % 10);
        n /= 10;
        var ten = Math.floor(n % 10);
        var tenWord = (ten > 0 ? (tens[ten] + " ") : '');
        var unitWord = (unit > 0 ? units[unit] : '');
        return tenWord + unitWord;
    };

    var getBelowThousand = function (n) {
        if (n >= 1000) {
            return "greater than or equal to 1000";
        };
        var word = getBelowHundred(Math.floor(n % 100));

        n = Math.floor(n / 100);
        var hun = Math.floor(n % 10);
        word = (hun > 0 ? (units[hun] + " Hundred ") : '') + word;

        return word;
    };

    return {
        numberToWords: function (n) {
            if (isNaN(n)) {
                return "Not a number";
            };

            var word = '';
            var val;

            val = Math.floor(n % 1000);
            n = Math.floor(n / 1000);

            word = getBelowThousand(val);

            if (this.currentMode == INDIAN_MODE) {
                othersArr = othersIndian;
                divisor = 100;
                func = getBelowHundred;
            } else if (this.currentMode == INTERNATIONAL_MODE) {
                othersArr = othersIntl;
                divisor = 1000;
                func = getBelowThousand;
            } else {
                throw "Invalid mode - '" + this.currentMode
                  + "'. Supported modes: " + INDIAN_MODE + "|"
                  + INTERNATIONAL_MODE;
            };

            var i = 0;
            while (n > 0) {
                if (i == othersArr.length - 1) {
                    word = this.numberToWords(n) + " " + othersArr[i] + " "
                      + word;
                    break;
                };
                val = Math.floor(n % divisor);
                n = Math.floor(n / divisor);
                if (val != 0) {
                    word = func(val) + " " + othersArr[i] + " " + word;
                };
                i++;
            };
            return word;
        },
        setMode: function (mode) {
            if (mode != INDIAN_MODE && mode != INTERNATIONAL_MODE) {
                throw "Invalid mode specified - '" + mode
                  + "'. Supported modes: " + INDIAN_MODE + "|"
                  + INTERNATIONAL_MODE;
            };
            this.currentMode = mode;
        }
    }
}