﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SBI.Web.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
      

        .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
           
        }

        .form-signin-heading {
            font-size: 20px;
            font-weight: 100;
        }

        .form-signin .form-signin-heading, .form-signin .checkbox {
            margin-bottom: 10px;
        }

        .form-signin .checkbox {
            font-weight: normal;
        }

        .form-signin .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

            .form-signin .form-control:focus {
                z-index: 2;
            }

        .form-signin input[type="text"] {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>
    <%--OnClientClick="EncryptPassword();"--%>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1"  runat="Server" >
    <div style="  background-repeat: no-repeat;
         overflow: hidden;
    background: url(images/share-market-traning-courses.jpg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
        margin: -42px -19px -19px -19px;
    border-radius: 10px;
        padding: 30px 0 67px 0;
    ">
    
    <div  class="container"  >
   
        <div class="logo " >
            My Sales
        </div>
        <div class="form-signin well well-lg" >
            <h2 class="form-signin-heading text-center">Sign in</h2>
            <asp:TextBox ID="txtEmpId" placeholder="Enter Employee Id" class="form-control " runat="server" ClientIDMode="Static" autofocus required AutoCompleteType="Disabled"></asp:TextBox>
            <asp:TextBox ID="txtPassword" runat="server" class="form-control" placeholder="Password" ClientIDMode="Static" required AutoCompleteType="Disabled"
                TextMode="Password"></asp:TextBox>
            <label class="checkbox">
                <asp:CheckBox ID="chkPersistCookie" runat="server" AutoPostBack="false" />
                Remember me<br />

            </label>
            <div>
                <a href="forgetpassword.aspx">Forgot password?</a>
                  <div style="margin-bottom: 10px"></div>
            </div>
            <div id="alertMsg" visible="false" runat="server">
                <asp:Label ID="lblAlert" runat="server" Style="color: red"></asp:Label>
                <div style="margin-bottom: 10px"></div>
            </div>
            <asp:Button Text="Login" ID="btnSignIn" runat="server"  CssClass="btn btn-lg btn-primary btn-block"   OnClick="btnSignIn_Click" />
        </div>
    </div>
  </div>
    <script>
        function EncryptPassword() {
            $('#txtPassword').val($.md5($('#txtPassword').val()));
        }
    </script>
</asp:Content>
