﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditCustomer.aspx.cs" Inherits="SBI.Web.EditCustomer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <%--   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">--%>
   <script src="Angularjs/angular15.js"></script>
   <script src="Angularjs/MainModule.js"></script>
   <script src="Angularjs/TrackCustomerLead.js"></script>
   <link href="css/jquery-ui.css" rel="stylesheet" />
   <style>
      .abc {
      margin: 0px;
      padding: 2px;
      background: rgb(251,251,251); /* Old browsers */
      /* IE9 SVG, needs conditional override of 'filter' to 'none' */
      background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZiZmJmYiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmMmYyZjIiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
      background: -moz-linear-gradient(top, rgba(251,251,251,1) 0%, rgba(242,242,242,1) 100%); /* FF3.6+ */
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(251,251,251,1)), color-stop(100%,rgba(242,242,242,1))); /* Chrome,Safari4+ */
      background: -webkit-linear-gradient(top, rgba(251,251,251,1) 0%,rgba(242,242,242,1) 100%); /* Chrome10+,Safari5.1+ */
      background: -o-linear-gradient(top, rgba(251,251,251,1) 0%,rgba(242,242,242,1) 100%); /* Opera 11.10+ */
      background: -ms-linear-gradient(top, rgba(251,251,251,1) 0%,rgba(242,242,242,1) 100%); /* IE10+ */
      background: linear-gradient(to bottom, rgba(251,251,251,1) 0%,rgba(242,242,242,1) 100%); /* W3C */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fbfbfb', endColorstr='#f2f2f2',GradientType=0 ); /* IE6-8 */
      font-size: 18px;
      padding: 5px;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      border: 1px solid #ddd;
      margin: 7px auto;
      filter: none;
      }
   </style>
   <div  style="margin-top: 20px" class="container" ng-app="MTSBI" ng-controller="ctrltracklead">
      <div  class="container">
         <div class="tab-control" data-role="tab-control" data-effect="fade[slide]" id="tab-with-event">
            <asp:HiddenField ID="hdnEmpId" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="hdnStaffType" ClientIDMode="Static" runat="server" />
            <div data-effect="helix" id="Div7" class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
               <div >
                  <div id="Divalert" runat="server">
                     <div class="alert alert-danger" role="alert" >
                        <strong>Sorry,</strong> you can not do any processing on this customer since it has not yet beet approved by rtmu head.Please contact your rtmu head. 
                     </div>
                  </div>
                  <h1 class="tab-head">Customer Details</h1>
                  <table class="table">
                     <tbody>
                        <tr>
                           <td > <b>Customer Id :-</b> </td>
                           <td>
                              <asp:Label ID="lblcustomerid" runat="server" Text="Label"></asp:Label>
                           </td>
                           <td>Customer Name</td>
                           <td>
                              <asp:Label ID="lblcustomername" runat="server" Text="Label"></asp:Label>
                           </td>
                        </tr>
                        <tr>
                           <td>Customer Address</td>
                           <td>
                              <asp:Label ID="lblcustome_address" runat="server" Text="Label"></asp:Label>
                           </td>
                           <td>Customer Phone</td>
                           <td>
                              <asp:Label ID="lblcustomer_phn" runat="server" Text="Label"></asp:Label>
                           </td>
                        </tr>
                        <tr id="DivAction" runat="server">
                           <td style="text-align:right;" colspan="4">
                             
                              <a target="_blank" href="tCustomer.aspx?Customer_id=<%=Customer_id %> " id="tabedit"> <button class="btn btn-danger btn-sm" ><span class="fa fa-pencil-square-o" aria-hidden="true"> </span>&nbsp;Edit</button> </a>
                              <%--   <a href="#" class="btn btn-primary btn-xs" >Move to New</a>--%>
                              <button type="button" id="btntransfer"  onclick='Moveto()' class="btn btn-danger btn-sm" style="    height: 32px;">
                                 <span class="fa fa-arrow-h" aria-hidden="true"> </span>&nbsp;<asp:Label ID="lblmovename" runat="server" ></asp:Label>
                              </button>
                              <a target="_blank" href="PrintUserDetails.html?Customer_id=<%=Customer_id %> "><button class="btn btn-danger btn-sm" ><span class="fa fa-eye" aria-hidden="true"> </span>&nbsp;Single View</button></a>
                              <button type="button" id="btndelete"  onclick='deleteItem()' class="btn btn-danger btn-sm">
                                  <span class="fa fa-trash" aria-hidden="true"> </span>&nbsp;Delete</button>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <ul class="tabs" runat="server" id="DivAllAction">
                     <li><a href="#_page_lead_list" id="tablleadlist">  Leads </a></li>
                     <li><a href="#_page_meeting" id="tabeMeeting">Meeting</a></li>
                     <%-- <li><a href="#_page_feed" id="tabFeed"> Feeds</a></li>--%>
                     <li><a href="#_page_products" id="tabProduct"> Product</a></li>
                     <li><a href="#_page_flag_list" id="tabflaglist">Flag</a></li>
                     <li><a href="#_page_schedulemeetinglist" id="tabscedule">Scheduled Meeting</a></li>
                     <li><a href="#_page_comment" id="tabcomment">Comment</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div style="margin-top: 20px" class="container" >
         <div class="tab-control" data-role="tab-control" data-effect="fade[slide]" id="Div1">
            <asp:HiddenField ID="HiddenField1" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HiddenField2" ClientIDMode="Static" runat="server" />
            <div class="frames">
               <div class="frame leadClousre" id="_page_add_newlead">
                  <h1 class="tab-head">Add New Lead</h1>
                  <div data-effect="helix" id="panels" class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
                     <div class="panel-body">    
                     </div>
                     <div id="Div2" visible="false" runat="server">
                        <asp:Label ID="Label2" runat="server" Style="color: red"></asp:Label>
                        <div style="margin-bottom: 10px"></div>
                     </div>
                     <table class="table">
                        <tr>
                           <td><b> Self Allocate</b></td>
                           <td colspan="2">
                              <input type="checkbox"  id="chckMeeting" />
                           </td>
                        </tr>
                        <tr id="divtmomeeting" style="display:none">
                           <td><b>Select Tmo</b></td>
                           <td colspan="2">
                              <div id="divtmo">
                                 <select id="DdlTmo" class="form-control" ">
                                    <option value="">Select </option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr  style="width:100%">
                           <td><b> Lead Added By  </b></td>
                           <td colspan="2">
                              <asp:TextBox ID="txtEmpId" Enabled="false" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:TextBox>
                           </td>
                        </tr>
                        <tr  style="width:100%">
                           <td><b> Customer Id </b></td>
                           <td colspan="2">
                              <asp:TextBox ID="txtlgid" Enabled="false" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:TextBox>
                           </td>
                        </tr>
                        <%--Product 1--%>
                        <tr>
                           <td><b>Product Interest 1 </b></td>
                           <td colspan="2">
                              <div id="divProductInterest">
                                 <select id="DDLProductInterest" class="form-control"></select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divplain">
                           <td><b>Plain vanilla Option 1</b></td>
                           <td colspan="2">
                              <div id="">
                                 <select id="DDLPlain" class="form-control">
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divSwaps">
                           <td><b></b></td>
                           <td colspan="2">
                              <div >
                                 <select id="DDLSwaps" class="form-control">
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="AddMore">
                           <td></td>
                           <td><input type="button" class="btn btn-primary  btn-sm" value="Add More Product " id="Addproduct"></td>
                           <td style="text-align:right;">&nbsp;</td>
                        </tr>
                        <%--Add Product 2--%>
                        <tr id="Product1" style="display: none">
                           <td><b>Product Interest 2</b></td>
                           <td colspan="2">
                              <div id="divProductInterest1">
                                 <select id="DDLProductInterest1" class="form-control" onchange="ddl_change(this.id)">
                                    <option value="0">Select Product</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divplain1">
                           <td><b>Plain vanilla Option 2</b></td>
                           <td colspan="2">
                              <div id="">
                                 <select id="DDLPlain1" class="form-control" >
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divSwaps1">
                           <td><b></b></td>
                           <td colspan="2">
                              <div >
                                 <select id="DDLSwaps1" class="form-control">
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="Delete1" style="display:none">
                           <td></td>
                           <td colspan="2" style="text-align:right;">
                              <input type="button" class="btn btn-primary  btn-sm" value="Delete" id="DeleteProduct1" />
                           </td>
                        </tr>
                        <tr id="AddMore1" style="display:none">
                           <td></td>
                           <td><input type="button" class="btn btn-primary  btn-sm" value="Add More Product" id="Addproduct1"></td>
                           <td style="text-align:right;"></td>
                        </tr>
                        <%--Add Product 3--%>
                        <tr id="Product2" style="display:none">
                           <td><b>Product Interest 3</b></td>
                           <td colspan="2">
                              <div id="divProductInterest2">
                                 <select id="DDLProductInterest2" class="form-control" onchange="ddl_change(this.id)">
                                    <option value="0">Select Product</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divplain2">
                           <td><b>Plain vanilla Option 3</b></td>
                           <td colspan="2">
                              <div id="">
                                 <select id="DDLPlain2" class="form-control" >
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divSwaps2">
                           <td><b></b></td>
                           <td colspan="2">
                              <div >
                                 <select id="DDLSwaps2" class="form-control">
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="Delete2" style="display:none">
                           <td></td>
                           <td colspan="2" style="text-align:right;">
                              <input type="button" class="btn btn-primary  btn-sm" value="Delete" id="DeleteProduct2" onclick="reply_click(this.id)" />
                           </td>
                        </tr>
                        <tr id="AddMore2" style="display:none">
                           <td></td>
                           <td colspan="2"><input type="button" class="btn btn-primary  btn-sm" value="Add More Product" id="Addproduct2"></td>
                        </tr>
                        <%--Add Product 4--%>
                        <tr id="Product3" style="display:none">
                           <td><b>Product Interest 4</b></td>
                           <td colspan="2">
                              <div id="divProductInterest3">
                                 <select id="DDLProductInterest3" class="form-control" onchange="ddl_change(this.id)">
                                    <option value="0">Select Product</option>
                                 </select>
                              </div>
                           </td>
                        <tr id="divplain3">
                           <td><b>Plain vanilla Option 4</b></td>
                           <td colspan="2">
                              <div id="">
                                 <select id="DDLPlain3" class="form-control" >
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divSwaps3">
                           <td><b></b></td>
                           <td colspan="2">
                              <div >
                                 <select id="DDLSwaps3" class="form-control">
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="Delete3" style="display:none">
                           <td></td>
                           <td colspan="2" style="text-align:right;">
                              <input type="button" class="btn btn-primary  btn-sm" value="Delete" id="DeleteProduct3" onclick="reply_click(this.id)" />
                           </td>
                        </tr>
                        <tr id="AddMore3" style="display:none">
                           <td></td>
                           <td colspan="2"><input type="button" class="btn btn-primary  btn-sm" value="Add More Product" id="Addproduct3"></td>
                        </tr>
                        <%--Add Product 5--%>
                        <tr id="Product4" style="display:none">
                           <td><b>Product Interest 5</b></td>
                           <td colspan="2">
                              <div id="divProductInterest4">
                                 <select id="DDLProductInterest4" class="form-control" onchange="ddl_change(this.id)">
                                    <option value="0">Select Product</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divplain4">
                           <td><b>Plain vanilla Option 5</b></td>
                           <td colspan="2">
                              <div id="">
                                 <select id="DDLPlain4" class="form-control" >
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divSwaps4">
                           <td><b></b></td>
                           <td colspan="2">
                              <div >
                                 <select id="DDLSwaps4" class="form-control">
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="Delete4" style="display:none">
                           <td></td>
                           <td colspan="2" style="text-align:right;">
                              <input type="button" class="btn btn-primary  btn-sm" value="Delete" id="DeleteProduct4" onclick="reply_click(this.id)" />
                           </td>
                        </tr>
                        <tr id="AddMore4" style="display:none">
                           <td></td>
                           <td colspan="2"><input type="button" class="btn btn-primary  btn-sm" value="Add More Product" id="Addproduct4"></td>
                        </tr>
                        <%--Add Product 6--%>
                        <tr id="Product5" style="display:none">
                           <td><b>Product Interest 6</b></td>
                           <td colspan="2">
                              <div id="divProductInterest5">
                                 <select id="DDLProductInterest5" class="form-control" onchange="ddl_change(this.id)">
                                    <option value="0">Select Product</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divplain5">
                           <td><b>Plain vanilla Option 6</b></td>
                           <td colspan="2">
                              <div id="">
                                 <select id="DDLPlain5" class="form-control" >
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divSwaps5">
                           <td><b></b></td>
                           <td colspan="2">
                              <div >
                                 <select id="DDLSwaps5" class="form-control">
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="Delete5" style="display:none">
                           <td></td>
                           <td colspan="2" style="text-align:right;">
                              <input type="button" class="btn btn-primary  btn-sm" value="Delete" id="DeleteProduct5" onclick="reply_click(this.id)" />
                           </td>
                        </tr>
                        <tr id="AddMore5" style="display:none">
                           <td></td>
                           <td colspan="2"><input type="button" class="btn btn-primary  btn-sm" value="Add More Product" id="Addproduct5"></td>
                        </tr>
                        <%--Add Product 7--%>
                        <tr id="Product6" style="display:none">
                           <td><b>Product Interest 7</b></td>
                           <td colspan="2">
                              <div id="divProductInterest6">
                                 <select id="DDLProductInterest6" class="form-control" onchange="ddl_change(this.id)">
                                    <option value="0">Select Product</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divplain6">
                           <td><b>Plain vanilla Option 7</b></td>
                           <td colspan="2">
                              <div id="">
                                 <select id="DDLPlain6" class="form-control" >
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divSwaps6">
                           <td><b></b></td>
                           <td colspan="2">
                              <div >
                                 <select id="DDLSwaps6" class="form-control">
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="Delete6" style="display:none">
                           <td></td>
                           <td colspan="2" style="text-align:right;">
                              <input type="button" class="btn btn-primary  btn-sm" value="Delete" id="DeleteProduct6" onclick="reply_click(this.id)" />
                           </td>
                        </tr>
                        <tr id="AddMore6" style="display:none">
                           <td></td>
                           <td colspan="2"><input type="button" class="btn btn-primary  btn-sm" value="Add More Product" id="Addproduct6"></td>
                        </tr>
                        <%--Add Product 8--%>
                        <tr id="Product7" style="display:none">
                           <td><b>Product Interest 8</b></td>
                           <td colspan="2">
                              <div id="divProductInterest7">
                                 <select id="DDLProductInterest7" class="form-control" onchange="ddl_change(this.id)">
                                    <option value="0">Select Product</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divplain7">
                           <td><b>Plain vanilla Option 8</b></td>
                           <td colspan="2">
                              <div id="">
                                 <select id="DDLPlain7" class="form-control" >
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divSwaps7">
                           <td><b></b></td>
                           <td colspan="2">
                              <div >
                                 <select id="DDLSwaps7" class="form-control">
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="Delete7" style="display:none">
                           <td></td>
                           <td colspan="2" style="text-align:right;">
                              <input type="button" class="btn btn-primary  btn-sm" value="Delete" id="DeleteProduct7" onclick="reply_click(this.id)" />
                           </td>
                        </tr>
                        <tr id="AddMore7" style="display:none">
                           <td></td>
                           <td colspan="2"><input type="button" class="btn btn-primary  btn-sm" value="Add More Product" id="Addproduct7"></td>
                        </tr>
                        <%--Add Product 9--%>
                        <tr id="Product8" style="display:none">
                           <td><b>Product Interest 9</b></td>
                           <td colspan="2">
                              <div id="divProductInterest8">
                                 <select id="DDLProductInterest8" class="form-control" onchange="ddl_change(this.id)">
                                    <option value="0">Select Product</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divplain8">
                           <td><b>Plain vanilla Option 9</b></td>
                           <td colspan="2">
                              <div id="">
                                 <select id="DDLPlain8" class="form-control" >
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divSwaps8">
                           <td><b></b></td>
                           <td colspan="2">
                              <div >
                                 <select id="DDLSwaps8" class="form-control">
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="Delete8" style="display:none">
                           <td></td>
                           <td colspan="2" style="text-align:right;">
                              <input type="button" class="btn btn-primary  btn-sm" value="Delete" id="DeleteProduct8" onclick="reply_click(this.id)" />
                           </td>
                        </tr>
                        <tr id="AddMore8" style="display:none">
                           <td></td>
                           <td colspan="2"><input type="button" class="btn btn-primary  btn-sm" value="Add More Product" id="Addproduct8"></td>
                        </tr>
                        <%--Add Product 10--%>
                        <tr id="Product9" style="display:none">
                           <td><b>Product Interest 10</b></td>
                           <td colspan="2">
                              <div id="divProductInterest9">
                                 <select id="DDLProductInterest9" class="form-control" onchange="ddl_change(this.id)">
                                    <option value="0">Select Product</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divplain9">
                           <td><b>Plain vanilla Option 10</b></td>
                           <td colspan="2">
                              <div id="">
                                 <select id="DDLPlain9" class="form-control" >
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divSwaps9">
                           <td><b></b></td>
                           <td colspan="2">
                              <div >
                                 <select id="DDLSwaps9" class="form-control">
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="Delete9" style="display:none">
                           <td></td>
                           <td></td>
                           <td style="text-align:right;">
                              <input type="button" class="btn btn-primary  btn-sm" value="Delete" id="DeleteProduct9" onclick="reply_click(this.id)" />
                           </td>
                        </tr>
                        <tr style="border: none;">
                           <td><b>Appointment Date </b></td>
                           <td>
                              <input type='text' class="form-control" id="txtAppointmentDate" placeholder="dd/mm/yyyy" autocomplete="off" />
                           </td>
                        </tr>
                        <tr style="border: none;">
                           <td><b>Comment : </b></td>
                           <td>
                              <div id="divComments">
                                 <textarea id="txtComments" placeholder="Comments" class="form-control"></textarea>
                              </div>
                           </td>
                        </tr>
                     </table>
                     <div class="row">
                        <input type="button" style="font-weight: bold;" class="info smtBtn" value="Submit" id="btnExistlead" />
                     </div>
                  </div>
                  <div class="frame" id="_page_add_product">
                  </div>
               </div>
               <div class="frame leadClousre" id="_page_add_productitem">
                  <h1 class="tab-head">Add New Product</h1>
                  <div data-effect="helix" id="Div3" class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
                     <div class="panel-body">    
                     </div>
                     <div id="Div5" visible="false" runat="server">
                        <asp:Label ID="Label5" runat="server" Style="color: red"></asp:Label>
                        <div style="margin-bottom: 10px"></div>
                     </div>
                     <table class="table">
                        <%--Product 1--%>
                        <tr>
                           <td><b>Product Interest </b></td>
                           <td colspan="2">
                              <div id="divProductInterestNew">
                                 <select id="DDLProductInterestNew" class="form-control">
                                    <option value="">select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divplain10">
                           <td><b>Plain vanilla Option 1</b></td>
                           <td colspan="2">
                              <div id="">
                                 <select id="DDLPlain10" class="form-control" >
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr id="divSwaps10">
                           <td><b></b></td>
                           <td colspan="2">
                              <div >
                                 <select id="DDLSwaps10" class="form-control">
                                    <option value="" >Select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr style="border: none;">
                           <td><b>Product Qty</b></td>
                           <td>
                              <input type='text' class="form-control"onkeypress="return isNumber(event)" maxlength="3" id="txtQuantity" placeholder="Quantity"  />
                           </td>
                        </tr>
                        <tr style="border: none;">
                           <td><b>Amount</b></td>
                           <td>
                              <input type='text' class="form-control"onkeypress="return isNumber(event)" maxlength="12" id="txtAmount" placeholder="Amount"  />
                           </td>
                        </tr>
                     </table>
                     <div class="row">
                        <input type="button" style="font-weight: bold;" class="info smtBtn" value="Submit" id="BtnaddProduct" />
                     </div>
                  </div>
                  <div class="frame" id="Div4">
                  </div>
               </div>
               <div class="frame" id="_page_feed">
                  <div class="container app">
                     <div class="col-sm-12">
                        <div data-effect="helix" id="Div6" class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
                           <div class="panel-heading">Feeds1</div>
                           <div class="panel-body">
                           </div>
                           <div id="alertMsg" visible="false" runat="server">
                              <asp:Label ID="lblAlert" runat="server" Style="color: red"></asp:Label>
                              <div style="margin-bottom: 10px"></div>
                           </div>
                           <!--Dynamic table-->
                           <%--<table class="display" id="tblFeed">
                              <thead>
                                 <tr>
                                    <th class="text-left">Lead Id</th>
                                    <th class="text-left">Lead Date </th>
                                    <th class="text-left">	Product Interest</th>
                                    <th class="text-left">Status</th>
                                    <th class="text-left">Edit</th>
                                 </tr>
                              </thead>
                              </table>--%>
                           <!-- End  table-->
                           <!--Tamp table-->
                           <div id="tblFeed_wrapperu" class="dataTables_wrapper" role="grid">
                              <div class="dataTables_length" id="tblFeed_length">
                                 <label>
                                    Show 
                                    <select name="tblFeed_length" aria-controls="tblFeedu" class="">
                                       <option value="25">25</option>
                                       <option value="30">30</option>
                                       <option value="45">45</option>
                                       <option value="60">60</option>
                                    </select>
                                    entries
                                 </label>
                              </div>
                              <div id="tblFeed_filter" class="dataTables_filter"><label>Search <input type="search" class="" aria-controls="tblFeed"></label></div>
                              <div id="tblFeed_processing" class="dataTables_processing" style="visibility: hidden;">  </div>
                              <table class="display dataTable no-footer" id="tblFeedu" aria-describedby="tblFeed_infou">
                                 <thead>
                                    <tr role="row">
                                       <th class="text-left sorting_asc" tabindex="0" aria-controls="tblFeed" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Stage: activate to sort column ascending">Stage</th>
                                       <th class="text-left sorting" tabindex="0" aria-controls="tblFeed" rowspan="1" colspan="1" aria-label="Completion Date: activate to sort column ascending">Completion Date</th>
                                       <th class="text-left sorting" tabindex="0" aria-controls="tblFeed" rowspan="1" colspan="1" aria-label="Description: activate to sort column ascending">Description</th>
                                       <th class="text-left sorting" tabindex="0" aria-controls="tblFeed" rowspan="1" colspan="1" aria-label="Completed By: activate to sort column ascending">Completed By</th>
                                       <th class="text-left sorting" tabindex="0" aria-controls="tblFeed" rowspan="1" colspan="1" aria-label="Reason: activate to sort column ascending">Reason</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr class="odd">
                                       <td class=" ">Lead Added</td>
                                       <td class=" ">03/03/2016</td>
                                       <td class=" ">asdf</td>
                                       <td class=" ">Admin</td>
                                       <td class=" "></td>
                                    </tr>
                                    <tr class="even">
                                       <td class=" ">Open For Action</td>
                                       <td class=" ">02/03/2016</td>
                                       <td class=" ">adsfasdf</td>
                                       <td class=" ">Admin</td>
                                       <td class=" "></td>
                                    </tr>
                                 </tbody>
                              </table>
                              <div class="dataTables_info" id="tblFeed_info" role="alert" aria-live="polite" aria-relevant="all">Showing 1 to 2 of 2 entries</div>
                              <div class="dataTables_paginate paging_simple_numbers" id="tblFeed_paginate"><a class="paginate_button previous disabled" aria-controls="tblFeed" tabindex="0" id="tblFeed_previous">Previous</a><span><a class="paginate_button current" aria-controls="tblFeed" tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="tblFeed" tabindex="0" id="tblFeed_next">Next</a></div>
                           </div>
                           <!-- End  table-->
                        </div>
                     </div>
                  </div>
               </div>
               <div class="frame" id="_page_products">
                  <div class="container app">
                     <div class="col-sm-12">
                        <div data-effect="helix" id="Div7" class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
                           <div class="panel-heading">Product</div>
                           <div class="panel-body">
                           </div>
                           <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL5">
                              <span class="pull-left">
                                 <li><a href="#_page_products" class="btn btn-primary btn-sm" id="A1">Product</a></li>
                                 <li><a href="#_page_forex" class="btn btn-primary btn-sm" id="tabforex">Forex</a></li>
                                 <li><a href="#_page_budget" class="btn btn-primary btn-sm" id="tabbudget">Budget</a></li>
                              </span>
                           </ul>
                           <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL9">
                              <span class="pull-right">
                                 <li><a href="#_page_add_productitem" class="btn btn-primary btn-sm" id="A2">Add  Product</a></li>
                              </span>
                           </ul>
                           <div id="Div8" visible="false" runat="server">
                              <asp:Label ID="Label1" runat="server" Style="color: red"></asp:Label>
                              <div style="margin-bottom: 10px"></div>
                           </div>
                           <!--Dynamic table -->
                           <table class="display" id="tblProduct">
                              <thead>
                                 <tr>
                                    <th class="text-left">	Product Name</th>
                                    <th class="text-left">Quantity</th>
                                    <th class="text-left">Amount</th>
                                    <th class="text-left">Date</th>
                                 </tr>
                              </thead>
                           </table>
                           <!--END table -->
                        </div>
                     </div>
                  </div>
               </div>
               <div class="frame" id="_page_forex">
                  <div class="container app">
                     <div class="col-sm-12">
                        <div data-effect="helix" id="Div9" class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
                           <div class="panel-heading">Forex Turnover</div>
                           <div class="panel-body">
                           </div>
                           <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL6">
                              <span class="pull-left">
                                 <li><a href="#_page_products" class="btn btn-primary btn-sm" id="A3">Product</a></li>
                                 <li><a href="#_page_forex" class="btn btn-primary btn-sm" id="A4">Forex</a></li>
                                 <li><a href="#_page_budget" class="btn btn-primary btn-sm" id="A5">Budget</a></li>
                              </span>
                           </ul>
                           <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL10">
                              <span class="pull-right">
                                 <li><a href="#_page_add_forex" class="btn btn-primary btn-sm" id="tabaddforex"> Add Forex</a></li>
                              </span>
                           </ul>
                           <div id="Div10" visible="false" runat="server">
                              <asp:Label ID="Label3" runat="server" Style="color: red"></asp:Label>
                              <div style="margin-bottom: 10px"></div>
                           </div>
                           <!--dybaneic table -->
                           <table class="display" id="tblforex">
                              <thead>
                                 <tr>
                                    <th class="text-left">Year</th>
                                    <th class="text-left">Turnover (INR in Crores)</th>
                                    <th class="text-left">	Margin</th>
                                    <th class="text-left">Derivative No.</th>
                                    <th class="text-left">Derivative Value</th>
                                 </tr>
                              </thead>
                           </table>
                           <!--END table -->
                        </div>
                     </div>
                  </div>
               </div>
               <div class="frame" id="_page_budget">
                  <div class="container app">
                     <div class="col-sm-12">
                        <div data-effect="helix" id="Div11" class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
                           <div class="panel-heading">Budget</div>
                           <div class="panel-body">
                           </div>
                           <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL7">
                              <span class="pull-left">
                                 <li><a href="#_page_products" class="btn btn-primary btn-sm" id="A6">Product</a></li>
                                 <li><a href="#_page_forex" class="btn btn-primary btn-sm" id="A7">Forex</a></li>
                                 <li><a href="#_page_budget" class="btn btn-primary btn-sm" id="A8">Budget</a></li>
                              </span>
                           </ul>
                           <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL11">
                              <span class="pull-right">
                                 <li><a href="#_page_add_budget" class="btn btn-primary btn-sm" id="A9">Add Budget</a></li>
                              </span>
                           </ul>
                           <div id="Div12" visible="false" runat="server">
                              <asp:Label ID="Label4" runat="server" Style="color: red"></asp:Label>
                              <div style="margin-bottom: 10px"></div>
                           </div>
                           <!--Dynamic table -->
                           <table class="display" id="tblbudget">
                              <thead>
                                 <tr>
                                    <th class="text-left">Year</th>
                                    <th class="text-left">Turnover (INR in Crores)</th>
                                    <th class="text-left">	Margin</th>
                                    <th class="text-left">Derivative No</th>
                                    <th class="text-left">Derivative Value</th>
                                 </tr>
                                 </tr>
                              </thead>
                           </table>
                           <!--END table -->
                        </div>
                     </div>
                  </div>
               </div>
               <div class="frame" id="_page_meeting">
                  <div data-effect="helix" id="" class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
                     <h1 class="tab-head"> Meeting List</h1>
                     <div class="row">
                        <div class="col-xs-2"><b>Select Meeting Type :- </b>  </div>
                        <div class="col-xs-2">
                           <select id="ddlmeetingstatus"  class="form-control" >
                              <option value="">Select</option>
                              <option value="Open">Open</option>
                              <option value="Close">Close</option>
                           </select>
                        </div>
                        <div class="col-xs-8">
                           <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL13">
                              <span class="pull-right">
                                 <li><a href="#_page_add_meeting" class="btn btn-primary btn-sm" id="tabAddMeeting1">Add New Meeting</a></li>
                              </span>
                           </ul>
                        </div>
                     </div>
                     <div style="margin-top:10px">
                        <!--dynamic table-->
                        <table class="display" id="tblMeeting">
                           <thead>
                              <tr>
                                 <th class="text-left">Meeting Type</th>
                                 <th class="text-left">Meeting Date</th>
                                 <th class="text-left">Along with person</th>
                                 <th class="text-left">Description</th>
                                 <th class="text-left"> View</th>
                              </tr>
                           </thead>
                        </table>
                        <!--dynamic end-->
                     </div>
                  </div>
               </div>
               <div class="frame" id="_page_schedulemeetinglist">
                  <h1 class="tab-head">Scheduled Meeting List</h1>
                  <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL8">
                     <span class="pull-right">
                        <li><a href="#_page_add_schedulemeeting_meeting" class="btn btn-primary btn-sm" id="A10">Add New Schedule Meeting</a></li>
                     </span>
                  </ul>
                  <div style="margin-top:10px">
                     <!--dynamic table-->
                     <table class="display" id="tblscedulemeeting">
                        <thead>
                           <tr>
                              <th class="text-left">Meeting Type</th>
                              <th class="text-left">Meeting Date</th>
                              <th class="text-left">Along with person</th>
                              <th class="text-left">Description</th>
                              <th class="text-left"> View</th>
                           </tr>
                        </thead>
                     </table>
                     <!--dynamic end-->
                  </div>
               </div>
               <div class="frame" id="_page_comment">
                  <h1 class="tab-head">Comment List</h1>
                  <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL1">
                     <span class="pull-right">
                        <li><a href="#_page_add_comment" class="btn btn-primary btn-sm" id="A1125">Add New Comment</a></li>
                     </span>
                  </ul>
                  <div style="margin-top:10px">
                     <!--dynamic table-->
                     <table class="display" id="tblcomment">
                        <thead>
                           <tr>
                              <th class="text-left">Customer Id</th>
                              <th class="text-left">Comment</th>
                              <th class="text-left">Comment Date</th>
                           </tr>
                        </thead>
                     </table>
                     <!--dynamic end-->
                  </div>
               </div>
               <div class="frame leadClousre" id="_page_add_comment">
                  <h1 class="tab-head">Add New Comment</h1>
                  <div data-effect="helix" id="Div16" class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
                     <div class="panel-body">    
                     </div>
                     <div id="Div21" visible="false" runat="server">
                        <asp:Label ID="Label6" runat="server" Style="color: red"></asp:Label>
                        <div style="margin-bottom: 10px"></div>
                     </div>
                     <table class="table">
                        <tr style="border: none;">
                           <td><b>Comment : </b></td>
                           <td>
                              <div id="divComment">
                                 <textarea id="txtComment" placeholder="Comments" class="form-control"></textarea>
                              </div>
                           </td>
                        </tr>
                     </table>
                     <div class="row">
                        <input type="button" style="font-weight: bold;" class="info smtBtn" value="Submit" id="btnComment" />
                     </div>
                  </div>
                  <div class="frame" id="Div18">
                  </div>
               </div>
               <div class="frame add-meeting" id="_page_add_schedulemeeting_meeting">
                  <h1 class="tab-head">Scheduled Meeting </h1>
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Meeting Type</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="input-control select" >
                           <select id="ddlSMeetingTypes" ng-model="ddlSMeetingTypes" class="form-control" >
                              <option value="">Select</option>
                              <option value="Customer">Customer</option>
                              <option value="Branch">Branch</option>
                              <option value="Other">Other</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row" id="DivBranchs">
                     <div class="col-sm-3">
                        <label>Select Branch</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="input-control select" >
                           <select id="DddlBranchMeetings" ng-model="ddlbranchs" class="form-control" >
                              <option value="">Select</option>
                           </select>
                           <input type="hidden" value="" id="Hidden1s" />
                        </div>
                     </div>
                  </div>
                  <div class="row" id="DivOthers">
                     <label class="col-sm-3">Enter Details for Others</label>
                     <div class="col-sm-9 input-control textarea" data-role="input-control" >
                        <input type="text" ng-model="txtotherdetailss" id="txtotherdetailss" placeholder="Enter Details for Others">
                     </div>
                  </div>
                  <div id ="divMeets">
                     <div class="row" id="DivMeetings">
                        <div class="col-sm-3">
                           <label>Meeting For</label>
                        </div>
                        <div class="col-sm-9">
                           <div class="input-control select" >
                              <select ng-model="DdlMeetingWiths" class="form-control" id="DdlMeetingWiths">
                                 <option value="">Select</option>
                                 <option value="Specified Meeting">Specified Meeting</option>
                                 <option value="Genral Meeting">General Meeting</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="row" id="DivCustomerLeads">
                        <div class="col-sm-3">
                           <label>Select Lead</label>
                        </div>
                        <div class="col-sm-9">
                           <div class="input-control select">
                              <select ng-model="Meetingleadids" id="Meetingleadids" class="form-control" >
                                 <option value="">Select</option>
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row" id="DivSchedules12">
                     <label class="col-sm-3">Schedule<i>Meeting</i></label>
                     <div class='input-group input-append date ' id='divaddMeetingNextAppointmentDate'>
                        <input type='text' ng-model="ddlmeetingapointdate"  class="form-control date" id="txtAddMeetingNextAppointmentDate" placeholder="dd/mm/yyyy" />
                     </div>
                  </div>
                  <div class="row">
                     <label class="col-sm-3">Meeting Time</label> 
                     <div class="row">
                        <div class="col-md-2">      <input type='text'  ng-model="txtFromTimes" class="form-control date" id="txtFromTimes" placeholder="00:00" /></div>
                        <div class="col-md-1">    <label class="col-sm-1">To </label>    </div>
                        <div class="col-md-2">    <input type='text'  ng-model="txtToTimes" class="form-control date" id="txtToTimes" placeholder="00:00" /></div>
                     </div>
                  </div>
                  <div class="row">
                     <label class="col-sm-3">Person to meet</label>
                     <div class="col-sm-9 input-control textarea" data-role="input-control">
                        <input type="text"  ng-model="txtmeetpersons" id="txtmeetpersons" placeholder="Enter Person to meet ">
                     </div>
                  </div>
                  <div class="row">
                     <label class="col-sm-3">Along With Other users</label>
                     <div class="col-sm-9 input-control textarea" data-role="input-control">
                        <input type="text"  ng-model="txtalongotherusers" id="txtalongotherusers" placeholder="Enter Username of the users along with you for meeting">
                     </div>
                  </div>
                  <div class="row">
                     <label class="col-sm-3">Meeting Description</label>
                     <div class="col-sm-9 input-control textarea" data-role="input-control" id="divdescipritionst">
                        <textarea id="txtmeetingdescriptions" ng-model="txtmeetingdescriptiontss" placeholder="Meeting Description"></textarea>
                     </div>
                  </div>
                  <div class="row">
                     <input type="button" style="font-weight: bold;" class="info smtBtn" value="Submit" ng-click="SaveScheduleMeeting()"  />
                  </div>
               </div>
               <div class="frame" id="_page_lead_list">
                  <div class="container app">
                     <div data-effect="helix"  class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
                        <h1 class="tab-head">Lead List</h1>
                        <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL3">
                           <span class="pull-right">
                              <li><a href="#_page_add_newlead" class="btn btn-primary btn-sm" id="tabAddlead">Add New Lead</a></li>
                           </span>
                        </ul>
                        <div style="margin-top:10px">
                           <!-- Dynamic tble-->
                           <table class="display" id="tblleadlists">
                              <thead>
                                 <tr>
                                    <th class="text-left">Lead Id</th>
                                    <th class="text-left">Product Interest</th>
                                    <th class="text-left">Status</th>
                                    <th class="text-left">Date</th>
                                    <th class="text-left">Comment</th>
                                    <th class="text-left">Edit</th>
                                 </tr>
                              </thead>
                           </table>
                           <!--End Dynamic tble-->
                        </div>
                     </div>
                  </div>
               </div>
               <div class="frame" id="_page_flag_list">
                  <h1 class="tab-head">Flag List</h1>
                  <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL4">
                     <span class="pull-right">
                        <li><a href="#_page_add_flag" class="btn btn-primary btn-sm" id="A11">Add New Flag</a></li>
                     </span>
                  </ul>
                  <div style="margin-top:10px">
                     <!--dynamic table-->
                     <table class="display" id="tblflaglist">
                        <thead>
                           <tr>
                              <th class="text-left">CustomerId</th>
                              <th class="text-left">Meeting Person</th>
                              <th class="text-left">Assing Flag to</th>
                              <th class="text-left">Assign Authority  </th>
                              <th class="text-left">Assign Date  </th>
                              <th class="text-left">Description</th>
                           </tr>
                        </thead>
                     </table>
                     <!--dynamic end-->
                  </div>
               </div>
               <div class="frame add-meeting" id="_page_add_meeting">
                  <h1 class="tab-head">Add Meeting </h1>
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Meeting Type</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="input-control select" >
                           <select id="ddlSMeetingType" ng-model="ddlSMeetingType" class="form-control" >
                              <option value="">Select</option>
                              <option value="Customer">Customer</option>
                              <option value="Branch">Branch</option>
                              <option value="Other">Other</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row" id="DivBranch">
                     <div class="col-sm-3">
                        <label>Select Branch</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="input-control select" >
                           <select id="DddlBranchMeeting" ng-model="ddlbranch" class="form-control" >
                              <option value="">Select</option>
                           </select>
                           <input type="hidden" value="" id="Hidden1" />
                        </div>
                     </div>
                  </div>
                  <div class="row" id="DivOther">
                     <label class="col-sm-3">Enter Details for Others</label>
                     <div class="col-sm-9 input-control textarea" data-role="input-control" >
                        <input type="text" ng-model="txtotherdetails" id="txtotherdetails" placeholder="Enter Details for Others">
                     </div>
                  </div>
                  <div id ="divMeet">
                     <div class="row" id="DivMeeting">
                        <div class="col-sm-3">
                           <label>Meeting For</label>
                        </div>
                        <div class="col-sm-9">
                           <div class="input-control select" >
                              <select ng-model="DdlMeetingWith" class="form-control" id="DdlMeetingWith">
                                 <option value="">Select</option>
                                 <option value="Specified Meeting">Specified Meeting</option>
                                 <option value="Genral Meeting">General Meeting</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="row" id="DivCustomerLead">
                        <div class="col-sm-3">
                           <label>Select Lead</label>
                        </div>
                        <div class="col-sm-9">
                           <div class="input-control select">
                              <select ng-model="Meetingleadid" id="Meetingleadid" class="form-control" >
                                 <option value="">Select</option>
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <label class="col-sm-3">Meeting Date</label>                
                     <div class='input-group input-append date ' id='divaddMeetingMeetDate'>
                        <input type='text'  ng-model="ddlmeetingdate" class="form-control date" id="txtAddMeetingMeetDate" placeholder="dd/mm/yyyy" />
                     </div>
                  </div>
                  <div class="row">
                     <label class="col-sm-3">Meeting Time</label> 
                     <div class="row">
                        <div class="col-md-2">      <input type='text'  ng-model="txtFromTime" class="form-control date" id="txtFromTime" placeholder="00:00" /></div>
                        <div class="col-md-1">    <label class="col-sm-1">To </label>    </div>
                        <div class="col-md-2">    <input type='text'  ng-model="txtToTime" class="form-control date" id="txtToTime" placeholder="00:00" /></div>
                     </div>
                  </div>
                  <div class="row">
                     <label class="col-sm-3">Person to meet</label>
                     <div class="col-sm-9 input-control textarea" data-role="input-control">
                        <input type="text"  ng-model="txtmeetperson" id="txtmeetperson" placeholder="Enter Person to meet ">
                     </div>
                  </div>
                  <div class="row">
                     <label class="col-sm-3">Along With Other users</label>
                     <div class="col-sm-9 input-control textarea" data-role="input-control">
                        <input type="text"  ng-model="txtalongotheruser" id="txtalongotheruser" placeholder="Enter Username of the users along with you for meeting">
                     </div>
                  </div>
                  <div class="row">
                     <label class="col-sm-3">Meeting Description</label>
                     <div class="col-sm-9 input-control textarea" data-role="input-control" id="divdesciprition">
                        <textarea id="txtmeetingdescription" ng-model="txtmeetingdescription" placeholder="Meeting Description"></textarea>
                     </div>
                  </div>
                  <div class="row">
                     <table class="table">
                        <thead>
                           <th></th>
                           <th> </th>
                           <th> </th>
                           <th></th
                           <th></th>
                           </tr>
                        </thead>
                        <tbody >
                        </tbody>
                     </table>
                  </div>
                  <div style="background-color:#CCDEDE">
                     <div class="row">
                        <label class="col-sm-3">Issue</label>
                        <div class="col-sm-9 input-control textarea" data-role="input-control" id="divissue">
                           <textarea id="txtissue" ng-model="txtissue" placeholder="Issue"></textarea>
                        </div>
                     </div>
                     <div class="row">
                        <table class="table">
                           <thead>
                              <th></th>
                              <th> </th>
                              <th> </th>
                              <th></th
                              <th></th>
                              </tr>
                           </thead>
                           <tbody >
                           </tbody>
                        </table>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Action Plan</label>
                        <div class="col-sm-9 input-control textarea" data-role="input-control" id="divaction">
                           <textarea id="txtactionplan" ng-model="txtactionplan" placeholder="Action Plan"></textarea>
                        </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Mail Text</label>
                        <div class="col-sm-9 input-control textarea" data-role="input-control" id="divmailtext">
                           <textarea id="txtmailtext" ng-model="txtmailtext" placeholder="Email Body"></textarea>
                        </div>
                     </div>
                     <div class="row" id="divreciption">
                        <label class="col-sm-3">Recipient Id</label>
                        <div class="col-sm-9 input-control textarea" data-role="input-control" id="divrecipientid">
                           <input type="text" id="txtrecipientid" ng-model="txtrecipientid" placeholder="Enter comma saperated email ID">
                        </div>
                     </div>
                     <div class="row" id="div13">
                        <label class="col-sm-3"></label>
                        <div class="col-sm-9 input-control textarea" data-role="input-control" id="div14">
                           <div class="row">
                              <input style="font-weight: bold; width:auto;" ng-click="addactioncustomer()" class="btn-success pull-right btn-sm" value="Submit  Action Plan"  type="button">
                           </div>
                           <div class="row">
                              <input style="font-weight: bold; width:auto;" ng-click="New()" class="btn-warning pull-right btn-sm" value="New Issue  "  type="button">
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <table class="table">
                           <thead>
                              <th>Issue</th>
                              <th>Action Plan</th>
                              <th>Mail Text</th>
                              <th>Recipient</th>
                              <th>Remove</th>
                              </tr>
                           </thead>
                           <tbody >
                              <tr ng-repeat="item in actionlist1 ">
                                 <td>{{item.issue}}</td>
                                 <td>{{item.action_plan}}</td>
                                 <td>{{item.mailtext}}</td>
                                 <td>{{item.recipient_id}}</td>
                                 <td>
                                    <div ng-click="removeItem1($index)" class="btn btn-sm btn-danger">Remove</div>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="row">
                     <input type="button" style="font-weight: bold;" class="info smtBtn" value="Submit" ng-click="SaveMeetingcustomer()"  />
                  </div>
               </div>
               <div class="frame add-meeting" id="_page_add_flag">
                  <h1 class="tab-head">Add Flag </h1>
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Flag to</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="input-control select" >
                           <select id="DdlMeetingwithPerson1" class="form-control" >
                              <option value="">Select</option>
                              <option value="Branch">Branch</option>
                              <option value="TMO">TMO</option>
                              <option value="Other">Other</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row" id="DivBranch1">
                     <div class="col-sm-3">
                        <label>Select Branch</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="input-control select" >
                           <select id="DDLBRANCH1" class="form-control" >
                              <option value="-1">Select</option>
                           </select>
                           <input type="hidden" value="" id="currentStatus3" />
                        </div>
                     </div>
                  </div>
                  <div class="row" id="DivTmo1">
                     <div class="col-sm-3">
                        <label>Select TMO</label>
                     </div>
                     <div class="col-sm-9">
                        <select id="DDLTMU1" class="form-control"  >
                           <option value="-1">Select</option>
                        </select>
                     </div>
                  </div>
                  <div class="row" id="DivOther1">
                     <label class="col-sm-3">Enter email of other</label>
                     <div class="col-sm-9 input-control textarea" data-role="input-control" >
                        <input type="text" id="txtotherdetails1" placeholder="Enter Details for Others">
                     </div>
                  </div>
                  <div class="row">
                     <label class="col-sm-3"> Description</label>
                     <div class="col-sm-9 input-control textarea" data-role="input-control" id="div15">
                        <textarea id="txtmeetingdescription1" placeholder=" Description"></textarea>
                     </div>
                  </div>
                  <div class="row">
                  </div>
                  <div class="row">
                     <input type="button" style="font-weight: bold;" class="info smtBtn" id="btnSubmitFlag" value="Save Flag"      />
                  </div>
               </div>
               <div class="frame leadClousre" id="_page_add_forex">
                  <h1 class="tab-head">Add New Forex</h1>
                  <div data-effect="helix" id="Div16" class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
                     <div class="panel-body">    
                     </div>
                     <div id="Div17" visible="false" runat="server">
                        <asp:Label ID="Label8" runat="server" Style="color: red"></asp:Label>
                        <div style="margin-bottom: 10px"></div>
                     </div>
                     <table class="table">
                        <tr>
                           <td><b>Year </b></td>
                           <td colspan="2">
                              <div >
                                 <select id="Ddlforexyear" class="form-control">
                                    <option value="">select</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Month </b></td>
                           <td colspan="2">
                              <div >
                                 <select name="month" id="DdlForexMonth" class="form-control" >
                                    <option value="">select</option>
                                    <option value="January">January</option>
                                    <option value="February">February</option>
                                    <option value="March">March</option>
                                    <option value="April">April</option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>
                                 </select>
                              </div>
                           </td>
                        </tr>
                        <tr style="border: none;">
                           <td><b>Turnover (INR in Crores)</b></td>
                           <td>
                              <input type='text' class="form-control"onkeypress="return isNumber(event)" maxlength="12" id="txtforextturnover" placeholder="Turnover (INR in Crores)"  />
                           </td>
                        </tr>
                        <tr style="border: none;">
                           <td><b>Margin</b></td>
                           <td>
                              <input type='text' class="form-control"onkeypress="return isNumber(event)" maxlength="12" id="txtforexmargin" placeholder="Margin"  />
                           </td>
                        </tr>
                        <tr style="border: none;">
                           <td><b>Derivative No.</b></td>
                           <td>
                              <input type='text' class="form-control"onkeypress="return isNumber(event)" maxlength="12" id="txtforexderivativeno" placeholder="Derivative No."  />
                           </td>
                        </tr>
                        <tr style="border: none;">
                           <td><b>Derivative Value  </b></td>
                           <td>
                              <input type='text' class="form-control"onkeypress="return isNumber(event)" maxlength="12" id="txtforexderivativevalue" placeholder="Derivative Value"  />
                           </td>
                        </tr>
                     </table>
                     <div class="row">
                        <input type="button" style="font-weight: bold;" class="info smtBtn" value="Submit" id="btnaddforex" />
                     </div>
                  </div>
                  <div class="frame" id="Div18">
                  </div>
               </div>
               <div class="frame leadClousre" id="_page_add_budget">
                  <h1 class="tab-head">Add New Budget</h1>
                  <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL12">
                     <span class="pull-left">
                        <li><a href="#_page_products" class="btn btn-primary btn-sm" id="A12">Product</a></li>
                        <li><a href="#_page_forex" class="btn btn-primary btn-sm" id="A13">Forex</a></li>
                        <li><a href="#_page_budget" class="btn btn-primary btn-sm" id="A14">Budget</a></li>
                     </span>
                  </ul>
                  <div data-effect="helix" id="Div19" class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
                     <div class="panel-body">    
                     </div>
                     <div id="Div20" visible="false" runat="server">
                        <asp:Label ID="Label9" runat="server" Style="color: red"></asp:Label>
                        <div style="margin-bottom: 10px"></div>
                     </div>
                     <table class="table">
                        <tr style="border: none;">
                           <td><b>Year</b></td>
                           <td>
                              <asp:TextBox ID="txtbudgetyear" class="form-control"   runat="server" ReadOnly="True"></asp:TextBox>
                           </td>
                        </tr>
                        <tr style="border: none;">
                           <td><b>Turnover (INR in Crores)</b></td>
                           <td>
                              <input type='text' class="form-control"onkeypress="return isNumber(event)" maxlength="12" id="txtbudgtturnover" placeholder="Turnover (INR in Crores)"  />
                           </td>
                        </tr>
                        <tr style="border: none;">
                           <td><b>Margin</b></td>
                           <td>
                              <input type='text' class="form-control"onkeypress="return isNumber(event)" maxlength="12" id="txtbudgetmargin" placeholder="Margin"  />
                           </td>
                        </tr>
                        <tr style="border: none;">
                           <td><b>Derivative No.</b></td>
                           <td>
                              <input type='text' class="form-control"onkeypress="return isNumber(event)" maxlength="12" id="txtbudgetderivativeno" placeholder="Derivative No."  />
                           </td>
                        </tr>
                        <tr style="border: none;">
                           <td><b>Derivative Value  </b></td>
                           <td>
                              <input type='text' class="form-control"onkeypress="return isNumber(event)" maxlength="12" id="txtbudgetderivativevalue" placeholder="Derivative Value"  />
                           </td>
                        </tr>
                     </table>
                     <div class="row">
                        <input type="button" style="font-weight: bold;" class="info smtBtn" value="Submit" id="Btnaddbudget" />
                     </div>
                  </div>
               </div>
               <asp:Label ID="lblMsg" runat="server" Text="This lead is under process so it can not be deleted." ForeColor="Red" Visible="False"></asp:Label>
            </div>
         </div>
         <script src="Scripts/EditCustomer.js"></script>
         <script src="js/jquery-ui.js"></script>
      </div>
   </div>
</asp:Content>