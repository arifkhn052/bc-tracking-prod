﻿/// <reference path="../FlagResolve.aspx" />
$(document).ready(function () {
    FlagDetails();
    GetFlagDetails();

})

function FlagDetails()
{
    
    var leadId = getParameterByName('lead_id');
 
    $("#txtleadid").val(leadId);
    $("#txtdescription").val(getParameterByName('description'));
    $("#txtflagdate").val(getParameterByName('flagdate'));

}
var errorMsg;
var errorMessage = '';
function validate()
{
    
    var result = true;
  
    var comment = $("#txtcomment").val();

    if (comment=="") {
        errorMsg = errorMsg + "<br/>Enter Comment";
        $("#txtcomment").addClass("form-group has-error");
        result = false;
    } else {
        $("#divcomment").removeClass("form-group has-error");

    }
    return result;

}
$("#btnAddUser").click(function () {
    
    
    var leadId = getParameterByName('id');
    if (validate() == true) {

        $.ajax({
            url: 'FlagResolve.aspx/saveflagresolves',
            type: "POST",
            dataType: "json",
            data: "{'leadId': '" + leadId + "','comment' : '" + $("#txtcomment").val() + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {


                var messageContent = "<center><h1>Thank You</h1><h6>" + data.d + " </h6>" +
                    "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
              
                ShowDialog(messageContent, 400, 600, 'Message', 10, '<span class="icon-info"></span>', true, 'FlagLead.aspx');
              
            }
        });

    } else {

        ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    }

});

function GetFlagDetails() {
    
    var leadId = getParameterByName('lead_id');
    $.ajax({
        url: 'FlagResolve.aspx/GefflagsLeadDeatails',
        type: "POST",
        dataType: "json",
        data: "{'leadId': '" + leadId + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            var flaglist = JSON.parse( data.d);
            $("#txtleadid").val(flaglist[0]["lead_id"]);
            $("#txtdescription").val(flaglist[0]["flag_description"]);
            $("#txtflagdate").val(flaglist[0]["dataa"]);

       
        }
    });
}

$("#BtnCancel").click(function () {
    
    window.location.href = 'FlagLead.aspx';
});
