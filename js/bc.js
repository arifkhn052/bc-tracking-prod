﻿$(document).ready(function () {
    //   $('#frmMain').parsley().on('field:validated', function () {
    //       var ok = $('.parsley-error').length === 0;
    //       $('.bs-callout-info').toggleClass('hidden', !ok);
    //       $('.bs-callout-warning').toggleClass('hidden', ok);
    //   }).on('form:submit', function () {
    //    return false; 
    //});

    var baseControlName = '';
    var fullurl = $(location).attr('href');
    var m = fullurl.match(/mode=([^&]+)/);
    var mode = '';
    if (m != null)
        mode = m[1];

    var b = fullurl.match(/bc=([^&]+)/);
    var bcId = '';
    if (b != null)
        bcId = b[1];

    var certs = [];
    var exps = [];
    var areas = [];
    var devices = [];
    var conns = [];
    var prods = [];

    function GetStates() {
        var allStates = new Array();
        var requestUrl = 'GetBCHandler.ashx?mode=state';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            async: false,
            success: function (stateList) {
                if (stateList.length != 0) {
                    for (let i = 0; i < stateList.length; i++) {
                        let state = new Object();
                        state.StateId = stateList[i].StateId;
                        state.StateName = stateList[i].StateName;
                        /*state.StateCircles = stateList[i].StateCircles;
                        state.Branches = stateList[i].Branches;*/
                        state.Districts = stateList[i].Districts;
                        allStates.push(state);
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allStates;
    }

    let states = GetStates();
    if (states.length >= 1) {
        $('#selPLState').empty();
        $('#selAddressState').empty();
        $('#selPLState').append($('<option>', { value: 0, text: 'Select State' }));
        $('#selAddressState').append($('<option>', { value: 0, text: 'Select State' }));
        for (let i = 0; i < states.length; i++) {
            $('#selPLState').append($('<option>', { value: states[i].StateId, text: states[i].StateName }));
            $('#selAddressState').append($('<option>', { value: states[i].StateId, text: states[i].StateName }));
        }
    }

    function getAllTalukas(stateId, stateList) {
        let talukas = new Array();
        var stateObject = function (stateIdNumber) {
            var stateOb;
            for (let i = 0; i < stateList.length; i++) {
                if (stateList[i].StateId == stateId) {
                    stateOb = stateList[i];
                    break;
                }
            }
            return stateOb;
        }(stateId);
        if (stateObject != null) {
            for (let i = 0; i < stateObject.Districts.length; i++) {
                for (let j = 0; j < stateObject.Districts[i].Talukas.length; j++) {
                    var t = new Object();
                    t.TalukaId = stateObject.Districts[i].Talukas[j].TalukaId;
                    t.TalukaName = stateObject.Districts[i].Talukas[j].TalukaName;
                    talukas.push(t);
                }
            }
        }
        return talukas;
    }

    function getDistricts(stateId, stateList) {
        let districts = new Array();
        if (stateList.length != 0) {
            for (let i = 0; i < stateList.length; i++) {
                if (stateId == stateList[i].StateId) {
                    districts = stateList[i].Districts;
                    break;
                }
            }
        }
        return districts;
    }

    function getCities(districtId, districtList) {
        let cities = new Array();
        if (districtList.length != 0) {
            for (let i = 0; i < districtList.length; i++) {
                if (districtList[i].DistrictId == districtId) {                  
                    cities = districtList[i].Cities;
                    break;
                }
            }
        }
        return cities;
    }

    $('#selAddressState').on('change', function () {
        let allDistricts = getDistricts($('#selAddressState').val(), states);
        if (allDistricts.length >= 1) {
            $('#selAddressDistrict').empty();
            $('#selAddressDistrict').append($('<option>', {value: 0, text: 'Select District'}));
            for (let i = 0; i < allDistricts.length; i++) {
                $('#selAddressDistrict').append($('<option>', {value: allDistricts[i].DistrictId, text: allDistricts[i].DistrictName}));
            }
        }
    });

    $('#selPLState').on('change', function () {
        let talukas = getAllTalukas($('#selPLState').val(), states);
        $('#selPLTaluk').empty();
        $('#selPLTaluk').append($('<option>', {value: 0, text: 'Select Taluka'}));
        if (talukas.length >= 1) {
            for (i = 0; i < talukas.length; i++) {
                $('#selPLTaluk').append($('<option>', { value: talukas[i].TalukaId, text: talukas[i].TalukaName }));
            }
        }
    });

    $('#selAddressDistrict').on('change', function () {
        let cities = new Array();
        let distrcts = getDistricts($('#selAddressState').val(), states);
        cities = getCities($('#selAddressDistrict').val(), distrcts);
        console.log(JSON.stringify(cities));
        $('#selAddressCity').empty();
        $('#selAddressCity').append($('<option>', {
            value: 0,
            text: 'Select City'
        }));
        if (cities.length >= 1) {
            for (let i = 0; i < cities.length; i++) {
                $('#selAddressCity').append($('<option>', {value: cities[i].CityId, text: cities[i].CityName}));
            }
        }
    });

    var t2 = $('#tblCertificates').DataTable({
        "paging": true,
        "ordering": true,
        "info": false,
        "searching": false
    });

    var t3 = $('#tblPreviousExperience').DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false
    });

    var t4 = $('#tblOperationAreas').DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false
    });

    var t5 = $('#tblDevices').DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false
    });

    var t6 = $('#tblConnectivity').DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false
    });

    var t7 = $('#tblProducts').DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false
    });

    var bcorrespondene = new Object();

    //if (bcId != '') {
    //    $('.loader').show();
    //    $.ajax({
    //        type: 'POST',
    //        dataType: 'json',
    //        contentType: 'application/json',
    //        url: "GetBCHandler.ashx?mode=One&bcId=" + bcId,
    //        async: false,
    //        success: function (bankc) {
    //            //console('Got it!');
    //            if (bankc != null) {
    //                bcorrespondene = bankc;
    //                //Set Controls
    //                $('#' + baseControlName + 'txtName').val(bankc.Name);
    //                $('#' + baseControlName + 'selGender').val(bankc.Gender);
    //                var dt = new Date(bankc.DOB);
    //                //       var dtString = dt.toDateString("YYYY-MM-DD");
    //                var dtString = dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate();

    //                $('#' + baseControlName + 'txtDOB').val(dtString);

    //                $('#' + baseControlName + 'txtFather').val(bankc.FatherName);
    //                $('#' + baseControlName + 'txtSpouse').val(bankc.SpouseName);
    //                $('#' + baseControlName + 'selCategory').val(bankc.Category);

    //                if (bankc.Handicap == 'Yes')
    //                    $('#' + baseControlName + 'radHandicapYes').checked = true;
    //                else
    //                    $('#' + baseControlName + 'radHandicapNo').checked = true;


    //                $('#' + baseControlName + 'txtPhone1').val(bankc.PhoneNumber1);
    //                $('#' + baseControlName + 'txtPhone2').val(bankc.PhoneNumber2);
    //                $('#' + baseControlName + 'txtPhone3').val(bankc.PhoneNumber3);
    //                $('#' + baseControlName + 'txtEmail').val(bankc.Email);
    //                $('#' + baseControlName + 'txtAadharCard').val(bankc.AadharCard);
    //                $('#' + baseControlName + 'txtPanCard').val(bankc.PanCard);
    //                $('#' + baseControlName + 'txtVoterId').val(bankc.VoterCard);
    //                $('#' + baseControlName + 'txtDriverLic').val(bankc.DriverLicense);
    //                $('#' + baseControlName + 'txtNregaCard').val(bankc.NregaCard);
    //                $('#' + baseControlName + 'txtRationCard').val(bankc.RationCard);
    //                $('#' + baseControlName + 'selAddressState').val(bankc.State);
    //                $('#' + baseControlName + 'selAddressCity').val(bankc.City);
    //                $('#' + baseControlName + 'selAddressDistrict').val(bankc.District);
    //                $('#' + baseControlName + 'txtAddressSubDistrict').val(bankc.Subdistrict);
    //                $('#' + baseControlName + 'txtAddressArea').val(bankc.Area);
    //                $('#' + baseControlName + 'txtPinCode').val(bankc.PinCode);
    //                $('#' + baseControlName + 'SelAlternateOccupation').val(bankc.AlternateOccupationType);
    //                $('#' + baseControlName + 'txtAlternateOccupationDtl').val(bankc.AlternateOccupationDetail);
    //                $('#' + baseControlName + 'txtUniqueId').val(bankc.UniqueIdentificationNumber);
    //                $('#' + baseControlName + 'txtBankReferenceNumber').val(bankc.BankReferenceNumber);

    //                $('#' + baseControlName + 'selQualification').val(bankc.Qualification);

    //                $('#' + baseControlName + 'txtOtherQualification').val(bankc.OtherQualification);
    //                $('#' + baseControlName + 'selCorporate').val(bankc.CorporateId);

    //                if (bankc.isAllocated)
    //                    $('#radAllocatedYes').prop('checked', true);
    //                else {
    //                    $('#radAllocatedNo').prop('checked', true);
    //                    $('#divAllocationDetails').hide();
    //                }

    //                if (bankc.AppointmentDate != null && bankc.AppointmentDate != undefined) {
    //                    var dt2 = new Date(bankc.AppointmentDate);
    //                    var dt2String = dt2.getFullYear() + '-' + (dt2.getMonth() + 1) + '-' + dt2.getDate();

    //                    $('#' + baseControlName + 'txtAppointmentDate').val(dtString);
    //                }


    //                $('#' + baseControlName + 'ifscCode').val(bankc.AllocationIFSCCode);

    //                $('#' + baseControlName + 'selAllocationBank').val(bankc.AllocationBankId);
    //                $('#' + baseControlName + 'selAllocationBranch').val(bankc.AllocationBranchId);

    //                $('#' + baseControlName + 'selBCType').val(bankc.BCType);
    //                $('#' + baseControlName + 'txtWorkingDays').val(bankc.WorkingDays);
    //                $('#' + baseControlName + 'txtWorkingHours').val(bankc.WorkingHours);
    //                $('#' + baseControlName + 'txtPostalAddress').val(bankc.PLPostalAddress);
    //                $('#' + baseControlName + 'txtVillageCode1').val(bankc.PLVillageCode);
    //                $('#' + baseControlName + 'txtVillageDetail1').val(bankc.PLVillageDetail);
    //                $('#' + baseControlName + 'selPLState').val(bankc.PLStateId);
    //                //$('#' + baseControlName + 'selPLDistrict').val(bankc.PLDistrictId);
    //                $('#' + baseControlName + 'selPLTaluk').val(bankc.PLTaluk);
    //                $('#' + baseControlName + 'txtPLPinCode').val(bankc.PLPinCode);
    //                //$('#' + baseControlName + 'selProductsOffered').val(bankc.ProductsOffered);
    //                $('#' + baseControlName + 'txtMinCash').val(bankc.MinimumCashHandlingLimit);
    //                $('#' + baseControlName + 'txtMonthlyFixed').val(bankc.MonthlyFixedRenumeration);
    //                $('#' + baseControlName + 'txtMonthlyVariable').val(bankc.MonthlyVariableRenumeration);

    //                $('#lblName').html(bankc.Name);

    //                if (bankc.Certifications != null) {
    //                    for (var c = 0; c < bankc.Certifications.length; c++) {
    //                        var cert = bankc.Certifications[c];
    //                        certs.push(cert);
    //                        AddCertificates(t2, cert.DateOfPassing, cert.InstituteName, cert.CourseName, cert.Grade);

    //                    }
    //                }
    //                if (bankc.PreviousExperience != null) {
    //                    for (var c = 0; c < bankc.PreviousExperience.length; c++) {
    //                        var exp = bankc.PreviousExperience[c];
    //                        //exps.push(bankc.PreviousExperience[c]);
    //                        exps.push(exp);
    //                        AddPrevExp(t3, exp.BankId, exp.Branchid, exp.FromDate, exp.ToDate, exp.Reason);
    //                        //AddPrevExp(t3, bankc.BankId, bankc.Branchid, bankc.FromDate, bankc.ToDate, bankc.Reason);
    //                    }

    //                }


    //                if (bankc.OperationalAreas != null) {
    //                    for (var c = 0; c < bankc.OperationalAreas.length; c++) {
    //                        var oprarea = bankc.OperationalAreas[c];
    //                        areas.push(oprarea);
    //                        AddOpAreas(t4, oprarea.VillageCode, oprarea.VillageDetail);
    //                        //areas.push(bankc.OperationalAreas[c]);
    //                        //AddOpAreas(t4, bankc.VillageCode, bankc.VillageDetail);
    //                    }
    //                }


    //                if (bankc.Devices != null) {
    //                    for (var c = 0; c < bankc.Devices.length; c++) {
    //                        var device = bankc.Devices[c];
    //                        devices.push(device);
    //                        AddDevices(t5, device.GivenOn, device.Device);
    //                        //devices.push(bankc.Devices[c]);
    //                        //AddDevices(t5, bankc.GivenOn, bankc.Device);
    //                    }
    //                }


    //                if (bankc.ConnectivityDetails != null) {
    //                    for (var c = 0; c < bankc.ConnectivityDetails.length; c++) {
    //                        var conn = bankc.ConnectivityDetails[c];
    //                        conns.push(conn);
    //                        AddConnDetails(t6, conn.ConnectivityMode, conn.ConnectivityProvider, conn.ContactNumber);
    //                        //conns.push(bankc.ConnectivityDetails[c]);
    //                        //AddConnDetails(t6, bankc.ConnectivityMode, bankc.ConnectivityProvider, bankc.ContactNumber);
    //                    }
    //                }

    //                if (bankc.Products != null) {
    //                    for (var c = 0; c < bankc.Products.length; c++) {
    //                        var prod = bankc.AddProducts[c];
    //                        prods.push(prod);
    //                        AddProducts(t7, prod.minCashHandlingLimit, prod.MonthlyFixedRenumeration, prod.MonthlyVariableRenumeration);
    //                        //prods.push(bankc.Products[c]);
    //                        //AddProducts(t7, bankc.ProductName, bankc.MinimumCashHandlingLimit, bankc.MonthlyFixedRenumeration, bankc.MonthlyVariableRenumeration);
    //                    }
    //                }


    //            }

    //            $('.loader').hide();

    //        },
    //        error: function (err) {
    //            alert(err);
    //            $('.loader').hide();
    //        }
    //    });
    //}

    $('#frmMain').on('form:submit', function () {
        return false;
    });

    $('#txtAddMoreCerts').on('click', function () {
        AddCertificates(t2,
            $('#' + baseControlName + 'txtPassingDate').val(),
            $('#' + baseControlName + 'SelInstitute').val(),
            $('#' + baseControlName + 'SelCourse').val(),
            $('#' + baseControlName + 'txtGrades').val());


        var cert = new Object();
        cert.DateOfPassing = $('#' + baseControlName + 'txtPassingDate').val();
        cert.InstituteId = $('#' + baseControlName + 'SelInstitute').val();
        cert.CourseId = $('#' + baseControlName + 'SelCourse').val();
        cert.Grade = $('#' + baseControlName + 'txtGrades').val();
        certs.push(cert);

        $('#' + baseControlName + 'txtPassingDate').val("");
        $('#' + baseControlName + 'SelInstitute').val("");
        $('#' + baseControlName + 'SelCourse').val("");
        $('#' + baseControlName + 'txtGrades').val("");


    });

    $('#btnAddMoreExperience').on('click', function () {
        AddPrevExp(t3,
            $('#' + baseControlName + 'selPreviousExpBank').val(),
            $('#' + baseControlName + 'selPreviousExpBranch').val(),
            $('#' + baseControlName + 'txtFromDateExp').val(),
            $('#' + baseControlName + 'txtToDateExp').val(),
            $('#' + baseControlName + 'txtReasons').val());

        var exp = new Object();
        exp.BankId = $('#' + baseControlName + 'selPreviousExpBank').val();
        exp.Branchid = $('#' + baseControlName + 'selPreviousExpBranch').val();
        // exp.FromDate = $('#' + baseControlName + 'txtFromDateExp').val();
        var frmDate = new Date($('#' + baseControlName + 'txtFromDateExp').val());
        var dtString = frmDate.getFullYear() + '-' + (frmDate.getMonth() + 1) + '-' + frmDate.getDate();
        exp.FromDate = dtString;
        var to_Date = new Date($('#' + baseControlName + 'txtToDateExp').val());
        var dtToString = to_Date.getFullYear() + '-' + (to_Date.getMonth() + 1) + '-' + to_Date.getDate();
        exp.ToDate = dtToString;
        //exp.ToDate = $('#' + baseControlName + 'txtToDateExp').val();
        exp.Reason = $('#' + baseControlName + 'txtReasons').val();
        exps.push(exp);

        $('#' + baseControlName + 'selPreviousExpBank').val("");
        $('#' + baseControlName + 'selPreviousExpBranch').val("");
        $('#' + baseControlName + 'txtFromDateExp').val("");
        $('#' + baseControlName + 'txtToDateExp').val("");
        $('#' + baseControlName + 'txtReasons').val("");

    });

    $('#btnAddMoreOpAreas').on('click', function () {
        AddOpAreas(t4,
            $('#' + baseControlName + 'txtOperationVillageCode').val(),
            $('#' + baseControlName + 'txtOperationVillageDetail').val());

        var area = new Object();
        area.VillageCode = $('#' + baseControlName + 'txtOperationVillageCode').val();
        area.VillageDetail = $('#' + baseControlName + 'txtOperationVillageDetail').val();
        areas.push(area);

        $('#' + baseControlName + 'txtOperationVillageCode').val("");
        $('#' + baseControlName + 'txtOperationVillageDetail').val("");

    });

    $('#btnAddMoreDevices').on('click', function () {
        AddDevices(t5,
            $('#' + baseControlName + 'txtGivenOn').val(),
            $('#' + baseControlName + 'selDeviceName').val());

        var device = new Object();
        device.GivenOn = $('#' + baseControlName + 'txtGivenOn').val();
        device.Device = $('#' + baseControlName + 'selDeviceName').val();
        devices.push(device);

        $('#' + baseControlName + 'txtGivenOn').val("");
        $('#' + baseControlName + 'selDeviceName').val("");

    });

    $('#btnAddMoreConnectivity').on('click', function () {
        AddConnDetails(t6,
            $('#' + baseControlName + 'selConnType').val(),
            $('#' + baseControlName + 'txtProvider').val(),
             $('#' + baseControlName + 'txtNumber').val());

        var conn = new Object();

        conn.ConnectivityMode = $('#' + baseControlName + 'selConnType').val();

        conn.ConnectivityProvider = $('#' + baseControlName + 'txtProvider').val();
        conn.ContactNumber = $('#' + baseControlName + 'txtNumber').val();
        conns.push(conn);



        $('#' + baseControlName + 'selConnType').val("");
        $('#' + baseControlName + 'txtProvider').val("");
        $('#' + baseControlName + 'txtNumber').val("");

    });

    //$('#btnAddCorporate').on('click', function () {
    //    var corporate = new Object();
    //    corporate.CorporateName = $('#txtName').val();
    //    corporate.CorporateType = $('#txtType').val();
    //    corporate.Email = $('#txtEmail').val();
    //    corporate.Address = $('#txtAddress').val();
    //    corporate.ContactNumber = $('#txtContact').val();

    //    $.ajax({
    //        url: "GetCorporateHandler.ashx",
    //        method: 'POST',
    //        datatype: 'json',
    //        data: { 'info': JSON.stringify(corporate) },
    //        success: function () { alert('Posted!'); },
    //        error: function (XMLHttpRequest, status, errorThrown) { alert(errorThrown); }
    //    });
    //});

    $('#btnAddMoreProducts').on('click', function () {
        AddProducts(t7,
            $('#' + baseControlName + 'selProductsOffered').val(),
            $('#' + baseControlName + 'txtMinCash').val(),
            $('#' + baseControlName + 'txtMonthlyFixed').val());

        var prod = new Object();
        prod.ProductsOffered = $('#' + baseControlName + 'selProductsOffered').val();
        prod.MinCash = $('#' + baseControlName + 'txtMinCash').val();
        prod.MonthlyFixed = $('#' + baseControlName + 'txtMonthlyFixed').val();
        prod.MonthlyVariable = $('#' + baseControlName + 'txtMonthlyVariable').val();
        prods.push(prod);

        $('#' + baseControlName + 'selProductsOffered').val("");
        $('#' + baseControlName + 'txtMinCash').val("");
        $('#' + baseControlName + 'txtMonthlyFixed').val("");
        $('#' + baseControlName + 'txtMonthlyVariable').val("");

    });

    function AddCertificates(table, passDate, inst, course, grade) {
        table.row.add([passDate, inst, course, grade,
            '<td><a href="#" id="delCert" name="delCert" class="pull-right btn btn-danger" >Delete</a> </td>',
        ]).draw(false);
    }

    function AddPrevExp(table, bankId, branch, fromDate, toDate, reason) {
        table.row.add([bankId, branch, fromDate, toDate, reason,
            '<td><a href="#" id="delPrevExp" name="delPrevExp" class="pull-right btn btn-danger" >Delete</a> </td>',
        ]).draw(false);
    }

    function AddOpAreas(table, villCode, villDetail) {
        table.row.add([villCode, villDetail,
            '<td><a href="#" id="delOpAreas" name="delOpAreas" class="pull-right btn btn-danger" >Delete</a> </td>',
        ]).draw(false);
    }

    function AddDevices(table, GivenOn, Device) {
        table.row.add([GivenOn, Device,
            '<td><a href="#" id="delDevices" name="delDevices" class="pull-right btn btn-danger" >Delete</a> </td>',
        ]).draw(false);
    }

    function AddConnDetails(table, type, provider, number) {
        table.row.add([type, provider, number,
            '<td><a href="#" id="delConn" name="delConn" class="pull-right btn btn-danger" >Delete</a> </td>',
        ]).draw(false);
    }

    function AddProducts(table, minCashHandlingLimit, monthlyFixedRenum, monthlyVariableRenum) {
        table.row.add([minCashHandlingLimit, monthlyFixedRenum, monthlyVariableRenum,
            '<td><a href="#" id="delProd" name="delProd" class="pull-right btn btn-danger" >Delete</a> </td>',
        ]).draw(false);
    }

    //$(document).on('change', '.uploaderCss', function () {
    //    var fileName = $(this).val().replace(/^.*[\\\/]/, '');
    //    var currFile = $(this)[0].files[0];
    //    var fieldId = $(this).attr('id');
    //    var xhr = new XMLHttpRequest();
    //    var uploadData = new FormData();
    //    var url = 'FileUploader.ashx';
    //    uploadData.append('file', currFile);
    //    xhr.open("POST", url, true);
    //    xhr.send(uploadData);
    //    alert('File uploaded successfully');
    //});

    $('#btnSubmitSingleEntry').on('click', function () {

        bcorrespondene.Name = $('#' + baseControlName + 'txtName').val();
        //Handle Duplicate Image Name Issue

        //bcorrespondene.ImagePath = $('#' + baseControlName + 'fileImage.PostedFile.FileName').val();


        //append ts to img name
        var fileName = $('#' + baseControlName + 'fileImage').val();
        var d = new Date();
        var ts = '_' + d.getDate().toString() + '-' + (d.getMonth() + 1).toString() + '-' + d.getFullYear().toString() + '-' + d.getHours().toString() + '-' + d.getMinutes().toString();
        bcorrespondene.ImagePath = (fileName.substring(12, fileName.length - 4)).concat(ts);

        bcorrespondene.Gender = $('#' + baseControlName + 'selGender').val();
        bcorrespondene.DOB = $('#' + baseControlName + 'txtDOB').val();
        bcorrespondene.FatherName = $('#' + baseControlName + 'txtFather').val();
        bcorrespondene.SpouseName = $('#' + baseControlName + 'txtSpouse').val();
        bcorrespondene.Category = $('#' + baseControlName + 'selCategory').val();
        if ($('#' + baseControlName + 'radHandicapNo').checked)
            bcorrespondene.Handicap = $('#' + baseControlName + 'radHandicapNo').val()
        else
            bcorrespondene.Handicap = $('#' + baseControlName + 'radHandicapYes').val()

        // bcorrespondene.Handicap = $('#' + baseControlName + 'radHandicapNo.Checked ? radHandicapNo).val() : radHandicapYes').val();
        bcorrespondene.PhoneNumber1 = $('#' + baseControlName + 'txtPhone1').val();
        bcorrespondene.PhoneNumber2 = $('#' + baseControlName + 'txtPhone2').val();
        bcorrespondene.PhoneNumber3 = $('#' + baseControlName + 'txtPhone3').val();
        bcorrespondene.Email = $('#' + baseControlName + 'txtEmail').val();
        bcorrespondene.AadharCard = $('#' + baseControlName + 'txtAadharCard').val();
        bcorrespondene.PanCard = $('#' + baseControlName + 'txtPanCard').val();
        bcorrespondene.VoterCard = $('#' + baseControlName + 'txtVoterId').val();
        bcorrespondene.DriverLicense = $('#' + baseControlName + 'txtDriverLic').val();
        bcorrespondene.NregaCard = $('#' + baseControlName + 'txtNregaCard').val();
        bcorrespondene.RationCard = $('#' + baseControlName + 'txtRationCard').val();
        bcorrespondene.State = $('#' + baseControlName + 'selAddressState').val();
        bcorrespondene.City = $('#' + baseControlName + 'selAddressCity').val();
        bcorrespondene.District = $('#' + baseControlName + 'selAddressDistrict').val();
        bcorrespondene.Subdistrict = $('#' + baseControlName + 'txtAddressSubDistrict').val();
        bcorrespondene.Area = $('#' + baseControlName + 'txtAddressArea').val();
        bcorrespondene.PinCode = $('#' + baseControlName + 'txtPinCode').val();
        bcorrespondene.AlternateOccupationType = $('#' + baseControlName + 'SelAlternateOccupation').val();
        bcorrespondene.AlternateOccupationDetail = $('#' + baseControlName + 'txtAlternateOccupationDtl').val();
        bcorrespondene.UniqueIdentificationNumber = $('#' + baseControlName + 'txtUniqueId').val();
        bcorrespondene.BankReferenceNumber = $('#' + baseControlName + 'txtBankReferenceNumber').val();
        bcorrespondene.Qualification = $('#' + baseControlName + 'selQualification').val();

        bcorrespondene.OtherQualification = $('#' + baseControlName + 'txtOtherQualification').val();
        bcorrespondene.CorporateId = $('#' + baseControlName + 'selCorporate').val();


        bcorrespondene.Allocation = $('#' + baseControlName + 'radAllocatedNo').checked ? "No" : "Yes";
        bcorrespondene.isAllocated = (bcorrespondene.Allocation == 'Yes');
        if (bcorrespondene.Allocation == 'Yes') {

            bcorrespondene.AppointmentDate = $('#' + baseControlName + 'txtAppointmentDate').val();
            bcorrespondene.AllocationIFSCCode = $('#' + baseControlName + 'ifscCode').val();

            bcorrespondene.AllocationBankId = $('#' + baseControlName + 'selAllocationBank').val();
            bcorrespondene.AllocationBranchId = $('#' + baseControlName + 'selAllocationBranch').val();

            bcorrespondene.BCType = $('#' + baseControlName + 'selBCType').val();
            bcorrespondene.WorkingDays = $('#' + baseControlName + 'txtWorkingDays').val();
            bcorrespondene.WorkingHours = $('#' + baseControlName + 'txtWorkingHours').val();
            bcorrespondene.PLPostalAddress = $('#' + baseControlName + 'txtPostalAddress').val();
            bcorrespondene.PLVillageCode = $('#' + baseControlName + 'txtVillageCode1').val();
            bcorrespondene.PLVillageDetail = $('#' + baseControlName + 'txtVillageDetail1').val();
            bcorrespondene.PLStateId = $('#' + baseControlName + 'selPLState').val();
            //bcorrespondene.PLDistrictId = $('#' + baseControlName + 'selPLDistrict').val();
            bcorrespondene.PLTaluk = $('#' + baseControlName + 'selPLTaluk').val();
            bcorrespondene.PLPinCode = $('#' + baseControlName + 'txtPLPinCode').val();
            bcorrespondene.MinimumCashHandlingLimit = $('#' + baseControlName + 'txtMinCash').val();
            bcorrespondene.MonthlyFixedRenumeration = $('#' + baseControlName + 'txtMonthlyFixed').val();
            bcorrespondene.MonthlyVariableRenumeration = $('#' + baseControlName + 'txtMonthlyVariable').val();
        }


        $.ajax({
            url: "InsertUpdateHandler.ashx",
            method: "POST",
            datatype: 'json',
            data: {
                "bcorr": JSON.stringify(bcorrespondene), "allCerts": JSON.stringify(certs), "allExps": JSON.stringify(exps),
                "allAreas": JSON.stringify(areas), "allDevices": JSON.stringify(devices), "allConns": JSON.stringify(conns)
            },
            success: function (data) {
                //handle img upload on btn click
                var fileName = $('#' + baseControlName + 'fileImage').val().replace(/^.*[\\\/]/, '');
                var currFile = $('#' + baseControlName + 'fileImage')[0].files[0];
                var fieldId = $('#' + baseControlName + 'fileImage').attr('id');
                var xhr = new XMLHttpRequest();
                var uploadData = new FormData();
                var url = 'FileUploader.ashx';
                uploadData.append('file', currFile);
                xhr.open("POST", url, true);
                xhr.send(uploadData);

                $("#getCodeModal").modal('show');
                //alert('posted');                
            }
        });
    });

    $('#radAllocatedNo').on('change', function () {
        $('#divAllocationDetails').hide();


    });

    $('#radAllocatedYes').on('change', function () {
        $('#divAllocationDetails').show();


    });
    
    $('#btnSubmitBlackList').on('click', function () {
        var bc = new Object();
        bc.BankCorrespondId = bcId; //From url
        bc.IsBlackListed = true;
        bc.BlacklistDate = $('#txtDate').val();
        bc.BlackListReason = $('#txtReason').val();
        bc.BlackListApprovalNumber = $('#txtApprovalNumber').val();

        $.ajax({
            url: "InsertUpdateHandler.ashx?mode=BlackList",
            method: "POST",
            datatype: "json",
            data: { "bcorr": JSON.stringify(bc) },
            success: function () { alert('Black Listed!'); },
            error: function (xmlHttpRequest, errorString, errorMessage) { }
        });
    });

    $('#btnSubmitChangeStatus').on('click', function () {
        var bc = new Object();
        bc.BankCorrespondId = bcId; //From url
        bc.Status = $('#selNewStatus').val();
        bc.CurrentStatusSince = $('#txtDate').val();
        bc.StatusChangeReason = $('#txtReason').val();

        $.ajax({
            url: "InsertUpdateHandler.ashx?mode=ChangeStatus",
            method: "POST",
            datatype: "json",
            data: { "bcorr": JSON.stringify(bc) },
            success: function () { alert('Status Changed!'); },
            error: function (xmlHttpRequest, errorString, errorMessage) { }
        });
    });
});