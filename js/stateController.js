﻿bctrackApp.controller("stateController", function ($scope) {
    $.ajaxSetup({
        cache: false
    });
    var leadTable = $('#tblLeadList').dataTable({
        "oLanguage": {
            "sZeroRecords": "No records to display",
            "sSearch": "Search "
        },
        "sDom": 'T<"clear">lfrtip',
        "tableTools": {

            "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
        },
        "iDisplayLength": 15,
        "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
        "bSortClasses": false,
        "bStateSave": false,
        "bPaginate": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "StateList.aspx/getState",
        "columnDefs": [

            {
                "orderable": false,
                "targets": -2,
                "render": function (data, type, full, meta) {
                    return '<a  class="btn btn-success btn-sm btn-labeled btn-rounded" ui-sref="partyDetail({ stateId: ' + full[0] + '})" href="editState.aspx?stateId=' + full[0] + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                }
            }, { orderable: true, "targets": -1 }, {
                "orderable": true,
                "targets": -1,
                "render": function (data, type, full, meta) {
                    return '<a href="#" class="btn btn-danger btn-sm btn-labeled btn-rounded"  onclick="return deleteUser(' + full[0] + ');"><i class="fa fa-trash-o" aria-hidden="true"></a>';


                }
            }
        ],
        "bDeferRender": true,
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success":
                            function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblLeadList").show();
                            }
            });
        }
    });
    function deleteUser(stateid) {

        if (confirm("Do you want to delete?")) {

            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: "DeleteBcHandler.ashx?mode=state&stateid=" + stateid,
                async: false,
                success: function (user) {
                    window.location.reload();

                }
            });
        }
        return false;
    }

    var fullUrl = $(location).attr('href');
    var toCaptureStateFromURL = /stateId=(\d+)/ig;
    var s = toCaptureStateFromURL.exec(fullUrl);
    var stateId = '';
    if (s != null) {
        stateId = s[1];
    }
    $('form').parsley();

    function GetStates() {
        var requestUrl = "GetBCHandler.ashx?mode=state";
        /*$('.loader').show();*/
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            async: false,
            success: function (States) {

                for (var cnt = 0; cnt < States.length; cnt++) {
                    $('#selState').append($('<option>', {
                        value: States[cnt].StateId,
                        text: States[cnt].StateName
                    }));
                }
                $('.loader').hide();
            },
            error: function (err) {
                alert('An error occured!');
                $('.loader').hide();
            }
        });
    }

    if ($('#tblStateList').length > 0) {
        var t2 = $('#tblStateList').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": true
        });
        GetStates();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: "GetBCHandler.ashx?mode=state",
            async: false,
            success: function (States) {
                for (var i = 0; i < States.length; i++) {
                    t2.row.add([States[i].StateId, States[i].StateName,
                        '<td><a href="editState.aspx?stateId=' + States[i].StateId + '" class="btn btn-success btn-rounded icon-only"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>',
                         '<td><input type="button" class="btn btn-danger btn-sm btn-labeled btn-rounded" value="Delete">'

                    ]).draw(false);
                }
                /*$('.loader').hide();*/
            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }
        });
    }
    $('#tblStateList').on('click', 'tr', function () {
        
        var stateid = $(this).find('td:first').text();
        //$.ajax({
        //    type: 'POST',
        //    dataType: 'json',
        //    contentType: 'application/json',
        //    url: "DeleteBcHandler.ashx?mode=state&stateid=" + stateid,
        //    async: false,
        //    success: function (user) {
        //        window.location.reload();

        //    }
        //});


    });


    var districts = [];
    var cities = [];
    var talukas = [];
    var villages = [];

    var tableDistricts = $('#tblDistricts').DataTable({
        "paging": true,
        "ordering": true,
        "info": false,
        "searching": true
    });

    var tableCities = $('#tblCities').DataTable({
        "paging": true,
        "ordering": true,
        "info": false,
        "searching": true
    });

    var tableTalukas = $('#tblTalukas').DataTable({
        "paging": true,
        "ordering": true,
        "info": false,
        "searching": true
    });

    var tableVillages = $('#tblVillages').DataTable({
        "paging": true,
        "ordering": true,
        "info": false,
        "searching": true
    });

    //talk to the DB first to get all the details of the state with stateId    
    //if (stateId != '') {
    /*$('.loader').show();*/
    //    $.ajax({
    //        type: 'POST',
    //        dataType: 'json',
    //        contentType: 'application/json',
    //        url: 'GetBCHandler.ashx?mode=state&stateId=' + stateId,
    //        async: false,
    //        success: function (state) {
    //            $('#txtStateName').val(state[0].StateName);
    //            for (let i = 0; i < state.length; i++) {
    //                console.log(JSON.stringify(state[i]) + '\n');
    //                if (state[i].Districts != null) {
    //                    for (let j = 0; j < state[i].Districts.length; j++) {
    //                        let tempDistrict = new Object();
    //                        tempDistrict.DistrictName = state[i].Districts[j].DistrictName;
    //                        districts.push(tempDistrict);
    //                        tableDistricts.row.add([state[i].Districts[j].DistrictName, '<td><button id="btnDeleteDistrict" district=' + state[i].Districts[j].DistrictName + ' class="btn btn-danger">Delete</button></td>']).draw(false);
    //                        for (let k = 0; k < state[i].Districts[j].Cities.length; k++) {
    //                            let tempCity = new Object();
    //                            tempCity.CityName = state[i].Districts[j].Cities[k].CityName;
    //                            tempCity.DistrictName = state[i].Districts[j].DistrictName;
    //                            cities.push(tempCity);
    //                            tableCities.row.add([state[i].Districts[j].Cities[k].CityName, state[i].Districts[j].DistrictName, '<td><button id="btnDeleteCity" city=' + state[i].Districts[j].Cities[k].CityName + ' class="btn btn-danger">Delete</button></td>']).draw(false);
    //                        }

    //                        for (let k = 0; k < state[i].Districts[j].Talukas.length; k++) {
    //                            let tempTaluka = new Object();
    //                            tempTaluka.TalukaName = state[i].Districts[j].Talukas[k].TalukaName;
    //                            tempTaluka.DistrictName = state[i].Districts[j].DistrictName;
    //                            talukas.push(tempTaluka);
    //                            tableTalukas.row.add([state[i].Districts[j].Talukas[k].TalukaName, state[i].Districts[j].DistrictName, '<td><button id="btnDeleteTaluka" taluka=' + state[i].Districts[j].Talukas[k].TalukaName + ' class="btn btn-danger">Delete</button></td>']).draw(false);
    //                        }

    //                        for (let k = 0; k < state[i].Districts[j].Villages.length; k++) {
    //                            let tempVillage = new Object();
    //                            tempVillage.DistrictName = state[i].Districts[j].DistrictName;
    //                            tempVillage.VillageName = state[i].Districts[j].Villages[k].VillageName;
    //                            villages.push(tempVillage);
    //                            tableVillages.row.add([state[i].Districts[j].Villages[k].VillageName, state[i].Districts[j].Villages[k].TalukaName, state[i].Districts[j].DistrictName,'<td><button id="btnDeleteVillage" village=' + state[i].Districts[j].Villages[k].VillageName + ' class="btn btn-danger">Delete</button></td>']).draw(false);
    //                        }                            
    //                    }
    //                }
    //            }
    //            PopulateDistricts();
    //            PopulateTalukas();
    //        },
    //        error: function (XmlHttpRequest, errorString, errorMessage) {
    //            alert(errorMessage);
    //        }
    //    });
    //}

    function PopulateDistricts() {
        $('#selectDistrictNameForCity').empty();
        $('#selectDistrictNameForVillage').empty();
        $('#selectDistrictNameForTaluka').empty();
        $('#selectDistrictNameForCity').append($('<option>', {
            value: 0,
            text: '---Select District---'
        }));
        $('#selectDistrictNameForVillage').append($('<option>', {
            value: 0,
            text: '---Select District---'
        }));
        $('#selectDistrictNameForTaluka').append($('<option>', {
            value: 0,
            text: '---Select District---'
        }));
        for (var i = 0; i < districts.length; i++) {
            $('#selectDistrictNameForCity').append($('<option>', {
                value: 0,
                text: districts[i].DistrictName
            }));
            $('#selectDistrictNameForVillage').append($('<option>', {
                value: 0,
                text: districts[i].DistrictName
            }));
            $('#selectDistrictNameForTaluka').append($('<option>', {
                value: 0,
                text: districts[i].DistrictName
            }));
        }
    }

    function PopulateTalukas() {
        $('#selectTalukaNameForVillage').empty();
        $('#selectTalukaNameForVillage').append($('<option>', {
            value: 0,
            text: '---Select Taluka---'
        }));
        for (var i = 0; i < talukas.length; i++) {
            $('#selectTalukaNameForVillage').append($('<option>', {
                value: 0,
                text: talukas[i].TalukaName
            }));
        }
    }

    $('#btnAddMoreDistricts').on('click', function () {
        var district = new Object();
        district.DistrictName = $('#txtDistrictName').val();
        districts.push(district);
        tableDistricts.row.add([district.DistrictName, '<td><button id="btnDeleteDistrict" district=' + district.DistrictName + ' class="btn btn-danger">Delete</button></td>']).draw(false);
        PopulateDistricts();
    });

    $('#btnAddMoreCities').on('click', function () {
        var city = new Object();
        city.CityName = $('#txtCityName').val();
        var districtName = '';
        districtName = $('#selectDistrictNameForCity :selected').text();
        if (districtName === '---Select District---')
            districtName = ' ';
        city.DistrictName = districtName;
        cities.push(city);
        tableCities.row.add([city.CityName, city.DistrictName, '<td><button id="btnDeleteCity" city=' + city.CityName + ' class="btn btn-danger">Delete</button></td>']).draw(false);
    });

    $('#btnAddMoreTalukas').on('click', function () {
        var taluka = new Object();
        taluka.TalukaName = $('#txtTalukaName').val();
        var districtName = '';
        districtName = $('#selectDistrictNameForTaluka :selected').text();
        if (districtName === '---Select District---')
            districtName = ' ';
        taluka.DistrictName = districtName;
        talukas.push(taluka);
        tableTalukas.row.add([taluka.TalukaName, taluka.DistrictName, '<td><button id="btnDeleteTaluka" taluka=' + taluka.TalukaName + ' class="btn btn-danger">Delete</button></td>']).draw(false);
        PopulateTalukas();
    });

    $('#btnAddMoreVillages').on('click', function () {
        var village = new Object();
        village.VillageName = $('#txtVillageName').val();
        var talukaName = '', districtName = '';
        talukaName = $('#selectTalukaNameForVillage :selected').text();
        districtName = $('#selectDistrictNameForVillage :selected').text();
        if (talukaName === '---Select Taluka---')
            talukaName = ' ';
        village.TalukaName = talukaName;
        if (districtName === '---Select District---')
            districtName = ' ';
        village.DistrictName = districtName;
        villages.push(village);
        tableVillages.row.add([village.VillageName, village.TalukaName, village.DistrictName, '<td><button id="btnDeleteVillage" village=' + village.VillageName + ' class="btn btn-danger">Delete</button></td>']).draw(false);
    });

    $('#btnSubmitStateInfo').on('click', function () {
        let collectedDistricts = [];
        for (let i = 0; i < districts.length; i++) {
            let district = new Object();
            district.StateId = stateId;
            district.DistrictName = districts[i].DistrictName;
            district.Cities = [];
            district.Talukas = [];
            district.Villages = [];
            for (let j = 0; j < cities.length; j++) {
                if (cities[j].DistrictName === districts[i].DistrictName) {
                    let city = new Object();
                    city.CityName = cities[j].CityName;
                    city.DistrictName = districts[i].DistrictName;
                    district.Cities.push(city);
                }
            }
            for (let j = 0; j < talukas.length; j++) {
                if (talukas[j].DistrictName === districts[i].DistrictName) {
                    let taluka = new Object();
                    taluka.DistrictName = districts[i].DistrictName;
                    taluka.TalukaName = talukas[j].TalukaName;
                    district.Talukas.push(taluka);
                }
            }
            for (let j = 0; j < villages.length; j++) {
                let village = new Object();
                if (villages[j].DistrictName === districts[i].DistrictName) {
                    village.DistrictName = districts[i].DistrictName;
                    village.VillageName = villages[j].VillageName;
                    district.Villages.push(village);
                }
            }
            for (let j = 0; j < district.Talukas.length; j++) {
                district.Talukas[j].Villages = [];
                for (let k = 0; k < villages.length; k++) {
                    if (villages[k].TalukaName === district.Talukas[j].TalukaName) {
                        let village = new Object();
                        village.TalukaName = district.Talukas[j].TalukaName;
                        village.VillageName = villages[k].VillageName;
                        district.Talukas[j].Villages.push(village);
                    }
                }
            }
            collectedDistricts.push(district);
        }
        var state = new Object();
        state.StateId = '';

        let capturedStateName = $('#txtStateName').val();
        if (capturedStateName != null)
            state.StateName = capturedStateName;

        state.Districts = collectedDistricts;

        let requestUrl = 'InsertUpdateStateHander.ashx';
        $.ajax({
            url: requestUrl,
            datatype: 'json',
            data: { "state": JSON.stringify(state) },
            method: 'POST',
            success: function () {
                alert('Saved this state\'s information');
            },
            error: function (xhr, errorString, errorMessage) {
                alert(requestUrl + '\n' + errorMessage + '\n' + errorString);
            }
        });
    });

})