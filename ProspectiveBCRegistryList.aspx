﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProspectiveBCRegistryList.aspx.cs" Inherits="BCTrackingWeb.ProspectiveBCRegistryList" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container-fluid">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">List of Prospective BC</h2>

            </div>

            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a>List of Prospective BC</a></li>

                </ul>
            </div>

            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    

        <section class="section">
            <div class="container-fluid">

                <div class="row">

                    <div class="col-md-12">
                        <div class="panel">

                            <div class="panel-body p-20">


                                <div class="panel-body">

                                    <div class="formmargin">


                                     <div class="col-md-4">
                         <asp:HiddenField ID="HiddenField1" runat="server" />

              <%--   <asp:Button ID="btnDownload" Text="Export Excel" runat="server" OnClick="btnDownload_Click" CssClass="btn btn-primary  btn-sm" />--%>
             

                    </div>
                                    
                                    </div>
                                </div>


                                <hr/>
                                   <table id="tbl_User_List" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                     
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Phone</th>                                     
                                        <th>Qualification</th>
                                        <th>Action</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                             <div class="span3">
            <asp:GridView ID="gvDetails" runat="server">
            </asp:GridView>
        </div>


                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>

                </div>
            </div>

        </section>
    <!-- /.section -->
    </form>
</body>
</html>

