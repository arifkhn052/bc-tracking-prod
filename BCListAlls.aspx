﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="BCListAlls.aspx.cs" Inherits="BCTrackingWeb.BCListAlls" %>

    <div class="container-fluid">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">List All</h2>

            </div>

            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a>List All</a></li>

                </ul>
            </div>

            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <section class="section">
        <div class="container-fluid">

            <div class="row">


                <!-- /.col-md-6 -->

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                            </div>
                        </div>
                        <div class="panel-body p-20">

                            <table id="tbl_User_List" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                           <th>BC Code</th>
                                        <th>Name</th>
                                        <th>Phone</th>                                     
                                        <th>Qualification</th>
                                        <th>Villages</th>
                                        <th>Action</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->


                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.section -->
