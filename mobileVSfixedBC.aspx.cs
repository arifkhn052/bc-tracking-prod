﻿using BCEntities;
using BCTrackingBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCTrackingWeb
{
    public partial class mobileVSfixedBC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string getFixedVsMbileBC()
        {
            // Paging parameters:
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);

            // Sorting parameters
            var iSortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var iSortDir = HttpContext.Current.Request.Params["sSortDir_0"];
            // Search parameters
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToLower();
            int totalRecords = 0;
            if (System.Web.HttpContext.Current.Session["UserInfo"] != null) ;
            var userId = HttpContext.Current.Request.Params["adminuserid"].ToLower();
            var state = HttpContext.Current.Request.Params["State"].ToLower();
            var district = HttpContext.Current.Request.Params["District"].ToLower();
            var subdistrict = HttpContext.Current.Request.Params["SubDistrict"].ToLower();
            var village = HttpContext.Current.Request.Params["Village"].ToLower();
            var type = HttpContext.Current.Request.Params["type"].ToLower();

            var leadList = Common.getFixedVsMbileBC(iDisplayStart, iDisplayLength, iSortCol, iSortDir, sSearch, out totalRecords, userId, state, district, subdistrict, village,Convert.ToInt32(type));

            Func<BankCorrespondence, object> order = p =>
            {
                if (iSortCol == 0)
                {
                    return p.Name;
                }

                else if (iSortCol == 1)
                {
                    return p.no_of_fixed;
                }
                else if (iSortCol == 2)
                {
                    return p.no_of_mobile;
                }
                else if (iSortCol == 3)
                {
                    return p.no_of_both;
                }
              



                return p.Name;
            };

            //    // Define the order direction based on the iSortDir parameter
            if ("desc" == iSortDir)
            {
                leadList = leadList.OrderByDescending(order).ToList<BankCorrespondence>();
            }
            else
            {
                leadList = leadList.OrderBy(order).ToList<BankCorrespondence>();
            }
            var finalResult = leadList
                        .Select(p => new[] { p.Name, p.no_of_mobile, p.no_of_fixed, p.no_of_both });
            if (type == "1")
            {
                 finalResult = leadList
                        .Select(p => new[] { p.Name,p.District, p.no_of_mobile, p.no_of_fixed, p.no_of_both });
            }
            if (type == "2")
            {
                finalResult = leadList
                       .Select(p => new[] { p.Name,p.District,p.Subdistrict, p.no_of_mobile, p.no_of_fixed, p.no_of_both });
            }
            if (type == "3")
            {
                finalResult = leadList
                       .Select(p => new[] { p.Name,p.District,p.Subdistrict,p.Village, p.no_of_mobile, p.no_of_fixed, p.no_of_both });
            }
            int count = finalResult.Count();

            // prepare an anonymous object for JSON serialization
            var result = new
            {
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = finalResult
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            return json;
        }
    }

}