﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="bclistUnallocated.aspx.cs" Inherits="BCTrackingWeb.bclistUnallocated" %>

        <div class="container-fluid" >
            <div class="row page-title-div">
                <div class="col-md-12">
                    <h2 class="title">List of Unallocated Business Correspondent</h2>

                </div>
              
                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                       <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="#"><span>Unallocated Business Correspondent</span></a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">


                    <!-- /.col-md-6 -->

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">

                                </div>
                            </div>
                            <div class="panel-body p-20">

                                <table id="tbl_User_List" class="display" cellspacing="0" width="100%">
	        <thead>
	            <tr>
                    <th>ID</th>
                    <th>BC Code</th>
	                <th>Name</th>
	                <th>Phone</th>
                 
                    <th>Certificates</th>
                    <th>Villages</th>
                    <th>Action</th>
                     <th></th>

                         <th></th>
                         <th></th>
 
	            </tr>
	        </thead>
            <tbody>
                
            </tbody>
	    </table>

                                
                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-8 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->


    
    <%--<script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/getbc.js" type="text/javascript"></script>--%>

